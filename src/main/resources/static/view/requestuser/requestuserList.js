// ------------------------------------------------------------------------------------
// 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
// ------------------------------------------------------------------------------------
const ApproveList = new Vue({
    el: "#app",
    data: {
        input: '',
        logincode: cmCookie.getCookie("loginTjCode"),
        counts: 0,
        id: 0,
        fileList: '',
        //预览对话框
        previewdialogVisible: false,
        // 画面検索条件用のForm

        fileIdList: [],
        // 画面検索条件用のForm
        approveListForm: {
            filename: "",
            koshinname: "",
            userId: "",
            currentPage: 1,
            pageSize: 20,
        },
        // 画面一览数据
        datagrid: [],
        datas: [],
        nowOrders: "",
        maxOrders: "",
        start: 0,
        end: 0,
        // 当前页
        currentPage: 1,
        // 总件数
        datagridTotal: 0,

        // 页数
        pageTotal: 1,
        dialogVisible: false,
        active: 0,
        // < 和 << 活性flg
        disFlagHome: true,
        // > 和>> 活性flg
        disFlagTail: false,
        reviewScheduleFlag: false,
        Requestform: {
            counts: 0,
            loginUserId: "",
            id: "",
        },
        fileForm: {
            file: "",
            fileAbstract: '01',
            updateConfirmer: "01",
            btnstatus: true,
            position: "",
            currentPage: 1,
            pageSize: 20,
            sortColumn: "",
            sortType: "",
            addDatas: [],
            perModifyDatas: [],
            fileId: "",
            fName: "",
            perModifyApplicationValue: "",
            perModifyContentValue: "",
            cdCode: "",
            confirmationPerson: "",
            addApplication: "",
            addContent: "",
            // 签名sheet
            reviewerSheet: "",
            // 确认人
            fileAddConfirmer: "",
            // 作成者
            createName: "",
            // 登录者
            loginUserName: "",
            // 更新者
            koshinName: "",
            },
        reviewinfo: [{
            position: '',
            reviewerValue: ''
        }],
        viewChangesData:[],

        tablechangeData:[],
    },

    // 初始化
    created: function () {
        this.onSearch();
    },

    methods: {
        // 点击检索按钮
        onSearchReady: function (value) {
            // 设定第一页
            ApproveList.approveListForm.currentPage = 1;
            ApproveList.selectFlag = false;
            // 清空升降序的选择
            this.$refs.sortTable.clearSort();
            ApproveList.approveListForm.sortColumn = "approveNum";
            ApproveList.approveListForm.sortType = "descending";
            // 调用查询一览方法
            this.Search();
        },

        // 查询一览方法
        Search: function () {
            ApproveList.approveListForm.userId = cmCookie.getCookie("loginTjCode");
            ApproveList.approveListForm.filename = this.approveListForm.filename;
            ApproveList.approveListForm.koshinname = this.approveListForm.koshinname;
            //计算开始和结束位置的值
            let end = (this.approveListForm.pageSize) * this.approveListForm.currentPage;
            let start = (this.approveListForm.currentPage - 1) * this.approveListForm.pageSize + 1;
            axios.post("/api/requsetUser/search", ApproveList.approveListForm
            ).then(function (res) {
                ApproveList.selectFlag = true;
                // 一览数据
                ApproveList.datagrid = res.data
                // 总件数
                ApproveList.datagridTotal = res.data[0].total;
                console.log(ApproveList)
                // 计算页数
                ApproveList.pageTotal = Math.ceil(ApproveList.datagridTotal / ApproveList.approveListForm.pageSize);
                if (ApproveList.datagridTotal < end) {
                    end = ApproveList.datagridTotal;
                }
                if (ApproveList.datagridTotal == 0) {
                    start = 0;
                }
                // 画面显示开始结束位置的值
                ApproveList.start = start;
                ApproveList.end = end;
                // 开始条数为0条或首页时首页上一页按钮不可用，否则可用
                if (start <= 1) {
                    ApproveList.disFlagHome = true;
                } else {
                    ApproveList.disFlagHome = false;
                }
                // 结束条数为总条数时尾页和下一页按钮不可用，否则可用
                if (end == ApproveList.datagridTotal) {
                    ApproveList.disFlagTail = true;
                } else {
                    ApproveList.disFlagTail = false;
                }
            }).catch(function (res) {
            })
        },

        //初始化
        onSearch: function () {
            // 获取登录者卡号
            let id = cmCookie.getCookie("loginTjCode");
            let data = {
                "userId": id,
                "searchFileName": "",
                "searchStatus": "",
                "currentPage": "1",
                "pageSize": "20",
                "sortColumn": "",
                "sortType": ""
            };
            axios.post("/api/requsetUser/init", data
            ).then(function (res) {
                if (res.status && res.data.length > 0) {
                    ApproveList.counts = res.data.counts;
                    ApproveList.id = res.data.id;
                    // 一览数据
                    ApproveList.datagrid = res.data;
                    // 总件数
                    ApproveList.datagridTotal = res.data[0].total;
                    // 计算页数
                    ApproveList.pageTotal = Math.ceil(ApproveList.datagridTotal / ApproveList.approveListForm.pageSize);
                    // 判断开始位置的值
                    if (ApproveList.datagridTotal != 0) {
                        ApproveList.start = 1;
                    } else {
                        ApproveList.start = 0;
                    }
                    // 判断结束位置的值
                    if (ApproveList.datagridTotal < ApproveList.approveListForm.pageSize) {
                        ApproveList.end = ApproveList.datagridTotal;
                    }
                    if (ApproveList.datagridTotal >= ApproveList.approveListForm.pageSize) {
                        ApproveList.end = ApproveList.approveListForm.pageSize;
                    }
                    // 画面显示的条数小于每页条数下一页尾页不可用
                    if (ApproveList.end == ApproveList.datagridTotal) {
                        ApproveList.disFlagTail = true;
                    }
                }
            }).catch(function (res) {
            })
        },
        //通过
        pass: function (a) {
            const _this = this
            this.$confirm("确定要通过吗？", "通过确认", {
                confirmButtonText: "是",
                cancelButtonText: "否",
                type: "",
                callback: action => {
                    if (action === "confirm") {
                        if (a.counts < 3) {
                            _this.examinepass(a)
                        } else if (a.counts == 3) {
                            _this.submitviewChanges(a)

                            _this.examinepass(a)
                        }
                    }
                }
            })
        },
        //审核通过
        examinepass(a){
            const  _this=this
            this.Requestform.loginTjCode = cmCookie.getCookie("loginTjCode");
            this.Requestform.counts = a.counts
            this.Requestform.fileName = a.fileName
            this.Requestform.currentPage = a.currentPage
            this.Requestform.fourExamine = a.fourExamine
            this.Requestform.fourcode = a.fourcode
            this.Requestform.id = a.id
            this.Requestform.koshinName = a.koshinName
            this.Requestform.koshincode = a.koshincode
            this.Requestform.oneExamine = a.oneExamine
            this.Requestform.onecode = a.onecode
            this.Requestform.pageSize = a.pageSize
            this.Requestform.threeExamine = a.threeExamine
            this.Requestform.threecode = a.threecode
            this.Requestform.twocode = a.twocode
            this.Requestform.total = a.total
            this.Requestform.twoExamine = a.twoExamine
            console.log(this.Requestform)
            axios.post("/api/requsetUser/pass", this.Requestform
            ).then(function (res) {
                _this.Search();
            }).catch(function (res) {
            })
        },
        //重置
        resetapproveListForm() {
            this.approveListForm.appDate = null
            this.approveListForm.searchFileName = ""
            this.approveListForm.searchStatus = ""
            this.approveListForm.appUserNameCard = ""
            this.approveListForm.affirmant = ""
        },

        // 上一页
        onUpPage: function () {
            // 当前页不是首页
            if (ApproveList.approveListForm.currentPage != 1) {
                // 计算画面显示页为当前页-1
                ApproveList.approveListForm.currentPage = ApproveList.approveListForm.currentPage - 1;
            }
            this.Search();
        },

        // 下一页
        onDownPage: function () {
            // 当前页不是尾页
            if (ApproveList.approveListForm.currentPage != ApproveList.pageTotal) {
                // 计算画面显示页为当前页+1
                ApproveList.approveListForm.currentPage = ApproveList.approveListForm.currentPage + 1;
            }
            ApproveList.datagridTotal = ApproveList.datagridTotal;
            this.Search();
        },

        // 首页
        onHomePage: function () {
            // 画面显示页为首页

            ApproveList.approveListForm.currentPage = 1;
            this.Search();
        },

        // 尾页
        onTailPage: function () {
            // 画面显示页为尾页
            ApproveList.approveListForm.currentPage = ApproveList.pageTotal;
            ApproveList.datagridTotal = ApproveList.datagridTotal;
            this.Search();
        },

        // 排序
        onSortChange: function ({column, prop, order}) {
            // 排序的字段
            ApproveList.approveListForm.sortColumn = prop;
            // 排序的升降
            ApproveList.approveListForm.sortType = order;
            // 画面显示页为首页
            ApproveList.approveListForm.currentPage = 1;
            this.Search();
        },
        //变更数据展示
        viewChangesdata(data){
            const _this=this

            this. viewChanges(data)
            axios.get("/api/requsetUser/viewChangesdata/" + data.applicationdate).then(function (res) {

                  _this.tablechangeData=res.data

            })

            this.dialogVisible=true

        },
        //查找更改数据
        viewChanges(changedata) {
            const _this = this
            _this.reviewsearch(changedata.applicationdate)

            if (changedata.applicationdate != null || changedata.applicationdate != '') {
                axios.get("/api/requsetUser/changedatasearch/" + changedata.applicationdate).then(function (res) {

                    //_this.datasubmit(res.data)
                    _this.viewChangesData=res.data
                   _this.input=_this.viewChangesData[0].fName

                })
            }


        },
        //审核人和签名查询
        reviewsearch(applicationdate) {
            console.log(applicationdate)
            axios.get("/api/requsetUser/reviewsearch/" + applicationdate).then(function (res) {
                ApproveList.fileForm.perModifyDatas = res.data
            })
        },
        //查找更改数据
        submitviewChanges(changedata) {
            const _this = this
         console.log("777777777777777777777",changedata)
            _this.reviewsearch(changedata.applicationdate)

            if (changedata.applicationdate != null || changedata.applicationdate != '') {
                axios.get("/api/requsetUser/changedatasearch/" + changedata.applicationdate).then(function (res) {

                    _this.datasubmit(res.data)

                })
            }


        },
        //提交
        datasubmit(value) {

            ApproveList.fileForm.fileId = value[0].fileId
            ApproveList.fileForm.fName = value[0].fName
            ApproveList.fileForm.confirmationPerson = value[0].confirmationPerson

            ApproveList.fileForm.loginTjCode = cmCookie.getCookie("loginTjCode")
            ApproveList.fileForm.koshinName = value[0].koshinName
            ApproveList.fileForm.cdCode = value[0].cdCode
            ApproveList.fileForm.perModifyApplicationValue = value[0].perModifyApplicationValue
            ApproveList.fileForm.perModifyContentValue = value[0].perModifyContentValue
            console.log(ApproveList.fileForm)
            axios.post("/api/perModify/perModifySubmit", ApproveList.fileForm).then(function (res) {



            }).catch(function (res) {
                FileManage.$message.error("发生了错误！")
            })
        },
        //确认驳回
        bohui:function (a) {
            this.$confirm("确定要驳回该申请？", "驳回确认", {
                confirmButtonText: "是",
                cancelButtonText: "否",
                type: "",
                callback: action => {
                    if (action === "confirm") {
                        const _this = this
                        console.log(a)
                        this.Requestform.loginTjCode = cmCookie.getCookie("loginTjCode");
                        this.Requestform.counts = a.counts
                        this.Requestform.fileName = a.fileName
                        this.Requestform.currentPage = a.currentPage
                        this.Requestform.fourExamine = a.fourExamine
                        this.Requestform.fourcode = a.fourcode
                        this.Requestform.id = a.id
                        this.Requestform.koshinName = a.koshinName
                        this.Requestform.koshincode = a.koshincode
                        this.Requestform.oneExamine = a.oneExamine
                        this.Requestform.onecode = a.onecode
                        this.Requestform.pageSize = a.pageSize
                        this.Requestform.threeExamine = a.threeExamine
                        this.Requestform.threecode = a.threecode
                        this.Requestform.twocode = a.twocode
                        this.Requestform.total = a.total
                        this.Requestform.twoExamine = a.twoExamine
                        console.log(this.Requestform)
                        axios.post("/api/requsetUser/bohui", this.Requestform
                        ).then(function (res) {
                            _this.Search();
                        }).catch(function (res) {
                        })
                    }
                }
            })
        }
    },

})

