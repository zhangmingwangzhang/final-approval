// ------------------------------------------------------------------------------------
// 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
// ------------------------------------------------------------------------------------
const PasswordModify = new Vue({
  el: "#app",
  data: {
    // 密码修改Form
    ruleForm: {
      oldPwd: "",
      newPwd: "",
      confirmPwd: "",
      loginTjCode:"",
      loginUserName:"",
      loginPwd:""
    },
    loading: false,
    // 必填项
    rules: {
      oldPwd: [{
        required: true,
        validator: " "
      }],
      newPwd: [{
        required: true,
        validator: " "
      }],
      confirmPwd: [{
        required: true,
        validator: " "
      }]
    },
    // 确认按钮二重压下flag
    submitDisabled: false
  },
  created: function() {
    this.ruleForm.loginTjCode = cmCookie.getCookie("loginTjCode")
    this.ruleForm.loginUserName = cmCookie.getCookie("loginUserName")
    this.ruleForm.loginPwd = cmCookie.getCookie("loginPwd")
  },
  methods: {
    // 密码修改
    submitForm() {
      // 调用密码修改
      axios.post("/api/admin/password",PasswordModify.ruleForm
      ).then(function(res) {
        if (!cmMessage.hasError(PasswordModify, res)) {
          // 确认按钮二重压下禁止
          PasswordModify.submitDisabled = true
          // 弹窗提示
          cmMessage.showSuccess(PasswordModify,"密码修改成功。3秒后自动跳转到登录页面")
          // 画面迁移
          setTimeout('window.location.href = "/index.html"',3000); 
        }
      }).catch(function(res) {
      })
    },
    // 点击清空按钮
    resetForm(formName) {
      this.$refs[formName].resetFields();
    },
    // 旧密码ime
    oldPwdIme: function () {
      this.ruleForm.oldPwd = this.ruleForm.oldPwd.replace(/[^0-9a-zA-Z]/g, "");
    },
    // 新密码ime
    newPwdIme: function () {
      this.ruleForm.newPwd = this.ruleForm.newPwd.replace(/[^0-9a-zA-Z]/g, "");
    },
    // 新密码确认ime
    confirmPwdIme: function () {
      this.ruleForm.confirmPwd = this.ruleForm.confirmPwd.replace(/[^0-9a-zA-Z]/g, "");
    },
  }
})
