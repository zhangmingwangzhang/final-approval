// ------------------------------------------------------------------------------------
// 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
// ------------------------------------------------------------------------------------
const UserList = new Vue({
  el: "#app",
  data: {
    // 登录者姓名
    loginUserName:"",
    // Info信息
    infoMessage: "",
    // 画面检索条件Form
    userForm: {
      userName: "",
      tjCode: "",
      positionId: "",
      deptId: "",
      postCode: "",
      roleCode: "",
      manageRoleFlag: "",
      sortColumn: "",
      sortType: "",
      currentPage: 1,
      pageSize: 20,
      deleteTjCode: "",
      deleteUserName: ""
    },
    // 画面检索用Form
    userSearchForm: {
    },
    title: "",
    // 追加popUP天津卡号活性非活性
    disabledFlag: false,
    // 首页活性非活性
    disabledFirst: false,
    // 尾页活性非活性
    disabledLast: false,
    // 取消按钮显示或隐藏
    cancelBtnFlag: "",
    // 检索条件部门
    searchDept: [],
    // 检索条件职位
    searchPosition: [],
    // 检索条件-职务权限
    searchPost: [],
    // 检索条件-审核权限
    searchReviewRole: [],
    // 检索条件-管理权限
    manageRole: [],
    // 用户追加-部门
    addDept: [],
    // 用户追加-职位
    addPosition: [],
    // 用户追加-职务权限
    postCode: [],
    // 用户追加-审核权限
    reviewRoleCode: [],
    // 画面一览data
    userData: [],
    // 画面一览data总数
    userDataTotal: 0,
    // 用户追加Popup显示隐藏
    dialogVisible: false,
    // 用户追加PopupForm
    userPopUpForm: {
      popUserName: "",
      popTjCode: "",
      popPositionId: "",
      popDeptId: "",
      popPostCode: "",
      popReviewRoleCode: "",
      popEmail: "",
      popRoleFlag: "",
      loginUserName: "",
      updateUserName: ""
    },
    // 按钮二重压下禁止
    submitDisabled: false,
    addDisabled: false
  },
  // 一览初期
  created: function() {
    this.init()
  },
  methods: {
    // 初期化
    init:function(){
      this.loginUserName = cmCookie.getCookie("loginUserName")
      axios.post("/api/userlist/init"
      ).then(function(res) {
        // infoMessage
        UserList.infoMessage = res.data.infoMessage
        // 下拉框
        UserList.searchDept = res.data.deptPullDown
        UserList.addDept = res.data.deptPullDown
        UserList.searchPosition = res.data.positionPullDown
        UserList.addPosition = res.data.positionPullDown
        UserList.searchPost = res.data.postPullDown
        UserList.postCode = res.data.postPullDown
        UserList.searchReviewRole = res.data.reviewRolePullDown
        UserList.reviewRoleCode = res.data.reviewRolePullDown
        UserList.manageRole = res.data.manageRolePullDown
        
        // 一览数据
        UserList.userData = res.data.userData
        UserList.userDataTotal = res.data.total
        UserList.userForm.userName = ""
        UserList.userForm.tjCode = ""
        UserList.userForm.positionId = ""
        UserList.userForm.deptId = ""
        UserList.userForm.postCode = ""
        UserList.userForm.roleCode = ""
        UserList.userForm.manageRoleFlag = ""
        UserList.userForm.sortColumn = "tjCode"
        UserList.userForm.sortType = "asc"
        UserList.userForm.currentPage = 1
        UserList.userForm.pageSize = 20
        
        // 检索用Form赋值
        UserList.userSearchForm.userName = UserList.userForm.userName
        UserList.userSearchForm.tjCode = UserList.userForm.tjCode
        UserList.userSearchForm.positionId = UserList.userForm.positionId
        UserList.userSearchForm.deptId = UserList.userForm.deptId
        UserList.userSearchForm.postCode = UserList.userForm.postCode
        UserList.userSearchForm.roleCode = UserList.userForm.roleCode
        UserList.userSearchForm.manageRoleFlag = UserList.userForm.manageRoleFlag
        UserList.userSearchForm.currentPage = UserList.userForm.currentPage
        UserList.userSearchForm.pageSize = UserList.userForm.pageSize
        UserList.userSearchForm.sortType = UserList.userForm.sortType
        UserList.userSearchForm.sortColumn = UserList.userForm.sortColumn
        // 首页活性
        if ((UserList.userSearchForm.currentPage - 1) *UserList.userSearchForm.pageSize +1 > 20) {
          UserList.disabledFirst = false
        } else{
          UserList.disabledFirst = true
        }
        // 尾页活性
        if (((UserList.userSearchForm.pageSize) * UserList.userSearchForm.currentPage) >= UserList.userDataTotal) {
            UserList.disabledLast = true
          } else{
            UserList.disabledLast = false
          }
      }).catch(function(res) {
      })
    },
    
    // 点击检索按钮
    onSearchBtn: function () {
      UserList.userForm.currentPage = 1
      this.onSearch(true)
    },
    
    // 调用检索
    onSearch: function (value) {
      if(value){
        // 检索用Form赋值
        this.userSearchForm.userName = this.userForm.userName
        this.userSearchForm.tjCode = this.userForm.tjCode
        this.userSearchForm.positionId = this.userForm.positionId
        this.userSearchForm.deptId = this.userForm.deptId
        this.userSearchForm.postCode = this.userForm.postCode
        this.userSearchForm.roleCode = this.userForm.roleCode
        this.userSearchForm.manageRoleFlag = this.userForm.manageRoleFlag
        this.userSearchForm.sortType = this.userForm.sortType
        this.userSearchForm.sortColumn = this.userForm.sortColumn
        this.userSearchForm.currentPage = this.userForm.currentPage
        this.userSearchForm.pageSize = this.userForm.pageSize
      }
      axios.post("/api/userList/search",UserList.userSearchForm
      ).then(function(res) {
        if (!cmMessage.hasError(UserList, res)) {
          // 检索到的数据是否为空
          if(res.data.userData.length != 0){
            UserList.userData = res.data.userData
            UserList.userDataTotal = res.data.total
          } else{
            UserList.userData = []
            UserList.userDataTotal = res.data.total
          }
          // 首页活性
          if ((UserList.userSearchForm.currentPage - 1) *UserList.userSearchForm.pageSize +1 > 20) {
            UserList.disabledFirst = false
          } else{
            UserList.disabledFirst = true
          }
          // 尾页活性
          if (((UserList.userSearchForm.pageSize) * UserList.userSearchForm.currentPage) >= UserList.userDataTotal) {
            UserList.disabledLast = true
          } else{
            UserList.disabledLast = false
          }
        }
      }).catch(function(res) {
      })
    },
    
    // 用户追加初期化
    onAdd:function () {
      this.title="用户追加"
      this.userPopUpForm.popUserName = ""
      this.userPopUpForm.popTjCode = ""
      this.userPopUpForm.popPostCode = ""
      this.userPopUpForm.popPositionId = ""
      this.userPopUpForm.popDeptId = ""
      this.userPopUpForm.popEmail = ""
      this.userPopUpForm.popReviewRoleCode = ""
      this.userPopUpForm.popRoleFlag = "MANGE_YOU"
      this.dialogVisible = true
      this.disabledFlag = false
      this.cancelBtnFlag = ""
      UserList.addDisabled = true
      UserList.submitDisabled = false
    },
    
    // 用户变更初期化
    onOpenUpdate: function (row) {
      this.title="用户变更"
      this.userPopUpForm.popUserName = row.userName
      this.userPopUpForm.popTjCode = row.tjCode
      this.userPopUpForm.popPostCode = row.postCode
      this.userPopUpForm.popPositionId = row.positionId
      this.userPopUpForm.popDeptId = row.deptId
      this.userPopUpForm.popReviewRoleCode = row.roleCode
      this.userPopUpForm.popRoleFlag = row.manageRoleFlag
      this.dialogVisible = true
      let email = row.email
      let index = email.indexOf("@")
      this.userPopUpForm.popEmail = email.substring(0,index)
      this.disabledFlag = true
      this.userPopUpForm.updateUserName = row.userName
      this.cancelBtnFlag = "none"
      this.submitDisabled = false
    },
    
    // 点击用户追加Popup的提交按钮
    submitModify: function () {
      UserList.userPopUpForm.loginUserName = UserList.loginUserName
      if(this.disabledFlag){
        axios.post("/api/userList/update",UserList.userPopUpForm
        ).then(function(res) { 
          if (!cmMessage.hasError(UserList, res)) {
            // 关闭弹窗
            UserList.dialogVisible = false
            // 亲画面原条件刷新
            UserList.userForm.userName = UserList.userSearchForm.userName
            UserList.userForm.tjCode = UserList.userSearchForm.tjCode
            UserList.userForm.positionId = UserList.userSearchForm.positionId
            UserList.userForm.deptId = UserList.userSearchForm.deptId
            UserList.userForm.postCode = UserList.userSearchForm.postCode
            UserList.userForm.roleCode = UserList.userSearchForm.roleCode
            UserList.userForm.manageRoleFlag = UserList.userSearchForm.manageRoleFlag
            UserList.submitDisabled = true
            
            // 部门和职务权限下拉框刷新
            UserList.searchDept = res.data.deptPullDown
            UserList.addDept = res.data.deptPullDown
            UserList.searchPost = res.data.postPullDown
            UserList.postCode = res.data.postPullDown
            // 调用检索
            UserList.onSearch(false)
            UserList.infoMessage = res.data.infoMessage
            cmMessage.showSuccess(UserList,UserList.infoMessage)
          }
        }).catch(function(res) {
        })
      } else {
        axios.post("/api/userList/insert",UserList.userPopUpForm
        ).then(function(res) {
          if (!cmMessage.hasError(UserList, res)) {
            // 清空入力项
            UserList.userPopUpForm.popUserName = ""
            UserList.userPopUpForm.popTjCode = ""
            UserList.userPopUpForm.popPostCode = ""
            UserList.userPopUpForm.popPositionId = ""
            UserList.userPopUpForm.popDeptId = ""
            UserList.userPopUpForm.popEmail = ""
            UserList.userPopUpForm.popReviewRoleCode = ""
            UserList.userPopUpForm.popRoleFlag = "MANGE_YOU"
            UserList.infoMessage = res.data.infoMessage
            // 部门和职务权限下拉框刷新
            UserList.searchDept = res.data.deptPullDown
            UserList.addDept = res.data.deptPullDown
            UserList.searchPost = res.data.postPullDown
            UserList.postCode = res.data.postPullDown
            cmMessage.showSuccess(UserList,UserList.infoMessage)
          }
        }).catch(function(res) {
        })
      }
    },
    
    // 点击用户追加Popup的取消或关闭按钮
    closePopup: function () {
      // 关闭弹窗
      this.dialogVisible = false
      // 亲画面原条件刷新
      this.userForm.userName = this.userSearchForm.userName
      this.userForm.tjCode = this.userSearchForm.tjCode
      this.userForm.positionId = this.userSearchForm.positionId
      this.userForm.deptId = this.userSearchForm.deptId
      this.userForm.postCode = this.userSearchForm.postCode
      this.userForm.roleCode = this.userSearchForm.roleCode
      this.userForm.manageRoleFlag = this.userSearchForm.manageRoleFlag
      this.onSearch(false)
      UserList.addDisabled = false
      UserList.submitDisabled = false
    },
    
    // 删除确认
    open:function(row) {
      this.$confirm("确定要删除用户吗？", "删除确认", {
        confirmButtonText: "是",
        cancelButtonText: "否",
        type: "",
        callback: action => {
          if (action === "confirm") {
            this.onDelete(row)
          }
        }
      })
    },
    
    // 删除确认时点击是
    onDelete: function (row) {
      this.userForm.deleteTjCode = row.tjCode
      this.userForm.deleteUserName = row.userName
      axios.post("/api/userList/delete",UserList.userForm
      ).then(function(res) { 
        if (!cmMessage.hasError(UserList, res)) {
          UserList.infoMessage = res.data.infoMessage
          cmMessage.showSuccess(UserList,UserList.infoMessage)
          // 初期化
          setTimeout("UserList.init()",3000); 
        }
      }).catch(function(res) {
      })
    },
    
    // 上一页下一页
    onCurrentChange: function (currentPage) {
      this.userForm.currentPage = currentPage;
      this.userSearchForm.currentPage = currentPage;
      this.onSearch(false);
    },
    
    // 首页
    onFirstPage: function () {
      UserList.userForm.currentPage = 1
      UserList.userSearchForm.currentPage = UserList.userForm.currentPage
      this.onSearch(false);
    },
    
    // 尾页
    onLastPage: function () {
      UserList.userForm.currentPage = Math.ceil(UserList.userDataTotal/UserList.userForm.pageSize)
      UserList.userSearchForm.currentPage = UserList.userForm.currentPage
      this.onSearch(false);
    },
    
    // 排序
    onSortChange: function ({column, prop, order}) {
      this.userForm.sortColumn = prop;
      this.userSearchForm.sortColumn = prop;
      if(order != null){
        this.userForm.sortType = order.replace(/ending/g, "");
        this.userSearchForm.sortType = this.userForm.sortType
      }
      this.userForm.currentPage = 1
      this.userSearchForm.currentPage = 1
      this.onSearch(false);
    },
    
    // popUp天津卡号ime
    popTjCodeIme: function () {
      this.userPopUpForm.popTjCode = this.userPopUpForm.popTjCode.replace(/[^0-9a-zA-Z]/g, "");
    },
    
    // 检索条件天津卡号ime
    searchTjCodeIme: function () {
      this.userForm.tjCode = this.userForm.tjCode.replace(/[^0-9a-zA-Z]/g, "");
    },
    
    // popUp邮箱ime
    popEmailIme: function () {
      this.userPopUpForm.popEmail = this.userPopUpForm.popEmail.replace(/[^0-9a-zA-Z.]/g, "");
    },
    
    // popUp部门下拉框可编辑
    deptBlur(e) {
      if (e.target.value) {
        UserList.userPopUpForm.popDeptId = e.target.value;
        UserList.isName = true;
      } else {
        UserList.isName = false;
      }
    },
    // popup职务权限下拉框可编辑
    postBlur(e) {
      if (e.target.value) {
        UserList.userPopUpForm.popPostCode = e.target.value;
        UserList.isName = true;
      } else {
        UserList.isName = false;
      }
    },
  },
})