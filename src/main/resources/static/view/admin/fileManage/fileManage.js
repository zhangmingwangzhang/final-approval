// ------------------------------------------------------------------------------------
// 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
// ------------------------------------------------------------------------------------
const systemError = "发生了错误！";
const FileManage = new Vue({
  el: "#app",
  data: {
    // 登录者名称
    loginTjCode:"",
    loginUserName:"",


    submitbutton1:true,
    submitbutton2:false,
    // Info信息
    infoMessage: "",
    fileForm: {
      file: "",
      fileAbstract:'01',
      updateConfirmer:"01",
      btnstatus:true,
      position: "",
      currentPage: 1,
      pageSize: 20,
      sortColumn: "",
      sortType: "",
      addDatas:[],
      perModifyDatas:[],
      fileId:"",
      fName:"",
      perModifyApplicationValue:"",
      perModifyContentValue:"",
      cdCode:"",
      confirmationPerson:"",
      addApplication:"",
      addContent:"",
      // 签名sheet
      reviewerSheet:"",
      // 确认人
      fileAddConfirmer:"",
      // 作成者
      createName:"",
      // 登录者
      loginUserName:"",
      // 更新者
      koshinName:"",
    },
    fileSearchForm: {
        
    },
    // 首页活性非活性
    disabledFirst: false,
    // 尾页活性非活性
    disabledLast: false,
    isShow: false,
    index: 1,
    perModifyData:[],
    // 文件列表
    fileList:[],
    tableData:[],
    addData:[],
    datagridTotal: 0,
    number: 0,
    perModify: false,
    deleteDialog:false,
    fileAdd: false,
    selectedOptions:[],
    // 文件类别
    fileValue:[],
    // 确认人
    confirmer:[],
    confirmerValue:"",
    // 适用范围
    perModifyApplication:[],
    application:"",
    // 业务内容
    perModifyContent:[],
    content:"",
    // 审核人
    reviewer: [],
    reviewerValue:"",
    // 文件追加按钮二重压下flag
    fileAddDisabled:false,
    // 权限变更popup确认按钮二重压下flag
    perModifySubmitDisabled:false,
  },
  // 初期表示
  mounted: function () {
    this.search();
  },
  methods: {
    // 初期
    search: function () {
      this.loginUserName = cmCookie.getCookie("loginUserName")
      this.$refs.sortTable.clearSort();
      let fileForm = {"fileName":"","fileValue":"","pageSize":20,"currentPage":1,"sortColumn":"uploadTime","sortType":"desc"};
      axios.post("/api/fileManage/search",fileForm).then(function(res) {
        // 下拉列表
        FileManage.fileValue = res.data.fileType
        // 文件管理一览
        FileManage.tableData = res.data.tableData
        FileManage.datagridTotal = res.data.total
        FileManage.fileForm.sortColumn = res.data.sortColumn
        FileManage.fileForm.sortType = res.data.sortType
        // 检索用Form赋值
        FileManage.fileSearchForm.fileName = FileManage.fileForm.fileName
        FileManage.fileSearchForm.fileValue = FileManage.fileForm.fileValue
        FileManage.fileSearchForm.currentPage = FileManage.fileForm.currentPage
        FileManage.fileSearchForm.pageSize = FileManage.fileForm.pageSize
        FileManage.fileSearchForm.sortType = FileManage.fileForm.sortType
        FileManage.fileSearchForm.sortColumn = FileManage.fileForm.sortColumn
        // 首页活性
        if ((FileManage.fileSearchForm.currentPage - 1) *FileManage.fileSearchForm.pageSize +1 > 20) {
          FileManage.disabledFirst = false
        } else{
          FileManage.disabledFirst = true
        }
        // 尾页活性
        if (((FileManage.fileSearchForm.pageSize) * FileManage.fileSearchForm.currentPage) >= FileManage.datagridTotal) {
          FileManage.disabledLast = true
        } else{
          FileManage.disabledLast = false
        }
      }).catch(function(res) {
      })
    },
    
    // 点击检索按钮
    onSearchBtn: function () {
      FileManage.fileForm.currentPage = 1
      this.$refs.sortTable.clearSort();
      this.fileForm.sortType = "desc"
      this.fileForm.sortColumn = "uploadTime"
      this.onSearch(true)
    },
    
    // 检索
    onSearch: function (value) {
      if (value) {
        this.fileSearchForm.fileName = this.fileForm.fileName
        this.fileSearchForm.fileValue = this.fileForm.fileValue
        this.fileSearchForm.currentPage = this.fileForm.currentPage
        this.fileSearchForm.pageSize = this.fileForm.pageSize
        this.fileSearchForm.sortType = this.fileForm.sortType
        this.fileSearchForm.sortColumn = this.fileForm.sortColumn
      }
      axios.post("/api/fileManage/search", FileManage.fileSearchForm
      ).then(function(records) {
        if (!cmMessage.hasError(FileManage, records)) {
          FileManage.fileForm.sortColumn = records.data.sortColumn
          FileManage.fileForm.sortType = records.data.sortType
          FileManage.datagridTotal = records.data.total
          if(records.data.tableData.length != 0){
            FileManage.tableData = records.data.tableData
          } else {
            FileManage.tableData = []
          }
          // 首页活性
          if ((FileManage.fileSearchForm.currentPage - 1) *FileManage.fileSearchForm.pageSize +1 > 20) {
            FileManage.disabledFirst = false
          } else{
            FileManage.disabledFirst = true
          }
          // 尾页活性
          if (((FileManage.fileSearchForm.pageSize) * FileManage.fileSearchForm.currentPage) >= FileManage.datagridTotal) {
            FileManage.disabledLast = true
          } else{
            FileManage.disabledLast = false
          }
        }
      }).catch(function(records) {
      })
    },
    
    // 上一页下一页
    onCurrentChange: function (currentPage) {
      this.fileForm.currentPage = currentPage;
      this.fileSearchForm.currentPage = currentPage;
      this.onSearch(false);
    },
    
    // 首页
    onFirstPage: function () {
      FileManage.fileForm.currentPage = 1
      FileManage.fileSearchForm.currentPage = FileManage.fileForm.currentPage
      this.onSearch(false);
    },

    // 尾页
    onLastPage: function () {
      FileManage.fileForm.currentPage = Math.ceil(FileManage.datagridTotal/FileManage.fileForm.pageSize)
      FileManage.fileSearchForm.currentPage = FileManage.fileForm.currentPage
      this.onSearch(false);
    },

    // 排序
    onSortChange: function ({column, prop, order}) {
      this.fileForm.sortColumn = prop;
      this.fileSearchForm.sortColumn = prop;
      if(order != null){
        this.fileForm.sortType = order.replace(/ending/g, "");
        this.fileSearchForm.sortType = this.fileForm.sortType
      }
      this.fileForm.currentPage = 1
      this.fileSearchForm.currentPage = 1
      this.onSearch(false);
    },

    // 文件管理一览删除
    delete_fileManage:function(file,fileAbstract) {
      this.$confirm("确定要删除文件吗？", "删除确认", {
        confirmButtonText: "是",
        cancelButtonText: "否",
        type: "",
        callback: action => {
          if (action === "confirm") {
            this.onDelete(file,fileAbstract)
          }
        }
      })
    },
    onDelete: function (file,fileAbstract) {
      // 截取空格前的文件编号
      FileManage.fileForm.fileId = file.trim().split(" ")[0];
      // 截取空格后的文件名称
      FileManage.fileForm.fName = file.substring(file.indexOf(" ") + 1);
      // 文件类别
      FileManage.fileForm.fileAbstractData = fileAbstract;
      axios.post("/api/fileManage/delete", FileManage.fileForm).then(function(records) {
        if (!cmMessage.hasError(FileManage, records)) {
          // info
          FileManage.infoMessage = records.data.infoMessage
          cmMessage.showSuccess(FileManage,FileManage.infoMessage)
          // 初期化
          setTimeout("FileManage.search()",0);
          FileManage.fileForm.fileName = ""
          FileManage.fileForm.fileValue = ""
        }
      }).catch(function(records) {
        FileManage.$message.error("发生了错误！")
      })
    },

    // 级联选择器
    handleChange (value) {
      console.log(value);
    },

    // 权限变更弹窗显示
    perModifys: function (file,fileAbstract) {
      this.perModify = true;
      // 截取空格前的文件编号
      FileManage.fileForm.fileId = file.trim().split(" ")[0];
      FileManage.fileForm.fName = file.substring(file.indexOf(" ") + 1);
      // 文件类别
      FileManage.fileForm.fileAbstractData = fileAbstract;
      // 业务内容初始化
      this.perModifyContent = [];
      axios.post("/api/perModify/searchApplicationList", FileManage.fileForm).then(function(records) {
        // 适用范围下拉列表
        FileManage.perModifyApplication = records.data.perModifyApplication
        // 确认人下拉列表
        FileManage.confirmer = records.data.confirmer
        // 确认人下拉列表初期值
        FileManage.confirmerValue = records.data.cdCode
        // 确认人文本框初期值
        FileManage.fileForm.confirmationPerson = records.data.confirmationPerson
      }).catch(function(records) {

      })

    },
    // 查看审核人
    shows: function (file,fileAbstract) {
      this.show = true;
      // 截取空格前的文件编号
      FileManage.fileForm.fileId = file.trim().split(" ")[0];
      FileManage.fileForm.fName = file.substring(file.indexOf(" ") + 1);
      // 文件类别
      FileManage.fileForm.fileAbstractData = fileAbstract;
      // 业务内容初始化
      this.perModifyContent = [];
      axios.post("/api/perModify/searchApplicationList", FileManage.fileForm).then(function(records) {

        console.log(12333)
        console.log(records.data)

        // 适用范围下拉列表
        FileManage.perModifyApplication = records.data.perModifyApplication
        // 确认人下拉列表
        FileManage.confirmer = records.data.confirmer
        // 确认人下拉列表初期值
        FileManage.confirmerValue = records.data.cdCode
        // 确认人文本框初期值
        FileManage.fileForm.confirmationPerson = records.data.confirmationPerson
      }).catch(function(records) {

      })

    },

    // 权限变更点击radio button隐藏部件
    confirmerChange: function (val) {
      var applicationAndContent = document.getElementById("applicationAndContent");
      var confirmerSelect = document.getElementById("confirmerSelect");
      var confirmerInput = document.getElementById("confirmerInput");
      if(val == '02'){
       this.submitbutton1=false
       this.submitbutton2=true
        applicationAndContent.style.display = "none";
        confirmerSelect.style.display = "block";
        confirmerInput.style.display = "none";
        document.getElementById("nav").style.display = "none";
        document.getElementById("deleteBtn").style.display = "none";
      } else {
        this.submitbutton1=true
        this.submitbutton2=false
        FileManage.application = "";
        FileManage.content = "";
        applicationAndContent.style.display = "block";
        confirmerSelect.style.display = "none";
        confirmerInput.style.display = "block";
        // 业务内容初始化
        this.perModifyContent = [];
      }
    },

    // 级联列表显示
    chooseOne (val) {
      let oneVal = val;
      this.perModifyContent = [];
      this.content = "";
      this.perModifyData =[];
      this.contentSearch(oneVal);
      // 确认人，审核人和签名位置不显示
      document.getElementById("nav").style.display = "none";
    },

    // 业务内容下拉列表
    contentSearch: function (oneVal) {
      FileManage.fileForm.perModifyApplicationValue = oneVal;
      axios.post("/api/perModify/searchConditionList", FileManage.fileForm).then(function(records) {
        FileManage.perModifyContent = records.data.perModifyContent
      }).catch(function(records) {

      })
    },
    chooseComfig(val){
      let TwoVal = val;
      this.comfigSearch(TwoVal);
      this.$forceUpdate();
      if (TwoVal == "") {
        // 确认人，审核人和签名位置不显示
        document.getElementById("nav").style.display = "none";
        document.getElementById("deleteBtn").style.display = "none";
      } else {
        // 确认人，审核人和签名位置显示
        document.getElementById("nav").style.display = "block";
        document.getElementById("deleteBtn").style.display = "block";
      }
    },

    // 确认人，审核人和签名位置
    comfigSearch: function (TwoVal) {
      FileManage.fileForm.perModifyContentValue = TwoVal;
      axios.post("/api/perModify/searchPerModifyData", FileManage.fileForm).then(function(records) {

        FileManage.perModifyData = records.data.perModifyData
        FileManage.reviewer = records.data.reviewer
      })
    },

    // 权限变更删除按钮压下
    scopeConditionDel: function () {
      if(FileManage.perModifyApplication.length == 1 && FileManage.perModifyContent.length == 1){
        FileManage.deleteDialog = true;
      }
      else{
        FileManage.deleteScope();
      }
    },

    //删除确认Dialog点击Yes
    deleteYes: function () {
      FileManage.deleteScope();
      FileManage.deleteDialog = false;
    },

   //删除确认Dialog点击No
    closeDialog: function () {
      FileManage.deleteDialog = false;
    },
    // 适用范围，业务内容删除
    deleteScope: function () {
      axios.post("/api/perModify/scopeConditionDel",FileManage.fileForm).then(function(res){
        if (!cmMessage.hasError(FileManage, res)) {
          // 适用范围下拉列表
          FileManage.perModifyApplication = res.data.perModifyApplication
          // 业务内容下拉列表
          FileManage.perModifyContent = [];
          // 确认人文本框初期值
          FileManage.fileForm.confirmationPerson = res.data.confirmationPerson
          // 确认人下拉列表
          FileManage.confirmer = res.data.confirmer
          // 确认人下拉列表初期值
          FileManage.confirmerValue = res.data.cdCode
          // info
          FileManage.infoMessage = res.data.infoMessage
          cmMessage.showSuccess(FileManage,FileManage.infoMessage)
          // 确认人，审核人和签名位置不显示
          document.getElementById("nav").style.display = "none";
          FileManage.application = "";
          FileManage.content = "";
          FileManage.perModifySubmitDisabled = false;
          FileManage.fileForm.updateConfirmer = "01";
          FileManage.application = "";
          FileManage.content = "";
          applicationAndContent.style.display = "block";
          confirmerSelect.style.display = "none";
          confirmerInput.style.display = "block";
          // 初期化
          if( FileManage.perModifyApplication.length == 0){
            FileManage.perModify = false;
            setTimeout("FileManage.search()",0);
            FileManage.fileForm.fileName = ""
            FileManage.fileForm.fileValue = ""
          }
        }
      }).catch(function(res) {
        FileManage.$message.error("发生了错误！")
      })
    },

    // 权限变更确认
    perModifySubmit: function () {
      FileManage.fileForm.loginTjCode = cmCookie.getCookie("loginTjCode")
      FileManage.fileForm.koshinName = FileManage.loginUserName
      FileManage.fileForm.cdCode = FileManage.confirmerValue;
      FileManage.fileForm.perModifyApplicationValue = FileManage.application;
      FileManage.fileForm.perModifyContentValue = FileManage.content;
      FileManage.fileForm.perModifyDatas = FileManage.perModifyData;

      axios.post("/api/perModify/perModifyadd",FileManage.fileForm).then(function(res){
        if (!cmMessage.hasError(FileManage, res)) {
          // 权限变更确认按钮二重压下禁止
          FileManage.perModifySubmitDisabled = true
          FileManage.infoMessage = res.data.infoMessage
          cmMessage.showSuccess(FileManage,FileManage.infoMessage)
          FileManage.perModify = false;
          // 确认人，审核人和签名位置隐藏
          document.getElementById("nav").style.display = "none";
          // 删除按钮隐藏
          document.getElementById("deleteBtn").style.display = "none";
          FileManage.application = "";
          FileManage.content = "";
          FileManage.perModifySubmitDisabled = false;
          FileManage.fileForm.updateConfirmer = "01";
          FileManage.application = "";
          FileManage.content = "";
          applicationAndContent.style.display = "block";
          confirmerSelect.style.display = "none";
          confirmerInput.style.display = "block";
          // 初期化
          setTimeout("FileManage.search()",0);
        }
      }).catch(function(res) {
        FileManage.$message.error("发生了错误！")
      })
    },
    // 点击弹窗的x，刷新父窗口
    handleDialogClose () {
      this.fileAdd = false;
      this.perModify = false;
      window.location.reload();
    },

    // 确认人权限变更确认
    confirmederperModifySubmit: function () {
      FileManage.fileForm.koshinName = FileManage.loginUserName
      FileManage.fileForm.cdCode = FileManage.confirmerValue;
      FileManage.fileForm.perModifyApplicationValue = FileManage.application;
      FileManage.fileForm.perModifyContentValue = FileManage.content;
      FileManage.fileForm.perModifyDatas = FileManage.perModifyData;
      axios.post("/api/perModify/perModifySubmit",FileManage.fileForm).then(function(res){
        if (!cmMessage.hasError(FileManage, res)) {
          // 权限变更确认按钮二重压下禁止
          FileManage.perModifySubmitDisabled = true
          FileManage.infoMessage = res.data.infoMessage
          cmMessage.showSuccess(FileManage,FileManage.infoMessage)
          FileManage.perModify = false;
          // 确认人，审核人和签名位置隐藏
          document.getElementById("nav").style.display = "none";
          // 删除按钮隐藏
          document.getElementById("deleteBtn").style.display = "none";
          FileManage.application = "";
          FileManage.content = "";
          FileManage.perModifySubmitDisabled = false;
          FileManage.fileForm.updateConfirmer = "01";
          FileManage.application = "";
          FileManage.content = "";
          applicationAndContent.style.display = "block";
          confirmerSelect.style.display = "none";
          confirmerInput.style.display = "block";
          // 初期化
          setTimeout("FileManage.search()",0);
        }
      }).catch(function(res) {
        FileManage.$message.error("发生了错误！")
      })
    },
    // 文件追加弹窗显示
    fileAdds: function () {
      this.confirmerValue = "";
      axios.post("/api/fileAdd/searchConfirmerValue",FileManage.fileForm).then(function(res) {
        // 文件追加按钮二重压下禁止
        FileManage.fileAddDisabled = true
        FileManage.confirmer = res.data.confirmer
        FileManage.reviewer = res.data.reviewer
      })

      axios.post("/api/fileAdd/searchApplicationValue",FileManage.fileForm).then(function(res) {
        // 文件追加按钮二重压下禁止
        FileManage.fileAddDisabled = true
        FileManage.perModifyApplication = res.data.perModifyApplication
      })

      axios.post("/api/fileAdd/searchContentValue",FileManage.fileForm).then(function(res) {
        // 文件追加按钮二重压下禁止
        FileManage.fileAddDisabled = true
        FileManage.perModifyContent = res.data.perModifyContent
      })

      this.fileAdd = true;
      this.addData.push({});
    },

    // 文件追加点击radio button隐藏部件
    agreeChange:function (val) {
      FileManage.fileList.splice( 0, FileManage.fileList.length );
      console.log(FileManage.fileList);
      this.confirmerValue = "";
      this.fileForm.reviewerSheet = "";
      FileManage.application = "";
      FileManage.content = "";
      this.addData = null;
      this.addData = new Array();
      this.addData.push({});
      var a = document.getElementById("Template");
      var b = document.getElementById("sample");
      var c = document.getElementById("picture");
      var d = document.getElementById("aa");
      FileManage.fileForm.fileAbstract = val;
      if(val == '01'){
        a.style.display = "block";
        b.style.display = "none";
        c.style.display = "none";
        d.style.display = "block";
      } else {
        a.style.display = "none";
        b.style.display = "block";
        c.style.display = "block";
        d.style.display = "none";
      }
    },

    // 文件追加添加按钮
    addReviewerData:function () {
      console.log(this.addData);
      this.addData.push({});
    },

    // 权限变更添加按钮
    addPerModifyData:function () {
        console.log(this.perModifyData);
        this.perModifyData.push({});
      },

    // 文件追加删除按钮
    deleteData: function (index) {
      this.addData.splice(index, 1);
    },

    // 权限变更删除按钮
    deletePerModifyData: function (index) {
      this.perModifyData.splice(index, 1);
    },

    // 文件追加清空按钮
    empty: function () {
      this.confirmerValue = "";
      this.fileForm.reviewerSheet = "";
      FileManage.application = "";
      FileManage.content = "";
      this.addData = null;
      this.addData = new Array();
      this.addData.push({});
    },

    // 文件追加画面文件上传
    handlePreview (file) {
      console.log(file);
    },
    handleExceed (files, fileList) {
      this.$message.warning(`当前限制选择1个文件，本次选择了 ${files.length} 个文件，共选择了 ${files.length + fileList.length} 个文件`);
    },
    beforeRemove (file, fileList) {
      return this.$confirm(`确定移除 ${ file.name }？`);
    },
    changeFile (file, fileList) {
      console.log('change', file, file.raw)
      this.fileList.push(file);
    },
    handleRemove (file, fileList) {
      this.fileList.splice( file, 1 );
      console.log(file, fileList);
    },

    selectBlur(e) {
        if (e.target.value) {
         // 不在表单的时候不需要判断，也不需要isname字段
            FileManage.application = e.target.value;
            this.isName = true;
        } else {
            this.isName = false;
        }
    },

    selectContent(e) {
        if (e.target.value) {
         // 不在表单的时候不需要判断，也不需要isname字段
            FileManage.content = e.target.value;
            this.isName = true;
        } else {
            this.isName = false;
        }
    },

    // 文件追加确认
    confirm: function () {
      FileManage.fileForm.createName = FileManage.loginUserName;
      FileManage.fileForm.fileAddConfirmer = FileManage.confirmerValue;
      FileManage.fileForm.application = FileManage.application;
      FileManage.fileForm.content = FileManage.content;
      FileManage.fileForm.addDatas = FileManage.addData;
      console.log(FileManage.fileForm.addDatas);
      let config = {
        'Content-Type': 'multipart/form-data'
      };
      let formData = new FormData();
      this.fileList.forEach(function (file) {
        formData.append("file", file.raw, file.name);
      });
      formData.append("fileAbstract", FileManage.fileForm.fileAbstract);
      formData.append("fileAddConfirmer", FileManage.fileForm.fileAddConfirmer);
      formData.append("reviewerSheet", FileManage.fileForm.reviewerSheet);
      formData.append("application", FileManage.fileForm.application);
      formData.append("content", FileManage.fileForm.content);
      formData.append("createName", FileManage.fileForm.createName);
      for (var i = 0;i < FileManage.fileForm.addDatas.length;i++) {
        formData.append('addDatas['+i+'].reviewerValue',FileManage.fileForm.addDatas[i].reviewerValue)
        formData.append('addDatas['+i+'].position',FileManage.fileForm.addDatas[i].position)
      }
      axios.post("/api/fileAdd/FileAddComfirm", formData, config
      ).then(function(res) {
        if (!cmMessage.hasError(FileManage, res)) {
          FileManage.infoMessage = res.data.infoMessage
          cmMessage.showSuccess(FileManage,FileManage.infoMessage)
          FileManage.fileList.splice( 0, FileManage.fileList.length );
          console.log(FileManage.fileList);
        }
     }).catch(function(res) {
       FileManage.$message.error("发生了错误！")
     })
   },

  }
})

