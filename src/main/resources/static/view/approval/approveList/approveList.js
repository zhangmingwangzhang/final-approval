// ------------------------------------------------------------------------------------
// 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
// ------------------------------------------------------------------------------------
const ApproveList = new Vue({
    el: "#app",
    data: {
        logincode: cmCookie.getCookie("loginTjCode"),
        fileList: '',
        //预览对话框
        previewdialogVisible: false,
        qmpreviewdialogVisible: false,
        // 画面検索条件用のForm
        requestApproveForm: {
            //申请者code
            tjCode: '',
            //登陆者code
            loginCode: '',
            userName: '',
            //部门id
            deptId: '',
            //部门名称
            deptName: '',
            applicationNumber: '',
            fileId: '',
            fileName: '',
            //文件名称code
            fileNameCode: '',
            //业务范围code
            scopeApplicationCode: '',
            scopeApplication: '',
            //业务内容code
            conditionCode: '',
            condition: '',
            examineConfirReasons: '',
            fileRemark: '',
            examineStatus: '',
            //审核状态code
            examineStatusCode: '',
            loginUserId: cmCookie.getCookie("loginUserId"),

        },
        fileIdList: [],
        // 画面検索条件用のForm
        approveListForm: {
            userId: "",
            searchFileName: "",
            searchStatus: "",
            postCode: "",
            currentPage: 1,
            pageSize: 10,
            approveNumber: "",
            filePath: "",
            sortColumn: "",
            sortType: "",
            username: "",
            confirmer: "",
            applicationDate: "",
            affirmant:"",
            appUserNameCard:"",
            appDate: [],

        },
        rejectConfirCount: "",
        rejectWaitCount: "",
        examineWaitCount: "",
        examineConfirCount: "",
        // 画面一览数据
        datagrid: [],
        selectStatus: [],
        datas: [],
        nowOrders: "",
        maxOrders: "",
        start: 0,
        end: 0,
        // 当前页
        currentPage: 1,
        // 总件数
        datagridTotal: 0,
        // 页数
        pageTotal: 1,
        dialogVisible: false,
        active: 0,
        // < 和 << 活性flg
        disFlagHome: true,
        // > 和>> 活性flg
        disFlagTail: false,
        reviewScheduleFlag: false,
    },

    // 初期化
    created: function () {
        // 获取登录者卡号
        let id = cmCookie.getCookie("loginTjCode");
        let data = {
            "userId": id,
            "searchFileName": "",
            "searchStatus": "",
            "currentPage": "1",
            "pageSize": "10",
            "sortColumn": "",
            "sortType": ""
        };
        axios.post("/api/approvelist/search", data
        ).then(function (res) {
            // 一览数据
            ApproveList.datagrid = res.data.datagrid;
            // 状态下拉框
            ApproveList.selectStatus = res.data.selectStatus;
            // 待处理事件
            ApproveList.rejectConfirCount = res.data.rejectConfirCount;
            ApproveList.rejectWaitCount = res.data.rejectWaitCount;
            ApproveList.examineWaitCount = res.data.examineWaitCount;
            ApproveList.examineConfirCount = res.data.examineConfirCount;
            // 总件数
            ApproveList.datagridTotal = res.data.allCount;

            // 计算页数
            ApproveList.pageTotal = Math.ceil(ApproveList.datagridTotal / ApproveList.approveListForm.pageSize);
            // 判断开始位置的值
            if (ApproveList.datagridTotal != 0) {
                ApproveList.start = 1;
            } else {
                ApproveList.start = 0;
            }
            // 判断结束位置的值
            if (ApproveList.datagridTotal < ApproveList.approveListForm.pageSize) {
                ApproveList.end = ApproveList.datagridTotal;
            }
            if (ApproveList.datagridTotal >= ApproveList.approveListForm.pageSize) {
                ApproveList.end = ApproveList.approveListForm.pageSize;
            }

            // 画面显示的条数小于每页条数下一页尾页不可用
            if (ApproveList.end == ApproveList.datagridTotal) {
                ApproveList.disFlagTail = true;
            }
        }).catch(function (res) {
        })

    },

    methods: {
        //重置
        resetapproveListForm() {
            this.approveListForm.appDate=null
            this.approveListForm.searchFileName=""
            this.approveListForm.searchStatus=""
            this.approveListForm.appUserNameCard=""
            this.approveListForm.affirmant=""
        },

        //超链接快捷查询
        showSeacher: function (value) {

            // 设定第一页
            ApproveList.approveListForm.currentPage = 1;
            ApproveList.selectFlag = false;
            // 清空升降序的选择
            this.$refs.sortTable.clearSort();
            ApproveList.approveListForm.sortColumn = "approveNum";
            ApproveList.approveListForm.sortType = "descending";
            let htStatusdata = this.value;
            ApproveList.approveListForm.searchStatus = value;
            // 调用查询一览方法
            this.onSearch(true);
        },
        // 点击检索按钮
        onSearchReady: function (value) {
            // 设定第一页
            ApproveList.approveListForm.currentPage = 1;
            ApproveList.selectFlag = false;
            // 清空升降序的选择
            this.$refs.sortTable.clearSort();
            ApproveList.approveListForm.sortColumn = "approveNum";
            ApproveList.approveListForm.sortType = "descending";
            // 调用查询一览方法
            this.onSearch(true);
        },

        // 查询一览
        onSearch: function (value) {
            ApproveList.approveListForm.userId = cmCookie.getCookie("loginTjCode");
            ApproveList.approveListForm.searchFileName = this.approveListForm.searchFileName;
            ApproveList.approveListForm.searchStatus = this.approveListForm.searchStatus;
            //计算开始和结束位置的值
            let end = (this.approveListForm.pageSize) * this.approveListForm.currentPage;
            let start = (this.approveListForm.currentPage - 1) * this.approveListForm.pageSize + 1;
            axios.post("/api/approvelist/search", ApproveList.approveListForm
            ).then(function (res) {
                console.log(res)
                ApproveList.selectFlag = true;
                // 一览数据
                ApproveList.datagrid = res.data.datagrid;
                // 待处理事件
                ApproveList.rejectConfirCount = res.data.rejectConfirCount;
                ApproveList.rejectWaitCount = res.data.rejectWaitCount;
                ApproveList.examineWaitCount = res.data.examineWaitCount;
                ApproveList.examineConfirCount = res.data.examineConfirCount;
                // 总件数
                ApproveList.datagridTotal = res.data.allCount;
                // 计算页数
                ApproveList.pageTotal = Math.ceil(ApproveList.datagridTotal / ApproveList.approveListForm.pageSize);
                if (ApproveList.datagridTotal < end) {
                    end = ApproveList.datagridTotal;
                }
                if (ApproveList.datagridTotal == 0) {
                    start = 0;
                }
                // 画面显示开始结束位置的值
                ApproveList.start = start;
                ApproveList.end = end;

                // 开始条数为0条或首页时首页上一页按钮不可用，否则可用
                if (start <= 1) {
                    ApproveList.disFlagHome = true;
                } else {
                    ApproveList.disFlagHome = false;
                }
                // 结束条数为总条数时尾页和下一页按钮不可用，否则可用
                if (end == ApproveList.datagridTotal) {
                    ApproveList.disFlagTail = true;
                } else {
                    ApproveList.disFlagTail = false;
                }
            }).catch(function (res) {
            })
        },

        // 确认
        confirm: function (value) {
            reviewScheduleFlag = true;
            cmCookie.setCookie("approveNum1", value, "1");
            cmCookie.setCookie("tjCard1", cmCookie.getCookie("loginTjCode"), "1");
            window.location.href = "/view/approval/requestNewConfirm/requestNewConfirm.html";
        },

        // 修正
        changeTure: function (value) {
            reviewScheduleFlag = true;
            cmCookie.setCookie("approveNum2", value, "2");
            cmCookie.setCookie("tjCard2", cmCookie.getCookie("loginTjCode"), "2");
            window.location.href = "/view/approval/requestNewModify/requestNewModify.html";
        },

        // 审核
        check: function (value) {
            reviewScheduleFlag = true;
            cmCookie.setCookie("approveNum3", value, "3");
            cmCookie.setCookie("tjCard3", cmCookie.getCookie("loginTjCode"), "3");
            window.location.href = "/view/approval/requestApprove/requestApprove.html";
        },
        //文件预览弹框
        previewFileList: function (value) {

            //reviewScheduleFlag = true;
            cmCookie.setCookie("approveNum3", value, "3");
            cmCookie.setCookie("tjCard3", cmCookie.getCookie("loginTjCode"), "3");
            //接收申请编号
            this.requestApproveForm.applicationNumber = cmCookie.getCookie("approveNum3")
            this.requestApproveForm.loginCode = cmCookie.getCookie("tjCard3")
            window.sessionStorage.removeItem("requestApprove");

            // 预览文件
            let requestApproveForm = this.requestApproveForm;
            console.log(requestApproveForm)
            const _this = this;
            axios.post("/api/requestApprove/searchApprove", requestApproveForm).then(function (res) {
                _this.fileIdList = res.data.fileIdList

                _this.fileList=res.data.fileIdList[0]
                _this.previewdialogVisible = true;

            }).catch(function (res) {
            })

        },
        //带签名文件预览弹框
        qmpreviewFileList: function (value) {

            //reviewScheduleFlag = true;
            cmCookie.setCookie("approveNum3", value, "3");
            cmCookie.setCookie("tjCard3", cmCookie.getCookie("loginTjCode"), "3");
            //接收申请编号
            this.requestApproveForm.applicationNumber = cmCookie.getCookie("approveNum3")
            this.requestApproveForm.loginCode = cmCookie.getCookie("tjCard3")
            window.sessionStorage.removeItem("requestApprove");

            // 预览文件
            let requestApproveForm = this.requestApproveForm;
            const _this = this;
            axios.post("/api/requestApprove/searchApprove", requestApproveForm).then(function (res) {
                _this.fileIdList = res.data.fileIdList


                _this.fileList=res.data.fileIdList[0]
                _this.qmpreviewdialogVisible = true;

            }).catch(function (res) {
            })

        },
        // 下载
        download: function (value, approveNum, reviewerSheet) {
            ApproveList.approveListForm.filePath = value;
            axios.post("/api/approvelist/filepath", ApproveList.approveListForm
            ).then(function (res) {
                if (!cmMessage.hasError(ApproveList, res)) {
                    // 获取url
                    var curWwwPath = window.document.location.href;
                    var pathName = window.document.location.pathname;
                    var pos = curWwwPath.indexOf(pathName);
                    var localhostPath = curWwwPath.substring(0, pos);

                    // 遍历文件路径下载文件
                    for (let i = 0; i < res.data.fileSize; i++) {
                        const iframe = document.createElement("iframe");
                        // 防止影响页面
                        iframe.style.display = "none";
                        // 防止影响页面
                        iframe.style.height = 0;
                        iframe.src = localhostPath + "/api/approvelist/filedownLoad/?filePath=" + encodeURI(value)
                            + "&File=" + encodeURI(i) + "&approvalNum=" + encodeURI(approveNum) + "&reviewerSheet=" + encodeURI(reviewerSheet);
                        // 这一行必须，iframe挂在到dom树上才会发请求
                        document.body.appendChild(iframe);
                        setTimeout(() => {
                            iframe.remove();
                        }, 5 * 60 * 1000);
                    }
                }
            }).catch(function (res) {
            })
        },

        //无水印下载
        download1: function (value, approveNum, reviewerSheet) {
            ApproveList.approveListForm.filePath = "C:\\G20005J5\\temp\\" + approveNum;
            console.log(ApproveList.approveListForm.filePath)
            axios.post("/api/approvelist/filepath", ApproveList.approveListForm
            ).then(function (res) {
                if (!cmMessage.hasError(ApproveList, res)) {
                    // 获取url
                    var curWwwPath = window.document.location.href;
                    var pathName = window.document.location.pathname;
                    var pos = curWwwPath.indexOf(pathName);
                    var localhostPath = curWwwPath.substring(0, pos);

                    // 遍历文件路径下载文件
                    for (let i = 0; i < res.data.fileSize; i++) {
                        const iframe = document.createElement("iframe");
                        // 防止影响页面
                        iframe.style.display = "none";
                        // 防止影响页面
                        iframe.style.height = 0;
                        iframe.src = localhostPath + "/api/approvelist/filedownLoad1/?filePath=" + encodeURI("C:\\G20005J5\\temp\\" + approveNum)
                            + "&File=" + encodeURI(i) + "&approvalNum=" + encodeURI(approveNum) + "&reviewerSheet=" + encodeURI(reviewerSheet);
                        // 这一行必须，iframe挂在到dom树上才会发请求
                        document.body.appendChild(iframe);
                        setTimeout(() => {
                            iframe.remove();
                        }, 5 * 60 * 1000);
                    }
                }
            }).catch(function (res) {
            })
        },

        //删除
        myconfirm (filePath,approveNum) {
               const _this=this
            if(confirm('确定要删除吗')==true){
                axios.get("/api/approvelist/delete/"+approveNum
                ).then(function (res) {
                    _this.onSearch(true);

                        })
            }
        },

        // 进度
        dialogVisibles: function (nowOrder, maxOrder) {
            ApproveList.reviewScheduleFlag = true;
            ApproveList.maxOrders = maxOrder;
            ApproveList.active = nowOrder;
            // 审核终了时设定active值
            if (nowOrder == maxOrder + 2) {
                ApproveList.active = nowOrder + 1;
            }
            // 弹dialog
            ApproveList.dialogVisible = true
        },

        closePopup: function () {
            ApproveList.reviewScheduleFlag = false;
        },
        // 上一页
        onUpPage: function () {
            // 当前页不是首页
            if (ApproveList.approveListForm.currentPage != 1) {
                // 计算画面显示页为当前页-1
                ApproveList.approveListForm.currentPage = ApproveList.approveListForm.currentPage - 1;
            }
            this.onSearch(true);
        },

        // 下一页
        onDownPage: function () {
            // 当前页不是尾页
            if (ApproveList.approveListForm.currentPage != ApproveList.pageTotal) {
                // 计算画面显示页为当前页+1
                ApproveList.approveListForm.currentPage = ApproveList.approveListForm.currentPage + 1;
            }
            ApproveList.datagridTotal = ApproveList.datagridTotal;
            this.onSearch(true);
        },

        // 首页
        onHomePage: function () {
            // 画面显示页为首页

            ApproveList.approveListForm.currentPage = 1;
            this.onSearch(true);
        },

        // 尾页
        onTailPage: function () {
            // 画面显示页为尾页
            ApproveList.approveListForm.currentPage = ApproveList.pageTotal;
            ApproveList.datagridTotal = ApproveList.datagridTotal;
            this.onSearch(true);
        },

        // 排序
        onSortChange: function ({column, prop, order}) {
            // 排序的字段
            ApproveList.approveListForm.sortColumn = prop;
            // 排序的升降
            ApproveList.approveListForm.sortType = order;
            // 画面显示页为首页
            ApproveList.approveListForm.currentPage = 1;
            this.onSearch(true);
        },
        //文件预览
        preview: function () {
            const _this=this
            this.previewdialogVisible = false
            let name = this.fileList
            ApproveList.approveListForm.filePath = "C:\\G20005J5\\temp\\" + this.requestApproveForm.applicationNumber;
            axios.post("/api/approvelist/filepath", ApproveList.approveListForm
            ).then(function (res) {
                if (!cmMessage.hasError(ApproveList, res)) {
                    var curWwwPath = window.document.location.href;
                    var pathName = window.document.location.pathname;
                    var pos = curWwwPath.indexOf(pathName);
                    var localhostPath = curWwwPath.substring(0, pos);
                    console.log(_this.requestApproveForm.applicationNumber)
                    window.open(localhostPath + "/api/approvelist/previewPdf?fileName=" + encodeURI(name) + "&approveNum=" + encodeURI(_this.requestApproveForm.applicationNumber))
                }
            }).catch(function (res) {
            })
        },
        //带签名文件预览
        qmpreview: function () {
            const _this=this
            this.qmpreviewdialogVisible = false
            let name = this.fileList
            ApproveList.approveListForm.filePath = "C:\\G20005J5\\uploads\\" + this.requestApproveForm.applicationNumber;
            axios.post("/api/approvelist/filepath", ApproveList.approveListForm
            ).then(function (res) {
                if (!cmMessage.hasError(ApproveList, res)) {
                    var curWwwPath = window.document.location.href;
                    var pathName = window.document.location.pathname;
                    var pos = curWwwPath.indexOf(pathName);
                    var localhostPath = curWwwPath.substring(0, pos);
                    console.log(_this.requestApproveForm.applicationNumber)
                    window.open(localhostPath + "/api/requestNewConfirm/previewPdf?fileName=" + encodeURI(name) + "&approveNum=" + encodeURI(_this.requestApproveForm.applicationNumber))
                }
            }).catch(function (res) {
            })
        }

    }
})
