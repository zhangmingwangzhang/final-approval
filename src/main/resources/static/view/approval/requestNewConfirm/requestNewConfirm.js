// ------------------------------------------------------------------------------------
// 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
// ------------------------------------------------------------------------------------
const requestNewConfirm = new Vue({
  el: "#app",
  data: {
    // 初期化用のForm
    requestNewConfirmForm: {
      // 申请编号
      approveNum:'',
      // 文件编号
      fileId:'',
      // 文件名
      fileName:'',
      // 申请者/卡号
      appUserNameCard:'',
      // 申请人部门
      appUserDepartment:'',
      // 范围
      scopeApplication:'',
      // 业务内容
      condition:'',
      // 状态
      status:'',
      // 更新者
      koshinName:'',
      // 备注
      fileRemark:'',
      // 驳回理由
      reason:'',
      // 文件
      file:''  
    },    
    // 单选按钮
    radio: '',
    // 选中的文件名
    fileName:'',
    // 确认通过按钮二重压下flag
    confirmFlag:false,
    // 确认驳回按钮二重压下flag
    clearFlag:false,
    // Info信息
    infoMessage: "",
    // 删除按钮Flag
    deleteFlag: false,
    send_radio: '',
    loading:false,
    // 确认文件一览
    fileIdList: [],
    // 上传文件集合
    fileList: []
     
  },
  created : function (){
    // 确认画面初期化 
    let formData = new FormData();
    // 从Cookie 取得申请编号
    formData.append("approveNum", cmCookie.getCookie("approveNum1"));
    // 从Cookie 取得申请者卡号
    formData.append("tjCode", cmCookie.getCookie("tjCard1"));
      axios.post("/api/requestNewConfirm/init", formData).then(function(res) {
        // 确认文件一览
        requestNewConfirm.fileIdList = res.data.fileIdList;
        // 申请编号
        requestNewConfirm.requestNewConfirmForm.approveNum = res.data.approveNum;
        // 文件编号
        requestNewConfirm.requestNewConfirmForm.fileId = res.data.fileId;
        // 文件名
        requestNewConfirm.requestNewConfirmForm.fileName = res.data.fileName;
        // 申请人部门
        requestNewConfirm.requestNewConfirmForm.appUserDepartment = res.data.appUserDepartment;
        // 申请者/卡号
        requestNewConfirm.requestNewConfirmForm.appUserNameCard = res.data.appUserNameCard;
        // 范围
        requestNewConfirm.requestNewConfirmForm.scopeApplication = res.data.scopeApplication;
        // 业务内容
        requestNewConfirm.requestNewConfirmForm.condition = res.data.condition;
        // 状态
        requestNewConfirm.requestNewConfirmForm.status = res.data.status;
        //radio默认选中
        requestNewConfirm.fileName = res.data.fileIdList[0];
        requestNewConfirm.radio = requestNewConfirm.fileIdList.indexOf(res.data.fileIdList[0]);     
      })
  },
  
  methods: {
    // 设置按钮
    showRow(row){
      event.preventDefault();
      requestNewConfirm.radio = this.fileIdList.indexOf(row);   
      requestNewConfirm.fileName = row;
    },
    
    // 文件上传
    handlePreview(file) {
      console.log(file);
    },
    handleExceed(files, fileList) {
      this.$message.warning(`当前限制选择 3 个文件，本次选择了 ${files.length} 个文件，共选择了 ${files.length + fileList.length} 个文件`);
    },
    beforeRemove(file, fileList) {
      return this.$confirm(`确定移除 ${ file.name }？`);
    },
    // 上传文件显示
    changeFile (file, fileList) {
      if(file.status == 'ready'){
        let myFile = file.raw;
          // 判断上传文件个数
          if(fileList.length > 1){
            // 去掉重复的上传文件
            for(let i=0; i < fileList.length-1; i++){
              if(myFile.name == fileList[i].name){
                fileList.splice(i, 1);
                  return  fileList;
              }                     
            }
              return this.fileList = fileList;              
          }
          return this.fileList.push(file);
      }    
    },
    // 上传文件删除
    handleRemove(file, fileList) {
      this.$refs.upload.uploadFiles;
      return this.fileList = fileList;
    },
    
    // 确认驳回
    onClear: function () {
      let formData = new FormData();
      // 申请者卡号
      formData.append("tjCode", cmCookie.getCookie("tjCard1"));
      // 更新者
      formData.append("koshinName", cmCookie.getCookie("loginUserName")); 
      // 驳回理由
      formData.append("reason", requestNewConfirm.requestNewConfirmForm.reason);
      // 备注
      formData.append("fileRemark", requestNewConfirm.requestNewConfirmForm.fileRemark);
      // 申请编号
      formData.append("approveNum", requestNewConfirm.requestNewConfirmForm.approveNum);
      // 文件编号
      formData.append("fileId", requestNewConfirm.requestNewConfirmForm.fileId);
      // 文件名称
      formData.append("fileName", requestNewConfirm.requestNewConfirmForm.fileName); 
      // 确认文件一览
      formData.append("fileIdList", this.fileIdList);
        axios.post("/api/requestNewConfirm/Clear", formData
        ).then(function(res) {
          if (!cmMessage.hasError(requestNewConfirm, res)) {
            this.clearFlagFlag = true;
            // Info信息
            requestNewConfirm.infoMessage = res.data.infoMessage
            // 判断有没有Info
            if(requestNewConfirm.infoMessage== null || requestNewConfirm.infoMessage== "" || requestNewConfirm.infoMessage== undefined){                                                                                             
              window.location.href = "/view/approval/approveList/approveList.html";                                             
            }else{
              // 提示框内容
              requestNewConfirm.$alert(requestNewConfirm.infoMessage, '发送邮件问题', {
                confirmButtonText: '确定',
                customClass:'msgbox',
                callback: action => {
                if (action === "confirm") {
                  window.location.href = "/view/approval/approveList/approveList.html";
                }else{
                  window.location.href = "/view/approval/approveList/approveList.html";
                }            
                }  
              });                   
            }                                                           
          }
        }).catch(function(res) {         
        });
    },
    
    // 确认通过
    onOpenInsert: function () {
      let config = {
        'Content-Type': 'multipart/form-data'
      };
     
      // 上传文件      
      let formData = new FormData();
      // 判断是否有上传文件
      if(this.fileList.length > 0){
        this.fileList.forEach(function (file) {
          // 追加上传文件
          formData.append("file", file.raw, file.name);
          formData.append("uploadFlag", true);
        });
      }else{
        // 没有上传文件返回uploadFlag = false
        formData.append("uploadFlag", false);
      }
     
      // 更新者
      formData.append("koshinName", cmCookie.getCookie("loginUserName"));
      // 驳回理由
      formData.append("reason", requestNewConfirm.requestNewConfirmForm.reason);
      // 备注
      formData.append("fileRemark", requestNewConfirm.requestNewConfirmForm.fileRemark);
      // 申请编号
      formData.append("approveNum", requestNewConfirm.requestNewConfirmForm.approveNum);
      // 文件编号
      formData.append("fileId", requestNewConfirm.requestNewConfirmForm.fileId);
      // 文件名称
      formData.append("fileName", requestNewConfirm.requestNewConfirmForm.fileName); 
      // 确认文件一览
      formData.append("fileIdList", this.fileIdList);
      // 删除文件Flag      
      formData.append("deleteFlag", this.deleteFlag);
        axios.post("/api/requestNewConfirm/confirm", formData, config
          ).then(function(res) {             
            if (!cmMessage.hasError(requestNewConfirm, res)) {
              this.confirmFlag = true;
              // Info信息
              requestNewConfirm.infoMessage = res.data.infoMessage
              // 判断有没有Info
              if(requestNewConfirm.infoMessage== null || requestNewConfirm.infoMessage== "" || requestNewConfirm.infoMessage== undefined){                                                  
                window.location.href = "/view/approval/approveList/approveList.html";                             
              }else{
                // 提示框内容
                requestNewConfirm.$alert(requestNewConfirm.infoMessage, '发送邮件问题', {
                  confirmButtonText: '确定',
                  customClass:'msgbox',
                  callback: action => {
                  if (action === "confirm") {
                    window.location.href = "/view/approval/approveList/approveList.html";
                  }else{
                    window.location.href = "/view/approval/approveList/approveList.html";
                  }                              
                  }
                });                                  
              }                                            
            }
         }).catch(function(res) {         
         });
    },
    
    // 编辑下载
    onDownload: function () {
      let formData = new FormData();
      // 申请编号
      formData.append("approveNum", this.requestNewConfirmForm.approveNum)
      formData.append("fileName", requestNewConfirm.fileName)  
      axios.post("/api/requestNewConfirm/downLoadCheck", formData).then(function(res) {
        // 文件下载
        if (!cmMessage.hasError(requestNewConfirm, res)) {      
          window.location.href ="/api/requestNewConfirm/download/?filePath=" + encodeURI(res.data.filePath) + "&fileName=" + encodeURI(requestNewConfirm.fileName)
        }
      });
    },
    
    // 删除
    Delete: function (row) {
      this.fileIdList.splice(requestNewConfirm.radio,1); 
      this.deleteFlag = true;
    },
    
    // 在线预览
    showPdf: function () {
      var curWwwPath = window.document.location.href;
      var pathName = window.document.location.pathname;
      var pos = curWwwPath.indexOf(pathName);
      var localhostPath = curWwwPath.substring(0, pos);
      // 调用pdfjs的viem.html页面来展示获得pdf
      let formData = new FormData();
      // 申请编号
      formData.append("approveNum", this.requestNewConfirmForm.approveNum)
      // 文件名
      formData.append("fileName", requestNewConfirm.fileName)  
      // 获取登陆者卡号
      const tjCard = cmCookie.getCookie("tjCard1")
      axios.post("/api/requestNewConfirm/downLoadCheck", formData).then(function(res) {
        // 在线预览
        if (!cmMessage.hasError(requestNewConfirm, res)) {      
          window.open(localhostPath + "/api/requestNewConfirm/previewPdf/?fileName=" + encodeURI(requestNewConfirm.fileName) + "&approveNum=" + encodeURI(requestNewConfirm.requestNewConfirmForm.approveNum));
        }
      });     
    },
    // 关闭预览
    closePdf(){
      var container = document.getElementById("container");
      container.style.display = "none";
    }
  }
  
}) 