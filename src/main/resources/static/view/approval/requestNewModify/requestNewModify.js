// ------------------------------------------------------------------------------------
// 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
// ------------------------------------------------------------------------------------
const requestNewModify = new Vue({
  el: "#app",
  data: {
    // 画面初始值
    requestNewModifyForm: {
      // 申请编号
      approveNum:'',
      // 文件编号
      fileId:'',
      // 文件名
      fileName:'',
      // 申请者/卡号
      appUserNameCard:'',
      // 申请人部门
      appUserDepartment:'',
      // 范围
      scopeApplication:'',
      // 业务内容
      condition:'',
      // 状态
      status:'',
      // 更新者
      koshinName:'',
      // 驳回理由
      reason:'',
      // 文件
      file:''
    },
    // 申请文件一览
    fileIdList: [],
    // 上传文件集合
    fileList: [],
    // 单选按钮
    radio: '',
    // 删除按钮Flag
    deleteFlag: false,
    // 选中的文件名
    fileName:'',
    // 确认按钮二重压下flag
    confirmFlag:false,
    send_radio: '',
    loading:false,
    // Info信息
    infoMessage: "",
    rules: {
      resource: [
        { required: true, message: '请选择文件', trigger: 'change' }
      ],
    }
    },
    created : function (){
      // 申请修正画面初期化
      let formData = new FormData();
      // 从Cookie 取得申请编号
      formData.append("approveNum", cmCookie.getCookie("approveNum2"));
      // 从Cookie 取得申请者卡号
      formData.append("tjCode", cmCookie.getCookie("tjCard2"));      
        axios.post("/api/requestNewModify/init", formData).then(function(res) {
          // 申请文件一览
          requestNewModify.fileIdList = res.data.fileIdList;
          // 申请编号
          requestNewModify.requestNewModifyForm.approveNum = res.data.approveNum;
          // 文件编号
          requestNewModify.requestNewModifyForm.fileId = res.data.fileId;
          // 文件名
          requestNewModify.requestNewModifyForm.fileName = res.data.fileName;
          // 申请人部门
          requestNewModify.requestNewModifyForm.appUserDepartment = res.data.appUserDepartment;
          // 申请者/卡号
          requestNewModify.requestNewModifyForm.appUserNameCard = res.data.appUserNameCard;
          // 驳回理由
          requestNewModify.requestNewModifyForm.reason = res.data.reason;
          // 范围
          requestNewModify.requestNewModifyForm.scopeApplication = res.data.scopeApplication;
          // 业务内容
          requestNewModify.requestNewModifyForm.condition = res.data.condition;
          // 状态
          requestNewModify.requestNewModifyForm.status = res.data.status;
          //radio默认选中
          requestNewModify.fileName = res.data.fileIdList[0];
          requestNewModify.radio = requestNewModify.fileIdList.indexOf(res.data.fileIdList[0]);    
       })
    },   
    methods: {     
      // 设置按钮
      showRow(row){
        event.preventDefault();
        requestNewModify.radio = this.fileIdList.indexOf(row);  
        requestNewModify.fileName = row;
      },
      // 编辑下载
      editDownload: function () {             
        let formData = new FormData();
        // 取得申请编号
        formData.append("approveNum", this.requestNewModifyForm.approveNum)
        // 文件名
        formData.append("fileName", requestNewModify.fileName)
          axios.post("/api/requestNewModify/downLoadCheck", formData).then(function(res) {
            // 文件下载 
            if (!cmMessage.hasError(requestNewModify, res)) {
              window.location.href = "/api/requestNewModify/download/?filePath=" + encodeURI(res.data.filePath) +"&fileName="+ encodeURI(requestNewModify.fileName)
            }
          });
      },
      // 删除
      Delete: function (row) {     
        requestNewModify.fileIdList.splice(requestNewModify.radio,1); 
        this.deleteFlag = true;
      },
      // 文件上传
      handlePreview(file) {
        console.log(file);
      },
      handleExceed(files, fileList) {
        this.$message.warning(`当前限制选择 3 个文件，本次选择了 ${files.length} 个文件，共选择了 ${files.length + fileList.length} 个文件`);
      },
      beforeRemove(file, fileList) {
        return this.$confirm(`确定移除 ${ file.name }？`);
      },
      // 上传文件显示
      changeFile (file, fileList) {
        // 去掉重复的上传文件
        if(file.status == 'ready'){
          let myFile = file.raw;
            // 判断上传文件个数
            if(fileList.length > 1){
              // 去掉重复的上传文件
              for(let i=0; i < fileList.length-1; i++){
                if(myFile.name == fileList[i].name){
                  fileList.splice(i, 1);
                  return  fileList;
                 }                     
               }
                 return this.fileList = fileList;
             }  
               return this.fileList.push(file);
         }
       },
      // 上传文件删除
      handleRemove(file, fileList) { 
        this.$refs.upload.uploadFiles;
        return this.fileList = fileList;
      },
      // 确认
      confirm: function () {
        let config = {
          'Content-Type': 'multipart/form-data'
        };
        // 上传文件
        let formData = new FormData();
        this.fileList.forEach(function (file) {
          formData.append("file", file.raw, file.name);
        });
        // 更新者
        formData.append("koshinName", cmCookie.getCookie("loginUserName")); 
        // 申请编号
        formData.append("approveNum", requestNewModify.requestNewModifyForm.approveNum);
        // 文件编号
        formData.append("fileId", requestNewModify.requestNewModifyForm.fileId);
        // 文件名称
        formData.append("fileName", requestNewModify.requestNewModifyForm.fileName); 
        // 范围
        formData.append("scopeApplication", requestNewModify.requestNewModifyForm.scopeApplication);
        // 业务内容
        formData.append("condition", requestNewModify.requestNewModifyForm.condition);
        // 申请文件一览        
        formData.append("fileIdList", requestNewModify.fileIdList);
        // 删除文件Flag      
        formData.append("deleteFlag", this.deleteFlag);
          axios.post("/api/requestNewModify/confirm", formData, config
            ).then(function(res) {   
              this.confirmFlag= true;
              if (!cmMessage.hasError(requestNewModify, res)) {
                  // info信息
                  requestNewModify.infoMessage = res.data.infoMessage 
                  // 判断有没有info信息
                  if(requestNewModify.infoMessage== null || requestNewModify.infoMessage== "" || requestNewModify.infoMessage== undefined){                                                  
                    window.location.href = "/view/approval/approveList/approveList.html";                                                   
                  }else{
                    // 提示框内容 
                    requestNewModify.$alert(requestNewModify.infoMessage, '发送邮件问题', {
                      confirmButtonText: '确定',
                      customClass:'msgbox',
                      callback: action => {
                      if (action === "confirm") {
                        window.location.href = "/view/approval/approveList/approveList.html";
                      }else{
                        window.location.href = "/view/approval/approveList/approveList.html";
                      }                       
                      }
                    });                 
                  }                                                
                 }               
            }).catch(function(res) {  
               
          })
      }
      
    }
})