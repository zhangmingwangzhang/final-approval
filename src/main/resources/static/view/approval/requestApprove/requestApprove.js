// ------------------------------------------------------------------------------------
// 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
// ------------------------------------------------------------------------------------
const requestApprove = new Vue({
  el: "#app",
  data: {
    // 画面検索条件用のForm
    requestApproveForm: {
      //申请者code
      tjCode:'',
      //登陆者code
      loginCode:'',
      userName:'',
      //部门id
      deptId:'',
      //部门名称
      deptName:'',
      applicationNumber:'',
      fileId:'',
      fileName:'',
      //文件名称code
      fileNameCode:'',
      //业务范围code
      scopeApplicationCode:'',
      scopeApplication:'',
      //业务内容code
      conditionCode:'',
      condition:'',
      examineConfirReasons:'',
      fileRemark:'',
      examineStatus:'',
      //审核状态code
      examineStatusCode:'',
      loginUserId: cmCookie.getCookie("loginUserId"),
    },
    //文件显示列表
    fileIdList: [],
    radio:'',
    send_radio: '',
    applicationNumber:'',
    //按钮二重压下禁止
    approveOk: false,
    approveNo: false,
    // Info信息
    infoMessage: ""
  },
  //接收申请编号
  created : function (){  
    this.requestApproveForm.applicationNumber = cmCookie.getCookie("approveNum3")
    this.requestApproveForm.loginCode = cmCookie.getCookie("tjCard3")
    window.sessionStorage.removeItem("requestApprove");        
  },
  //初期显示
  mounted:function (){
    this.approveSearch();
  },
  methods: {
    //初期检索显示
    approveSearch: function(){
      let requestApproveForm = this.requestApproveForm;
      axios.post("/api/requestApprove/searchApprove",requestApproveForm).then(function(res) {
        requestApprove.requestApproveForm.userName = res.data.userName
        requestApprove.requestApproveForm.tjCode = res.data.tjCode
        requestApprove.requestApproveForm.fileName = res.data.fileName
        requestApprove.requestApproveForm.fileNameCode =res.data.fileNameCode
        requestApprove.requestApproveForm.deptId = res.data.deptId
        requestApprove.requestApproveForm.deptName = res.data.deptName
        requestApprove.requestApproveForm.scopeApplication = res.data.scopeApplication
        requestApprove.requestApproveForm.scopeApplicationCode = res.data.scopeApplicationCode
        requestApprove.requestApproveForm.condition = res.data.condition
        requestApprove.requestApproveForm.conditionCode = res.data.conditionCode
        requestApprove.requestApproveForm.fileRemark = res.data.fileRemark
        requestApprove.requestApproveForm.examineStatus = res.data.examineStatus
        requestApprove.requestApproveForm.examineStatusCode = res.data.examineStatusCode
        requestApprove.fileIdList = res.data.fileIdList
        //radio默认选中
        requestApprove.requestApproveForm.fileId = requestApprove.fileIdList[0];
        requestApprove.radio = requestApprove.fileIdList.indexOf(res.data.fileIdList[0]);
        }).catch(function(res) {
          })
        },
    // 预览文件
    preview: function () {
        let requestApproveForm = this.requestApproveForm;
        axios.post("/api/requestApprove/fileCheck",requestApproveForm)
        .then(function(res) {
          if (!cmMessage.hasError(requestApprove, res)) {
              let name =requestApprove.requestApproveForm.applicationNumber+"\\"+
              requestApprove.requestApproveForm.fileId;
              window.open("/api/requestApprove/previewPdf?fileName=" + encodeURI(name)
                      + "&applicationNumber=" + encodeURI(requestApprove.requestApproveForm.applicationNumber)
                      + "&loginCode=" + encodeURI(requestApprove.requestApproveForm.loginCode));
          }
       }).catch(function(res) {
       })
      },
    //审核通过
    approved: function () {
      let requestApproveForm = this.requestApproveForm;
      //二重压下
      requestApprove.approveOk = true;
      axios.post("/api/requestApprove/approve",requestApproveForm)
         .then(function(res) {
           if (!cmMessage.hasError(requestApprove, res)) {
               //Info信息
               requestApprove.infoMessage = res.data.infoMessage
               //判断有没有Info
               if(requestApprove.infoMessage== null || requestApprove.infoMessage== "" || requestApprove.infoMessage== undefined){                                                  
                   window.location.href = "/view/approval/approveList/approveList.html";                                                   
               }else{
                   //提示框内容
                   requestApprove.$alert(requestApprove.infoMessage, '发送邮件问题', {
                       confirmButtonText: '确定',
                       customClass:'msgbox',
                       callback: action => {
                           if (action === "confirm") {
                               window.location.href = "/view/approval/approveList/approveList.html";
                             }else{
                               window.location.href = "/view/approval/approveList/approveList.html";
                             }                       
                       }
                     });
               }
              }
           //初始化二重压下
           requestApprove.approveOk = false;
        }).catch(function(res) {
           //初始化二重压下
           requestApprove.approveOk = false;
        })
     },
    //审核驳回
     approvedReject: function () {
       let requestApproveForm = this.requestApproveForm;
       //二重压下
       requestApprove.approveNo = true;
       axios.post("/api/requestApprove/approvedReject",requestApproveForm)
        .then(function(res) {
            if (!cmMessage.hasError(requestApprove, res)) {
               //Info信息
               requestApprove.infoMessage = res.data.infoMessage 
               //判断有没有Info
               if(requestApprove.infoMessage== null || requestApprove.infoMessage== "" || requestApprove.infoMessage== undefined){                                                  
                   window.location.href = "/view/approval/approveList/approveList.html";                                                   
               }else{
                   //提示框内容
                   requestApprove.$alert(requestApprove.infoMessage, '发送邮件问题', {
                       confirmButtonText: '确定',
                       callback: action => {
                           if (action === "confirm") {
                               window.location.href = "/view/approval/approveList/approveList.html";
                             }else{
                               window.location.href = "/view/approval/approveList/approveList.html";
                             }                       
                       }
                     });
               }
              }
            //二重压下
            requestApprove.approveNo = false;
         }).catch(function(res) {
          //初始化二重压下
          requestApprove.approveNo = false;
         })
    },
    //文件选择
    showRow(row){
        event.preventDefault();
        requestApprove.radio = this.fileIdList.indexOf(row);
        requestApprove.requestApproveForm.fileId=row
      },
  }
}) 

