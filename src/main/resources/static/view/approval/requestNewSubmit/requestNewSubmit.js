// ------------------------------------------------------------------------------------
// 审批管理システム（NTTD向け） COPYRIGHT(c) 2020 trans-cosmos.com.cn
// ------------------------------------------------------------------------------------
const requestNewSubmit = new Vue({
  el: "#app",
  data: {
    // 画面初始值
    requestNewSubmitForm: {
      fileId:'',
      fileAbstract:'',
      //文件名字
      fileName:'',
      //登陆着卡号
      loginCode:'',
      //登录者名
      userName:'',
      //名+卡号
      cardNumber:'',
      //部门
      deptId:'',
      //部门名称
      deptName:'',
      number:'',
      rejectReason:'',
      status:'',
      filePath:'',
      file:'',
      //文件名称code
      aFileName:'',
      //一级code
      oneCode:'',
      //二级code
      twoCode:'',
      //三级code
      threeCode:''
    },
    cdCode:[],
    // 文件列表
    fileIdList: [],
    fileList : [],
    //code列表
    defaultData:{
      value: 'cdCode',
      label: 'kjName',
    },
    //二重压下
    confirmOk:false,
    // Info信息
    infoMessage: "",
    //申请成功Info信息
    successInFo:"",
    oneData:[],
    twoDate:[],
    threeDate:[],
    businessList:[],
    //文件路径
    radio: '',
    loading:false,
    rules: {
      resource: [
        { required: true, message: '请选择文件', trigger: 'change' }
        ],
    },
    cascaderData1: [],
  },
  //接收登陆卡号
  created : function (){  
    this.requestNewSubmitForm.loginCode = cmCookie.getCookie("loginTjCode")
    window.sessionStorage.removeItem("requestNewSubmit");        
  },
  // 触发检索文件方法
  mounted:function (){
    this.fileListSearch();
    this.businessSearch();
  },
  methods: {
      // 点击检索按钮
      onSearchReady: function (value) {
          const _this=this;
          requestNewSubmit.requestNewSubmitForm.fileName=this.requestNewSubmitForm.fileName
          axios.post("/api/requestNewSubmit/showby", requestNewSubmit.requestNewSubmitForm
          ).then(function (res) {
             /* console.log(res.data)*/
              requestNewSubmit.fileIdList = res.data;
//文件下载默认选中第一个文件
              console.log(requestNewSubmit.fileIdList[0])
              requestNewSubmit.requestNewSubmitForm.fileId = requestNewSubmit.fileIdList[0].fileId;
              requestNewSubmit.requestNewSubmitForm.aFileName = requestNewSubmit.fileIdList[0].aFileName;
              requestNewSubmit.requestNewSubmitForm.fileAbstract = requestNewSubmit.fileIdList[0].fileAbstract;
              requestNewSubmit.radio=0;
              requestNewSubmit.radio = requestNewSubmit.fileIdList.indexOf(res.data.fileIdList[0]);
          }).catch(function (res) {
          })
          /*
            this.Search();*/
      },



      // 初期显示
    fileListSearch: function () {
      let formData = new FormData();
      formData.append("loginCode",this.requestNewSubmitForm.loginCode);
      axios.post("/api/requestNewSubmit/searchData",formData).then(function(res) {
      requestNewSubmit.requestNewSubmitForm.userName = res.data.userName
      requestNewSubmit.requestNewSubmitForm.cardNumber = requestNewSubmit.requestNewSubmitForm.userName
      +"/"+requestNewSubmit.requestNewSubmitForm.loginCode;
      requestNewSubmit.requestNewSubmitForm.deptId = res.data.deptId;
      requestNewSubmit.requestNewSubmitForm.deptName = res.data.deptName;
      requestNewSubmit.fileIdList = res.data.fileIdList;
      //文件下载默认选中第一个文件
      requestNewSubmit.requestNewSubmitForm.fileId = requestNewSubmit.fileIdList[0].fileId;
      requestNewSubmit.requestNewSubmitForm.aFileName = requestNewSubmit.fileIdList[0].aFileName;
      requestNewSubmit.requestNewSubmitForm.fileAbstract = requestNewSubmit.fileIdList[0].fileAbstract;
      requestNewSubmit.radio = requestNewSubmit.fileIdList.indexOf(res.data.fileIdList[0]);
      }).catch(function(res) {
        requestNewSubmit.fileIdList=[]
        })
   },
    //一级级联列表获取
    businessSearch:function(){
       let requestNewSubmitForm = "";
       axios.post("/api/requestNewSubmit/businessSearch",requestNewSubmitForm).then(function(res) {
       requestNewSubmit.oneData = res.data.businessList
     }).catch(function(res) {
       requestNewSubmit.oneData=[]
       })
    },
    //二级级联列表获取
    businessSearch2:function(oneVal){
       let formData = new FormData();
       requestNewSubmit.requestNewSubmitForm.oneCode=oneVal;
       formData.append("oneCode", requestNewSubmit.requestNewSubmitForm.oneCode);
       axios.post("/api/requestNewSubmit/businessSearch2",formData).then(function(res) {
       requestNewSubmit.twoDate = res.data.businessList
     }).catch(function(res) {
       requestNewSubmit.twoData=[]
       })
     },
    //三级级联列表获取
    businessSearch3:function(twoVal){
       let formData = new FormData();
       requestNewSubmit.requestNewSubmitForm.twoCode=twoVal;
       formData.append("oneCode", requestNewSubmit.requestNewSubmitForm.oneCode);
       formData.append("twoCode", requestNewSubmit.requestNewSubmitForm.twoCode);
       axios.post("/api/requestNewSubmit/businessSearch3",formData).then(function(res) {
       requestNewSubmit.threeDate = res.data.businessList
     }).catch(function(res) {
       requestNewSubmit.threeData=[]
       })
     },
    //联列表显示
    chooseOne(val){
      let oneVal = val;
      this.twoDate=[];
      this.threeDate=[];
      this.businessSearch2(oneVal);
    
    },
    chooseTwo(val){
      let twoVal = val;
      this.threeDate=[];
      this.businessSearch3(twoVal);
     
    },
    chooseThree(val){
      requestNewSubmit.requestNewSubmitForm.threeCode = val;
      this.$forceUpdate();
    },
     //文件下载
     templateDownload: function () {
      let formData = new FormData();
      formData.append("fileId", requestNewSubmit.requestNewSubmitForm.fileId);
      formData.append("aFileName", requestNewSubmit.requestNewSubmitForm.aFileName);
      formData.append("fileAbstract", requestNewSubmit.requestNewSubmitForm.fileAbstract);
      axios.post("/api/templateDownload/tpldownLoad", formData
       ).then(function(res) {
         if (!cmMessage.hasError(requestNewSubmit, res)) {
           //获取查询的文件路径
           requestNewSubmit.requestNewSubmitForm.filePath = res.data.filePath
           //调用后台io流方式下载
           window.location.href="/api/templateDownload/fileIdDown/?filePath=" 
               + encodeURI(requestNewSubmit.requestNewSubmitForm.filePath);
         }
      }).catch(function(res) {
      })
       
     },
     //点击radio按钮获取文件的id
     showRow(row){
      event.preventDefault();
      this.radio = this.fileIdList.indexOf(row);
      // 点那个赋那个
      this.requestNewSubmitForm.fileId = row.fileId;
      this.requestNewSubmitForm.aFileName = row.aFileName;
      this.requestNewSubmitForm.fileAbstract = row.fileAbstract;
    },
   //文件上传     
   handlePreview(file) {
   console.log(file);
   },
   handleExceed(files, fileList) {
   this.$message.warning(`当前限制选择 3 个文件，本次选择了 ${files.length} 个文件，共选择了 ${files.length + fileList.length} 个文件`);
   },
   beforeRemove(file, fileList) {
     return this.$confirm(`确定移除 ${ file.name }？`);
   },
   //上传文件若相同文件则替换
   changeFile (file, fileList) {
     if(file.status == 'ready'){
       console.log('change', file, file.raw)
       let myFile = file.raw;
       if(fileList.length > 1){
         for(let i=0;i<fileList.length-1;i++){
           if(myFile.name == fileList[i].name){
             fileList.splice(i, 1);
             return fileList ;
           }
         }
         return this.fileList = fileList;
       }
       return this.fileList.push(file);
     }
  },
  //上传文件删除
  handleRemove(file, fileList) {
      this.$refs.upload.uploadFiles;
      return this.fileList = fileList;
  },
    
    //确认
    confirm: function () {
      let config = {
          'Content-Type': 'multipart/form-data'
        };
      let formData = new FormData();
      this.fileList.forEach(function (file) {
        formData.append("file", file.raw, file.name);
      });
      formData.append("oneCode", requestNewSubmit.requestNewSubmitForm.oneCode);  
      formData.append("twoCode", requestNewSubmit.requestNewSubmitForm.twoCode);
      formData.append("threeCode", requestNewSubmit.requestNewSubmitForm.threeCode);
      formData.append("loginCode", requestNewSubmit.requestNewSubmitForm.loginCode);
      formData.append("userName", requestNewSubmit.requestNewSubmitForm.userName);
      //二重压下禁止
      requestNewSubmit.confirmOk = true;
      axios.post("/api/requestNewSubmit/confirm", formData, config
      ).then(function(res) {
              if (!cmMessage.hasError(requestNewSubmit, res)) {
                  //Info信息
                  requestNewSubmit.infoMessage = res.data.infoMessage;
                  requestNewSubmit.successInFo = res.data.successInFo;
                  //判断有没有Info
                  if(requestNewSubmit.infoMessage== null || requestNewSubmit.infoMessage== "" || requestNewSubmit.infoMessage== undefined){                                                  
                      //成功Info信息提示
                      cmMessage.showSuccess(requestNewSubmit,requestNewSubmit.successInFo);
                      setTimeout("window.location.reload()",1000); 
                  }else{
                    //提示框内容
                      requestNewSubmit.$alert(requestNewSubmit.infoMessage, '发送邮件问题', {
                          confirmButtonText: '确定',
                          customClass:'msgbox',
                          callback: action => {
                              if (action === "confirm") {
                                 //成功Info信息提示
                                 cmMessage.showSuccess(requestNewSubmit,requestNewSubmit.successInFo);
                                 setTimeout("window.location.reload()",1000); 
                                  
                                }else{
                                  //成功Info信息提示
                                  cmMessage.showSuccess(requestNewSubmit,requestNewSubmit.successInFo);
                                  setTimeout("window.location.reload()",1000); 
                                }                       
                          }
                        });                 
                  }                                                
                 }
              //初始化二重压下
              requestNewSubmit.confirmOk = false;
     }).catch(function(res) {
         //初始化二重压下
         requestNewSubmit.confirmOk = false;
    })
    },
  }
}) 

