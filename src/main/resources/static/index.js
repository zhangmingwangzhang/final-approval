// ------------------------------------------------------------------------------------
// 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
// ------------------------------------------------------------------------------------
const UserLogin = new Vue({
  el: "#app",
  data: {

    // 画面Form
    userLoginForm: {
      loginTjCode: "",
      loginPwd:""
    },
    rules: {
      loginTjCode: [
        { required: true, message: ' '},
      ],
      loginPwd: [
        { required: true, message: ' '},
      ]
    },
    // 登录按钮二重压下flag
    loginDisabled:false,
  },
  methods: {

    // 点击登录按钮
    onLogin : function () {
      axios.post("/api/user/login",UserLogin.userLoginForm
    ).then(function(res) {

        if (!cmMessage.hasError(UserLogin, res)) {
          // 登录按钮二重压下禁止
          UserLogin.loginDisabled = true
          // cookie设置
          cmCookie.setCookie("loginTjCode", res.data.loginTjCode, "1")
          cmCookie.setCookie("loginUserName", res.data.loginUserName, "1")
          cmCookie.setCookie("loginPwd", res.data.loginPwd, "1")
          cmCookie.setCookie("manageFlag", res.data.manageFlag, "1")
          // 画面迁移
          if(res.data.firstLogin == "0"){
            alert("首次登录，请修改密码。")
            window.location.href = "/view/admin/passwordModify/passwordModify.html"
          } else{
            window.location.href = "/common/main.html"
          }
        }
      }).catch(function(res) {
      })
    },
    // ime
    loginTjCodeIme: function () {
      this.userLoginForm.loginTjCode = this.userLoginForm.loginTjCode.replace(/[^0-9a-zA-Z]/g, "");
    },
    loginPwdIme: function () {
      this.userLoginForm.loginPwd = this.userLoginForm.loginPwd.replace(/[^0-9a-zA-Z]/g, "");
    },
    // 点击清除按钮
    onClear : function () {
      this.userLoginForm.loginTjCode = ""
      this.userLoginForm.loginPwd = ""
    }
  }
}) 
