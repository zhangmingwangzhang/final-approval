
const cmCookie = {
    setCookie: function (cname, cvalue, exdays) {
        var d = new Date()
        d.setTime(d.getTime() + (exdays*24*60*60*1000))
        var expires = "expires="+ d.toUTCString()
        window.document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/"
    },
    getCookie: function (cname) {
        var name = cname + "="
        var decodedCookie = decodeURIComponent(document.cookie)
        var ca = decodedCookie.split(';')
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i]
            while (c.charAt(0) == ' ') {
                c = c.substring(1)
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length)
            }
        }
        return "unknown"
    },
}

const cmMessage = {
        htmlMessage: "",
        hasError: function (page, res) {
            if (res.data.errorCode != "0") {
                this.showErrorList(page, res.data.errorMessages)
                return true
            } else {
                return false
            }
        },
        showErrorList: function (page, messages) {
            this.htmlMessage = ""
            for (var i = 0; i < messages.length; i++) {
                this.htmlMessage += "<p>" + messages[i] + "</p>"
            }
            page.$message({
                type: "error",
                dangerouslyUseHTMLString: true,
                message: cmMessage.htmlMessage
            })
        },
        showError: function (page, message) {
            page.$message.error(message)
        },
        showSuccessList: function (page, messages) {
            this.htmlMessage = ""
            for (var i = 0; i < messages.length; i++) {
                this.htmlMessage += "<p>" + messages[i] + "</p>"
            }
            page.$message({
                type: "success",
                dangerouslyUseHTMLString: true,
                message: cmMessage.htmlMessage
            })
        },
        showSuccess: function (page, message) {
            page.$message.success(message)
        },
}

window.onload = function(){
    if (window.history && window.history.pushState) {
        window.addEventListener('popstate', function () {
            window.history.pushState('forward', null, '');
            window.history.forward(1);
        });
    }
    window.history.pushState('forward', null, '');
    window.history.forward(1);
}


document.onkeydown = keydown;
function keydown() {
  var keycode = event.keyCode;
  if(keycode == 116) {
  window.event.keyCode = 0;
  window.event.cancelBubble = true;
  window.location.reload();
  return false
  }
}

//PC端浏览器画面只能缩小不能放大
document.addEventListener('keydown', function (event) {
  if ((event.ctrlKey === true || event.metaKey === true)
    && (event.which === 61 || event.which === 107
      || event.which === 173 || event.which === 187)) {
    event.preventDefault();
  }
}, false);
// Chrome IE 360
window.addEventListener('mousewheel', function (event) {
  if ((event.ctrlKey === true || event.metaKey) && event.wheelDelta > 0) {
    event.preventDefault();
  }
}, { passive: false });

//firefox
window.addEventListener('DOMMouseScroll', function (event) {
  if (event.ctrlKey === true || event.metaKey) {
    event.preventDefault();
  }
}, { passive: false });