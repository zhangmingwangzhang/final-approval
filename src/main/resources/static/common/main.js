// ------------------------------------------------------------------------------------
// 資産管理システム（NTTD向け） COPYRIGHT(c) 2020 trans-cosmos.com.cn
// ------------------------------------------------------------------------------------
var pgMain = new Vue({
  el: "#app",
  data: {
    loginUserId: "",
    loginUserName:"",
    loginPwd:"",
    menuview: "hidden-sm-and-down",
    iconview: ["el-icon-s-unfold", "hidden-md-and-up"],
    activeIndex: "ApproveList",
    editableTabsValue: "ApproveList",
    //lastEditableTabsValue: "",
    tabs:[],
    keywords: '',
    fullscreenLoading: false,
    tabs: ["RequestNewSubmit","RequestUserList", "UserList", "FileManage"],
    loading: true,
    activeIndex2: "",
    manageMenuFlag: "",
    },
  created: function() {
    this.loginUserId = cmCookie.getCookie("loginTjCode")
    this.loginUserName = cmCookie.getCookie("loginUserName")
    this.loginPwd = cmCookie.getCookie("loginPwd")
    this.manageFlag = cmCookie.getCookie("manageFlag")
    if(this.manageFlag == "MANGE_WU"){
      this.manageMenuFlag = "none"
    } else {
      this.manageMenuFlag = ""
    }
    setTimeout(() => {
      this.tabs.forEach((value, key, iterable) => {
        document.getElementById("tab-" + value).style.display = "none";
      })
      document.getElementById("tabs").style.display = "block";
    }, 500)
  },
  mounted() {
    this.handleSelect(this.editableTabsValue);
  },
  methods: {
    // 点击退出系统
    onLogout: function() {
    // Cookie清空
    cmCookie.setCookie("loginUserId", "0", 0)
    cmCookie.setCookie("loginUserName", "0", 0)
    cmCookie.setCookie("loginPwd", "0", 0)
    let data = {};
    axios.post("/api/user/logout", data
        ).then(function(res) {
            console.log("さよなら")
        }).catch(function(res) {
            console.log(res)
    });
    // 登录画面迁移
    window.location.href = "/"
    },
    chanView() {
      if (this.menuview != "") {
        this.iconview[0] = "el-icon-s-fold";
        this.menuview = "";
      } else {
        this.iconview[0] = "el-icon-s-unfold";
        this.menuview = "hidden-sm-and-down";
      }
    },
    handleSelect(key) {
      this.lastEditableTabsValue = key;
      this.editableTabsValue = key; 
      if (key == "ApproveList") {
        this.iframeLoad(key, "/view/approval/approveList/approveList.html")
      } else if (key == "RequestNewSubmit") {
        this.iframeLoad(key, "/view/approval/requestNewSubmit/requestNewSubmit.html")
      } else if (key == "RequestNewAmend") {
        this.iframeLoad(key, "/view/approval/requestNewAmend/requestNewAmend.html")
      } else if (key == "UserList") {
        cmCookie.setCookie("loginUserName", this.loginUserName, "1")
        this.iframeLoad(key, "/view/admin/userList/userList.html")
      } else if (key == "RequestUserList") {
        this.iframeLoad(key, "/view/requestuser/requestuserList.html")
      } else if (key == "FileManage") {
        this.iframeLoad(key, "/view/admin/fileManage/fileManage.html")
      } 
    },
    iframeLoad(key, url) {
      this.loading = true;
      let removeDom = document.getElementById(key + "iframe");
      if (removeDom != null) {
        document.getElementById(key).removeChild(removeDom);
      }
      /** iframe 加载 **/
      var iframe = document.createElement("iframe");
      iframe.setAttribute("id", key + "iframe");
      iframe.setAttribute("name", 'operator');
      iframe.setAttribute("frameborder", '0');
      iframe.setAttribute("height", '100%');
      iframe.setAttribute("width", '100%');
      iframe.setAttribute("scrolling", 'yes');
      iframe.setAttribute("marginheight", '20px');
      iframe.setAttribute("marginwidth", '30px');
      iframe.src = url;
      if (iframe.attachEvent) {
        iframe.attachEvent("onload", function() {
          pgMain.loading = false;
        });
      } else {
        iframe.onload = function() {
          pgMain.loading = false;
        };
      }
      document.getElementById(key).appendChild(iframe);
      let tab = document.getElementById("tab-" + key);
      if (tab != null) {
        document.getElementById("tab-" + key).style.display = "inline-block";
        document.getElementById("pane-" + key).style.display = "block";
      }
    },
    removeTab(targetName) {
      if(targetName == this.lastEditableTabsValue){
        this.editableTabsValue = "ApproveList";
      }
      document.getElementById("tab-" + targetName).style.display = "none";
      document.getElementById("pane-" + targetName).style.display = "none";
    },
    tabClick(targetName){
      this.lastEditableTabsValue = this.editableTabsValue;
    },
    // 点击修改密码
    updatePwd(){
      cmCookie.setCookie("loginTjCode", this.loginUserId, "1")
      cmCookie.setCookie("loginPwd", this.loginPwd, "1")
      cmCookie.setCookie("loginUserName", this.loginUserName, "1")
      window.location.href = "/view/admin/passwordModify/passwordModify.html"
    },
    handleCommand(command){
      command == "updatePwd" ? this.updatePwd() : this.onLogout()
    }
  }
})