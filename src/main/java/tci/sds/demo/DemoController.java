/*
 * 資産管理システム（NTTD向け） COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.demo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import tci.sds.common.entity.FileTypeEnum;
import tci.sds.common.unit.EamilOperatingClass;
import tci.sds.common.unit.ExcelOperatingClass;

/**
 * FileController
 * 
 * @author machenguang
 * @since 2020/09/29
 * @version 0.1
 */
@Controller
public class DemoController {
    private final Logger logger = LoggerFactory.getLogger(DemoController.class);

    /**
     * 上传文件
     * 
     * @param file
     * @param request
     * @throws IOException
     */
    @RequestMapping("/upload1")
    @ResponseBody
    public void Upload1(@RequestParam MultipartFile file, HttpServletRequest request) throws IOException {
        InputStream in = file.getInputStream();
        String packageName = file.getOriginalFilename();
        File tempFile = ExcelOperatingClass.saveTempFile(in, FileTypeEnum.FILE, packageName, request); // 将上传的文件保存到本地

        // 上传文件后,接下来把文件名,路径存到数据库中,tempFile为文件路径
        // .......
        // .......
    }

    /**
     * PDF预览
     * 
     * @param request
     * @param response
     */
    @RequestMapping(value = "/previewPdf", method = RequestMethod.GET)
    public void pdfStreamHandler(HttpServletRequest request, HttpServletResponse response) {

        File file = new File("C:\\Users\\user\\Desktop\\前期准备\\pdfCache\\mcg.pdf");
        if (file.exists()) {
            byte[] data = null;
            try {
                FileInputStream input = new FileInputStream(file);
                data = new byte[input.available()];
                input.read(data);
                response.getOutputStream().write(data);
                input.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            return;
        }
    }

    /**
     * 审批
     * 
     * @param approveId
     * @param request
     */
    @RequestMapping("/approve")
    @ResponseBody
    public void approve(HttpServletRequest request) {
        System.out.println("approve start : " + System.currentTimeMillis());
        int[] a = { 33, 2 };
        ExcelOperatingClass.ExcelInsertImage("C:/uploads/TRANSJ-R-01-41-V1.01 出差管理表.xlsx", "C:/uploads/aaa.png", a, 2,
                200, 40);
        System.out.println("approve end : " + System.currentTimeMillis());
    }

    /**
     * 邮件发送
     * 
     * @param request
     * @throws MessagingException
     * @throws AddressException
     */
    @RequestMapping("/sendEamil")
    @ResponseBody
    public void sendEamil(HttpServletRequest request) throws AddressException, MessagingException {
        EamilOperatingClass eamilOperatingClass = new EamilOperatingClass();
        eamilOperatingClass.sendTest("测试邮件", "fangshuai@g20005.cn", "masad<br/> adasdad<br> asdaada<br>","",null);
    }

    /*
     * public static void main(String[] args) throws Exception{ //1.配置发送邮箱的属性信息
     * Properties properties = new Properties();
     * properties.setProperty("mail.smtp.host","172.20.61.102");//
     * 设置stmp协议主机(案例:使用我们本机/实际:取邮箱POP3/SMTP服务查找)
     * properties.setProperty("mail.smtp.auth","true");//设置stmp是否需要认证
     * //2.使用属性打开一个mail的会话 -->这里的session使用的是javax.mail.Session; Session session =
     * Session.getInstance(properties); //3.设置会话为debug模式 ---> 可以不设置 设置后操作打印会更精细
     * session.setDebug(true); //4.创建邮件的主体信息对象 MimeMessage mimeMessage = new
     * MimeMessage(session); //5.写入邮件内容 mimeMessage.setFrom(new
     * InternetAddress("liwei@g20005.cn"));//设置发件人
     * mimeMessage.setSubject("测试邮件");//设置邮件主题 mimeMessage.setText("masad\n " +
     * " adasdad\n " + " asdaada\n");//设置邮件的返送文本内容
     *//**
        * TO : 发送 正常 一对一 发送 能看到收件人 CC : 抄送 一对多 很多人都能收到 能看到收件人 BCC : 密送 看不到收件人
        * Message.RecipientType.TO --> .BCC ---> .CC
        *//*
           * mimeMessage.setRecipient(Message.RecipientType.TO,new
           * InternetAddress("fangshuai@g20005.cn")); //设置收件人 //6.获取发送器对象 Transport
           * transport = session.getTransport("smtp");//提供使用协议 //7.设置发送人信息(补充发件人信息)
           * transport.connect("172.20.61.102","liwei@g20005.cn","liwei"); //8.发送邮箱
           * 填入发送的内容 收件人对象(此参数为所有的收件人)
           * transport.sendMessage(mimeMessage,mimeMessage.getAllRecipients()); //9.释放资源
           * transport.close(); }
           */
}