/*
 * 資産管理システム（NTTD向け） COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.common.service;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tci.sds.common.Mapper.SendEmailPasswordMapper;

/**
 * 查询密码用Service
 * 
 * @author fangshuai
 * @since 2020/12/16
 * @version 0.1
 */
@Service
public class SendEmailPasswordService {
    private final Logger logger = LoggerFactory.getLogger(SendEmailPasswordService.class);

    @Autowired
    private SendEmailPasswordMapper sendEmailPasswordMapper;
    
    @Transactional(readOnly = true)
    public String selectPassword() {
        logger.info("selectPassword#param:{}","");
        // 查询画面状态下拉框
        String password="";
        password = sendEmailPasswordMapper.searchPassword();
        logger.info("selectStatus#result:{}",password);
        return password;
    }
    /**
     * 查询发送邮箱账户密码修改时间
     *
     * @return kosinTime
     */
    public Date getKosinTime() {
        logger.info("selectPassword#param:{}","");
        Date kosinTime = new Date();
        kosinTime = sendEmailPasswordMapper.getKosinTime();
        logger.info("selectStatus#result:{}",kosinTime);
        // 邮箱发送用户密码更新时间返回
        return kosinTime;
    }
}