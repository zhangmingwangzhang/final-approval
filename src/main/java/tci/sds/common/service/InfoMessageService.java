/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.common.service;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

/**
 * Info信息获取Service
 * 
 * @author sunliqiang
 * @since 2020/11/27
 * @version 0.1
 */

@Service
public class InfoMessageService {
    private final Logger logger = LoggerFactory.getLogger(InfoMessageService.class);
    
    @Autowired
    private MessageSource messageSource;
    
    /**
     * Info信息获取
     * 
     * @param infoCode
     * @return message
     */
    public String getInfo(String infoCode) {

        logger.info("getInfo#param:{}", infoCode);
        
        String[] parStrings = new String[1];
        Locale locale = LocaleContextHolder.getLocale();
        String message = messageSource.getMessage(infoCode, parStrings, locale);
        
        logger.info("getInfo#result:{}", message);
        return message;
    }
}