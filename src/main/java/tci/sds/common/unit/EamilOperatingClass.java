package tci.sds.common.unit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import tci.sds.common.entity.ConfigProperties;

/**
 * 邮件工具类
 * 
 * @author machenguang
 * @since 2020/10/08
 *
 */
@Component
public class EamilOperatingClass {
    
    private static ConfigProperties configProperties;

    private static JavaMailSender javaMailSender;

    @Autowired
    public void setMail(JavaMailSender javaMail) {

        EamilOperatingClass.javaMailSender = javaMail;

    }

    @Autowired
    public void setConfig(ConfigProperties config) {

        EamilOperatingClass.configProperties = config;

    }
    
    // 时间格式实例化
    SimpleDateFormat formater = new SimpleDateFormat("yyyyMMdd");
    
    /**
     * 向指定用户发送邮件
     * 
     * @param Subject 主题
     * @param toEamil 发送给
     * @param text    文本内容
     */
  
    
public boolean sendTest(String subject,String toEamil, String text, String password,Date emailPwdUpateTime) throws AddressException, MessagingException {
	

    boolean flag = false;
    
    if (toEamil == null || toEamil.equals("")) {
       return flag = false;
    }
    if (password == null || password.equals("")) {
        return flag = false;
     } 
    //密码修改时间是否取得判断
    if (emailPwdUpateTime == null || "".equals(emailPwdUpateTime)) {
        return flag = false;
     } 
    //网络是否正常判定
     if (!isConnect()) {
       return flag = false;
     }

     // 是否发送邮箱密码修改邮件，一天判断一次，同一天多次发送邮件时，只执行一次
     boolean emailPwdTimeOut = false;
     if(configProperties.getRemindDay() == null || 
             !formater.format(Calendar.getInstance().getTime()).equals(configProperties.getRemindDay())) {
         
         // 修改密码邮件提醒,(邮箱密码，邮箱密码更改时间)
         emailPwdTimeOut = checkEmailPwd(password,emailPwdUpateTime); 
         // 是同一天的放多次判断，返回true
     }
     
     //邮箱过期邮件不发送
     if(emailPwdTimeOut) {
    	 return flag = false;
     } 

    //1.配置发送邮箱的属性信息
    Properties properties = new Properties();
    properties.setProperty("mail.smtp.host",configProperties.getSendUrltest());// 设置stmp协议主机(案例:使用我们本机/实际:取邮箱POP3/SMTP服务查找)
    properties.setProperty("mail.smtp.auth","true");//设置stmp是否需要认证
    //2.使用属性打开一个mail的会话 -->这里的session使用的是javax.mail.Session;
    Session session = Session.getInstance(properties);
    //3.设置会话为debug模式  ---> 可以不设置 设置后操作打印会更精细
    session.setDebug(true);
    //4.创建邮件的主体信息对象
    MimeMessage mimeMessage = new MimeMessage(session);
    //5.写入邮件内容
    mimeMessage.setFrom(new InternetAddress(configProperties.getSendTest()));//设置发件人
    mimeMessage.setSubject(subject);//设置邮件主题
    mimeMessage.setText(text);//设置邮件的返送文本内容
    /** TO : 发送   正常 一对一 发送  能看到收件人
     *  CC : 抄送   一对多  很多人都能收到  能看到收件人
     *  BCC : 密送    看不到收件人
     *  Message.RecipientType.TO --> .BCC  ---> .CC
     */
    mimeMessage.setRecipient(Message.RecipientType.TO,new InternetAddress(toEamil)); //设置收件人
//    if (!StringUtils.isEmpty(cc) && cc.length != 0) {
//        mimeMessage.addRecipient(Message.RecipientType.CC,new InternetAddress(cc)); // cc
//    }
    //6.获取发送器对象
    Transport transport = session.getTransport("smtp");//提供使用协议
    //7.设置发送人信息(补充发件人信息)
    transport.connect(configProperties.getSendUrltest(),configProperties.getSendTest(),password);
    //8.发送邮箱    填入发送的内容  收件人对象(此参数为所有的收件人)
    transport.sendMessage(mimeMessage,mimeMessage.getAllRecipients());
    //9.释放资源
    transport.close();
    
    flag = true;
    return flag;
}
/**
 * 本地网络Check
 * 
 */
public boolean isConnect(){

    boolean connect = false;

    Runtime runtime = Runtime.getRuntime();

    Process process;

    try {
        process = runtime.exec("ping " + "www.baidu.com");
        InputStream is = process.getInputStream(); 
        InputStreamReader isr = new InputStreamReader(is); 
        BufferedReader br = new BufferedReader(isr); 
        String line = null; 
        StringBuffer sb = new StringBuffer(); 
        while ((line = br.readLine()) != null) { 
            sb.append(line); 
        } 
        is.close(); 
        isr.close(); 
        br.close(); 
        if (null != sb && !sb.toString().equals("")) { 
            String logString = ""; 
            if (sb.toString().indexOf("TTL") > 0) { 
                // 网络畅通  
                connect = true;
            } else { 
                // 网络不畅通  
                connect = false;
            } 
        } 
    } catch (IOException e) {

        e.printStackTrace();
    } 
    return connect;

}
/**
 * 邮箱发送账户密码过期检查
 *
 * @param koshin_timestamp
 * @return
 * @throws MessagingException 
 * @throws AddressException 
 */
@SuppressWarnings("unused")
private Boolean checkEmailPwd(String password,Date koshin_timestamp)  throws AddressException {
    
//     Date koshin_timestamp  = this.sendEmailPasswordService.getKosinTime();

      //获取当前系统时间
      Calendar psdUpdateDate = Calendar.getInstance(), 
              psdUpdateNextDate = Calendar.getInstance(); 
      //密码更新时间设定
      psdUpdateNextDate.setTime(koshin_timestamp);
      //当前时间后90天的日期
      psdUpdateNextDate.add(psdUpdateNextDate.DATE, 90);
      // 设定下次修改密码时间
      psdUpdateNextDate.setTime(psdUpdateNextDate.getTime()); 
      // 距离下次修改密码的时间天数
      long diffDays =(psdUpdateNextDate.getTimeInMillis() - psdUpdateDate.getTimeInMillis()) / (1000 * 60 * 60 * 24);

      // 距离下次修改邮箱密码大于1天小于等于3天的时间，则提示邮箱管理员修改密码，而且只提醒一次，如果当日已经提醒则不再提醒
      if(0<=diffDays && diffDays <= 3 ) {
              try {
              // 1.配置发送邮箱的属性信息
              Properties properties = new Properties();
              properties.setProperty("mail.smtp.host", configProperties.getSendUrltest());// 设置stmp协议主机(案例:使用我们本机/实际:取邮箱POP3/SMTP服务查找)
              properties.setProperty("mail.smtp.auth", "true");// 设置stmp是否需要认证
              // 2.使用属性打开一个mail的会话 -->这里的session使用的是javax.mail.Session;
              Session session = Session.getInstance(properties);
              // 3.设置会话为debug模式 ---> 可以不设置 设置后操作打印会更精细
              //session.setDebug(true);
              // 4.创建邮件的主体信息对象
              MimeMessage mimeMessage = new MimeMessage(session);
              // 5.写入邮件内容
              mimeMessage.setFrom(new InternetAddress(configProperties.getSendTest()));// 设置发件人
              mimeMessage.setSubject("邮箱密码到期提醒");// 设置邮件主题
              String mailInfo = "管理员桑\r\n" + "工作辛苦了，济南审批管理系统。\r\n\r\n"+"      "+"距离下次修改邮箱密码还有【"+diffDays+"】天\r\n\r\n\r\n"+"为了不影响您的使用请及时修改密码\r\n\r\n\r\n"+"修改后的登陆密码与企业邮箱密码保持一直\r\n\r\n\r\n"+"以上，请多关照。\r\n"+"济南审批管理系统\r\n\r\n"+"该邮件为自动发送邮件，不需要回复。";
              mimeMessage.setText(mailInfo);
               /**
               * TO : 发送 正常 一对一 发送 能看到收件人 CC : 抄送 一对多 很多人都能收到 能看到收件人 BCC : 密送 看不到收件人
               * Message.RecipientType.TO --> .BCC ---> .CC
               */
              // 设置邮箱发送用户管理员邮箱设置
              mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(configProperties.getSendUseMangerEmail())); 
              // 6.获取发送器对象
              Transport transport = session.getTransport("smtp");// 提供使用协议
              // 7.设置发送人信息(补充发件人信息)
              
                transport.connect(configProperties.getSendUrltest(), configProperties.getSendTest(), password);
                // 8.发送邮箱 填入发送的内容 收件人对象(此参数为所有的收件人)
                transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());   
                // 9.释放资源
                transport.close();
                
                // 如果已经提醒过管理员修改密码，将是否需要提醒设置为false
                configProperties.setNotSendEmail(false);
                // 设定当前时间距离下次修改邮件的天数

                configProperties.setRemindDay(formater.format(Calendar.getInstance().getTime()));
                
          } catch (MessagingException e) {
              
              return true;
        }

    }else if(diffDays < 0) {
        //throw new AddressException("邮箱密码已到期，请联系管理员修改密码后再使用");
        //邮箱密码过期
        return true;

    }
    return false;
}
  
}
