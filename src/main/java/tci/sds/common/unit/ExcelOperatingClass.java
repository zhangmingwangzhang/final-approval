package tci.sds.common.unit;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import com.spire.presentation.Presentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.spire.doc.Document;
import com.spire.xls.ExcelPicture;
import com.spire.xls.FileFormat;
import com.spire.xls.Workbook;
import com.spire.xls.Worksheet;

import tci.sds.common.entity.ConfigProperties;
import tci.sds.common.entity.FileTypeEnum;
import tci.sds.common.interceptor.interceptorCofig;

/**
 * 文件操作类 Excel操作Class
 * 1. 上传文件
 * 2. Excel 转 PDF
 * 3. 向excel中插入图片（电子签名）
 *
 * @author machenguang
 * @version 1.0 2020/09/25
 *
 *
 */
@Component
public class ExcelOperatingClass {

    private static ConfigProperties configProperties;

    private static final String PDF = ".pdf";

    private static final int BYTESIZE = 64;

    @Autowired
    public void setConfig(ConfigProperties config) {

        ExcelOperatingClass.configProperties = config;

    }

    /**
     * 上传文件 1.上传模板 2.上传填写的文件
     *
     * @param is           输入文件
     * @param fileTypeEnum 上传类别 （文件、模板）
     * @param fileName     文件名
     * @param request      请求信息
     * @return 上传后的文件信息（新文件名、地址等）
     */
    public static File saveTempFile(InputStream is, FileTypeEnum fileTypeEnum,
            String fileName, HttpServletRequest request, String stockNo) {

        File temp = null;
        String path = "";
        switch (fileTypeEnum) {
        case FILE:
            path = configProperties.getExcelFileUrl();
            if(!StringUtils.isEmpty(stockNo)) {
                path += stockNo;
            }
            break;
        case TEMPLATE:
            path = configProperties.getExcelTemplateUrl();
            break;
        case SAMPLE:
            path = configProperties.getSampleUrl();
            break;
        default:
            path = configProperties.getSignatureUrl();
            break;
        }
        if (path != null && is != null) {
            String newfileName = fileName;
//            if (fileTypeEnum.equals(FileTypeEnum.FILE)) {
//                newfileName = UUID.randomUUID().toString() + "_" + fileName;
//            }
                File file = new File(path);
                //用来测试此路径名表示的文件或目录是否存在
                if (!file.exists()) {
                    //不存在
                    file.mkdirs();
                 }
                temp = new File(String.format("%s\\%s", path, newfileName));
                temp.getParentFile().mkdir();// 不存在则创建目录
                BufferedInputStream bis = null;
                BufferedOutputStream bos = null;
                try {
                    bis = new BufferedInputStream(is);
                    bos = new BufferedOutputStream(new FileOutputStream(temp));
                    // 把文件流转为文件，保存在临时目录
                    int len = 0;
                    byte[] buf = new byte[10 * BYTESIZE];
                    // 缓冲区
                    while ((len = bis.read(buf)) != -1) {
                        bos.write(buf, 0, len);
                    }
                    bos.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                      if (bos != null)
                          //  bos.close();
                        if (bis != null)
                            bis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

        }
        return temp;

    }
    public static File saveTempFile2(InputStream is, FileTypeEnum fileTypeEnum,
                                    String fileName, HttpServletRequest request, String stockNo) {

        File temp = null;
        String path = "";
        switch (fileTypeEnum) {
            case FILE:
                path = "C:\\G20005J5\\temp\\";
                if(!StringUtils.isEmpty(stockNo)) {
                    path += stockNo;
                }
                break;
            case TEMPLATE:
                path = "C:\\G20005J5\\temp\\";
                break;
            case SAMPLE:
                path = "C:\\G20005J5\\temp\\";
                break;
            default:
                path ="C:\\G20005J5\\temp\\";
                break;
        }
        if (path != null && is != null) {
            String newfileName = fileName;
//            if (fileTypeEnum.equals(FileTypeEnum.FILE)) {
//                newfileName = UUID.randomUUID().toString() + "_" + fileName;
//            }
            File file = new File(path);
            //用来测试此路径名表示的文件或目录是否存在
            if (!file.exists()) {
                //不存在
                file.mkdirs();
            }
            temp = new File(String.format("%s\\%s", path, newfileName));
            temp.getParentFile().mkdir();// 不存在则创建目录
            BufferedInputStream bis = null;
            BufferedOutputStream bos = null;
            try {
                bis = new BufferedInputStream(is);
                bos = new BufferedOutputStream(new FileOutputStream(temp));
                // 把文件流转为文件，保存在临时目录
                int len = 0;
                byte[] buf = new byte[10 * BYTESIZE];
                // 缓冲区
                while ((len = bis.read(buf)) != -1) {
                    bos.write(buf, 0, len);
                }
                bos.flush();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (bos != null)
                        bos.close();
                    if (bis != null)
                        bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        return temp;

    }
    public static File saveTempFile(InputStream is, FileTypeEnum fileTypeEnum,
            String fileName, HttpServletRequest request) {
        return saveTempFile(is, fileTypeEnum, fileName, request, null);
    }

    /**
     * Excel文件损坏check
     *
     * @param excelFileName Excel文件名
     * @param sheetNum      sheet页码
     */
    public static String ExcelToPdfCheck(String excelFileName) {

        // 创建Workbook实例
        Workbook wb = new Workbook();
        // 加载Excel文档
        try {
            wb.loadFromFile(excelFileName);

        } catch (Exception e) {

            return "fileError";
        }
        // 获取工作表

        return null;
    }

    /**
     * Word文件损坏check
     *
     * @param excelFileName Word文件名
     */
    public static String WordToPdfCheck(String docFileName) {

        // 创建Document实例
        Document document = new Document();
        // 加载Doc文档
        try {
            document.loadFromFile(docFileName);

        } catch (Exception e) {

            return "fileError";
        }

        return null;
    }

    /**
     * Excel 转 PDF (默认，第一页)
     *
     * @param excelFileName 需转换Excel文件名
     * @param sheetNum      sheet页码
     */
    public static String ExcelToPdf(String excelFileName , String approveNum) {

        // 创建Workbook实例
        Workbook wb = new Workbook();
        String pdfFileName = UUID.randomUUID().toString();
        // 加载Excel文档
        wb.loadFromFile(configProperties.getExcelFileUrl() + excelFileName);
        // 获取工作表
        pdfFileName = configProperties.getPDFFileUrl() + approveNum +"//" + pdfFileName + PDF;
        wb.saveToFile(pdfFileName);

        return pdfFileName;

    }

    /**
     * Excel 转 PDF。<br/>
     * sheet : 可选
     *
     * @param excelFileName 需转换Excel文件名
     * @param sheetNum      sheet页码
     */
    public static String  ExcelToPdf(String excelFileName, int sheetNum , String approveNum) {

        // 创建Workbook实例
        Workbook wb = new Workbook();
        // 加载Excel文档
        wb.loadFromFile(configProperties.getExcelFileUrl() + excelFileName);
        // 获取工作表
        Worksheet sheet = wb.getWorksheets().get(sheetNum);
        // 保存文档
        String pdfFileName = UUID.randomUUID().toString();
        sheet.saveToPdf(configProperties.getPDFFileUrl() + approveNum + "//" + pdfFileName + PDF);
        pdfFileName = configProperties.getPDFFileUrl() + approveNum + "//" + pdfFileName + PDF;
        return pdfFileName;

    }
    /**
     * Excel 转 PDF。<br/>
     * sheet : 可选
     *
     * @param excelFileName 需转换Excel文件名
     * @param sheetNum      sheet页码
     */
    public static String  ExcelTo(String excelFileName, int sheetNum , String approveNum) {

        // 创建Workbook实例
        Workbook wb = new Workbook();
        String path="C:\\G20005J5\\temp\\" + excelFileName;
        File file=new File(path);
        if(!file.exists()){
            path="C:\\G20005J5\\uploads\\"+excelFileName;
        }
        // 加载Excel文档
        wb.loadFromFile(path);
        // 获取工作表
        Worksheet sheet = wb.getWorksheets().get(sheetNum);
        // 保存文档
        String pdfFileName = UUID.randomUUID().toString();
        sheet.saveToPdf(configProperties.getPDFFileUrl() + approveNum + "//" + pdfFileName + PDF);
        pdfFileName = configProperties.getPDFFileUrl() + approveNum + "//" + pdfFileName + PDF;
        return pdfFileName;

    }

    /**
     * Word 转 PDF。<br/>
     *
     * @param docFileName 需转换Excel文件名
     * @param sheetNum      sheet页码
     * @throws IOException
     */
    public static String DocToPdf(String docFileName, String approveNum) throws IOException {

        // 创建Document实例
        Document document = new Document();
        // 加载Doc文档
        document.loadFromFile(configProperties.getExcelFileUrl() + docFileName);
        // 保存文档
        String pdfFileName = UUID.randomUUID().toString();
        File f = new File(configProperties.getPDFFileUrl() + approveNum);
        if(!f.exists()) {
          f.getParentFile().mkdirs();
          f.mkdir();
        }
        document.saveToFile(configProperties.getPDFFileUrl() + approveNum + "//" + pdfFileName + PDF, com.spire.doc.FileFormat.PDF);
        pdfFileName = configProperties.getPDFFileUrl() + approveNum + "//" + pdfFileName + PDF;
        return pdfFileName;

    }
    /**
     * PPt 转 PDF。<br/>
     */
    public static String pptToPdf(String pptfileurl, String approveNum,String fileName) throws IOException {
        Presentation ppt=new Presentation();
        String pdfFileName = UUID.randomUUID().toString();
        try {
            ppt.loadFromFile(pptfileurl+approveNum+"\\"+fileName);
            ppt.saveToFile(configProperties.getPDFFileUrl() + approveNum + "//" + pdfFileName + PDF, com.spire.presentation.FileFormat.PDF);
            ppt.dispose();
        } catch (Exception e) {
            e.printStackTrace();
        }
        pdfFileName = configProperties.getPDFFileUrl() + approveNum + "//" + pdfFileName + PDF;
        return pdfFileName;
    }
    /**
     * Word 转 PDF。<br/>
     *
     * @param docFileName 需转换Excel文件名
     * @param sheetNum      sheet页码
     * @throws IOException
     */
    public static String DocTo(String docFileName, String approveNum) throws IOException {

        // 创建Document实例
        Document document = new Document();
        // 加载Doc文档
        document.loadFromFile("C:\\G20005J5\\temp\\"+ docFileName);
        // 保存文档
        String pdfFileName = UUID.randomUUID().toString();
        File f = new File("C:\\G20005J5\\temp\\" + approveNum);
        if(!f.exists()) {
            f.getParentFile().mkdirs();
            f.mkdir();
        }
        document.saveToFile(configProperties.getPDFFileUrl() + approveNum + "//" + pdfFileName + PDF, com.spire.doc.FileFormat.PDF);
        pdfFileName = configProperties.getPDFFileUrl() + approveNum + "//" + pdfFileName + PDF;
        return pdfFileName;

    }

    /**
     * 向excel中插入图片。<br/>
     * sheet： 默认第一页。<br/>
     * 图片： 默认宽度，默认高度
     *
     * @param excelFileName Excel文件名
     * @param ImageFileName 插入图片名
     * @param coordinate    插入坐标 {eg:[x,y] -- 横纵坐标}
     */
    public static void ExcelInsertImage(String excelFileName,
            String ImageFileName, int[] coordinate) {

        ExcelInsertImage(excelFileName, ImageFileName, coordinate, 0);

    }

    /**
     * 向excel中插入图片.<br/>
     * sheet： 可选。<br/>
     * 图片： 默认宽度，默认高度。
     *
     * @param excelFileName Excel文件名
     * @param ImageFileName 插入图片名
     * @param sheetNum      sheet页码
     * @param coordinate    插入坐标 {eg:[x,y] -- 横纵坐标}
     */
    public static void ExcelInsertImage(String excelFileName,
            String ImageFileName, int[] coordinate, int sheetNum) {

        ExcelInsertImage(excelFileName, ImageFileName, coordinate, sheetNum,
                null, null);

    }

    /**
     * 向excel中插入图片，<br/>
     * sheet： 可选。<br/>
     * 图片： 自定义宽度，自定义高度。
     *
     * @param excelFileName Excel文件名
     * @param ImageFileName 插入图片名
     * @param sheetNum      sheet页码
     * @param coordinate    插入坐标 {eg:[x,y] -- 横纵坐标}
     */
    public static void ExcelInsertImage(String excelFileName,
            String ImageFileName, int[] coordinate, int sheetNum, Integer width,
            Integer height) {

        // 创建Workbook实例
        Workbook wb = new Workbook();
        // 加载Excel文档
        wb.loadFromFile(excelFileName);
        // 获取第一张工作表
        Worksheet sheet = wb.getWorksheets().get(sheetNum);
        // 添加图片到工作表的指定位置
        ExcelPicture pic = sheet.getPictures().add(coordinate[0], coordinate[1],
                ImageFileName);
        if (!StringUtils.isEmpty(width) && !StringUtils.isEmpty(height)) {
            // 设置图片的宽度和高度
            pic.setWidth(width);
            pic.setHeight(height);
        }
        // 保存文档
        wb.saveToFile(excelFileName, FileFormat.Version2007);
    }

    /**
     * 从excel中删除hu图片，<br/>
     * sheet： 可选。<br/>
     * @param excelFileName Excel文件名
     *
     */
    public static void ExcelDeleteImage(String excelFileName,int sheetNum) {
        // 创建Workbook实例
        Workbook wb = new Workbook();
        // 加载Excel文档
        wb.loadFromFile(excelFileName);
        // 获取第一张工作表
        Worksheet sheet = wb.getWorksheets().get(sheetNum);
        //获取指定图片，删除
        for(int i=0;i<sheet.getPictures().size();i++) {
            sheet.getPictures().get(i).remove();
        }
        // 保存文档
        wb.saveToFile(excelFileName, FileFormat.Version2007);
    }
    /*删除图片*/
    public static void DeleteImage(String excelFileName,int sheetNum) {
        // 创建Workbook实例
        Workbook wb = new Workbook();
        // 加载Excel文档
        wb.loadFromFile(configProperties.getExcelFileUrl()+excelFileName);
        // 获取第一张工作表
        Worksheet sheet = wb.getWorksheets().get(sheetNum);
        //获取指定图片，删除
        for(int i=0;i<sheet.getPictures().size();i++) {
            sheet.getPictures().get(i).remove();
        }
        // 保存文档
        wb.saveToFile(excelFileName, FileFormat.Version2007);
    }
}
