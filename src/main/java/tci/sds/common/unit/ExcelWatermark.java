package tci.sds.common.unit;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import org.springframework.stereotype.Component;

import static java.awt.image.BufferedImage.TYPE_INT_ARGB;


/**
 * 水印设置
 * 水印格式设置
 * 
 * @author fangshuai
 * @version 1.0 2020/12/23
 * 
 *
 */
@Component
public class ExcelWatermark {


    public static BufferedImage drawText (String text, Font font, Color textColor, Color backColor,double height, double width)
    {
        //定义图片宽度和高度
        BufferedImage img = new BufferedImage((int) width, (int) height, TYPE_INT_ARGB);
        Graphics2D loGraphic = img.createGraphics();

        //获取文本size
        FontMetrics loFontMetrics = loGraphic.getFontMetrics(font);
        int liStrWidth = loFontMetrics.stringWidth(text);
        int liStrHeight = loFontMetrics.getHeight();

        //文本显示样式及位置
        loGraphic.setColor(backColor);
        loGraphic.fillRect(0, 0, (int) width, (int) height);
        loGraphic.translate(((int) width - liStrWidth) / 2 - liStrWidth / 4, ((int) height - liStrHeight) / 2);
        loGraphic.rotate(Math.toRadians(0));

        //模板为纵版
//        if(height > width) {
//            loGraphic.translate(-100, -((int) height - liStrHeight) + 25);
//        }else {
//            //模板为横板
//            loGraphic.translate(0, -((int) width - liStrWidth) - 20);
//        }
        loGraphic.translate(0, -((int) height - liStrHeight) + 25);
        loGraphic.setFont(font);
        loGraphic.setColor(textColor);
        loGraphic.drawString(text, ((int) width - liStrWidth) / 2, ((int) height - liStrHeight) / 2);
        loGraphic.dispose();
        return img;
    }
}
