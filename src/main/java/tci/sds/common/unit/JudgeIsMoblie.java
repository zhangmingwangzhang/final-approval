// ************************************************************************
// * DCHA web
// * JudgeIsMoblie.java
// * 作成日 ： 2020/12/25
// * 更新日 ： 2020/12/25
// * Copyright Benesse InfoShell Co.,Ltd. 2020
// ************************************************************************
package tci.sds.common.unit;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

/**
 * 判断手机和pc端登陆
 *
 * @version 1.0.0
 * @author 孙
 */
@Component
public class JudgeIsMoblie{
  //判断是否为手机浏览器
    public static boolean JudgeIsMoblie(String userAgent) {
        boolean isMoblie = false;
        String[] mobileAgents = { "iphone", "android","ipad", "phone", "mobile", "wap", "netfront", "java", "opera mobi",
                "opera mini", "ucweb", "windows ce", "symbian", "series", "webos", "sony", "blackberry", "dopod",
                "nokia", "samsung", "palmsource", "xda", "pieplus", "meizu", "midp", "cldc", "motorola", "foma",
                "docomo", "up.browser", "up.link", "blazer", "helio", "hosin", "huawei", "novarra", "coolpad", "webos",
                "techfaith", "palmsource", "alcatel", "amoi", "ktouch", "nexian", "ericsson", "philips", "sagem",
                "wellcom", "bunjalloo", "maui", "smartphone", "iemobile", "spice", "bird", "zte-", "longcos",
                "pantech", "gionee", "portalmmm", "jig browser", "hiptop", "benq", "haier", "^lct", "320x320",
                "240x320", "176x220", "w3c ", "acs-", "alav", "alca", "amoi", "audi", "avan", "benq", "bird", "blac",
                "blaz", "brew", "cell", "cldc", "cmd-", "dang", "doco", "eric", "hipt", "inno", "ipaq", "java", "jigs",
                "kddi", "keji", "leno", "lg-c", "lg-d", "lg-g", "lge-", "maui", "maxo", "midp", "mits", "mmef", "mobi",
                "mot-", "moto", "mwbp", "nec-", "newt", "noki", "oper", "palm", "pana", "pant", "phil", "play", "port",
                "prox", "qwap", "sage", "sams", "sany", "sch-", "sec-", "send", "seri", "sgh-", "shar", "sie-", "siem",
                "smal", "smar", "sony", "sph-", "symb", "t-mo", "teli", "tim-", "tosh", "tsm-", "upg1", "upsi", "vk-v",
                "voda", "wap-", "wapa", "wapi", "wapp", "wapr", "webc", "winw", "winw", "xda", "xda-",
                "Googlebot-Mobile" };
        if (userAgent != null) {
            String agent = userAgent;
            for (String mobileAgent : mobileAgents) {
                if (agent.toLowerCase().indexOf(mobileAgent) >= 0&&agent.toLowerCase().indexOf("windows nt")<=0 &&agent.toLowerCase().indexOf("macintosh")<=0) {
                    isMoblie = true;
                    break;
                }
            }
        }
        return isMoblie;
    }

    /**
     * 下载指定路径下的文件,并自动打开
     * @param response
     * @return 文件
     * @throws IOException
     * @filePath 文件路径
     */
    public static void  downloadFile (HttpServletRequest request,HttpServletResponse response,String filePath)
          {
           try {
               File file=new File(filePath);
                  if(file.exists()){
                      String fileName  = file.getName().toString();
                      // firefox浏览器
                      if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {
                          fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
                      } // IE浏览器
                      else if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0) {
                          fileName = URLEncoder.encode(fileName, "UTF-8");
                      }// 谷歌
                      else if (request.getHeader("User-Agent").toUpperCase().indexOf("CHROME") > 0) {
                          fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
                      }
                      //首先设置响应的内容格式是force-download，那么你一旦点击下载按钮就会自动下载文件了
                      response.setContentType("application/force-download");
                      //通过设置头信息给文件命名，也即是，在前端，文件流被接受完还原成原文件的时候会以你传递的文件名来命名
                      response.addHeader("Content-Disposition","online;filename="+ fileName );
                      response.setCharacterEncoding("UTF-8");
                      //进行读写操作
                      byte[]buffer=new byte[1024];
                      FileInputStream fis=null;
                      BufferedInputStream bis=null;
                      try{
                          fis=new FileInputStream(file);
                          bis=new BufferedInputStream(fis);
                          OutputStream os=response.getOutputStream();
                          //从源文件中读
                          int i=bis.read(buffer);
                          while(i!=-1){
                              //写到response的输出流中
                              os.write(buffer,0,i);
                              i=bis.read(buffer);
                          }
                      }catch (IOException e){
                          e.printStackTrace();
                      }
 
                  }
              } catch (UnsupportedEncodingException e) {
                  e.printStackTrace();
              }
          }
}
