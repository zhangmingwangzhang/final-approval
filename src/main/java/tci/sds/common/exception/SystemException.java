/*
 * 資産管理システム（NTTD向け） COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.common.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * システムエラー
 * 
 * @author zhenweiqiang
 * @since 2020/01/09
 * @version 0.1
 */
public class SystemException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public static final String DEFAULT_FATAL_ERROR_CODE = "E9999";

    private final Logger logger = LoggerFactory
            .getLogger(SystemException.class);

    private String errorCode;

    private String[] errorMessages;

    public SystemException(String errorCode, String... errorMessages) {

        super(errorCode);
        this.errorCode = errorCode;
        this.errorMessages = errorMessages;
        logger.debug("System Fatal. description={}", errorCode);

    }

    public SystemException(Throwable errorInfo) {

        super(errorInfo);
        this.errorCode = DEFAULT_FATAL_ERROR_CODE;
        logger.debug("System Fatal. description={}", errorInfo);

    }

    public SystemException() {

        super();
        this.errorCode = DEFAULT_FATAL_ERROR_CODE;
        logger.debug("System Fatal. description={}", DEFAULT_FATAL_ERROR_CODE);

    }

    /**
     * @return the errorCode
     */
    public String getErrorCode() {

        return errorCode;

    }

    /**
     * @param errorCode the errorCode to set
     */
    public void setErrorCode(String errorCode) {

        this.errorCode = errorCode;

    }

    /**
     * @return the errorMessages
     */
    public String[] getErrorMessages() {

        return errorMessages;

    }

    /**
     * @param errorMessages the errorMessages to set
     */
    public void setErrorMessages(String[] errorMessages) {

        this.errorMessages = errorMessages;

    }

}
