/*
 * 資産管理システム（NTTD向け） COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.common.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ビジネスエラー
 * 
 * @author zhenweiqiang
 * @since 2020/01/09
 * @version 0.1
 */
public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private final Logger logger = LoggerFactory
            .getLogger(BusinessException.class);

    private String errorCode;

    private String[] errorMessages;

    public BusinessException(String errorCode, String... errorMessages) {

        super(errorCode);
        this.errorCode = errorCode;
        this.errorMessages = errorMessages;
        logger.debug("System Fatal. description={}", errorCode);

    }

    /**
     * @return the errorCode
     */
    public String getErrorCode() {

        return errorCode;

    }

    /**
     * @param errorCode the errorCode to set
     */
    public void setErrorCode(String errorCode) {

        this.errorCode = errorCode;

    }

    /**
     * @return the errorMessages
     */
    public String[] getErrorMessages() {

        return errorMessages;

    }

    /**
     * @param errorMessages the errorMessages to set
     */
    public void setErrorMessages(String[] errorMessages) {

        this.errorMessages = errorMessages;

    }

}
