/*
 * 資産管理システム（NTTD向け） COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.common.exception;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import tci.sds.common.form.BaseForm;

/**
 * 例外処理用のハンドリング
 * 
 * @author zhenweiqiang
 * @since 2020/01/09
 * @version 0.1
 */
@ControllerAdvice
public class GlobalExceptionHandler {
	private final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);
	private static final String LOGIN_USER_ID = "loginUserId";

	@Autowired
	private MessageSource messageSource;

	/**
	 * 単項目チェックに関するエラーハンドリング
	 * 
	 * @param request
	 * @param e
	 * @return BaseForm
	 */
	@ExceptionHandler(value = MethodArgumentNotValidException.class)
	@ResponseBody
	public BaseForm handleMethodArgumentNotValidException(HttpServletRequest request,
			MethodArgumentNotValidException e) {

		List<String> messages = new ArrayList<>();
		for (FieldError fieldError : e.getBindingResult().getFieldErrors()) {
			logger.info("handleMethodArgumentNotValidException={}/{}/{}/{}", fieldError.getField(),
					fieldError.getRejectedValue(), fieldError.getCode(), fieldError.getObjectName());
			if (LOGIN_USER_ID.equals(fieldError.getField())) {
				messages.add(String.format(fieldError.getDefaultMessage(),
						this.messageSource.getMessage(
								String.join(".", "baseForm", fieldError.getField()), null, null),
						fieldError.getRejectedValue()));
			} else {
				if ("Size".equals(fieldError.getCode())) {
					messages.add(String.format(fieldError.getDefaultMessage(),
							this.messageSource.getMessage(
									String.join(".", fieldError.getObjectName(), fieldError.getField()), null, null),
							fieldError.getArguments()[1].toString().equals(fieldError.getArguments()[2].toString()) ? fieldError.getArguments()[1].toString() : fieldError.getArguments()[2].toString() + " ～ " + fieldError.getArguments()[1].toString()));
				} else {
					messages.add(String.format(fieldError.getDefaultMessage(),
							this.messageSource.getMessage(
									String.join(".", fieldError.getObjectName(), fieldError.getField()), null, null),
							fieldError.getRejectedValue()));
				}
			}
		}
		BaseForm form = new BaseForm();
		form.setErrorCode(ErrorType.Validation.toString());
		form.setErrorMessages(messages);
		return form;

	}

	/**
	 * 項目相関チェックに関するエラーハンドリング
	 * 
	 * @param request
	 * @param e
	 * @return BaseForm
	 */
	@ExceptionHandler(value = ValidationException.class)
	@ResponseBody
	public BaseForm handleValidationException(HttpServletRequest request, ValidationException e) {

		String message = this.messageSource.getMessage(e.getErrorCode(), null, null);
		message = String.format(message, e.getErrorMessages());
		List<String> messages = new ArrayList<>();
		messages.add(message);
		BaseForm form = new BaseForm();
		form.setErrorCode(ErrorType.Validation.toString());
		form.setErrorMessages(messages);
		return form;

	}

	/**
	 * 業務ロジックに関するエラーハンドリング
	 * 
	 * @param request
	 * @param e
	 * @return BaseForm
	 */
	@ExceptionHandler(value = BusinessException.class)
	@ResponseBody
	public BaseForm handleBusinessException(HttpServletRequest request, BusinessException e) {

		String message = this.messageSource.getMessage(e.getErrorCode(), null, null);
		message = String.format(message, e.getErrorMessages());
		List<String> messages = new ArrayList<>();
		messages.add(message);
		BaseForm form = new BaseForm();
		form.setErrorCode(ErrorType.Business.toString());
		form.setErrorMessages(messages);
		return form;

	}

	/**
	 * システムに関するエラーハンドリング
	 * 
	 * @param request
	 * @param e
	 * @return BaseForm
	 */
	@ExceptionHandler(value = SystemException.class)
	@ResponseBody
	public BaseForm handleSystemException(HttpServletRequest request, SystemException e) {

		String message = this.messageSource.getMessage(e.getErrorCode(), null, null);
		message = String.format(message, e.getErrorMessages());
		List<String> messages = new ArrayList<>();
		messages.add(message);
		BaseForm form = new BaseForm();
		form.setErrorCode(ErrorType.System.toString());
		form.setErrorMessages(messages);
		return form;

	}
}
