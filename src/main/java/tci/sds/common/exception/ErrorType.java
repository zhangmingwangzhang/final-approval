/*
 * 資産管理システム（NTTD向け） COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.common.exception;

/**
 * エラータイプ
 * 
 * @author zhenweiqiang
 * @since 2020/01/09
 * @version 0.1
 */
public enum ErrorType {
    NotError(0), Validation(100), Business(200), System(900);

    private int errorCode;

    ErrorType(int errorCode) {

        this.errorCode = errorCode;

    }

    public int getCode() {

        return this.errorCode;

    }

    @Override
    public String toString() {

        return String.valueOf(this.errorCode);

    }

}
