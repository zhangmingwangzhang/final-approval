package tci.sds.common.interceptor;

import java.io.PrintWriter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class interceptorCofig implements HandlerInterceptor {

    private final Logger logger = LoggerFactory.getLogger(interceptorCofig.class);
    
    @Override
    public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
            throws Exception {
        // TODO Auto-generated method stub
    }

    
    @Override
    public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
            throws Exception {
        // TODO Auto-generated method stub
    }

    
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        
        String referer = request.getHeader("referer");
        
        System.out.println(referer);
        
        String sitePart = "http://"+request.getServerName();
        
        System.out.println(sitePart);
        
        Cookie[] cookies = request.getCookies();
        if (cookies != null && cookies.length > 0) {
              for(Cookie cookie:cookies) {
                  logger.debug("cookie===for遍历"+cookie.getName());
                    if ("isLogin".equalsIgnoreCase(cookie.getName())) {
                          logger.debug("有cookie ---isLogin，并且cookie还没过期...");
                          return true;
                    }
              }
        }
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<script>");
        out.println("window.open('/index.html', '_top')");
        out.println("</script>");
        out.println("</html>");
        out.flush();
        return false;
    }
}