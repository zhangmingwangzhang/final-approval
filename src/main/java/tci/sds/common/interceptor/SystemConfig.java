package tci.sds.common.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class SystemConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        // 注册TestInterceptor拦截器
        InterceptorRegistration registration = registry.addInterceptor(new interceptorCofig());
        // 所有路径都被拦截
        registration.addPathPatterns("/**");
        // 添加不拦截路径
        registration.excludePathPatterns(
                                         "/index.html",
//                                         "http://localhost:8888",
                                         "/**/*.js",
                                         "/**/*.css",
                                         "/**/fonts/*.ttf",
                                         "/**/fonts/*.woff",
                                         "/**/*.jpg",
                                         "/api/user/login"
        );
    }

}