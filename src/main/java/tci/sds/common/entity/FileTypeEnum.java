package tci.sds.common.entity;

/**
 * 上传类别
 * @author machenguang
 * @since 2020/10/08
 * 模板、文件、签名、Sample
 *
 */
public enum FileTypeEnum {
    TEMPLATE,FILE,SIGNATURE,SAMPLE;
}
