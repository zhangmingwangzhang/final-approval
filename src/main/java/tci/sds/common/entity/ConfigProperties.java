package tci.sds.common.entity;

import java.util.Date;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 配置信息
 * 1.文件上传下载路径信息
 * 2.邮件信息
 * 
 * @author machenguang
 * @since 2020/10/08
 *
 */
@Component
@ConfigurationProperties(prefix = "commpat.config")
@PropertySource("classpath:/application.properties")
public class ConfigProperties {
    
    // 文件路径
    private String excelFileUrl;
    
    // 模板路径
    private String excelTemplateUrl;
    
    // 电子签名
    private String signatureUrl;
    
    // pdf路径
    private String PDFFileUrl;
    
    // sample路径
    private String sampleUrl;
    
    // 发送用户
    private String sendUsername;
    
    private String sendTest;
    
    private String sendPwdtest;
    
    private String sendUrltest;
    
    // 修改密码管理员邮箱
    private String sendUseMangerEmail;
    
    // 修改密码提示邮件是否发送判定
    private Boolean notSendEmail;
    
    // 修改密码提示邮件发送时间
    private String remindDay;
    
    public String getSendUrltest() {
        return sendUrltest;
    }
    
    public void setSendUrltest(String sendUrltest) {
        this.sendUrltest = sendUrltest;
    }
    
    public String getSendPwdtest() {
        return sendPwdtest;
    }
    public void setSendPwdtest(String sendPwdtest) {
        this.sendPwdtest = sendPwdtest;
    }
    public String getSendTest() {
        return sendTest;
    }
    public void setSendTest(String sendTest) {
        this.sendTest = sendTest;
    }

    public String getSampleUrl() {

        return sampleUrl;

    }
    
    public void setSampleUrl(String sampleUrl) {

        this.sampleUrl = sampleUrl;

    }
    
    public String getSignatureUrl() {

        return signatureUrl;

    }
    
    public void setSignatureUrl(String signatureUrl) {

        this.signatureUrl = signatureUrl;

    }
    
    public String getExcelTemplateUrl() {

        return excelTemplateUrl;

    }
    
    public void setExcelTemplateUrl(String excelTemplateUrl) {

        this.excelTemplateUrl = excelTemplateUrl;

    }

    public String getExcelFileUrl() {
    
        return excelFileUrl;
    
    }

    public void setExcelFileUrl(String excelFileUrl) {
    
        this.excelFileUrl = excelFileUrl;
    
    }

    public String getPDFFileUrl() {
    
        return PDFFileUrl;
    
    }

    public void setPDFFileUrl(String pDFFileUrl) {
    
        PDFFileUrl = pDFFileUrl;
    
    }

    public String getSendUsername() {
    
        return sendUsername;
    
    }

    public void setSendUsername(String sendUsername) {
    
        this.sendUsername = sendUsername;
    
    }

    public String getSendUseMangerEmail() {
        return sendUseMangerEmail;
    }

    public void setSendUseMangerEmail(String sendUseMangerEmail) {
        this.sendUseMangerEmail = sendUseMangerEmail;
    }

    public Boolean getNotSendEmail() {
        return notSendEmail;
    }

    public void setNotSendEmail(Boolean notSendEmail) {
        this.notSendEmail = notSendEmail;
    }

    public String getRemindDay() {
        return remindDay;
    }

    public void setRemindDay(String remindDay) {
        this.remindDay = remindDay;
    }

    
    
    
}
