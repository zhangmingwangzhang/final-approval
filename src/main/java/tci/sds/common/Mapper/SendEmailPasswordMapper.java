/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.common.Mapper;

import java.util.Date;

/**
 * 查询密码mapper
 * 
 * @author fangshuai
 * @since 2020/12/16
 * @version 0.1
 */
public interface SendEmailPasswordMapper {

    /**
     * 查询发件人密码
     * @param 
     * @return
     */
    String searchPassword();

    /**
     * 查询发送邮箱账户修改密码时间
     * 
     * @return
     */
    Date getKosinTime();
}