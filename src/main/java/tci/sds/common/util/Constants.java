/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.common.util;

import java.io.Serializable;

/**
 * @author 王 文志 2020/11/25
 *
 */
public class Constants implements Serializable {

    /**
     * 定数说明
     */
    private static final long serialVersionUID = 1L;

    // 审核人状态
    public static String CD_REVIEWER_STATUS = "REVIEWER_STATUS";

    // 审核状态
    public static String CD_AUDIT_STATUS = "AUDIT_STATUS";

    // 职务权限
    public static String CD_POST_AUTHORITY = "POST_AUTHORITY";

    // 管理权限
    public static String CD_MANAGE_AUTHORITY = "MANAGE_AUTHORITY";

    // 审核权限
    public static String CD_AUDIT_AUTHORITY = "AUDIT_AUTHORITY";

    // 部门
    public static String CD_DEPART = "DEPART";

    // 文件类型
    public static String CD_FILE_TYPE = "FILE_TYPE";

    // 文件名称
    public static String CD_FILE_NAME = "FILE_NAME";
    
    // 文件名称CODE区分名称
    public static String CD_FILENAME = "文件名称";

    // 适用范围
    public static String CD_BUSINESS_SCOPE = "BUSINESS_SCOPE";
    
    // 适用范围CODE区分名称
    public static String CD_BUSINESSSCOPE = "适用范围";

    // 业务内容
    public static String CD_BUSINESS_CONTENT = "BUSINESS_CONTENT";

    // 业务内容CODE区分名称
    public static String CD_BUSINESSCONTENT = "业务内容";
    
    // 部门CODE区分名称
    public static String CD_DEPARTNAME = "部门";
    
    // 职务权限CODE区分名称
    public static String CD_POSTNAME = "职务权限";
    
    // 职位
    public static String CD_POSITION = "POSITION";
    
    // 初次登陆_是
    public static String CD_FIRST_LOGIN_YES = "0";
    
    // 初次登陆_否
    public static String CD_FIRST_LOGIN_NO = "1";
    
    // 管理权限_有
    public static String CD_MANAGE_AUTHORITY_YOU = "MANGE_YOU";
    
    // 管理权限_无
    public static String CD_MANAGE_AUTHORITY_WU = "MANGE_WU";

    // 审核权限_普通
    public static String CD_AUDIT_AUTHORITY_PU = "AUDIT_PU";

    // 审核权限_确认
    public static String CD_AUDIT_AUTHORITY_QUE = "AUDIT_QUE";

    // 审核权限_审核
    public static String CD_AUDIT_AUTHORITY_SHEN = "AUDIT_SHEN";
    
    // 审核状态_确认驳回
    public static String CD_REJECT_CONFIR = "REJECT_CONFIR";
    
    // 审核状态_待确认
    public static String CD_REJECT_WAIT = "REJECT_WAIT";
    
    // 审核状态_待审核
    public static String CD_EXAMINE_WAIT = "EXAMINE_WAIT";
    
    // 审核状态_审核驳回
    public static String CD_EXAMINE_CONFIR = "EXAMINE_CONFIR";
    
    // 审核状态_审核终了
    public static String CD_EXAMINE_OVER = "EXAMINE_OVER";
    
    // 审核人状态_已审核
    public static String CD_STATUS_YI = "STATUS_YI";
    
    // 审核人状态_未审核
    public static String CD_STATUS_WEI = "STATUS_WEI";
    
    // 济南事业部经理职务权限Code
    public static String CD_POST_JN1 = "POST_JN1";
    
    // 天津事业部部长职务权限Code
    public static String CD_POST_TJ1 = "POST_TJ1";
    
    // 普通员工职务权限Code
    public static String CD_POST_1 = "POST_1";
    
}
