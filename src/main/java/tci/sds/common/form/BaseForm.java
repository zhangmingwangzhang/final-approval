/*
 * 資産管理システム（NTTD向け） COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.common.form;

import java.util.List;

import javax.validation.constraints.NotEmpty;

import tci.sds.common.exception.ErrorType;

/**
 * 基底Form（エラーコードと埋め込み文字リストを提供する）
 * 
 * @author zhenweiqiang
 * @since 2020/01/09
 * @version 0.1
 */
public class BaseForm {

    /** ログインユーザID */
    @NotEmpty
    private String loginUserId;

    /** ログインユーザ名 */
    private String loginUserName;

    /** エラーコード */
    private String errorCode = ErrorType.NotError.toString();

    /** 埋め込み文字リスト */
    private List<String> errorMessages;

    /**
     * @return the loginUserId
     */
    public String getLoginUserId() {

        return loginUserId;

    }

    /**
     * @param loginUserId the loginUserId to set
     */
    public void setLoginUserId(String loginUserId) {

        this.loginUserId = loginUserId;

    }

    /**
     * @return the loginUserName
     */
    public String getLoginUserName() {
    
        return loginUserName;
    
    }

    /**
     * @param loginUserName the loginUserName to set
     */
    public void setLoginUserName(String loginUserName) {
    
        this.loginUserName = loginUserName;
    
    }

    /**
     * @return the errorCode
     */
    public String getErrorCode() {

        return errorCode;

    }

    /**
     * @param errorCode the errorCode to set
     */
    public void setErrorCode(String errorCode) {

        this.errorCode = errorCode;

    }

    /**
     * @return the errorMessages
     */
    public List<String> getErrorMessages() {

        return errorMessages;

    }

    /**
     * @param errorMessages the errorMessages to set
     */
    public void setErrorMessages(List<String> errorMessages) {

        this.errorMessages = errorMessages;

    }

    @Override
    public String toString() {

        return String.format("loginUserId=%s, loginUserName=%s, errorCode=%s, errorMessages=%s, ",
                loginUserId, loginUserName, errorCode, errorMessages);

    }

}
