/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.login.passwordModify.form;

import java.util.List;

import tci.sds.common.exception.ErrorType;

/**
 * 密码修改Form
 * 
 * @author sunliqiang
 * @since 2020/11/10
 * @version 0.1
 */
public class PasswordModifyForm{

    /** 旧密码 */
    private String oldPwd;

    /** 新密码 */
    private String newPwd;

    /** 新密码确认 */
    private String confirmPwd;
    
    /** 天津卡号 */
    private String loginTjCode;
    
    /** 用户登录密码 */
    private String loginPwd;
    
    /** 登录者姓名 */
    private String loginUserName;
    
    /** errorCode */
    private String errorCode = ErrorType.NotError.toString();

    /** 埋入文字List */
    private List<String> errorMessages;

    public String getOldPwd() {
        return oldPwd;
    }

    public void setOldPwd(String oldPwd) {
        this.oldPwd = oldPwd;
    }
    
    public String getNewPwd() {
        return newPwd;
    }

    public void setNewPwd(String newPwd) {
        this.newPwd = newPwd;
    }
    
    public String getConfirmPwd() {
        return confirmPwd;
    }

    public void setConfirmPwd(String confirmPwd) {
        this.confirmPwd = confirmPwd;
    }

    public String getLoginTjCode() {
        return loginTjCode;
    }

    public void setLoginTjCode(String loginTjCode) {
        this.loginTjCode = loginTjCode;
    }

    public String getLoginPwd() {
        return loginPwd;
    }

    public void setLoginPwd(String loginPwd) {
        this.loginPwd = loginPwd;
    }

    public String getLoginUserName() {
        return loginUserName;
    }

    public void setLoginUserName(String loginUserName) {
        this.loginUserName = loginUserName;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public List<String> getErrorMessages() {
        return errorMessages;
    }

    public void setErrorMessages(List<String> errorMessages) {
        this.errorMessages = errorMessages;
    }
}