/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.login.passwordModify.Entity;

import java.sql.Timestamp;

/**
 * 密码修改ExEntity
 * 
 * @author sunliqiang
 * @since 2020/11/10
 * @version 0.1
 */
public class PasswordModifyExEntity{

    /** 旧密码 */
    private String oldPwd;

    /** 新密码 */
    private String newPwd;

    /** 新密码确认 */
    private String confirmPwd;
    
    /** 天津卡号 */
    private String loginTjCode;
    
    /** 用户登录密码 */
    private String loginPwd;
    
    /** 密码修改时间 */
    private Timestamp updateTime;
    
    /** 登录者姓名 */
    private String loginUserName;
    
    /** 初次登陆与否 */
    private String firstLogin;
    

    public String getOldPwd() {
        return oldPwd;
    }

    public void setOldPwd(String oldPwd) {
        this.oldPwd = oldPwd;
    }
    
    public String getNewPwd() {
        return newPwd;
    }

    public void setNewPwd(String newPwd) {
        this.newPwd = newPwd;
    }
    
    public String getConfirmPwd() {
        return confirmPwd;
    }

    public void setConfirmPwd(String confirmPwd) {
        this.confirmPwd = confirmPwd;
    }

    public String getLoginTjCode() {
        return loginTjCode;
    }

    public void setLoginTjCode(String loginTjCode) {
        this.loginTjCode = loginTjCode;
    }

    public String getLoginPwd() {
        return loginPwd;
    }

    public void setLoginPwd(String loginPwd) {
        this.loginPwd = loginPwd;
    }

    public String getLoginUserName() {
        return loginUserName;
    }

    public void setLoginUserName(String loginUserName) {
        this.loginUserName = loginUserName;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public String getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(String firstLogin) {
        this.firstLogin = firstLogin;
    }
}