/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.login.passwordModify.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.util.regex.Pattern;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import tci.sds.common.exception.BusinessException;
import tci.sds.common.util.Constants;
import tci.sds.login.passwordModify.Entity.PasswordModifyExEntity;
import tci.sds.login.passwordModify.form.PasswordModifyForm;
import tci.sds.login.passwordModify.service.PasswordModifyService;

/**
 * 密码修改Controller
 * 
 * @author sunliqiang
 * @since 2020/11/10
 * @version 0.1
 */
@Controller
public class PasswordModifyController {
    private final Logger logger = LoggerFactory
            .getLogger(PasswordModifyController.class);

    @Autowired
    private PasswordModifyService passwordModifyService;

    /**
     * 密码修改
     * 
     * @param PasswordModifyForm
     * @return PasswordModifyForm
     */
    @Transactional(rollbackFor = Exception.class)
    @RequestMapping("/api/admin/password")
    @ResponseBody
    public PasswordModifyForm update(
            @Valid @RequestBody PasswordModifyForm passwordModifyForm) throws Exception {

        logger.info("update#param:{}", passwordModifyForm);

        // 单项目check
        // 旧密码必须Check
        String oldPwd = passwordModifyForm.getOldPwd();
        if(oldPwd == null || oldPwd == "") {
            throw new BusinessException("ERR0000001","旧密码");
        }
        
        // 旧密码半角数字Check
        Pattern pattern = Pattern.compile("[A-Za-z0-9]+"); 
        boolean oldPwdMatch = pattern.matcher(oldPwd).matches();
        if(!oldPwdMatch) {
            throw new BusinessException("ERR0000005","旧密码","半角英数字");
        }
        
        // 旧密码范围Check
        if(oldPwd.length() < 4 || oldPwd.length() > 12) {
            throw new BusinessException("ERR0000006","旧密码","4～12");
        }
        
        // 新密码必须Check
        String newPwd = passwordModifyForm.getNewPwd();
        if(newPwd == null || newPwd == "") {
            throw new BusinessException("ERR0000001","新密码");
        }
        
        // 新密码半角数字Check
        boolean newPwdMatch = pattern.matcher(newPwd).matches();
        if(!newPwdMatch) {
            throw new BusinessException("ERR0000005","新密码","半角英数字");
        }
        
        // 新密码范围Check
        if(newPwd.length() < 4 || newPwd.length() > 12) {
            throw new BusinessException("ERR0000006","新密码","4～12");
        }
        
        // 新密码确认必须Check
        String confirmPwd = passwordModifyForm.getConfirmPwd();
        if(confirmPwd == null || confirmPwd == "") {
            throw new BusinessException("ERR0000001","新密码确认");
        }
        
        // 新密码确认半角数字Check
        boolean confirmPwdMatch = pattern.matcher(confirmPwd).matches();
        if(!confirmPwdMatch) {
            throw new BusinessException("ERR0000005","新密码确认","半角英数字");
        }
        
        // 新密码确认范围Check
        if(confirmPwd.length() < 4 || confirmPwd.length() > 12) {
            throw new BusinessException("ERR0000006","新密码确认","4～12");
        }
        
        PasswordModifyExEntity passwordModifyExEntity = this.copyFormToExEntity(passwordModifyForm);
        // 项目间相关Check
        // 用户登录密码
        String loginPwd = passwordModifyExEntity.getLoginPwd();
        // 旧密码等于新密码
        if (oldPwd.equals(newPwd)){
            throw new BusinessException("ERR0000022");
        }
        
        // 新密码不等于确认密码
        if(!newPwd.equals(confirmPwd)) {
            throw new BusinessException("ERR0000014");
        }
        
        // 旧密码不等于登录原密码
        if(!oldPwd.equals(loginPwd)) {
            throw new BusinessException("ERR0000013");
        }
        
        // 更新时间
        Timestamp updateTime = new Timestamp(new Date().getTime());
        passwordModifyExEntity.setUpdateTime(updateTime);
        // 初次登陆与否
        passwordModifyExEntity.setFirstLogin(Constants.CD_FIRST_LOGIN_NO);
        // 更新密码修改
        int retCount = this.passwordModifyService.upadate(passwordModifyExEntity);
        
        logger.info("update#result:{}", retCount);
        
        PasswordModifyForm passwordModify = new PasswordModifyForm();
        // errorCode
        passwordModify.setErrorCode("0");
        return passwordModify;
    }

    /**
     * 把form的值保存到ExEntity中
     * 
     * @param PasswordModifyForm
     * @return PasswordModifyExEntity
     */
    private PasswordModifyExEntity copyFormToExEntity(PasswordModifyForm passwordModifyForm) {

        PasswordModifyExEntity passwordModifyExEntity = new PasswordModifyExEntity();
        // 旧密码
        passwordModifyExEntity.setOldPwd(passwordModifyForm.getOldPwd());
        // 新密码
        passwordModifyExEntity.setNewPwd(passwordModifyForm.getNewPwd());
        // 确认新密码
        passwordModifyExEntity.setConfirmPwd(passwordModifyForm.getConfirmPwd());
        // 天津卡号
        passwordModifyExEntity.setLoginTjCode(passwordModifyForm.getLoginTjCode());
        // 用户登录密码
        passwordModifyExEntity.setLoginPwd(passwordModifyForm.getLoginPwd());
        // 登录者姓名
        passwordModifyExEntity.setLoginUserName(passwordModifyForm.getLoginUserName());
        
        return passwordModifyExEntity;
    }
}