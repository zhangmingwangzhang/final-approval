/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.login.passwordModify.mapper;

import tci.sds.login.passwordModify.Entity.PasswordModifyExEntity;

/**
 * 密码修改Mapper
 * 
 * @author sunliqiang
 * @since 2020/11/10
 * @version 0.1
 */
public interface PasswordModifyExMapper {
    
    /**
     * 密码更新
     * @return 
     */
    int updatePassword(PasswordModifyExEntity passwordModifyExEntity);
    
}