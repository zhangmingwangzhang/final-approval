/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.login.passwordModify.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tci.sds.login.passwordModify.Entity.PasswordModifyExEntity;
import tci.sds.login.passwordModify.mapper.PasswordModifyExMapper;

/**
 * 密码修改Service
 * 
 * @author sunliqiang
 * @since 2020/11/10
 * @version 0.1
 */
@Service
public class PasswordModifyService {
    private final Logger logger = LoggerFactory.getLogger(PasswordModifyService.class);

    @Autowired
    private PasswordModifyExMapper passwordModifyExMapper;

    /**
     * 密码修改
     * 
     * @param passwordModifyExEntity 
     * @return 
     */
    @Transactional(readOnly = false)
    public int upadate(PasswordModifyExEntity passwordModifyExEntity) {

        logger.info("upadate#param:{}", passwordModifyExEntity);
        // 执行密码修改
        int result = this.passwordModifyExMapper.updatePassword(passwordModifyExEntity);
        
        logger.info("upadate#result:{}", result);
        
        return result;
    }
}