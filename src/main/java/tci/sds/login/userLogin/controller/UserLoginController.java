/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.login.userLogin.controller;

import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import tci.sds.common.exception.BusinessException;
import tci.sds.login.userLogin.Entity.UserLoginExEntity;
import tci.sds.login.userLogin.form.UserLoginForm;
import tci.sds.login.userLogin.service.UserLoginService;

/**
 * 登录画面Controller
 * 
 * @author sunliqiang
 * @since 2020/11/09
 * @version 0.1
 */
@Controller
public class UserLoginController {
    private final Logger logger = LoggerFactory
            .getLogger(UserLoginController.class);

    @Autowired
    private UserLoginService userLoginService;

    /**
     * 登录验证
     * 
     * @param UserLoginForm
     * @return UserLoginForm
     */
	@PostMapping("/api/user/login")
    @ResponseBody
    public UserLoginForm search(
            @Valid @RequestBody UserLoginForm userLoginForm, HttpServletRequest request, HttpServletResponse response) {

        logger.info("search#param:{}", userLoginForm);
        // 单项目Check
        // 用户名必须Check
        String loginTjCode = userLoginForm.getLoginTjCode();
        if(loginTjCode == null || loginTjCode == "") {
            throw new BusinessException("ERR0000001","用户名");
        }
        
        // 用户名半角数字Check
        Pattern pattern = Pattern.compile("[0-9]*"); 
        boolean tjCodeMatch = pattern.matcher(loginTjCode).matches();
        if(!tjCodeMatch) {
            throw new BusinessException("ERR0000005","用户名","半角数字");
        }
        
        // 用户名范围Check
        if(loginTjCode.length() < 4 || loginTjCode.length() > 6) {
            throw new BusinessException("ERR0000006","用户名","4～6");
        }
        
        
        // 密码必须Check
        String loginPwd = userLoginForm.getLoginPwd();
        if(loginPwd == null || loginPwd == "") {
            throw new BusinessException("ERR0000001","密码");
        }
        
        // 密码半角数字Check
        Pattern patternPwd = Pattern.compile("[A-Za-z0-9]+"); 
        boolean loginPwdMatch = patternPwd.matcher(loginPwd).matches();
        if(!loginPwdMatch) {
            throw new BusinessException("ERR0000005","密码","半角英数字");
        }
        
        // 密码范围Check
        if(loginPwd.length() < 4 || loginPwd.length() > 12) {
            throw new BusinessException("ERR0000006","密码","4～12");
        }
        
        UserLoginExEntity userLoginExEntity = this.copyFormToExEntity(userLoginForm);
        // 登录验证
        List<UserLoginExEntity> userInfo = this.userLoginService.searchUser(userLoginExEntity);

        UserLoginForm userLogin  = new UserLoginForm();
        // 登录用户姓名
        userLogin.setLoginUserName(userInfo.get(0).getLoginUserName());
        // 登录用户管理flag
        userLogin.setManageFlag(userInfo.get(0).getManageFlag());
        // 登录用户天津卡号
        userLogin.setLoginTjCode(userLoginExEntity.getLoginTjCode());
        // 登录用户密码
        userLogin.setLoginPwd(userLoginExEntity.getLoginPwd());
        // 初次登陆与否 
        userLogin.setFirstLogin(userInfo.get(0).getFirstLogin());
        // errorCode
        userLogin.setErrorCode("0");
        logger.info("search#result:{}", userLogin);
        
        Cookie cookie = new Cookie("isLogin", "success");
        cookie.setMaxAge(24*60*60);
        cookie.setPath("/");
        response.addCookie(cookie);
        request.setAttribute("isLogin", true);
        
        return userLogin;
    }

   /**
     * ログアウト処理
     * 
     * @param UserLoginForm
     * @return UserLoginForm
     */
    @RequestMapping("/api/user/logout")
    @ResponseBody
    public UserLoginForm logout(@RequestBody UserLoginForm userLoginForm, HttpServletRequest request, HttpServletResponse response) {

        Cookie[] cookies = request.getCookies();
        for(Cookie cookie:cookies){
            if("isLogin".equalsIgnoreCase(cookie.getName())){
                cookie.setMaxAge(0);
                cookie.setValue(null);
                cookie.setPath("/");
                response.addCookie(cookie);
                break;
            }
        }
        // 返却
        return userLoginForm;
    }

    /**
     * 把form的值保存到ExEntity中
     * 
     * @param UserLoginForm Form的设定值
     * @return ExEntity
     */
    private UserLoginExEntity copyFormToExEntity(UserLoginForm userLoginForm) {

        UserLoginExEntity userLoginExEntity = new UserLoginExEntity();
        // 天津卡号
        userLoginExEntity.setLoginTjCode(userLoginForm.getLoginTjCode());
        // 登录密码
        userLoginExEntity.setLoginPwd(userLoginForm.getLoginPwd());
        
        return userLoginExEntity;
    }
}