/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.login.userLogin.Entity;

/**
 * 登录ExEntity
 * 
 * @author sunliqiang
 * @since 2020/11/09
 * @version 0.1
 */
public class UserLoginExEntity {

    /** 天津卡号 */
    private String loginTjCode;
    
    /** 密码 */
    private String loginPwd;
    
    /** 姓名 */
    private String loginUserName;
    
    /** 管理flag */
    private String manageFlag;
    
    /** 初次登陆与否 */
    private String firstLogin;

	public String getLoginTjCode() {
		return loginTjCode;
	}

	public void setLoginTjCode(String loginTjCode) {
		this.loginTjCode = loginTjCode;
	}

	public String getLoginPwd() {
		return loginPwd;
	}

	public void setLoginPwd(String loginPwd) {
		this.loginPwd = loginPwd;
	}

    public String getLoginUserName() {
        return loginUserName;
    }

    public void setLoginUserName(String loginUserName) {
        this.loginUserName = loginUserName;
    }

    public String getManageFlag() {
        return manageFlag;
    }

    public void setManageFlag(String manageFlag) {
        this.manageFlag = manageFlag;
    }

    public String getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(String firstLogin) {
        this.firstLogin = firstLogin;
    }

}