/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.login.userLogin.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tci.sds.common.exception.BusinessException;
import tci.sds.login.userLogin.Entity.UserLoginExEntity;
import tci.sds.login.userLogin.mapper.UserLoginExMapper;

/**
 * 登录画面Service
 *
 * @author sunlqiiang
 * @since 2020/11/09
 * @version 0.1
 */
@Service
public class UserLoginService {
    private final Logger logger = LoggerFactory.getLogger(UserLoginService.class);

    @Autowired
    private UserLoginExMapper userLoginExMapper;

    /**
     * 检索用户账号密码
     *
     * @param userLoginExEntity
     * @return 用户名
     */
    @Transactional(readOnly = true)
    public List<UserLoginExEntity> searchUser(UserLoginExEntity userLoginExEntity) {

        logger.info("searchUser#param:{}", userLoginExEntity);
        // 验证登录账号密码
        List<UserLoginExEntity> userInfo  = this.userLoginExMapper.searchUser(userLoginExEntity);
        System.out.println("测试================"+userInfo);
        // 账号与输入密码不匹配
        if (userInfo.size() == 0) {
            logger.info("searchUser#error:Record is not found.");
            throw new BusinessException("ERR0000011");
        }
        logger.info("searchUser#result:{}", userInfo.size());

        return userInfo;
    }
}
