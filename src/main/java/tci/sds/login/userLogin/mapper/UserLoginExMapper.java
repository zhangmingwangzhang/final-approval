/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.login.userLogin.mapper;

import java.util.List;

import tci.sds.login.userLogin.Entity.UserLoginExEntity;

/**
 * 登录画面mapper
 * 
 * @author sunliqiang
 * @since 2020/11/09
 * @version 0.1
 */
public interface UserLoginExMapper {
    
    /**
     * 登录验证
     * @param userLoginExEntity
     * @return
     */
    List<UserLoginExEntity> searchUser(UserLoginExEntity userLoginExEntity);
    
}