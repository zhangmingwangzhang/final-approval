/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.login.userLogin.form;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import tci.sds.common.exception.ErrorType;

/**
 * 登录画面Form
 * 
 * @author sunliqiang
 * @since 2020/11/09
 * @version 0.1
 */
public class UserLoginForm{

    /** 天津卡号 */
    private String loginTjCode;
    
    /** 密码 */
    private String loginPwd;

    /** 姓名 */
    private String loginUserName;
    
    /** 管理flag */
    private String manageFlag;
    
    /** 初次登陆与否 */
    private String firstLogin;
    
    /** errorCode */
    private String errorCode = ErrorType.NotError.toString();

    /** 埋入文字List */
    private List<String> errorMessages;

    public String getLoginTjCode() {
        return loginTjCode;
    }

    public void setLoginTjCode(String loginTjCode) {
        this.loginTjCode = loginTjCode;
    }

    public String getLoginPwd() {
        return loginPwd;
    }

    public void setLoginPwd(String loginPwd) {
        this.loginPwd = loginPwd;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public List<String> getErrorMessages() {
        return errorMessages;
    }

    public void setErrorMessages(List<String> errorMessages) {
        this.errorMessages = errorMessages;
    }

    public String getLoginUserName() {
        return loginUserName;
    }

    public void setLoginUserName(String loginUserName) {
        this.loginUserName = loginUserName;
    }

    public String getManageFlag() {
        return manageFlag;
    }

    public void setManageFlag(String manageFlag) {
        this.manageFlag = manageFlag;
    }

    public String getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(String firstLogin) {
        this.firstLogin = firstLogin;
    }
}