/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.requestApprove.entity;

import tci.sds.common.form.BaseForm;

public class RequestApproveEntity extends BaseForm{
    //天津卡号
    private String tjCode;
    //姓名
    private String userName;
    //部门番号
    private String deptId;
    //部门名称
    private String deptName;
    //申请编号
    private String applicationNumber;
    //文件编号
    private String fileId;
    //文件名称
    private String fileName;
    //文件路径
    private String filePath;
    //适应范围
    private String scopeApplication;
    //业务内容
    private String condition;
    //审核状态code
    private String examineStatusCode;
    //审核状态
    private String examineStatus;
    //审核顺序
    private String reviewerOrder;
    //备注
    private String fileRemark;
    //申请者
    private String fileApplicantId;
    //适应范围code
    private String scopeApplicationCode;
    //业务内容code
    private String conditionCode;
    //文件名称code
    private String fileNameCode;
    //签名位置
    private String reviewerPosition;
    //签名所在sheet页
    private String reviewerSheet;
    //图片路径
    private String psPath;
    //登录者卡号
    private String loginCode;
    //确认人
    private String fileConfigId;
    //审核人
    private String firstReviewer;
    //审核人状态 
    private String reviewerStatus;
    //驳回理由
    private String examineConfirReasons;
    //邮箱
    private String personalMail;
    //Info文字
    private String infoMessage;
    
    /**
     * @return the infoMessage
     */
    public String getInfoMessage() {
        return infoMessage;
    }
    /**
     * @param infoMessage the infoMessage to set
     */
    public void setInfoMessage(String infoMessage) {
        this.infoMessage = infoMessage;
    }
    /**
     * @return the tjCode
     */
    public String getTjCode() {
        return tjCode;
    }
    /**
     * @param tjCode the tjCode to set
     */
    public void setTjCode(String tjCode) {
        this.tjCode = tjCode;
    }
    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }
    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }
    /**
     * @return the deptId
     */
    public String getDeptId() {
        return deptId;
    }
    /**
     * @param deptId the deptId to set
     */
    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }
    /**
     * @return the applicationNumber
     */
    public String getApplicationNumber() {
        return applicationNumber;
    }
    /**
     * @param applicationNumber the applicationNumber to set
     */
    public void setApplicationNumber(String applicationNumber) {
        this.applicationNumber = applicationNumber;
    }
    /**
     * @return the fileId
     */
    public String getFileId() {
        return fileId;
    }
    /**
     * @param fileId the fileId to set
     */
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }
    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }
    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    /**
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }
    /**
     * @param filePath the filePath to set
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    /**
     * @return the scopeApplication
     */
    public String getScopeApplication() {
        return scopeApplication;
    }
    /**
     * @param scopeApplication the scopeApplication to set
     */
    public void setScopeApplication(String scopeApplication) {
        this.scopeApplication = scopeApplication;
    }
    /**
     * @return the condition
     */
    public String getCondition() {
        return condition;
    }
    /**
     * @param condition the condition to set
     */
    public void setCondition(String condition) {
        this.condition = condition;
    }
    /**
     * @return the examineStatus
     */
    public String getExamineStatus() {
        return examineStatus;
    }
    /**
     * @param examineStatus the examineStatus to set
     */
    public void setExamineStatus(String examineStatus) {
        this.examineStatus = examineStatus;
    }
    /**
     * @return the fileRemark
     */
    public String getFileRemark() {
        return fileRemark;
    }
    /**
     * @param fileRemark the fileRemark to set
     */
    public void setFileRemark(String fileRemark) {
        this.fileRemark = fileRemark;
    }
    /**
     * @return the fileApplicantId
     */
    public String getFileApplicantId() {
        return fileApplicantId;
    }
    /**
     * @param fileApplicantId the fileApplicantId to set
     */
    public void setFileApplicantId(String fileApplicantId) {
        this.fileApplicantId = fileApplicantId;
    }
    
    /**
     * @return the reviewerPosition
     */
    public String getReviewerPosition() {
        return reviewerPosition;
    }
    /**
     * @param reviewerPosition the reviewerPosition to set
     */
    public void setReviewerPosition(String reviewerPosition) {
        this.reviewerPosition = reviewerPosition;
    }
    /**
     * @return the psPath
     */
    public String getPsPath() {
        return psPath;
    }
    /**
     * @param psPath the psPath to set
     */
    public void setPsPath(String psPath) {
        this.psPath = psPath;
    }

    /**
     * @return the loginCode
     */
    public String getLoginCode() {
        return loginCode;
    }
    /**
     * @param loginCode the loginCode to set
     */
    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }
    /**
     * @return the reviewerSheet
     */
    public String getReviewerSheet() {
        return reviewerSheet;
    }
    /**
     * @param reviewerSheet the reviewerSheet to set
     */
    public void setReviewerSheet(String reviewerSheet) {
        this.reviewerSheet = reviewerSheet;
    }
    /**
     * @return the scopeApplicationCode
     */
    public String getScopeApplicationCode() {
        return scopeApplicationCode;
    }
    /**
     * @param scopeApplicationCode the scopeApplicationCode to set
     */
    public void setScopeApplicationCode(String scopeApplicationCode) {
        this.scopeApplicationCode = scopeApplicationCode;
    }
    /**
     * @return the conditionCode
     */
    public String getConditionCode() {
        return conditionCode;
    }
    /**
     * @param conditionCode the conditionCode to set
     */
    public void setConditionCode(String conditionCode) {
        this.conditionCode = conditionCode;
    }
    /**
     * @return the fileNameCode
     */
    public String getFileNameCode() {
        return fileNameCode;
    }
    /**
     * @param fileNameCode the fileNameCode to set
     */
    public void setFileNameCode(String fileNameCode) {
        this.fileNameCode = fileNameCode;
    }
    /**
     * @return the examineStatusCode
     */
    public String getExamineStatusCode() {
        return examineStatusCode;
    }
    /**
     * @param examineStatusCode the examineStatusCode to set
     */
    public void setExamineStatusCode(String examineStatusCode) {
        this.examineStatusCode = examineStatusCode;
    }
    /**
     * @return the fileConfigId
     */
    public String getFileConfigId() {
        return fileConfigId;
    }
    /**
     * @param fileConfigId the fileConfigId to set
     */
    public void setFileConfigId(String fileConfigId) {
        this.fileConfigId = fileConfigId;
    }
    
    /**
     * @return the reviewerStatus
     */
    public String getReviewerStatus() {
        return reviewerStatus;
    }
    /**
     * @param reviewerStatus the reviewerStatus to set
     */
    public void setReviewerStatus(String reviewerStatus) {
        this.reviewerStatus = reviewerStatus;
    }
    /**
     * @return the examineConfirReasons
     */
    public String getExamineConfirReasons() {
        return examineConfirReasons;
    }
    /**
     * @param examineConfirReasons the examineConfirReasons to set
     */
    public void setExamineConfirReasons(String examineConfirReasons) {
        this.examineConfirReasons = examineConfirReasons;
    }
    /**
     * @return the reviewerOrder
     */
    public String getReviewerOrder() {
        return reviewerOrder;
    }
    /**
     * @param reviewerOrder the reviewerOrder to set
     */
    public void setReviewerOrder(String reviewerOrder) {
        this.reviewerOrder = reviewerOrder;
    }
    /**
     * @return the firstReviewer
     */
    public String getFirstReviewer() {
        return firstReviewer;
    }
    /**
     * @param firstReviewer the firstReviewer to set
     */
    public void setFirstReviewer(String firstReviewer) {
        this.firstReviewer = firstReviewer;
    }
    /**
     * @return the deptName
     */
    public String getDeptName() {
        return deptName;
    }
    /**
     * @param deptName the deptName to set
     */
    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }
    /**
     * @return the personalMail
     */
    public String getPersonalMail() {
        return personalMail;
    }
    /**
     * @param personalMail the personalMail to set
     */
    public void setPersonalMail(String personalMail) {
        this.personalMail = personalMail;
    }
    @Override
    public String toString() {
        return "RequestApproveEntity [tjCode=" 
                + tjCode + ", userName=" + userName + ", deptId=" + deptId
                + ", deptName=" + deptName + ", applicationNumber=" 
                + applicationNumber + ", fileId=" + fileId
                + ", fileName=" + fileName + ", filePath=" 
                + filePath + ", scopeApplication=" + scopeApplication
                + ", condition=" + condition + ", examineStatusCode=" + examineStatusCode + ", examineStatus="
                + examineStatus + ", reviewerOrder=" 
                + reviewerOrder + ", fileRemark=" + fileRemark
                + ", fileApplicantId=" + fileApplicantId 
                + ", scopeApplicationCode=" + scopeApplicationCode
                + ", conditionCode=" + conditionCode 
                + ", fileNameCode=" + fileNameCode + ", reviewerPosition="
                + reviewerPosition + ", reviewerSheet=" 
                + reviewerSheet + ", psPath=" + psPath 
                + ", loginCode="
                + loginCode + ", fileConfigId=" + fileConfigId 
                + ", firstReviewer=" + firstReviewer
                + ", reviewerStatus=" + reviewerStatus 
                + ", examineConfirReasons=" + examineConfirReasons
                + ", personalMail=" + personalMail + "]";
    }
   
    
    
}
