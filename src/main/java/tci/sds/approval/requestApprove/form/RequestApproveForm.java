/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.requestApprove.form;

import java.util.List;
import tci.sds.common.form.BaseForm;

public class RequestApproveForm extends BaseForm {
    //天津卡号
    private String tjCode;
    //姓名
    private String userName;
    //部门番号
    private String deptId;
    //部门名称
    private String deptName;
    //登录者卡号
    private String loginCode;
    //文件编号
    private String fileId;
    //文件名称
    private String fileName;
    //文件名称code
    private String fileNameCode;
    //适应范围code
    private String scopeApplicationCode;
    //适应范围
    private String scopeApplication;
    //业务内容
    private String condition;
    //业务内容code
    private String conditionCode;
    //备注
    private String fileRemark;
    //申请编号
    private String applicationNumber;
    //确认人
    private String fileConfigId;
    //图片路径
    private String filePath;
    //审核顺序
    private String reviewerOrder;
    //审核人状态 
    private String reviewerStatus;
    //审核人
    private String firstReviewer;
    //驳回理由
    private String examineConfirReasons;
    //审核状态
    private String examineStatus;
    //审核状态code
    private String examineStatusCode;
    //文件显示
    private List<String> fileIdList;
    //flag
    private Boolean flag;
    //Info文字 
    private String infoMessage;
    
    /**
     * @return the infoMessage
     */
    public String getInfoMessage() {
        return infoMessage;
    }

    /**
     * @param infoMessage the infoMessage to set
     */
    public void setInfoMessage(String infoMessage) {
        this.infoMessage = infoMessage;
    }

    /**
     * @return the fileIdList
     */
    public List<String> getFileIdList() {
        return fileIdList;
    }

    /**
     * @param fileIdList the fileIdList to set
     */
    public void setFileIdList(List<String> fileIdList) {
        this.fileIdList = fileIdList;
    }

    /**
     * @return the applicationNumber
     */
    public String getApplicationNumber() {
        return applicationNumber;
    }

    /**
     * @param applicationNumber the applicationNumber to set
     */
    public void setApplicationNumber(String applicationNumber) {
        this.applicationNumber = applicationNumber;
    }

    /**
     * @return the loginCode
     */
    public String getLoginCode() {
        return loginCode;
    }

    /**
     * @param loginCode the loginCode to set
     */
    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }

    /**
     * @return the fileId
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * @param fileId the fileId to set
     */
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * @return the fileNameCode
     */
    public String getFileNameCode() {
        return fileNameCode;
    }

    /**
     * @param fileNameCode the fileNameCode to set
     */
    public void setFileNameCode(String fileNameCode) {
        this.fileNameCode = fileNameCode;
    }

    /**
     * @return the scopeApplicationCode
     */
    public String getScopeApplicationCode() {
        return scopeApplicationCode;
    }

    /**
     * @param scopeApplicationCode the scopeApplicationCode to set
     */
    public void setScopeApplicationCode(String scopeApplicationCode) {
        this.scopeApplicationCode = scopeApplicationCode;
    }

    /**
     * @return the conditionCode
     */
    public String getConditionCode() {
        return conditionCode;
    }

    /**
     * @param conditionCode the conditionCode to set
     */
    public void setConditionCode(String conditionCode) {
        this.conditionCode = conditionCode;
    }

    /**
     * @return the fileRemark
     */
    public String getFileRemark() {
        return fileRemark;
    }

    /**
     * @param fileRemark the fileRemark to set
     */
    public void setFileRemark(String fileRemark) {
        this.fileRemark = fileRemark;
    }

    /**
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * @param filePath the filePath to set
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * @return the reviewerOrder
     */
    public String getReviewerOrder() {
        return reviewerOrder;
    }

    /**
     * @param reviewerOrder the reviewerOrder to set
     */
    public void setReviewerOrder(String reviewerOrder) {
        this.reviewerOrder = reviewerOrder;
    }

    /**
     * @return the reviewerStatus
     */
    public String getReviewerStatus() {
        return reviewerStatus;
    }

    /**
     * @param reviewerStatus the reviewerStatus to set
     */
    public void setReviewerStatus(String reviewerStatus) {
        this.reviewerStatus = reviewerStatus;
    }

    /**
     * @return the firstReviewer
     */
    public String getFirstReviewer() {
        return firstReviewer;
    }

    /**
     * @param firstReviewer the firstReviewer to set
     */
    public void setFirstReviewer(String firstReviewer) {
        this.firstReviewer = firstReviewer;
    }

    /**
     * @return the examineConfirReasons
     */
    public String getExamineConfirReasons() {
        return examineConfirReasons;
    }
    /**
     * @param examineConfirReasons the examineConfirReasons to set
     */
    public void setExamineConfirReasons(String examineConfirReasons) {
        this.examineConfirReasons = examineConfirReasons;
    }
    /**
     * @return the examineStatus
     */
    public String getExamineStatus() {
        return examineStatus;
    }

    /**
     * @param examineStatus the examineStatus to set
     */
    public void setExamineStatus(String examineStatus) {
        this.examineStatus = examineStatus;
    }

    /**
     * @return the examineStatusCode
     */
    public String getExamineStatusCode() {
        return examineStatusCode;
    }

    /**
     * @param examineStatusCode the examineStatusCode to set
     */
    public void setExamineStatusCode(String examineStatusCode) {
        this.examineStatusCode = examineStatusCode;
    }

    /**
     * @return the scopeApplication
     */
    public String getScopeApplication() {
        return scopeApplication;
    }
    /**
     * @param scopeApplication the scopeApplication to set
     */
    public void setScopeApplication(String scopeApplication) {
        this.scopeApplication = scopeApplication;
    }

    /**
     * @return the condition
     */
    public String getCondition() {
        return condition;
    }

    /**
     * @param condition the condition to set
     */
    public void setCondition(String condition) {
        this.condition = condition;
    }

    /**
     * @return the tjCode
     */
    public String getTjCode() {
        return tjCode;
    }

    /**
     * @param tjCode the tjCode to set
     */
    public void setTjCode(String tjCode) {
        this.tjCode = tjCode;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }
    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }
    /**
     * @return the deptId
     */
    public String getDeptId() {
        return deptId;
    }

    /**
     * @param deptId the deptId to set
     */
    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }
    
    /**
     * @return the fileConfigId
     */
    public String getFileConfigId() {
        return fileConfigId;
    }

    /**
     * @param fileConfigId the fileConfigId to set
     */
    public void setFileConfigId(String fileConfigId) {
        this.fileConfigId = fileConfigId;
    }

    /**
     * @return the flag
     */
    public Boolean getFlag() {
        return flag;
    }
    /**
     * @param flag the flag to set
     */
    public void setFlag(Boolean flag) {
        this.flag = flag;
    }
    /**
     * @return the deptName
     */
    public String getDeptName() {
        return deptName;
    }

    /**
     * @param deptName the deptName to set
     */
    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    @Override
    public String toString() {
        return "RequestApproveForm [tjCode=" + tjCode 
                + ", userName=" + userName + ", deptId=" 
                + deptId + ", deptName="
                + deptName + ", loginCode=" 
                + loginCode + ", fileId=" + fileId 
                + ", fileName=" + fileName
                + ", fileNameCode=" + fileNameCode 
                + ", scopeApplicationCode=" + scopeApplicationCode
                + ", scopeApplication=" + scopeApplication 
                + ", condition=" + condition 
                + ", conditionCode="
                + conditionCode + ", fileRemark=" 
                + fileRemark + ", applicationNumber=" 
                + applicationNumber
                + ", fileConfigId=" + fileConfigId 
                + ", filePath=" + filePath 
                + ", reviewerOrder=" + reviewerOrder
                + ", reviewerStatus=" + reviewerStatus 
                + ", firstReviewer=" + firstReviewer 
                + ", examineConfirReasons="
                + examineConfirReasons + ", examineStatus=" 
                + examineStatus + ", examineStatusCode=" 
                + examineStatusCode
                + ", fileIdList=" + fileIdList + ", flag=" + flag + "]";
    }
    
    
    
}
