/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.requestApprove.controller;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import tci.sds.approval.requestApprove.entity.RequestApproveEntity;
import tci.sds.approval.requestApprove.form.RequestApproveForm;
import tci.sds.approval.requestApprove.service.RequestApproveService;
import tci.sds.common.entity.ConfigProperties;
import tci.sds.common.exception.BusinessException;
import tci.sds.common.unit.ExcelOperatingClass;
import tci.sds.common.unit.JudgeIsMoblie;

/**
 * 审核Controller
 * 
 * @author 李伟
 * @since 2020/11/20
 * @version 0.1
 */
@Controller
public class RequestApproveController {
   
    private final Logger logger = LoggerFactory.
            getLogger(RequestApproveController.class);
    
    private static ConfigProperties configProperties;
    @Autowired
    private RequestApproveService requestApproveService;
    @Autowired
    public void setConfig(ConfigProperties config) {
        RequestApproveController.configProperties = config;
    }
    @Value("${commpat.config.temp-file-url}")
    private String tempfileurl;
    
    /**
     * 初期显示
     * @param requestApproveForm 申请编号
     * @return requestFromEntity 审核画面数据显示
     */
    @PostMapping("/api/requestApprove/searchApprove")
    @ResponseBody
    public RequestApproveForm search(@RequestBody RequestApproveForm requestApproveForm) {
        logger.info("search#RequestApproveEntity:{}", requestApproveForm);
        //拦截器
        HttpServletRequest request = ((ServletRequestAttributes)
                RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = request.getSession();  
        session.setAttribute("userName", "");
        //初期显示
        RequestApproveEntity requestApproveEntity = this.copyApproveEntity(requestApproveForm);
        RequestApproveEntity requestFromEntity = this.
                requestApproveService.searchApprove(requestApproveEntity);
        RequestApproveForm approveOutForm = new RequestApproveForm();
        //申请者
        approveOutForm.setUserName(requestFromEntity.getUserName());
        //卡号
        approveOutForm.setTjCode(requestFromEntity.getTjCode());
        //部门编号
        approveOutForm.setDeptId(requestFromEntity.getDeptId());
        //部门名称
        approveOutForm.setDeptName(requestFromEntity.getDeptName());
        //文件编号
        approveOutForm.setFileId(requestFromEntity.getFileId());
        //文件名称
        approveOutForm.setFileName(requestFromEntity.getFileName());
        //文件名称code
        approveOutForm.setFileNameCode(requestFromEntity.getFileNameCode());
        //范围
        approveOutForm.setScopeApplication(requestFromEntity.getScopeApplication());
        approveOutForm.setScopeApplicationCode(requestFromEntity.getScopeApplicationCode());
        //业务内容
        approveOutForm.setCondition(requestFromEntity.getCondition());
        //业务内容code
        approveOutForm.setConditionCode(requestFromEntity.getConditionCode());
        //审核状态
        approveOutForm.setExamineStatus(requestFromEntity.getExamineStatus());
        //审核状态code
        approveOutForm.setExamineStatusCode(requestFromEntity.getExamineStatusCode());
        //备注
        approveOutForm.setFileRemark(requestFromEntity.getFileRemark());
        //文件路径
        List<String> fileIdList = new ArrayList<>();
        String filePath = requestFromEntity.getFilePath();
        File file = new File(filePath);
        //获得该文件夹内的所有文件
        File[] array = file.listFiles();
        for (int i = 0; i < array.length; i++) {
            fileIdList.add(array[i].getName());
        }
        approveOutForm.setFileIdList(fileIdList);
        logger.info("searchApprove#RequestApproveEntity:{}", requestFromEntity);
        return approveOutForm;
        
    }
    
    /**
     * 审核文件check
     * @param requestApproveForm 编号
     * @throws IOException 
     */
    @PostMapping("/api/requestApprove/fileCheck")
    @ResponseBody
    public RequestApproveEntity fileCheck(@RequestBody RequestApproveForm requestApproveForm) throws IOException {
        logger.info("search#fileCheck:{}", requestApproveForm);
        String excleCheckBoolean = null;
        //初期显示
        RequestApproveEntity approveFormEntity = this.copyApproveEntity(requestApproveForm);
        RequestApproveEntity requestApproveEntity = new RequestApproveEntity();
        //审核文件存在check
        approveFormEntity.setFileId(requestApproveForm.getFileId());
        
        requestApproveEntity = this.requestApproveService.searchFilePath(approveFormEntity);
        
        //获取预览的文件路径
        String FullPath = configProperties.getExcelFileUrl() + requestApproveForm.getApplicationNumber()
        +"\\"+requestApproveForm.getFileId();
        
        //check预览文件是否损坏
        String suffix = FullPath.substring(FullPath.lastIndexOf("."));
        //判断word还是excle
        if(".doc".equalsIgnoreCase(suffix) || ".docx".equalsIgnoreCase(suffix)) {
            // word
            excleCheckBoolean = ExcelOperatingClass.WordToPdfCheck(FullPath);
            if("fileError".equals(excleCheckBoolean)) {
                throw new BusinessException("ERR0000046");
            }
          } else if (".xls".equalsIgnoreCase(suffix) || ".xlsx".equalsIgnoreCase(suffix)){
            // excle
              excleCheckBoolean = ExcelOperatingClass.ExcelToPdfCheck(FullPath);
              if("fileError".equals(excleCheckBoolean)) {
                  throw new BusinessException("ERR0000046");
              }
          }
        logger.info("search#approve:{}", requestApproveEntity);
        return requestApproveEntity;
    }
    
    /**
     * PDF预览
     * @param fileName 文件名
     * @throws IOException 
     */
    @RequestMapping(value = "/api/requestApprove/previewPdf", method = RequestMethod.GET)
    public void pdfStreamHandler(HttpServletRequest request,
            HttpServletResponse response, String fileName, String applicationNumber, String loginCode)
            throws IOException {
        //设置标志判断是否是生成pdf文件，是删除，不是保留
        boolean flage=true;
        //审核文件信息
        RequestApproveForm requestApproveForm = new RequestApproveForm();
        requestApproveForm.setApplicationNumber(applicationNumber);
        requestApproveForm.setLoginCode(loginCode);
        RequestApproveEntity requestApproveEntity = this.copyApproveEntity(requestApproveForm);
        String fileFullName = fileName;
        // 判断要预览的文件是附件还是申请文件
        if (!fileName.contains("附件")) {

            // 附件申请文件
            // excel转换pdf
            RequestApproveEntity requestFromEntity = this.requestApproveService.searchApprove(requestApproveEntity);
            // 登录者tjcode
            requestFromEntity.setLoginCode(requestApproveForm.getLoginCode());
            // 获取签名位置和sheet
            RequestApproveEntity requestEntity = this.requestApproveService.searchPosition(requestFromEntity);
            // 签名位置
            // 签名sheet
            String reviewerSheet = requestEntity.getReviewerSheet();
            int sheetNo = Integer.parseInt(reviewerSheet);
            //判断文件是pdf还是excle进行预览
            String pdfFileName = "";
            String suffix = fileName.substring(fileName.lastIndexOf("."));
            if(".doc".equalsIgnoreCase(suffix) || ".docx".equalsIgnoreCase(suffix)) {
                //pdf预览
                pdfFileName = ExcelOperatingClass.DocToPdf(fileFullName,applicationNumber);
            } else if (".xls".equalsIgnoreCase(suffix) || ".xlsx".equalsIgnoreCase(suffix)){
                //excle预览
                pdfFileName = ExcelOperatingClass.ExcelToPdf(fileFullName, sheetNo,applicationNumber);
            }
            File file = new File(pdfFileName);
            if (file.exists()) {
                byte[] data = null;
                try {
                    FileInputStream input = new FileInputStream(file);
                	// 手机端登陆/pc端登陆判定
                    boolean isMobileLogin = JudgeIsMoblie.JudgeIsMoblie(request.getHeader("User-Agent"));
                    //手机登录实现实现下载后自动打开
                    if(isMobileLogin) {
            	        response.setContentType("application/*");     //可以指定文件类型
            	        response.setHeader("Content-Disposition","inline;filename=\"" + pdfFileName +"\"");
                    } 
                    data = new byte[input.available()];
                    input.read(data);
                    response.getOutputStream().write(data);
                    input.close();
                    //在线预览临时生成的pdf文件删除             
                    file.delete();          
                    File fileDelete = new File(configProperties.getPDFFileUrl());
                    //listFiles方法：返回file路径下所有文件和文件夹的绝对路径
                    File[] listFiles = fileDelete.listFiles();
                    for (File file2 : listFiles) {
                        file2.delete();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                return;
            }
        } else {
            //判断附件是pdf还是excle进行预览
            String pdfFileName = "";
            String suffix = fileName.substring(fileName.lastIndexOf("."));
            if(".doc".equalsIgnoreCase(suffix) || ".docx".equalsIgnoreCase(suffix)) {
                //word预览
                pdfFileName = ExcelOperatingClass.DocToPdf(fileFullName,applicationNumber);
            } else if (".xls".equalsIgnoreCase(suffix) || ".xlsx".equalsIgnoreCase(suffix)){
                //excle预览
                pdfFileName = ExcelOperatingClass.ExcelToPdf(fileFullName,applicationNumber);
            } else if(".png".equalsIgnoreCase(suffix) || ".jpg".equalsIgnoreCase(suffix) || ".pdf".equalsIgnoreCase(suffix) ){

                flage=false;
                pdfFileName= configProperties.getExcelFileUrl()+fileName;
                System.out.println("fileName==="+fileName);
                System.out.println("pdfFileName=========="+configProperties.getExcelFileUrl());
            }else  if(".ppt".equalsIgnoreCase(suffix) || ".pptx".equalsIgnoreCase(suffix)){

                pdfFileName= ExcelOperatingClass.pptToPdf(configProperties.getExcelFileUrl(),applicationNumber,fileName);
            }
            File file = new File(pdfFileName);
            if (file.exists()) {
                byte[] data = null;
                try {
                    FileInputStream input = new FileInputStream(file);
                	// 手机端登陆/pc端登陆判定
                    boolean isMobileLogin = JudgeIsMoblie.JudgeIsMoblie(request.getHeader("User-Agent"));
                    //手机登录实现实现下载后自动打开
                    if(isMobileLogin) {
            	        response.setContentType("application/*");     //可以指定文件类型
            	        response.setHeader("Content-Disposition","inline;filename=\"" + pdfFileName +"\"");
                    } 
                    data = new byte[input.available()];
                    input.read(data);
                    response.getOutputStream().write(data);
                    input.close();
                    //在线预览临时生成的pdf文件删除             
                    if(flage){
                        file.delete();
                    }
                    File fileDelete = new File(configProperties.getPDFFileUrl());
                    //listFiles方法：返回file路径下所有文件和文件夹的绝对路径
                    File[] listFiles = fileDelete.listFiles();
                    for (File file2 : listFiles) {
                        file2.delete();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                return;
            }
        }
    }
    /**
     * 审核通过
     * @param requestApproveForm 申请编号
     * @return requestApproveForm flag
     * @throws Exception 
     */
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/api/requestApprove/approve")
    @ResponseBody
    public RequestApproveForm approve(@RequestBody RequestApproveForm requestApproveForm)
            throws Exception {
        logger.info("search#approve:{}", requestApproveForm);
        
        // chcek驳回理由是否为空
        if (!requestApproveForm.getExamineConfirReasons().equals("")) {       
          throw new BusinessException("ERR0000025","驳回理由");  
        }
        
        //初期显示
        RequestApproveEntity approveFormEntity = this.copyApproveEntity(requestApproveForm);
        RequestApproveEntity requestApproveEntity = new RequestApproveEntity();
        //申请编号check
        this.requestApproveService.searchCheck(approveFormEntity);
        //审核文件存在check
        requestApproveEntity = this.requestApproveService.searchFilePath(approveFormEntity);
        //用户存在check
        this.requestApproveService.searchUserCheck(requestApproveEntity);
        //登录者卡号
        requestApproveEntity.setLoginCode(requestApproveForm.getLoginCode());
        logger.info("searchFilePath#requestApproveService:{}",requestApproveEntity);
        //获取审核的文件的路径
        String filePath = requestApproveEntity.getFilePath();
        //获取签名位置和sheet
        RequestApproveEntity requestApproveEntity2 = this.requestApproveService
                .searchPosition(requestApproveEntity);
        //签名位置
        String reviewerPosition = requestApproveEntity2.getReviewerPosition();
        //签名sheet
        String reviewerSheet = requestApproveEntity2.getReviewerSheet();
        int sheetNo = Integer.parseInt(reviewerSheet);
        //获取图片路径
        String psPath =this.requestApproveService.searchPs(approveFormEntity);
        //String转换数组
        String str=reviewerPosition.substring(0, reviewerPosition.indexOf(","));
        String str2=reviewerPosition.substring(str.length()+1,reviewerPosition.length());
        int a =Integer.parseInt(str);
        int b =Integer.parseInt(str2);
        int [] coordinate = {a,b};
        //审核伦理存储表更新
        //登录者卡号
        requestApproveEntity.setLoginCode(requestApproveForm.getLoginCode());
        requestApproveEntity = this.requestApproveService.updateAppDate(requestApproveEntity);
        //文件路径 图片的路径 签字位置  第几sheet页 宽 高
        ExcelOperatingClass.ExcelInsertImage(filePath,psPath, coordinate,sheetNo, 82, 31);
        requestApproveForm.setInfoMessage(requestApproveEntity.getInfoMessage());
        logger.info("updateAppDate#requestApproveService:{}", requestApproveEntity);
        return requestApproveForm;
    }
    
    /**
     * 审核驳回
     * @param requestApproveForm 画面驳回理由
     * @return requestApproveForm flag
     * @throws Exception 
     */
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/api/requestApprove/approvedReject")
    @ResponseBody
    public RequestApproveForm approvedReject(@RequestBody RequestApproveForm requestApproveForm) 
            throws Exception{
        logger.info("approvedReject#RequestApproveForm:{}", requestApproveForm);
        //初期显示
        RequestApproveEntity approveFormEntity = this.copyApproveEntity(requestApproveForm);
        //画面数据查询
        approveFormEntity = this.requestApproveService.searchApprove(approveFormEntity);
        approveFormEntity.setLoginCode(requestApproveForm.getLoginCode());
        //审核驳回理由check
        String examineConfirReasons = requestApproveForm.getExamineConfirReasons();
        if(examineConfirReasons.equals("")) {
            throw new BusinessException("ERR0000001","驳回理由");
        }
        //获取签名sheet
        RequestApproveEntity requestSheetEntity = this.requestApproveService
                .searchPosition(approveFormEntity);
        //获取文件路径
        RequestApproveEntity requestFormEntity = this.copyApproveEntity(requestApproveForm);
        requestFormEntity = this.requestApproveService.searchFilePath(requestFormEntity);
        //申请编号
        requestFormEntity.setApplicationNumber(requestApproveForm.getApplicationNumber());
        //文件编号
        requestFormEntity.setFileId(requestSheetEntity.getFileId());
        //登录者卡号
        requestFormEntity.setLoginCode(requestApproveForm.getLoginCode());
        //申请者卡号
        requestFormEntity.setTjCode(requestApproveForm.getTjCode());
        //驳回理由
        requestFormEntity.setExamineConfirReasons(requestApproveForm.getExamineConfirReasons());
        //签名sheet
        requestFormEntity.setReviewerSheet( requestSheetEntity.getReviewerSheet());
        //审核伦理存储表更新
        requestFormEntity = this.requestApproveService.updateNotReviewed(requestFormEntity);
        logger.info("updateNotReviewed#requestApproveService:{}", requestFormEntity);
        requestApproveForm.setInfoMessage(requestFormEntity.getInfoMessage());
        return requestApproveForm;
    }
    
    /**
     * Form值保存 Entity用
     * @param requestApproveForm 设定值
     * @return requestApproveEntity
     */
    private RequestApproveEntity copyApproveEntity(RequestApproveForm requestApproveForm) {
        RequestApproveEntity requestApproveEntity = new RequestApproveEntity();
        //登录者卡号
        requestApproveEntity.setLoginCode(requestApproveForm.getLoginCode());
        //申请编号
        requestApproveEntity.setApplicationNumber(requestApproveForm.getApplicationNumber());
        return requestApproveEntity;
        
    }
    
}
