/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.requestApprove.service;

import java.io.File;
import java.io.IOException;
import java.util.Locale;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import tci.sds.approval.requestApprove.entity.RequestApproveEntity;
import tci.sds.approval.requestApprove.mapper.RequestApproveMapper;
import tci.sds.common.Mapper.SendEmailPasswordMapper;
import tci.sds.common.exception.BusinessException;
import tci.sds.common.unit.EamilOperatingClass;
import tci.sds.common.unit.ExcelOperatingClass;
import tci.sds.common.util.Constants;

/**
 * 审核Service
 * 
 * @author 李伟
 * @since 2020/11/20
 * @version 0.1
 */
@Service
public class RequestApproveService {
    private final Logger logger = LoggerFactory.getLogger(RequestApproveService.class);
    @Autowired
    private RequestApproveMapper requestApproveMapper;
    @Autowired
    private SendEmailPasswordMapper sendEmailPasswordMapper;
    @Autowired
    private MessageSource messageSource;
    
    //空格字符发邮件用
    private static final String UU = "\u3000\u3000";
    /**
     * 初期显示
     * 
     * @param approveEntity 申请编号
     * @return requestApproveEntity 所用信息
     */
    @Transactional(readOnly = true)
    public RequestApproveEntity searchApprove(RequestApproveEntity approveEntity) {
        logger.info("search#param:{}", approveEntity);
        RequestApproveEntity requestApproveEntity = this.requestApproveMapper.searchApprove(approveEntity);
        logger.info("search#requestApproveEntity", requestApproveEntity);
        return requestApproveEntity;

    }

    /**
     * 审核文件check
     * 
     * @param approveEntity
     * @return requestApproveEntity 文件路径
     * @throws Exception
     */
    @Transactional(readOnly = true)
    public RequestApproveEntity searchFilePath(RequestApproveEntity approveEntity) throws IOException {
        logger.info("searchFilePath#param:{}", approveEntity);
        RequestApproveEntity requestApproveEntity = this.requestApproveMapper.searchApprove(approveEntity);
        String filePaths = requestApproveEntity.getFilePath()+"\\"
        +approveEntity.getFileId();
        
        File files = new File(filePaths);
        //用来测试此路径名表示的文件或目录是否存在
        if(approveEntity.getFileId()!=null) {
            if (!files.exists()) {
                //不存在
                throw new BusinessException("ERR0000010");
               } 
        }
        // 文件路径
        String filePath = requestApproveEntity.getFilePath();
        // 审核文件存在check
        if (filePath != null) {
            File file = new File(filePath);
            // 用来测试此路径名表示的文件或目录是否存在
            if (file.exists()) {
                // 文件路径存在
                // 遍历文件夹下的文件路径
                File[] array = file.listFiles();
                for (int i = 0; i < array.length; i++) {
                    String fileNameString = array[i].getName();
                    if (!fileNameString.contains("附件")) {
                        requestApproveEntity.setFilePath(array[i].getAbsolutePath());
                    }
                }
            } else {
                // 文件路径不存在
                throw new BusinessException("ERR0000010");
            }
        } else {
            // 文件路径不存在
            throw new BusinessException("ERR0000010");
        }
        logger.info("searchFilePath#requestApproveEntity", requestApproveEntity);
        return requestApproveEntity;
    }

    /**
     * 申请编号存在check
     * @param requestApproveForm 申请编号和卡号
     */
    @Transactional(readOnly = true)
    public void searchCheck(RequestApproveEntity approveEntity) {
        logger.info("searchUser#RequestApproveEntity:{}", approveEntity);
        // 申请编号存在check
        String countApplication = this.requestApproveMapper.searchApplication(approveEntity);
        logger.info("searchApplication#requestApproveMapper:{}", countApplication);
        if (countApplication == null || countApplication.equals("")){
            throw new BusinessException("ERR0000009", "审批文件存储表", "申请编号");
        }
    }
    
    /**
     * 用户存在check
     * @param requestApproveForm 申请编号和卡号
     */
    @Transactional(readOnly = true)
    public void searchUserCheck(RequestApproveEntity approveEntity) {
        logger.info("searchUser#RequestApproveEntity:{}", approveEntity);
        String countUser = this.requestApproveMapper.searchUser(approveEntity);
        //用户存在check
        if (countUser == null || countUser.equals("")) {
            logger.info("searchUser#error:Record is not found.");
            throw new BusinessException("ERR0000009", "用户信息表","申请者");
        }
    }

    /**
     * 签名位置
     * 
     * @param Entity
     * @return 所用信息
     */
    @Transactional(readOnly = true)
    public RequestApproveEntity searchPosition(RequestApproveEntity approveEntity) {
        logger.info("searchPosition#RequestApproveEntity:{}", approveEntity);
        RequestApproveEntity reviewerPosition = this.requestApproveMapper.searchPosition(approveEntity);
        logger.info("searchPosition#requestApproveMapper:{}", reviewerPosition);
        return reviewerPosition;
    }

    /**
     * 图片路径check
     * 
     * @param approveListForm
     * @return 所用信息
     */
    @Transactional(readOnly = true)
    public String searchPs(RequestApproveEntity approveEntity) {
        logger.info("searchPs#RequestApproveEntity:{}", approveEntity);
        // 获取图片所在路径
        String reviewerPs = this.requestApproveMapper.searchPs(approveEntity);
        // 审核文件存在check
        if (reviewerPs != null) {
            File file = new File(reviewerPs);
            // 用来测试此路径名表示的文件或目录是否存在
            if (!file.exists()) {
                // 文件路径存在
                throw new BusinessException("ERR0000031");
            }
        } else {
            // 文件路径不存在
            throw new BusinessException("ERR0000031");
        }
        logger.info("searchPs#requestApproveMapper:{}", reviewerPs);
        return reviewerPs;
    }

    /**
     * 审核通过 审核伦理存储表更新
     * 
     * @param Entity
     * @return 所用信息
     * @throws Exception 
     */
    @Transactional(readOnly = false)
    public RequestApproveEntity updateAppDate(RequestApproveEntity approveEntity) throws Exception {
        boolean flag = false;
        logger.info("updateAppDate#RequestApproveEntity:{}", approveEntity);
        RequestApproveEntity infoMeaagEntity = new RequestApproveEntity();
        // 查询审核顺序
        infoMeaagEntity = this.requestApproveMapper.searchOrder(approveEntity);
        String reviewers = infoMeaagEntity.getReviewerStatus();
        if(reviewers.equals("STATUS_YI")) {
            throw new BusinessException("ERR0000038");
        }
        String reviewerOrder = infoMeaagEntity.getReviewerOrder();
        int a = 1;
        int parseInt = Integer.parseInt(reviewerOrder) + a;
        // 审核顺序转化
        String reviewerOrder1 = String.valueOf(parseInt);
        approveEntity.setReviewerOrder(reviewerOrder1);
        // 审核人状态：已审核PullDown取得
        String reviewerStatus = Constants.CD_STATUS_YI;
        approveEntity.setReviewerStatus(reviewerStatus);
        // 查询下一级审核人
        String firstReviewer = this.requestApproveMapper.searchReviewer(approveEntity);
        
        //邮箱密码
        String password = sendEmailPasswordMapper.searchPassword();
        if (firstReviewer == null || firstReviewer.equals("")) {
            // 审核完了
            // 审核状态：审核完了PullDown取得
            String examineStatus = Constants.CD_EXAMINE_OVER;
            approveEntity.setExamineStatusCode(examineStatus);
            int reviewerPs = this.requestApproveMapper.updateAppDate(approveEntity);
            logger.info("updateAppDate#requestApproveMapper:{}", reviewerPs);
            //最后一级审核人更新审核状态 完了
            if(reviewerPs>=1) {
                int exStatus = this.requestApproveMapper.updateAppDates(approveEntity);
                if(exStatus>=1) {
                  //审核文件存储表更新
                int applicationNo = this.requestApproveMapper.updateApprovalFile(approveEntity);
                if (applicationNo == 1) {
                    // 审核全部完成给确认人发邮件
                    String fileConfigId = approveEntity.getFileConfigId();
                    RequestApproveEntity configInfo = this.requestApproveMapper.searchApprovalEamil(fileConfigId);
                    //邮箱
                    String mailbox = configInfo.getPersonalMail();
                    
                    //文件名
                    String fileName= approveEntity.getFileId()+ " " +approveEntity.getFileName();
                    // 邮件内容
                    String mailInfo = configInfo.getUserName() 
                    +"桑\r\n" + "工作辛苦了，济南审批管理系统。\r\n\r\n"+UU+"名为【"+fileName+"】已审核完了，请您知晓。\r\n\r\n"+UU+"内网URL：http://172.17.0.30:8888/index.html\r\n"
                    +UU+"外网URL：http://60.216.7.187:9999"+"\r\n\r\n"+"以上，请多关照。\r\n"+"济南审批管理系统\r\n\r\n"+"该邮件为自动发送邮件，不需要回复。";        
                    EamilOperatingClass eamilOperatingClass = new EamilOperatingClass();
                    flag = eamilOperatingClass.sendTest("您确认的申请文件已审核完了。", mailbox,mailInfo,password,this.sendEmailPasswordMapper.getKosinTime());
                    if (!flag) {
                        // 获取InfoMessage信息
                        String infoMessage = this.getInfo("INF0000007");
                        infoMeaagEntity.setInfoMessage(infoMessage);
                    }
                 }
               }
            }
            
        } else {
            // 审核状态：待审核PullDown取得
            String examineStatus = Constants.CD_EXAMINE_WAIT;
            approveEntity.setExamineStatusCode(examineStatus);
            //更新审核状态
            int reviewerPs = this.requestApproveMapper.updateAppDate(approveEntity);
            logger.info("updateAppDate#requestApproveMapper:{}", reviewerPs);
            if (reviewerPs == 1) {
                // 给下一级审核人发邮件
                RequestApproveEntity configInfo = 
                        this.requestApproveMapper.searchApprovalEamil(firstReviewer);
                //文件名
                String fileName= approveEntity.getFileId()+ " " +approveEntity.getFileName();
                //邮件内容
                String mailInfo = configInfo.getUserName() 
                        +"桑\r\n" + "工作辛苦了，济南审批管理系统。\r\n\r\n"+UU+"名为【"+fileName+"】的申请文件请您审批。\r\n\r\n"
                        +UU+"内网URL：http://172.17.0.30:8888/index.html\r\n"
                        +UU+"外网URL：http://60.216.7.187:9999"+"\r\n\r\n"+"以上，请多关照。\r\n"+"济南审批管理系统\r\n\r\n"+"该邮件为自动发送邮件，不需要回复。";
                EamilOperatingClass eamilOperatingClass = new EamilOperatingClass();
                flag = eamilOperatingClass.sendTest("您有申请文件需要审批", 
                configInfo.getPersonalMail(),mailInfo,password,this.sendEmailPasswordMapper.getKosinTime());
                if (!flag) {
                    // 获取InfoMessage信息
                    String infoMessage = this.getInfo("INF0000007");
                    infoMeaagEntity.setInfoMessage(infoMessage);
                }
                flag = true;
            }
        }
        return infoMeaagEntity;
    }
    
    /**
     * Info信息获取
     * 
     * @param infoCode
     * @return message
     */
    public String getInfo(String infoCode) {
        logger.info("getInfo#param:{}", infoCode);
        String[] parStrings = new String[1];
        Locale locale = LocaleContextHolder.getLocale();
        String message = messageSource.getMessage(infoCode, parStrings, locale);
        logger.info("getInfo#result:{}", message);
        return message;
    }

    /**
     * 审核驳回 审核伦理存储表更新
     * 
     * @param RequestApproveForm
     * @return 所用信息
     * @throws Exception 
     */
    @Transactional(readOnly = false)
    public RequestApproveEntity updateNotReviewed(RequestApproveEntity approveEntity) throws Exception{
        logger.info("updateAppDate#RequestApproveEntity:{}", approveEntity);
        RequestApproveEntity infoMeaagEntity = new RequestApproveEntity();
        boolean flag = false;
        boolean flags=false;
        // 审核人状态：未审核 PullDown取得
        String reviewerStatus = Constants.CD_STATUS_WEI;
        approveEntity.setReviewerStatus(reviewerStatus);
        // 审核状态：审核驳回 PullDown取得
        String examineStatus = Constants.CD_EXAMINE_CONFIR;
        approveEntity.setExamineStatusCode(examineStatus);
        //邮箱密码
        String password = sendEmailPasswordMapper.searchPassword();
        int notReviewed = this.requestApproveMapper.updateNotReviewed(approveEntity);
        if (notReviewed >=1) {
            int reviewed = this.requestApproveMapper.updateReviewed(approveEntity);
            if(reviewed == 1) {
                // 查询审核顺序
                infoMeaagEntity= this.requestApproveMapper.searchOrder(approveEntity);
                //审核人顺序
                String reviewerOrder = infoMeaagEntity.getReviewerOrder();
                int parseInt = Integer.parseInt(reviewerOrder);
                if (parseInt > 1) {
                    //清空签名图片获取文件路径
                    String filePath = approveEntity.getFilePath();
                    //获取签名的sheet
                    //签名sheet
                    int sheetNo = Integer.parseInt(approveEntity.getReviewerSheet());
                    //删除签名图片
                    ExcelOperatingClass.ExcelDeleteImage(filePath,sheetNo);
                    //循环检索上级审核人
                    for (int i = parseInt-1; i > 0; i--) {
                        String order = String.valueOf(i);
                        approveEntity.setReviewerOrder(order);
                        // 检索上一审核人
                        String firstReviewer = this.requestApproveMapper.searchReviewer(approveEntity);
                        logger.info("clearSignature#requestApproveMapper:{}", firstReviewer);
                        approveEntity.setFirstReviewer(firstReviewer);
                        
                        //给上一审核人发邮件
                        RequestApproveEntity configInfo = 
                                this.requestApproveMapper.searchApprovalEamil(firstReviewer);
                        
                        //文件名
                        String fileName= approveEntity.getFileId()+ " " +approveEntity.getFileName();
                        //邮件内容
                        String mailInfo = configInfo.getUserName() 
                        +"桑\r\n" + "工作辛苦了，济南审批管理系统。\r\n\r\n"+UU+"您审核通过的文件【"+fileName+"】已被驳回。\r\n\r\n"+UU+"驳回理由为【"
                        +approveEntity.getExamineConfirReasons()+"】，请您知晓。。\r\n\r\n"+UU+"内网URL：http://172.17.0.30:8888/index.html\r\n"
                        +UU+"外网URL：http://60.216.7.187:9999"+"\r\n\r\n"+"以上，请多关照。\r\n"+"济南审批管理系统\r\n\r\n"+"该邮件为自动发送邮件，不需要回复。";
                        EamilOperatingClass eamilOperatingClass = new EamilOperatingClass();
                        flag = eamilOperatingClass.sendTest("您审核通过的申请文件被驳回", configInfo.getPersonalMail(),mailInfo,password,this.sendEmailPasswordMapper.getKosinTime());
                        if (!flag) {
                             flags=true;
                        }
                    }
                    //给确认人发邮件
                    String fileConfigId = approveEntity.getFileConfigId(); 
                    RequestApproveEntity configInfo= this.requestApproveMapper.searchApprovalEamil(fileConfigId);
                    
                    //文件名
                    String fileName= approveEntity.getFileId()+ " " +approveEntity.getFileName();
                    //邮件内容
                    String mailInfo = configInfo.getUserName()
                    +"桑\r\n" + "工作辛苦了，济南审批管理系统。\r\n\r\n"+UU+"您确认通过的文件【"+fileName+"】已被驳回。\r\n\r\n"+UU+"驳回理由为【"
                    +approveEntity.getExamineConfirReasons()+"】，请您知晓。\r\n\r\n"+UU+"内网URL：http://172.17.0.30:8888/index.html\r\n"
                    +UU+"外网URL：http://60.216.7.187:9999"+"\r\n\r\n"+"以上，请多关照。\r\n"+"济南审批管理系统\r\n\r\n"+"该邮件为自动发送邮件，不需要回复。";
                    EamilOperatingClass eamilOperatingClass = new EamilOperatingClass();
                    flag = eamilOperatingClass.sendTest("您确认通过的申请文件被驳回", configInfo.getPersonalMail(),mailInfo,password,this.sendEmailPasswordMapper.getKosinTime());
                    boolean flaga=false;
                    if (!flag) {
                        flaga=true;
                   }
                    //给申请人发邮件
                    String applicantId = approveEntity.getTjCode();
                    RequestApproveEntity configInfos = this.requestApproveMapper.searchApprovalEamil(applicantId);
                    
                    //邮件内容
                    String mailInfos = configInfos.getUserName()
                    +"桑\r\n" + "工作辛苦了，济南审批管理系统。\r\n\r\n"+UU+"您申请的名为【"+fileName+"】的申请文件已被驳回，\r\n\r\n"+UU+"驳回理由为【"
                    +approveEntity.getExamineConfirReasons()+"】，请登录审批管理系统进行修改。\r\n\r\n"+UU+"内网URL：http://172.17.0.30:8888/index.html\r\n"
                    +UU+"外网URL：http://60.216.7.187:9999"+"\r\n\r\n"+"以上，请多关照。\r\n"+"济南审批管理系统\r\n\r\n"+"该邮件为自动发送邮件，不需要回复。";
                    EamilOperatingClass eamilApplicant = new EamilOperatingClass();
                    flag = eamilApplicant.sendTest("您提交的申请文件被驳回", configInfos.getPersonalMail(),mailInfos,password,this.sendEmailPasswordMapper.getKosinTime());
                    boolean flagc=false;
                    if (!flag) {
                        flagc = true;
                    }
                    if(flags||flaga||flagc ) {
                       // 获取InfoMessage信息
                       String infoMessage = this.getInfo("INF0000007");
                       infoMeaagEntity.setInfoMessage(infoMessage); 
                    }
                }else {
                    //清空签名图片获取文件路径
                    String filePath = approveEntity.getFilePath();
                    //获取签名的sheet
                    //签名sheet
                    int sheetNo = Integer.parseInt(approveEntity.getReviewerSheet());
                    //删除签名图片
                    ExcelOperatingClass.ExcelDeleteImage(filePath,sheetNo);
                    //给确认人发邮件
                    String fileConfigId = approveEntity.getFileConfigId(); 
                    RequestApproveEntity configInfo= this.requestApproveMapper.searchApprovalEamil(fileConfigId);
                    
                    //文件名
                    String fileName= approveEntity.getFileId()+ " " +approveEntity.getFileName();
                    //邮件内容
                    String mailInfo = configInfo.getUserName()
                    +"桑\r\n" + "工作辛苦了，济南审批管理系统。\r\n\r\n"+UU+"您确认通过的文件【"+fileName+"】已被驳回。\r\n\r\n"+UU+"驳回理由为【"
                    +approveEntity.getExamineConfirReasons()+"】，请您知晓。\r\n\r\n"+UU+"内网URL：http://172.17.0.30:8888/index.html\r\n"
                    +UU+"外网URL：http://60.216.7.187:9999"+"\r\n\r\n"+"以上，请多关照。\r\n"+"济南审批管理系统\r\n\r\n"+"该邮件为自动发送邮件，不需要回复。";
                    EamilOperatingClass eamilOperatingClass = new EamilOperatingClass();
                    eamilOperatingClass.sendTest("您确认通过的申请文件被驳回", configInfo.getPersonalMail(),mailInfo,password,this.sendEmailPasswordMapper.getKosinTime());
                    
                    //给申请人发邮件
                    String applicantId = approveEntity.getTjCode();
                    RequestApproveEntity configInfos = this.requestApproveMapper.searchApprovalEamil(applicantId);
                    //邮件内容
                    String mailInfos = configInfos.getUserName()
                    +"桑\r\n" + "工作辛苦了，济南审批管理系统。\r\n\r\n"+UU+"您申请的名为【"+fileName+"】的申请文件已被驳回，\r\n\r\n"+UU+"驳回理由为【"
                    +approveEntity.getExamineConfirReasons()+"】，请登录审批管理系统进行修改。\r\n\r\n"+UU+"内网URL：http://172.17.0.30:8888/index.html\r\n"
                    +UU+"外网URL：http://60.216.7.187:9999"+"\r\n\r\n"+"以上，请多关照。\r\n"+"济南审批管理系统\r\n\r\n"+"该邮件为自动发送邮件，不需要回复。";
                    EamilOperatingClass eamilApplicant = new EamilOperatingClass();
                    flag = eamilApplicant.sendTest("您提交的申请文件被驳回", configInfos.getPersonalMail(),mailInfos,password,this.sendEmailPasswordMapper.getKosinTime());
                    if (!flag) {
                        // 获取InfoMessage信息
                        String infoMessage = this.getInfo("INF0000007");
                        infoMeaagEntity.setInfoMessage(infoMessage);
                    }
                }
                flag = true;
            }
           flag = true;
        }
        return infoMeaagEntity;
    }
}
