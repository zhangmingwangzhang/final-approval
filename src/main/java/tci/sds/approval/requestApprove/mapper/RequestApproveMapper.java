/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.requestApprove.mapper;

import org.apache.ibatis.annotations.Mapper;
import tci.sds.approval.requestApprove.entity.RequestApproveEntity;

/**
 * 审核Mapper
 * @author 李伟
 * @since 2020/11/20
 * @version 0.1
 */
@Mapper
public interface RequestApproveMapper {
    
    /**
     * 初期显示
     * @return RequestApproveEntity 页面数据显示
     */
    RequestApproveEntity searchApprove(RequestApproveEntity approveEntity);
    
    /**
     * 用户check
     * @return tjCode 天津卡号
     */
    String searchUser(RequestApproveEntity approveEntity);
    
    /**
     * 申请编号check
     * @return 申请编号
     */
    String searchApplication(RequestApproveEntity approveEntity);
    
    /**
     * 检索签名位置
     * @return searchPosition 签名位置
     */
    RequestApproveEntity searchPosition(RequestApproveEntity approveEntity);
    
    /**
     * 检索图片路径
     * @return searchPosition 图片路径
     */
    String searchPs(RequestApproveEntity approveEntity);
    
    /**
     * 审核通过 审核伦理存储表更新
     * @return int 更新条数
     */
    int updateAppDate(RequestApproveEntity approveEntity);
    
    /**
     * 审核通过最后一级审核人 审核伦理存储表更新
     * @return int 更新条数
     */
    int updateAppDates(RequestApproveEntity approveEntity);
    
    /**
     * 审核文件存储表更新
     * @return int 更新条数
     */
    int updateApprovalFile(RequestApproveEntity approveEntity);
    
    /**
     * 审核伦理存储表更新 审核顺序查询
     * @return  searchOrder 审核顺序
     */
    RequestApproveEntity searchOrder(RequestApproveEntity approveEntity);
    
    /**
     * 审核伦理存储表更新 查询审核人
     * @return firstReviewer 审核人
     */
    String searchReviewer(RequestApproveEntity approveEntity);
    
    /**
     * 查询邮箱
     * @return  searchApprovalEamil 邮箱
     */
    RequestApproveEntity searchApprovalEamil(String code);
    
    /**
     * 审核驳回 审核伦理存储表更新
     * @return int 更新条数
     */
    int updateNotReviewed(RequestApproveEntity approveEntity);
    
    /**
     * 审核驳回 审核文件存储表更新
     * @return int 更新条数
     */
    int updateReviewed(RequestApproveEntity approveEntity);
}
