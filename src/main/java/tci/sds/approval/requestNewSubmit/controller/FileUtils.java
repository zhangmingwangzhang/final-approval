/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.requestNewSubmit.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * 下载工具类
 * 
 * @author 李伟
 * @since 2020/11/09
 * @version 0.1
 */
public class FileUtils {
    /**
     * 下载指定路径下的文件
     * @param response
     * @return 文件
     * @throws IOException
     * @filePath 文件路径
     */
    public void  downloadFile (HttpServletRequest request,HttpServletResponse response,String filePath)
          {
           try {
               File file=new File(filePath);
                  if(file.exists()){
                      String fileName  = file.getName().toString();
                      // firefox浏览器
                      if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {
                          fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
                      } // IE浏览器
                      else if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0) {
                          fileName = URLEncoder.encode(fileName, "UTF-8");
                      }// 谷歌
                      else if (request.getHeader("User-Agent").toUpperCase().indexOf("CHROME") > 0) {
                          fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
                      }
                      //首先设置响应的内容格式是force-download，那么你一旦点击下载按钮就会自动下载文件了
                      response.setContentType("application/force-download");
                      //通过设置头信息给文件命名，也即是，在前端，文件流被接受完还原成原文件的时候会以你传递的文件名来命名
                      response.addHeader("Content-Disposition","attachment;filename="+ fileName );
                      response.setCharacterEncoding("UTF-8");
                      //进行读写操作
                      byte[]buffer=new byte[1024];
                      FileInputStream fis=null;
                      BufferedInputStream bis=null;
                      try{
                          fis=new FileInputStream(file);
                          bis=new BufferedInputStream(fis);
                          OutputStream os=response.getOutputStream();
                          //从源文件中读
                          int i=bis.read(buffer);
                          while(i!=-1){
                              //写到response的输出流中
                              os.write(buffer,0,i);
                              i=bis.read(buffer);
                          }
                      }catch (IOException e){
                          e.printStackTrace();
                      }
 
                  }
              } catch (UnsupportedEncodingException e) {
                  e.printStackTrace();
              }
          }
}
