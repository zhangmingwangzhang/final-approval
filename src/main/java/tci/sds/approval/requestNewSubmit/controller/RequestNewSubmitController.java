/*
 *  审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.requestNewSubmit.controller;
import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


import tci.sds.approval.requestNewSubmit.entity.RequestNewSubmitEntity;
import tci.sds.approval.requestNewSubmit.form.RequestNewSubmitForm;
import tci.sds.approval.requestNewSubmit.service.RequestNewSubmitService;
import tci.sds.common.entity.ConfigProperties;
import tci.sds.common.entity.FileTypeEnum;
import tci.sds.common.exception.BusinessException;
import tci.sds.common.service.InfoMessageService;
import tci.sds.common.service.SendEmailPasswordService;
import tci.sds.common.unit.EamilOperatingClass;
import tci.sds.common.unit.ExcelOperatingClass;
import tci.sds.common.util.Constants;

/**
 * Controller
 *
 * @author 李伟
 * @since 2020/11/09
 * @version 0.1
 */
@Controller
public class RequestNewSubmitController {
    private final Logger logger = LoggerFactory
            .getLogger(RequestNewSubmitController.class);

    @Autowired
    private RequestNewSubmitService requestNewSubmitService;
    @Autowired
    private SendEmailPasswordService sendEmailPasswordService;
    @Autowired
    private InfoMessageService infoMessageService;

    private static ConfigProperties configProperties;
    @Autowired
    public void setConfig(ConfigProperties config) {
        RequestNewSubmitController.configProperties = config;
    }



    /**
     * 初期显示 检索文件名和用户信息
     * @param requestNewSubmitForm 申请画面登录者信息
     * @return 申请页面显示Form
     * @exception ParseException error的场合
     */
    @RequestMapping("/api/requestNewSubmit/searchData")
    @ResponseBody
    public RequestNewSubmitForm fileNameSearch( @ModelAttribute
        RequestNewSubmitForm requestNewSubmitForm) throws Exception {
        //拦截器
        HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.
                getRequestAttributes()).getRequest();
        HttpSession session = request.getSession();
        session.setAttribute("userName", "");
        // 文件名检索
        List<RequestNewSubmitEntity> list =this.requestNewSubmitService.fileNameSearch();
        // 申请者信息检索
        RequestNewSubmitEntity requestNewSubmitEntity = new  RequestNewSubmitEntity();
        RequestNewSubmitForm outputForm = new RequestNewSubmitForm();
        //天津卡号
        requestNewSubmitEntity.setTjCode(requestNewSubmitForm.getLoginCode());
        //登录者信息
        requestNewSubmitEntity = this.requestNewSubmitService.searchUser(requestNewSubmitEntity);
        //登录者名字
        outputForm.setUserName(requestNewSubmitEntity.getUserName());
        //登录者卡号
        outputForm.setLoginCode(requestNewSubmitEntity.getTjCode());
        //登录者部门
        outputForm.setDeptId(requestNewSubmitEntity.getDeptId());
        //部门名
        outputForm.setDeptName(requestNewSubmitEntity.getDeptName());
        //文件list保存
        outputForm.setFileIdList(list);
        logger.info("fileNameSearch#param:{}",outputForm);
        return outputForm;
    }

/*检索*/
    @ResponseBody
    @PostMapping("/api/requestNewSubmit/showby")
    public List<RequestNewSubmitEntity> search(@RequestBody RequestNewSubmitEntity requestNewSubmitEntity){

        List<RequestNewSubmitEntity> list1 = requestNewSubmitService.showby(requestNewSubmitEntity);

        return list1;
    }

    /**
     * 一级级联列表查询显示 文件名和code
     * @return requestNewSubmitForm 申请页面下拉一级显示
     * @exception ParseException error的场合
     */
    @RequestMapping("/api/requestNewSubmit/businessSearch")
    @ResponseBody
    public RequestNewSubmitForm businessList() throws Exception {
        logger.info("businessList#param:{}");
        // 业务范围
        List<RequestNewSubmitEntity> codelist = this.requestNewSubmitService.codeOneSearch();
        // 返回接收
        RequestNewSubmitForm requestNewSubmitForm = new RequestNewSubmitForm();
        //下拉list保存
        requestNewSubmitForm.setBusinessList(codelist);
        return requestNewSubmitForm;
    }

    /**
     * 二级级联列表查询显示 文件名和code
     * @param codeForm 下拉一级code
     * @return 申请页面下拉二级显示
     */
    @RequestMapping("/api/requestNewSubmit/businessSearch2")
    @ResponseBody
    public RequestNewSubmitForm businessList2(@ModelAttribute
            RequestNewSubmitForm codeForm) throws Exception {
        logger.info("businessList2#param:{}",codeForm);
        RequestNewSubmitEntity newSubmitExEntity = jiOneCode(codeForm);
        List<RequestNewSubmitEntity> codelist = this.
        requestNewSubmitService.codeTwoSearch(newSubmitExEntity);
        // 返回接收
        RequestNewSubmitForm requestNewSubmitForm = new RequestNewSubmitForm();
        //下拉list保存
        requestNewSubmitForm.setBusinessList(codelist);
        logger.info("businessList#param:{}",requestNewSubmitForm);
        return requestNewSubmitForm;
    }

    /**
     * 三级级联列表查询显示 文件名和code
     * @param codeForm 下拉二级code
     * @return 申请页面下拉三级显示
     */
    @RequestMapping("/api/requestNewSubmit/businessSearch3")
    @ResponseBody
    public RequestNewSubmitForm businessList3(@ModelAttribute
        RequestNewSubmitForm codeForm) throws Exception {
        logger.info("businessList3#param:{}",codeForm);
        RequestNewSubmitEntity newSubmitExEntity = jiTwoCode(codeForm);
        List<RequestNewSubmitEntity> codelist = this.
        requestNewSubmitService.codeThreeSearch(newSubmitExEntity);
        // 返回接收
        RequestNewSubmitForm requestNewSubmitForm = new RequestNewSubmitForm();
        logger.info("businessList3#param:{}",requestNewSubmitForm);
        //下拉list保存
        requestNewSubmitForm.setBusinessList(codelist);
        return requestNewSubmitForm;
    }

    /**
     * 模板下载 查询模板文件路径
     * @param fileForm
     * @return outputForm
     */
    @PostMapping("/api/templateDownload/tpldownLoad")
    @ResponseBody
    public RequestNewSubmitForm downloadFile1(@ModelAttribute
        RequestNewSubmitForm fileForm)
        throws Exception {
        logger.info("downloadFile1#param:{}", fileForm);
        RequestNewSubmitEntity entStock = copyFormToEntity(fileForm);
        String filePath = this.requestNewSubmitService.searchList(entStock);
        RequestNewSubmitForm outputForm = new RequestNewSubmitForm();
        //文件list保存
        outputForm.setFilePath(filePath);
        return outputForm;
    }

    /**
     * 调用工具类进行io流形式下载
     * @param request 请求
     * @param response 响应
     * @exception UnsupportedEncodingException
     */
    @RequestMapping(value = "/api/templateDownload/fileIdDown",method = {RequestMethod.GET})
    @ResponseBody
    public void downloadFile(HttpServletRequest request, HttpServletResponse response)
        throws UnsupportedEncodingException {
        // filePath 前台传来的 文件路径
        String filePaths=request.getParameter("filePath");
        // 调用工具类
        FileUtils fileUtils = new FileUtils();
        fileUtils.downloadFile(request,response, filePaths);
   }

    /**
     * 上传文件和确认处理
     * @param request 请求
     * @param requestNewSubmitForm requestNewSubmitForm 設定値
     * @return checkFlag
     */
    @Transactional
    @RequestMapping("/api/requestNewSubmit/confirm")
    @ResponseBody
    public RequestNewSubmitForm confirm(HttpServletRequest request , @ModelAttribute RequestNewSubmitForm
            requestNewSubmitForm) throws Exception{
        logger.info("confirm#param:{}", requestNewSubmitForm);
        //空格字符发邮件用
        String uu = "\u3000\u3000";
        RequestNewSubmitForm requestForm = new RequestNewSubmitForm();
        RequestNewSubmitEntity requestNewSubmitExEntity = new RequestNewSubmitEntity();
        // 申请编号的查询
        String stockNo =  this.requestNewSubmitService.selectNo();
        requestNewSubmitExEntity.setApplicationNumber(stockNo);
        int size = 0;
        int conut=0;
        try {
            // 单项目check
            this.confirmCode(requestNewSubmitForm);
            //进行上传文件check
            //判断文件上传的个数
            if(requestNewSubmitForm.getFile().length > 1) {
                //循环判断文件list
                for (int i = 0; i < requestNewSubmitForm.getFile().length; i++) {
                    //获取文件名
                    String packageName = requestNewSubmitForm.getFile()[i].getOriginalFilename();
                    //附件加1
                    if(!packageName.contains("附件")) {
                        conut ++;
                    }
                }
                //附件不为1的情况
                if(conut!=1) {
                    throw new BusinessException("ERR0000040");
                  }
            //一个文件情况进行check不能为附件
            }else if(requestNewSubmitForm.getFile()[0].getOriginalFilename().contains("附件")){
                    throw new BusinessException("ERR0000041");
            }
             //文件名称code
             requestNewSubmitExEntity.setFileName(requestNewSubmitForm.getOneCode());
             //申请文件check
             this.requestNewSubmitService.fileCheck(requestNewSubmitExEntity);
             // 查询主表的确认人
             RequestNewSubmitEntity fileConfigIdEntity = this.requestNewSubmitService.
                   searchCongifId(requestNewSubmitExEntity);
             requestNewSubmitForm.setFileConfigId(fileConfigIdEntity.getFileConfigId());
             requestNewSubmitForm.setFileId(fileConfigIdEntity.getFileId());
             logger.info("searchCongifId#param:{}", fileConfigIdEntity);

             //审批文件存储表更新
             //文件编号
             requestNewSubmitExEntity.setFileId(requestNewSubmitForm.getFileId());
             //文件名称
             requestNewSubmitExEntity.setFileName(requestNewSubmitForm.getOneCode());
             //文件路径
             String filePath = configProperties.getExcelFileUrl()+stockNo+"\\";
             requestNewSubmitExEntity.setFilePath(filePath) ;
             //文件大小
             requestNewSubmitExEntity.setFileSize(size);
             //适应范围
             requestNewSubmitExEntity.setScopeApplication(requestNewSubmitForm.getTwoCode());
             //业务内容
             requestNewSubmitExEntity.setCondItion(requestNewSubmitForm.getThreeCode());
             //申请者卡号
             requestNewSubmitExEntity.setTjCode(requestNewSubmitForm.getLoginCode());
             //确认人
             requestNewSubmitExEntity.setFileConfigId(requestNewSubmitForm.getFileConfigId());

             // 审核状态：待确认PullDown取得
             String examineStatus = Constants.CD_REJECT_WAIT;
             requestNewSubmitExEntity.setExamineStatus(examineStatus);
             //作成者：登录者
             requestNewSubmitExEntity.setCreateName(requestNewSubmitForm.getUserName());
             //更新
             boolean res = this.requestNewSubmitService.insertApproval(requestNewSubmitExEntity);
             logger.info("insertApproval#param:{}", res);
        }catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw e;
        }
             //文件名
             String fileNames = this.requestNewSubmitService.searchFileName(requestNewSubmitExEntity.getFileName());
             //确认人邮件
             RequestNewSubmitEntity email = this.requestNewSubmitService.searchMail(requestNewSubmitExEntity);
             //邮箱密码
             String password = this.sendEmailPasswordService.selectPassword();
             //文件名
             String fileName = requestNewSubmitExEntity.getFileId() + " " + fileNames;
             //邮件内容
             String mailInfo = email.getUserName()+"桑\r\n" + "工作辛苦了，济南审批管理系统。\r\n\r\n"
             +uu+"名为【"+fileName+"】的申请文件请您确认。\r\n\r\n"+uu+"内网URL：http://172.17.0.30:8888/index.html\r\n"
             +uu+"外网URL：http://60.216.7.187:9999"+"\r\n\r\n"+"以上，请多关照。\r\n"+"济南审批管理系统\r\n\r\n"+"该邮件为自动发送邮件，不需要回复。";
             EamilOperatingClass eamilOperatingClass = new EamilOperatingClass();
             //   调用共同方法发送
             boolean flag = eamilOperatingClass.sendTest("您有申请文件需要确认",
             email.getPersonalMail(),mailInfo,password,this.sendEmailPasswordService.getKosinTime());
             if (!flag) {
               //获取邮件发送失败Info信息
               String infoMessage = infoMessageService.getInfo("INF0000007");
               requestForm.setInfoMessage(infoMessage);
             }
             //申请成功Info信息
             String successInfo = infoMessageService.getInfo("INF0000008");

             requestForm.setSuccessInFo(successInfo);
             //获取所有上传文件
             for (int i = 0; i < requestNewSubmitForm.getFile().length; i++) {
               //获取文件大小
               size=size + (int) requestNewSubmitForm.getFile()[i].getSize();
               InputStream in = requestNewSubmitForm.getFile()[i].getInputStream();
                 InputStream in2 = requestNewSubmitForm.getFile()[i].getInputStream();
               String packageName = requestNewSubmitForm.getFile()[i].getOriginalFilename();
               File tempFile = ExcelOperatingClass.saveTempFile(in, FileTypeEnum.FILE, packageName,request,stockNo);//将上传的文件保存到本地
                 //将文件保存，方便预览无水印和签名
                 File tempFile2 = ExcelOperatingClass.saveTempFile2(in2, FileTypeEnum.FILE, packageName,request,stockNo);
                 requestNewSubmitForm.setFilePath(tempFile.getParent());
             }
             requestForm.setCheckFlag(true);
             logger.info("Approval#param:{}", requestForm);
        return requestForm;
    }

    /**
     * 页面选择的模板文件
     * @param _inputForm 模板文件编号 名称 类型
     * @return newSubmitExEntity
     */
    public RequestNewSubmitEntity copyFormToEntity(RequestNewSubmitForm _inputForm)
            throws Exception {
        RequestNewSubmitEntity newSubmitExEntity = new RequestNewSubmitEntity();
        // 文件名
        newSubmitExEntity.setFileId(_inputForm.getFileId());
        // 文件类别
        newSubmitExEntity.setFileAbstract(_inputForm.getFileAbstract());
        // 文件名称
        newSubmitExEntity.setaFileName(_inputForm.getaFileName());
        return newSubmitExEntity;
    }

    /**
     * 一级code Entity保存
     * @param codeForm 一级code
     * @return newSubmitExEntity code值
     */
    public RequestNewSubmitEntity jiOneCode(RequestNewSubmitForm codeForm) throws Exception {
        RequestNewSubmitEntity newSubmitExEntity = new RequestNewSubmitEntity();
        // 一级code
        newSubmitExEntity.setCdCode(codeForm.getOneCode());
        return newSubmitExEntity;
    }

    /**
     * 二级code Entity保存
     * @param codeForm 二级code
     * @return newSubmitExEntity code值
     */
    public RequestNewSubmitEntity jiTwoCode(RequestNewSubmitForm codeForm) throws Exception {
        RequestNewSubmitEntity newSubmitExEntity = new RequestNewSubmitEntity();
        // 二级code
        newSubmitExEntity.setOneCode(codeForm.getOneCode());
        newSubmitExEntity.setTwoCode(codeForm.getTwoCode());
        return newSubmitExEntity;
    }

    /**
     * 单项目Check
     * @param requestNewSubmitForm 下拉値
     * @exception ParseException error的场合
     */
    public  void confirmCode( RequestNewSubmitForm requestNewSubmitForm)
            throws ParseException {
        if(requestNewSubmitForm.getOneCode().equals("")||
                requestNewSubmitForm.getOneCode().equals(null)) {
            //文件名check
            throw new BusinessException("ERR0000001","文件名");
        }else if(requestNewSubmitForm.getTwoCode().equals("")||
                requestNewSubmitForm.getTwoCode().equals(null)) {
            //范围check
            throw new BusinessException("ERR0000001","范围");
        }else if(requestNewSubmitForm.getThreeCode().equals("")||
                requestNewSubmitForm.getThreeCode().equals(null)) {
            //业务内容check
            throw new BusinessException("ERR0000001","业务内容");
        }else if(requestNewSubmitForm.getFile() == null){
            //上传的文件check
            throw new BusinessException("ERR0000001","上传文件");
        }
    }
}
