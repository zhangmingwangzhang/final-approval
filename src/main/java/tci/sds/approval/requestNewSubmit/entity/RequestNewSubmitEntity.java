/*
 *  审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.requestNewSubmit.entity;

import java.time.LocalDateTime;
/**
 * Entity
 * @author 李伟
 * @since 2020/11/09
 * @version 0.1
 */
public class RequestNewSubmitEntity {
    //申请者
    private String userName;
    //申请编号
    private String applicationNumber;
    //卡号
    private String tjCode;
    //条件文件名
    private String kjName;
    //下拉code
    private String cdCode;
    //一级code
    private String oneCode;
    //二级code
    private String twoCode;
    //业务范围
    private String scopeApplication;
    //业务内容
    private String condItion;
    //部门编号
    private String deptId;
    //部门名称
    private String deptName;
    //文件编号
    private String fileId;
    //文件名称
    private String fileName;
    //文件名称code
    private String aFileName;
    //文件所在的路径
    private String filePath;
    //文件类别
    private String fileAbstract;
    //文件大小
    private int fileSize;
    //作成者
    private String createName;
    //作成日时
    private LocalDateTime createTime;
    //更新者
    private String koshinName;
    //更新日时
    private LocalDateTime koshinTime;
    //确认驳回理由
    private String rejectConfirReasons;
    //审核驳回理由
    private String examineConfirReasons;
    //审核状态
    private String examineStatus;
    //确认人
    private String fileConfigId;
    //审核人
    private String reviewer;
    //审核顺序
    private String reviewerOrder;
    //审核人状态
    private String reviewerStatus;
    //邮箱
    private String personalMail;
    //Info文字 
    private String infoMessage;
    
    /**
     * @return the infoMessage
     */
    public String getInfoMessage() {
        return infoMessage;
    }
    /**
     * @param infoMessage the infoMessage to set
     */
    public void setInfoMessage(String infoMessage) {
        this.infoMessage = infoMessage;
    }
    /**
     * @return the deptName
     */
    public String getDeptName() {
        return deptName;
    }
    /**
     * @param deptName the deptName to set
     */
    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }
    /**
     * @return the fileId
     */
    public String getFileId() {
        return fileId;
    }
    /**
     * @param fileId the fileId to set
     */
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }
    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }
    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
    /**
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }
    /**
     * @param filePath the filePath to set
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    /**
     * @return the fileAbstract
     */
    public String getFileAbstract() {
        return fileAbstract;
    }
    /**
     * @param fileAbstract the fileAbstract to set
     */
    public void setFileAbstract(String fileAbstract) {
        this.fileAbstract = fileAbstract;
    }
    
    
    /**
     * @return the fileSize
     */
    public int getFileSize() {
        return fileSize;
    }
    /**
     * @param fileSize the fileSize to set
     */
    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }
    /**
     * @return the createName
     */
    public String getCreateName() {
        return createName;
    }
    /**
     * @param createName the createName to set
     */
    public void setCreateName(String createName) {
        this.createName = createName;
    }
    /**
     * @return the createTime
     */
    public LocalDateTime getCreateTime() {
        return createTime;
    }
    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
    /**
     * @return the koshinName
     */
    public String getKoshinName() {
        return koshinName;
    }
    /**
     * @param koshinName the koshinName to set
     */
    public void setKoshinName(String koshinName) {
        this.koshinName = koshinName;
    }
    /**
     * @return the koshinTime
     */
    public LocalDateTime getKoshinTime() {
        return koshinTime;
    }
    /**
     * @param koshinTime the koshinTime to set
     */
    public void setKoshinTime(LocalDateTime koshinTime) {
        this.koshinTime = koshinTime;
    }
    
    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }
    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }
    /**
     * @return the tjCode
     */
    public String getTjCode() {
        return tjCode;
    }
    /**
     * @param tjCode the tjCode to set
     */
    public void setTjCode(String tjCode) {
        this.tjCode = tjCode;
    }
    
    /**
     * @return the condItion
     */
    public String getCondItion() {
        return condItion;
    }
    /**
     * @param condItion the condItion to set
     */
    public void setCondItion(String condItion) {
        this.condItion = condItion;
    }
    /**
     * @return the scopeApplication
     */
    public String getScopeApplication() {
        return scopeApplication;
    }
    /**
     * @param scopeApplication the scopeApplication to set
     */
    public void setScopeApplication(String scopeApplication) {
        this.scopeApplication = scopeApplication;
    }
    
    /**
     * @return the kjName
     */
    public String getKjName() {
        return kjName;
    }
    /**
     * @param kjName the kjName to set
     */
    public void setKjName(String kjName) {
        this.kjName = kjName;
    }
    
    /**
     * @return the applicationNumber
     */
    public String getApplicationNumber() {
        return applicationNumber;
    }
    /**
     * @param applicationNumber the applicationNumber to set
     */
    public void setApplicationNumber(String applicationNumber) {
        this.applicationNumber = applicationNumber;
    }
    /**
     * @return the deptId
     */
    public String getDeptId() {
        return deptId;
    }
    /**
     * @param deptId the deptId to set
     */
    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }
    /**
     * @return the rejectConfirReasons
     */
    public String getRejectConfirReasons() {
        return rejectConfirReasons;
    }
    /**
     * @param rejectConfirReasons the rejectConfirReasons to set
     */
    public void setRejectConfirReasons(String rejectConfirReasons) {
        this.rejectConfirReasons = rejectConfirReasons;
    }
    /**
     * @return the examineConfirReasons
     */
    public String getExamineConfirReasons() {
        return examineConfirReasons;
    }
    /**
     * @param examineConfirReasons the examineConfirReasons to set
     */
    public void setExamineConfirReasons(String examineConfirReasons) {
        this.examineConfirReasons = examineConfirReasons;
    }
    /**
     * @return the examineStatus
     */
    public String getExamineStatus() {
        return examineStatus;
    }
    /**
     * @param examineStatus the examineStatus to set
     */
    public void setExamineStatus(String examineStatus) {
        this.examineStatus = examineStatus;
    }
    /**
     * @return the fileConfigId
     */
    public String getFileConfigId() {
        return fileConfigId;
    }
    /**
     * @param fileConfigId the fileConfigId to set
     */
    public void setFileConfigId(String fileConfigId) {
        this.fileConfigId = fileConfigId;
    }
    
    /**
     * @return the reviewerStatus
     */
    public String getReviewerStatus() {
        return reviewerStatus;
    }
    /**
     * @param reviewerStatus the reviewerStatus to set
     */
    public void setReviewerStatus(String reviewerStatus) {
        this.reviewerStatus = reviewerStatus;
    }
    /**
     * @return the reviewer
     */
    public String getReviewer() {
        return reviewer;
    }
    /**
     * @param reviewer the reviewer to set
     */
    public void setReviewer(String reviewer) {
        this.reviewer = reviewer;
    }
    /**
     * @return the reviewerOrder
     */
    public String getReviewerOrder() {
        return reviewerOrder;
    }
    /**
     * @param reviewerOrder the reviewerOrder to set
     */
    public void setReviewerOrder(String reviewerOrder) {
        this.reviewerOrder = reviewerOrder;
    }
    /**
     * @return the aFileName
     */
    public String getaFileName() {
        return aFileName;
    }
    /**
     * @param aFileName the aFileName to set
     */
    public void setaFileName(String aFileName) {
        this.aFileName = aFileName;
    }
    /**
     * @return the oneCode
     */
    public String getOneCode() {
        return oneCode;
    }
    /**
     * @param oneCode the oneCode to set
     */
    public void setOneCode(String oneCode) {
        this.oneCode = oneCode;
    }
    /**
     * @return the twoCode
     */
    public String getTwoCode() {
        return twoCode;
    }
    /**
     * @param twoCode the twoCode to set
     */
    public void setTwoCode(String twoCode) {
        this.twoCode = twoCode;
    }
    
    /**
     * @return the cdCode
     */
    public String getCdCode() {
        return cdCode;
    }
    /**
     * @param cdCode the cdCode to set
     */
    public void setCdCode(String cdCode) {
        this.cdCode = cdCode;
    }
    
    /**
     * @return the personalMail
     */
    public String getPersonalMail() {
        return personalMail;
    }
    /**
     * @param personalMail the personalMail to set
     */
    public void setPersonalMail(String personalMail) {
        this.personalMail = personalMail;
    }
    @Override
    public String toString() {
        return "RequestNewSubmitEntity [userName=" 
                + userName + ", applicationNumber=" 
                + applicationNumber + ", tjCode="
                + tjCode + ", kjName=" + kjName + ", cdCode=" 
                + cdCode + ", oneCode=" + oneCode 
                + ", twoCode=" + twoCode
                + ", scopeApplication=" + scopeApplication 
                + ", condItion=" + condItion 
                + ", deptId=" + deptId
                + ", deptName=" + deptName 
                + ", fileId=" + fileId + ", fileName=" 
                + fileName + ", aFileName="
                + aFileName + ", filePath=" 
                + filePath + ", fileAbstract=" 
                + fileAbstract + ", fileSize=" + fileSize
                + ", createName=" + createName 
                + ", createTime=" + createTime 
                + ", koshinName=" + koshinName
                + ", koshinTime=" + koshinTime 
                + ", rejectConfirReasons=" + rejectConfirReasons
                + ", examineConfirReasons=" 
                + examineConfirReasons + ", examineStatus=" 
                + examineStatus
                + ", fileConfigId=" + fileConfigId 
                + ", reviewer=" + reviewer + ", reviewerOrder=" 
                + reviewerOrder
                + ", reviewerStatus=" + reviewerStatus 
                + ", personalMail=" + personalMail + ", infoMessage="
                + infoMessage + "]";
    }
    
    
}