/*
 *  审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.requestNewSubmit.mapper;

import java.util.List;
import tci.sds.approval.requestNewSubmit.entity.RequestNewSubmitEntity;

/**
 * mapper
 * 
 * @author 李伟
 * @since 2020/11/09
 * @version 0.1
 */
public interface RequestNewSubmitMapper  {
    
    /**
     * 文件名检索
     * @return 文件名
     */
    List<RequestNewSubmitEntity> searchFileNameList();

    /**
     * 文件名检索
     * @return 文件名
     */
    List<RequestNewSubmitEntity> showby(RequestNewSubmitEntity requestNewSubmitEntity);
    
    /**
     * 用户名检索
     * @return 用户信息
     */
    RequestNewSubmitEntity searchUser(RequestNewSubmitEntity requestNewSubmitEntity);
    
    /**
     * 文件路径检索
     * @return 文件路径
     */
    String searchFilePath(RequestNewSubmitEntity exEntity);
    
    /**
     *级联下拉一级下拉
     * @return 文件名
     */
    List<RequestNewSubmitEntity> searchBusiness();
    
    /**
     *级联下拉二级
     * @return 文件名
     */
    List<RequestNewSubmitEntity> searchBusiness2(RequestNewSubmitEntity cdCode);
    
    /**
     *级联下拉三级
     * @return 文件名
     */
    List<RequestNewSubmitEntity> searchBusiness3(RequestNewSubmitEntity cdCode);
    
    /**
     *查询文件名
     * @return 文件名
     */
    String searchFileName(String code);
    /**
     *添加数据 APPROVAL_FILE表
     * @return 文件名
     */
    int insertApproval(RequestNewSubmitEntity requestNewModifyEntity);
    
    /**
     *检索主表
     * @return 确认人
     */
    RequestNewSubmitEntity searchCongifId(RequestNewSubmitEntity requestNewModifyEntity);
    
    /**
     *申请番号
     * @return 
     */
    String selectNo();
    
    /**
     *查询主表
     * @return 审核人 审核顺序 确认人
     */
    List<RequestNewSubmitEntity> searchMains(RequestNewSubmitEntity requestNewModifyEntity);
    
    /**
     *审核人
     * @return 审核人天津卡号
     */
    String searchReviewerCode(RequestNewSubmitEntity requestNewModifyEntity);
    
    /**
     *更新伦理表
     *  
     */
    int insertApproalEthic(RequestNewSubmitEntity requestNewModifyEntity);
    
    /**
     *邮箱查询
     * @return 邮箱
     */
    RequestNewSubmitEntity searchMail(RequestNewSubmitEntity exEntity);

    /**
     *申请者 确认者check
     * @return 
     */
    int userCheck(String tjCode);
    
    /**
     *审核者check
     * @return 
     */
    int reviewerCheck(String reviewer, String deptId);
    
    /**
     *申请文件check
     * @return 
     */
    int fileCheck(RequestNewSubmitEntity requestNewModifyEntity);
    
}
