/*
 *  审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.requestNewSubmit.form;
import java.util.Arrays;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;
import tci.sds.approval.requestNewSubmit.entity.RequestNewSubmitEntity;
import tci.sds.common.form.BaseForm;
/**
 * Form
 * 
 * @author 李伟
 * @since 2020/11/10
 * @version 0.1
 */
public class RequestNewSubmitForm extends BaseForm {
    //登陆着卡号
    private String loginCode;
    //部门Id
    private String deptId;
    //部门名称
    private String deptName;
    //用户名
    private String userName;
    //选择的文件id
    private String fileId;
    //选择的文件类别
    private String fileAbstract;
    //选择的文件名称code
    private String aFileName;
    //选择的业务内容
    private String condItion;
    //选择的业务范围
    private String scopeApplication;
    //画面模板文件信息
    private List<RequestNewSubmitEntity> fileIdList;
    //级联下拉列表
    private List<RequestNewSubmitEntity> businessList;
    //文件路径
    private String filePath;
    //一级code
    private String oneCode;
    //二级code
    private String twoCode;
    //三级code
    private String threeCode;
    //多文件上传
    private MultipartFile[] file;
    //审核人状态
    private String reviewerStatus;
    //审核状态
    private String examineStatus;
    //作成者
    private String createName;
    //Flag
    private boolean checkFlag;
    //确认人
    private String fileConfigId;
    //Info文字 
    private String infoMessage;
    //成功Info信息
    private String successInFo;
    
    /**
     * @return the successInFo
     */
    public String getSuccessInFo() {
        return successInFo;
    }

    /**
     * @param successInFo the successInFo to set
     */
    public void setSuccessInFo(String successInFo) {
        this.successInFo = successInFo;
    }

    /**
     * @return the deptName
     */
    public String getDeptName() {
        return deptName;
    }

    /**
     * @return the infoMessage
     */
    public String getInfoMessage() {
        return infoMessage;
    }

    /**
     * @param infoMessage the infoMessage to set
     */
    public void setInfoMessage(String infoMessage) {
        this.infoMessage = infoMessage;
    }

    /**
     * @param deptName the deptName to set
     */
    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    /**
     * @return the aFileName
     */
    public String getaFileName() {
        return aFileName;
    }

    /**
     * @param aFileName the aFileName to set
     */
    public void setaFileName(String aFileName) {
        this.aFileName = aFileName;
    }

    /**
     * @return the deptId
     */
    public String getDeptId() {
        return deptId;
    }

    /**
     * @param deptId the deptId to set
     */
    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the loginCode
     */
    public String getLoginCode() {
        return loginCode;
    }

    /**
     * @param loginCode the loginCode to set
     */
    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }

    /**
     * @return the fileId
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * @param fileId the fileId to set
     */
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    /**
     * @return the fileAbstract
     */
    public String getFileAbstract() {
        return fileAbstract;
    }

    /**
     * @param fileAbstract the fileAbstract to set
     */
    public void setFileAbstract(String fileAbstract) {
        this.fileAbstract = fileAbstract;
    }

    /**
     * @return the condItion
     */
    public String getCondItion() {
        return condItion;
    }

    /**
     * @param condItion the condItion to set
     */
    public void setCondItion(String condItion) {
        this.condItion = condItion;
    }

    /**
     * @return the scopeApplication
     */
    public String getScopeApplication() {
        return scopeApplication;
    }

    /**
     * @param scopeApplication the scopeApplication to set
     */
    public void setScopeApplication(String scopeApplication) {
        this.scopeApplication = scopeApplication;
    }

    /**
     * @return the fileIdList
     */
    public List<RequestNewSubmitEntity> getFileIdList() {
        return fileIdList;
    }

    /**
     * @param fileIdList the fileIdList to set
     */
    public void setFileIdList(List<RequestNewSubmitEntity> fileIdList) {
        this.fileIdList = fileIdList;
    }

    /**
     * @return the businessList
     */
    public List<RequestNewSubmitEntity> getBusinessList() {
        return businessList;
    }

    /**
     * @param businessList the businessList to set
     */
    public void setBusinessList(List<RequestNewSubmitEntity> businessList) {
        this.businessList = businessList;
    }

    /**
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * @param filePath the filePath to set
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    /**
     * @return the oneCode
     */
    public String getOneCode() {
        return oneCode;
    }

    /**
     * @param oneCode the oneCode to set
     */
    public void setOneCode(String oneCode) {
        this.oneCode = oneCode;
    }

    /**
     * @return the twoCode
     */
    public String getTwoCode() {
        return twoCode;
    }

    /**
     * @param twoCode the twoCode to set
     */
    public void setTwoCode(String twoCode) {
        this.twoCode = twoCode;
    }

    

    /**
     * @return the threeCode
     */
    public String getThreeCode() {
        return threeCode;
    }

    /**
     * @param threeCode the threeCode to set
     */
    public void setThreeCode(String threeCode) {
        this.threeCode = threeCode;
    }

    /**
     * @return the file
     */
    public MultipartFile[] getFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(MultipartFile[] file) {
        this.file = file;
    }
    
    /**
     * @return the reviewerStatus
     */
    public String getReviewerStatus() {
        return reviewerStatus;
    }

    /**
     * @param reviewerStatus the reviewerStatus to set
     */
    public void setReviewerStatus(String reviewerStatus) {
        this.reviewerStatus = reviewerStatus;
    }

    /**
     * @return the examineStatus
     */
    public String getExamineStatus() {
        return examineStatus;
    }

    /**
     * @param examineStatus the examineStatus to set
     */
    public void setExamineStatus(String examineStatus) {
        this.examineStatus = examineStatus;
    }

    /**
     * @return the createName
     */
    public String getCreateName() {
        return createName;
    }

    /**
     * @param createName the createName to set
     */
    public void setCreateName(String createName) {
        this.createName = createName;
    }
    /**
     * @return the checkFlag
     */
    public boolean isCheckFlag() {
        return checkFlag;
    }

    /**
     * @param checkFlag the checkFlag to set
     */
    public void setCheckFlag(boolean checkFlag) {
        this.checkFlag = checkFlag;
    }

    /**
     * @return the fileConfigId
     */
    public String getFileConfigId() {
        return fileConfigId;
    }

    /**
     * @param fileConfigId the fileConfigId to set
     */
    public void setFileConfigId(String fileConfigId) {
        this.fileConfigId = fileConfigId;
    }

    @Override
    public String toString() {
        return "RequestNewSubmitForm [loginCode=" 
                + loginCode + ", deptId=" + deptId 
                + ", deptName=" + deptName
                + ", userName=" + userName + ", fileId=" 
                + fileId + ", fileAbstract=" + fileAbstract + ", aFileName="
                + aFileName + ", condItion=" + condItion 
                + ", scopeApplication=" + scopeApplication 
                + ", fileIdList="
                + fileIdList + ", businessList=" 
                + businessList + ", filePath=" + filePath 
                + ", oneCode=" + oneCode
                + ", twoCode=" + twoCode + ", threeCode=" 
                + threeCode + ", file=" + Arrays.toString(file)
                + ", reviewerStatus=" + reviewerStatus 
                + ", examineStatus=" + examineStatus + ", createName="
                + createName + ", checkFlag=" + checkFlag 
                + ", fileConfigId=" + fileConfigId + "]";
    }
   
}
