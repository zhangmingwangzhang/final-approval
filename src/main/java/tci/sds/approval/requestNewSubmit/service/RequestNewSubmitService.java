/*
 *  审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.requestNewSubmit.service;

import java.io.File;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tci.sds.approval.requestNewSubmit.entity.RequestNewSubmitEntity;
import tci.sds.approval.requestNewSubmit.mapper.RequestNewSubmitMapper;
import tci.sds.common.exception.BusinessException;
import tci.sds.common.util.Constants;

/**
 * Service
 * @author 李伟
 * @since 2020/11/09
 * @version 0.1
 */
@Service
public class RequestNewSubmitService {
    private final Logger logger = LoggerFactory.
            getLogger(RequestNewSubmitService.class);
    @Autowired
    private RequestNewSubmitMapper requestNewSubmitExMapper;
    
    /**
     * 检索文件名所用
     * @return list 画面文件列表
     */
    @Transactional(readOnly = true)
    public List<RequestNewSubmitEntity> fileNameSearch() {
        //下载文件列表
        List<RequestNewSubmitEntity> list = 
                requestNewSubmitExMapper.searchFileNameList();
        logger.info("fileNameSearch#result:{}", list);
        return list;
    }


    /**
     * 检索==
     * @return list 画面文件列表
     */
    @Transactional(readOnly = true)
    public List<RequestNewSubmitEntity> showby(RequestNewSubmitEntity requestNewSubmitEntity) {
        //下载文件列表
        List<RequestNewSubmitEntity> list1 = requestNewSubmitExMapper.showby(requestNewSubmitEntity);

        return list1;
    }


    /**
     * 检索登录者信息
     * @param requestNewSubmitForm 申请画面登录者卡号
     * @return userForm 登录者信息
     */
    @Transactional(readOnly = true)
    public RequestNewSubmitEntity searchUser(RequestNewSubmitEntity requestNewSubmitEntity) {
        //登录者信息
        RequestNewSubmitEntity userForm = 
                requestNewSubmitExMapper.searchUser(requestNewSubmitEntity);
        logger.info("searchUser#result:{}", userForm);
        return userForm;
    }
    
    /**
     * 检索文件路径
     * @param fileManageExEntity 
     * @return filePathss 文件路径
     */
    @Transactional(readOnly = true)
    public String searchList(RequestNewSubmitEntity fileManageExEntity) {
        logger.info("searchList#param:{}", fileManageExEntity);
        String filePaths = this.requestNewSubmitExMapper.
                searchFilePath(fileManageExEntity);
      //DBCheck
        if(filePaths!=null){
            File file = new File(filePaths);
            //用来测试此路径名表示的文件或目录是否存在
            if (!file.exists()) {
                //不存在
                throw new BusinessException("ERR0000010");
               }
       }else{
           //路径为null
           throw new BusinessException("ERR0000010");
       }
        return filePaths;
    }
    
    /**
     * 申请页面下拉
     * @return businessList 一级下拉code
     */
    @Transactional(readOnly = true)
    public List<RequestNewSubmitEntity> codeOneSearch() {
        logger.info("searchList#param:{}");
        //一级下拉code
        List<RequestNewSubmitEntity> businessList = 
                this.requestNewSubmitExMapper.searchBusiness();
        return businessList;
    }
    
    /**
     * 申请页面二级下拉
     * @param cdCode 一级code
     * @return businessList2 二级级下拉code
     */
    @Transactional(readOnly = true)
    public List<RequestNewSubmitEntity> codeTwoSearch(RequestNewSubmitEntity cdCode) {
        logger.info("searchList#param:{}", cdCode);
        //二级下拉code
        List<RequestNewSubmitEntity> businessList2 = 
                this.requestNewSubmitExMapper.searchBusiness2(cdCode);
        return businessList2;
    }
    
    /**
     * 申请页面三级下拉
     * @param cdCode 二级code
     * @return businessList2 三级下拉
     */
    @Transactional(readOnly = true)
    public List<RequestNewSubmitEntity> codeThreeSearch(RequestNewSubmitEntity cdCode) {
        logger.info("searchList#param:{}", cdCode);
        List<RequestNewSubmitEntity> businessList3 = 
                this.requestNewSubmitExMapper.searchBusiness3(cdCode);
        return businessList3;
    }
    /**
     * 查询文件名
     * @param cdCode code值
     * @return businessList2 文件名
     */
    @Transactional(readOnly = true)
    public String searchFileName(String cdCode) {
        logger.info("searchList#param:{}", cdCode);
        String fileNames = this.requestNewSubmitExMapper.searchFileName(cdCode);
        return fileNames;
    }
    
    
    /**
     * 更新审批文件存储表和创建审核伦理存储表
     * @param requestNewSubmitExEntity 更新所需信息
     * @return res 1
     */
    public boolean insertApproval(RequestNewSubmitEntity requestNewSubmitExEntity) throws Exception{
        boolean flag = false;
        //申请者check
        int userCheck = this.requestNewSubmitExMapper.userCheck(requestNewSubmitExEntity.getTjCode());
        if(userCheck == 0) {
            throw new BusinessException("ERR0000009","用户信息表","申请者");
        }
        //确认者check
        int configIdCheck = this.requestNewSubmitExMapper.userCheck(requestNewSubmitExEntity.getFileConfigId());
        if(configIdCheck == 0) {
            throw new BusinessException("ERR0000009","用户信息表","确认者");
        }
        //更新审批文件存储表
        int res = this.requestNewSubmitExMapper.insertApproval(requestNewSubmitExEntity);
        if(res == 1) {
            //根据文件名/范围/业务内容，这个条件，以及文件编号，可以检索出主表的审核人、审核顺序等信息。
            List<RequestNewSubmitEntity> mains = this.requestNewSubmitExMapper.
                    searchMains(requestNewSubmitExEntity);
            logger.info("searchMains#param:{}", mains);
            //循环遍历 审核伦理存储表更新
            for(int i=0;i<mains.size();i++) {
               RequestNewSubmitEntity mainList = new RequestNewSubmitEntity() ;
               mainList.setDeptId(mains.get(i).getDeptId());
               mainList.setReviewer(mains.get(i).getReviewer());
               //审核人check
               int reviewerCheck = this.requestNewSubmitExMapper.reviewerCheck(mains.get(i).getReviewer(),mains.get(i).getDeptId());
               if(reviewerCheck == 0) {
                   throw new BusinessException("ERR0000009","用户信息表","第"+mains.get(i).getReviewerOrder()+"审核者");
               }
               //审核人
               String reviewerCode = this.requestNewSubmitExMapper.searchReviewerCode(mainList);
               //申请编号
               mainList.setApplicationNumber(mains.get(i).getApplicationNumber());
               //审核人
               mainList.setReviewer(reviewerCode);
               //审核顺序
               mainList.setReviewerOrder(mains.get(i).getReviewerOrder());
               //PullDown取得
               //审核人状态未审核
               String reviewerStatus = Constants.CD_STATUS_WEI;
               mainList.setReviewerStatus(reviewerStatus);
               //审核状态待确认
               String examineStatus = Constants.CD_REJECT_WAIT;
               mainList.setExamineStatus(examineStatus);
               //作成者
               mainList.setCreateName(requestNewSubmitExEntity.getCreateName());
               //审核伦理存储表更新
               int approalEthic = this.requestNewSubmitExMapper.insertApproalEthic(mainList);
               if(approalEthic >= 1) {
                   flag = true;
               }
           }
            flag = true;
        }
        logger.info("insertApproval#param:{}", flag);
        return flag;
    }
    
    /**
     * 申请文件存在check
     * @param requestApproveForm 申请编号和卡号
     */
    @Transactional(readOnly = true)
    public void fileCheck(RequestNewSubmitEntity requestNewSubmitEntity) {
        logger.info("fileCheck#RequestNewSubmitEntity:{}", requestNewSubmitEntity);
        //申请者check
        int fileCheck = this.requestNewSubmitExMapper.fileCheck(requestNewSubmitEntity);
        if(fileCheck == 0) {
            throw new BusinessException("ERR0000045");
        }
    }
    
    /**
     * 更新 审批文件存储表
     * @param requestNewSubmitForm
     * @return fileConfigId 确认人
     */
    public RequestNewSubmitEntity searchCongifId(RequestNewSubmitEntity requestNewSubmitExEntity) {
        //查询确认人
        RequestNewSubmitEntity fileConfigId = this.requestNewSubmitExMapper.searchCongifId(requestNewSubmitExEntity);
        logger.info("searchCongifId#param:{}", fileConfigId);
        return fileConfigId;
    }
    
    /**
     * 检索 邮件
     * @param requestNewSubmitForm 
     * @return meali 邮箱
     */
    @Transactional(readOnly = true)
    public RequestNewSubmitEntity searchMail(RequestNewSubmitEntity requestNewSubmitExEntity) {
        //获取邮箱
        RequestNewSubmitEntity mealiInfo = this.requestNewSubmitExMapper.searchMail(requestNewSubmitExEntity);
        logger.info("searchMail#param:{}", mealiInfo);
        return mealiInfo;
    }
    
    /**
     * 申请编号自动加一
     * @return selectNo 采番
     */
    @Transactional(readOnly = false)
    public String selectNo() {
        //获取番号
        String selectNo = this.requestNewSubmitExMapper.selectNo();
        logger.info("selectNo#param:{}", selectNo);
        return selectNo;
    }
    
  }
