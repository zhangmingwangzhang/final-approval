/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.approveList.Entity;

/**
 * 下拉框ExEntity
 * 
 * @author fangshuai
 * @since 2020/12/03
 * @version 0.1
 */
public class ListExEntity {
    
    /** CODE值 */
    private String code;
    
    /** CODE名称 */
    private String name;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

}
