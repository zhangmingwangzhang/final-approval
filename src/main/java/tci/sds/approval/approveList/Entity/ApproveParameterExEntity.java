/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.approveList.Entity;

import java.util.List;

/**
 * 下拉框ExEntity
 * 
 * @author fangshuai
 * @since 2020/12/03
 * @version 0.1
 */
public class ApproveParameterExEntity {

    /** 登录者 */
    private String userId;

    /** 确认者 */
    private String affirmant;

    /** 申请者 */
    private String appUserNameCard;

    /**  申请时间*/
    private String applicationDate;
    private String appstartDate;
    private String appendDate;
    private String[] appDate;

    /** 文件名 */
    private String searchFileName;

    /** 所属状态名 */
    private String searchStatus;

    /** 现在页数 */
    private int currentPage;

    /** １页内表示件数 */
    private int pageSize;

    /** 申请编号 */
    private String approveNumber;

    /** 文件路径 */
    private String filePath;

    /** 排序字段 */
    private String sortColumn;

    /** 排序的正倒 */
    private String sortType;

    /** 文件个数 */
    private int fileSize;

    public String getAppstartDate() {
        return appstartDate;
    }

    public void setAppstartDate(String appstartDate) {
        this.appstartDate = appstartDate;
    }

    public String getAppendDate() {
        return appendDate;
    }

    public void setAppendDate(String appendDate) {
        this.appendDate = appendDate;
    }

    public String[] getAppDate() {
        return appDate;
    }

    public void setAppDate(String[] appDate) {
        this.appDate = appDate;
    }

    /** 审核终了状态 */
    private String examileOver;
/**  当前用户待审核的数据*/
    private List<String> weishenhe;

    public List<String> getWeishenhe() {
        return weishenhe;
    }

    public void setWeishenhe(List<String> weishenhe) {
        this.weishenhe = weishenhe;
    }
    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the searchFileName
     */
    public String getSearchFileName() {
        return searchFileName;
    }

    /**
     * @param searchFileName the searchFileName to set
     */
    public void setSearchFileName(String searchFileName) {
        this.searchFileName = searchFileName;
    }

    /**
     * @return the searchStatus
     */
    public String getSearchStatus() {
        return searchStatus;
    }

    /**
     * @param searchStatus the searchStatus to set
     */
    public void setSearchStatus(String searchStatus) {
        this.searchStatus = searchStatus;
    }

    /**
     * @return the affirmant
     */
    public String getaffirmant() {
        return affirmant;
    }

    /**
     * @param affirmant the affirmant to set
     */
    public void setaffirmant(String affirmant) {
        this.affirmant = affirmant;
    }

    /**
     * @return the appUserNameCard
     */
    public String getappUserNameCard() {
        return appUserNameCard;
    }

    /**
     * @param appUserNameCard the appUserNameCard to set
     */
    public void setappUserNameCard(String appUserNameCard) {
        this.appUserNameCard = appUserNameCard;
    }

    /**
     * @return the applicationDate
     */
    public String getapplicationDate() {
        return applicationDate;
    }

    /**
     * @param applicationDate the applicationDate to set
     */
    public void setapplicationDate(String applicationDate) {
        this.applicationDate = applicationDate;
    }


    /**
     * @return the currentPage
     */
    public int getCurrentPage() {
        return currentPage;
    }

    /**
     * @param currentPage the currentPage to set
     */
    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    /**
     * @return the pageSize
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * @param pageSize the pageSize to set
     */
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * @return the approveNumber
     */
    public String getApproveNumber() {
        return approveNumber;
    }

    /**
     * @param approveNumber the approveNumber to set
     */
    public void setApproveNumber(String approveNumber) {
        this.approveNumber = approveNumber;
    }

    /**
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * @param filePath the filePath to set
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * @return the sortColumn
     */
    public String getSortColumn() {
        return sortColumn;
    }

    /**
     * @param sortColumn the sortColumn to set
     */
    public void setSortColumn(String sortColumn) {
        this.sortColumn = sortColumn;
    }

    /**
     * @return the sortType
     */
    public String getSortType() {
        return sortType;
    }

    /**
     * @param sortType the sortType to set
     */
    public void setSortType(String sortType) {
        this.sortType = sortType;
    }

    /**
     * @return the fileSize
     */
    public int getFileSize() {
        return fileSize;
    }

    /**
     * @param fileSize the fileSize to set
     */
    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    /**
     * @return the examileOver
     */
    public String getExamileOver() {
        return examileOver;
    }

    /**
     * @param examileOver the examileOver to set
     */
    public void setExamileOver(String examileOver) {
        this.examileOver = examileOver;
    }

    @Override
    public String toString() {
        return "ApproveParameterExEntity{" +
                "userId='" + userId + '\'' +
                ", affirmant='" + affirmant + '\'' +
                ", appUserNameCard='" + appUserNameCard + '\'' +
                ", applicationDate='" + applicationDate + '\'' +
                ", appstartDate='" + appstartDate + '\'' +
                ", appendDate='" + appendDate + '\'' +
                ", searchFileName='" + searchFileName + '\'' +
                ", searchStatus='" + searchStatus + '\'' +
                ", currentPage=" + currentPage +
                ", pageSize=" + pageSize +
                ", approveNumber='" + approveNumber + '\'' +
                ", filePath='" + filePath + '\'' +
                ", sortColumn='" + sortColumn + '\'' +
                ", sortType='" + sortType + '\'' +
                ", fileSize=" + fileSize +
                ", examileOver='" + examileOver + '\'' +
                '}';
    }

}
