/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.approveList.Entity;

/**
 * 审批一览用Entity
 * 
 * @author 房帅
 * @since 2020/11/11
 * @version 0.1
 */
public class ApproveListExEntity {

    /** 申请编号 */ 
    private String approveNum;

    /** 文件名 */
    private String fileName;

    /** 申请者/卡号 */
    private String appUserNameCard;

    /** 申请者部门 */
    private String appUserDepartment;

    /** 确认者 */ 
    private String affirmant;

    /** 确认者id */
    private String affirmantId;

    /** 驳回者*/ 
    private String rejectUser;

    /** 申请时间 */
    private String applicationDate;

    /** 状态 */
    private String status;

    /** 当前审核位置 */
    private int nowOrder;

    /** 审核人个数 */
    private int maxOrder;

    /** 审核人个数 */
    private int reviewSchedule;

    /** 审核状态CODE */
    private String statusCode;

    /** 操作按钮名字 */
    private String activeName;

    /** 操作按钮活性flg */
    private String activeFlg;

    /** 审核位置 */
    private String reviewerOrder;

    /** 文件路径 */
    private String filePath;

    /** 签名sheet页 */
    private String reviewerSheet;

    /**
     * @return the approveNum
     */
    public String getApproveNum() {
        return approveNum;
    }

    /**
     * @param approveNum the approveNum to set
     */
    public void setApproveNum(String approveNum) {
        this.approveNum = approveNum;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * @return the appUserNameCard
     */
    public String getAppUserNameCard() {
        return appUserNameCard;
    }

    /**
     * @param appUserNameCard the appUserNameCard to set
     */
    public void setAppUserNameCard(String appUserNameCard) {
        this.appUserNameCard = appUserNameCard;
    }

    /**
     * @return the appUserDepartment
     */
    public String getAppUserDepartment() {
        return appUserDepartment;
    }

    /**
     * @param appUserDepartment the appUserDepartment to set
     */
    public void setAppUserDepartment(String appUserDepartment) {
        this.appUserDepartment = appUserDepartment;
    }

    /**
     * @return the affirmant
     */
    public String getAffirmant() {
        return affirmant;
    }

    /**
     * @param affirmant the affirmant to set
     */
    public void setAffirmant(String affirmant) {
        this.affirmant = affirmant;
    }

    /**
     * @return the affirmantId
     */
    public String getAffirmantId() {
        return affirmantId;
    }

    /**
     * @param affirmantId the affirmantId to set
     */
    public void setAffirmantId(String affirmantId) {
        this.affirmantId = affirmantId;
    }

    /**
     * @return the rejectUser
     */
    public String getRejectUser() {
        return rejectUser;
    }

    /**
     * @param rejectUser the rejectUser to set
     */
    public void setRejectUser(String rejectUser) {
        this.rejectUser = rejectUser;
    }

    /**
     * @return the applicationDate
     */
    public String getApplicationDate() {
        return applicationDate;
    }

    /**
     * @param applicationDate the applicationDate to set
     */
    public void setApplicationDate(String applicationDate) {
        this.applicationDate = applicationDate;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the nowOrder
     */
    public int getNowOrder() {
        return nowOrder;
    }

    /**
     * @param nowOrder the nowOrder to set
     */
    public void setNowOrder(int nowOrder) {
        this.nowOrder = nowOrder;
    }

    /**
     * @return the maxOrder
     */
    public int getMaxOrder() {
        return maxOrder;
    }

    /**
     * @param maxOrder the maxOrder to set
     */
    public void setMaxOrder(int maxOrder) {
        this.maxOrder = maxOrder;
    }

    /**
     * @return the reviewSchedule
     */
    public int getReviewSchedule() {
        return reviewSchedule;
    }

    /**
     * @param reviewSchedule the reviewSchedule to set
     */
    public void setReviewSchedule(int reviewSchedule) {
        this.reviewSchedule = reviewSchedule;
    }

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return the activeName
     */
    public String getActiveName() {
        return activeName;
    }

    /**
     * @param activeName the activeName to set
     */
    public void setActiveName(String activeName) {
        this.activeName = activeName;
    }

    /**
     * @return the activeFlg
     */
    public String getActiveFlg() {
        return activeFlg;
    }

    /**
     * @param activeFlg the activeFlg to set
     */
    public void setActiveFlg(String activeFlg) {
        this.activeFlg = activeFlg;
    }

    /**
     * @return the reviewerOrder
     */
    public String getReviewerOrder() {
        return reviewerOrder;
    }

    /**
     * @param reviewerOrder the reviewerOrder to set
     */
    public void setReviewerOrder(String reviewerOrder) {
        this.reviewerOrder = reviewerOrder;
    }

    /**
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * @param filePath the filePath to set
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * @return the reviewerSheet
     */
    public String getReviewerSheet() {
        return reviewerSheet;
    }

    /**
     * @param reviewerSheet the reviewerSheet to set
     */
    public void setReviewerSheet(String reviewerSheet) {
        this.reviewerSheet = reviewerSheet;
    }

    @Override
    public String toString() {
        return "ApproveListExEntity [approveNum=" + approveNum + ", fileName=" + fileName + ", appUserNameCard="
                + appUserNameCard + ", appUserDepartment=" + appUserDepartment + ", affirmant=" + affirmant
                + ", affirmantId=" + affirmantId + ", rejectUser=" + rejectUser + ", applicationDate=" + applicationDate
                + ", status=" + status + ", nowOrder=" + nowOrder + ", maxOrder=" + maxOrder + ", reviewSchedule="
                + reviewSchedule + ", statusCode=" + statusCode + ", activeName=" + activeName + ", activeFlg="
                + activeFlg + ", reviewerOrder=" + reviewerOrder + ", filePath=" + filePath + ", reviewerSheet="
                + reviewerSheet + "]";
    }

}
