/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.approveList.mapper;

import java.util.List;

import tci.sds.approval.approveList.Entity.ApproveParameterExEntity;
import tci.sds.approval.approveList.Entity.ApproveListExEntity;
import tci.sds.approval.approveList.Entity.ListExEntity;

/**
 * 审批一览用Mapper
 * 
 * @author 房帅
 * @since 2020/11/11
 * @version 0.1
 */
public interface ApproveListMapper {

    /**
     * 查询确认驳回件数
     * @return 件数
     */
    int rejectConfirCount(String userId, String examineStatus);

    /**
     * 查询待确认件数
     * @return 件数
     */
    int rejectWaitCount(String userId, String examineStatus);

    /**
     * 检索登录者是待审核者的审核顺序
     * @return 审核顺序
     */
    List<ApproveListExEntity> allReviewerOrder(String userId,
                                               String reviewerStatus, String examineStatus);

    /**
     * 检索上一个审核人的审核人状态
     * @return 审核人状态
     */
    String lastReviewerStatus(String approveNum, String reviewerOrder);

    /**
     * 查询审核驳回件数
     * @return 件数
     */
    int examineConfirCount(String userId, String examineStatus);

    /**
     * 查询确认驳回件数
     * @return 件数
     */
    int nowOrder(String approveNum, String examineStatus);

    /**
     * 查询审核人个数
     * @return 个数
     */
    int maxOrder(String approveNum);

    /**
     * 查询登录者审核顺序
     * @return 件数
     */
    String myOrder(String approveNum, String userId);

    /**
     * 登录者的审核人状态
     * @return 件数
     */
    String reviewerStatus(String approveNum, String userId);

    /**
     * 查询状态下拉框
     * @return 件数
     */
    List<ListExEntity> selectStatus(String status);

    /**
     * 查询一览数据
     * @return 一览
     */
    List<ApproveListExEntity> searchApproval(ApproveParameterExEntity approveParameterExEntity);
    
    /**
     * 999999超级用户查询一览数据
     * @return 一览
     */
    List<ApproveListExEntity> searchApprovalNine(ApproveParameterExEntity approveParameterExEntity);

    void delete(String approveNum);
    void delete1(String approveNum);
}
