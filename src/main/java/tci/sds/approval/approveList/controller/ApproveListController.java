/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.approveList.controller;

import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URLEncoder;
import java.nio.channels.FileChannel;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.spire.xls.ExcelVersion;
import com.spire.xls.ViewMode;
import com.spire.xls.Workbook;
import com.spire.xls.Worksheet;

import tci.sds.approval.approveList.Entity.ApproveParameterExEntity;
import tci.sds.approval.approveList.Entity.ApproveListExEntity;
import tci.sds.approval.approveList.Entity.ListExEntity;
import tci.sds.approval.approveList.form.ApproveListForm;
import tci.sds.approval.approveList.service.ApproveListService;
import tci.sds.approval.requestApprove.entity.RequestApproveEntity;
import tci.sds.approval.requestApprove.form.RequestApproveForm;
import tci.sds.approval.requestApprove.service.RequestApproveService;
import tci.sds.approval.requestNewConfirm.Entity.RequestNewConfirmEntity;
import tci.sds.approval.requestNewConfirm.form.RequestNewConfirmForm;
import tci.sds.approval.requestNewConfirm.service.RequestNewConfirmService;
import tci.sds.common.entity.ConfigProperties;
import tci.sds.common.exception.BusinessException;
import tci.sds.common.unit.ExcelOperatingClass;
import tci.sds.common.unit.ExcelWatermark;
import tci.sds.common.unit.JudgeIsMoblie;

/**
 * 审批一览用Controller
 *
 * @author 房帅
 * @since 2020/11/11
 * @version 0.1
 */
@Controller
public class ApproveListController {
    private final Logger logger = LoggerFactory
            .getLogger(ApproveListController.class);
    private static ConfigProperties configProperties;
    @Autowired
    private RequestNewConfirmService requestNewConfirmService;
    @Autowired
    private ApproveListService approvelistService;
    private RequestApproveService requestApproveService;
    @Value("${commpat.config.temp-file-url}")
     private String tempfileurl;

    //    /**
//     * 审批一览初期化 检索
//     *
//     * @param ApproveListForm 检索用Form
//     * @return 检索结果用Form
//     */
    @PostMapping("/api/approvelist/search")
    @ResponseBody
    public ApproveListForm search(@Valid @RequestBody ApproveListForm approveListInputForm) {
        HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = request.getSession();
        session.setAttribute("userName", "");
        logger.info("search#param:{}", approveListInputForm);
        ApproveParameterExEntity approveParameterExEntity = this.copyFormToapproveParameterExEntity(approveListInputForm);
        String userId = approveParameterExEntity.getUserId();

        // 查询确认驳回件数
        int rejectConfirCount = this.approvelistService.searchRejectConfirCount(userId);
        // 查询待确认件数
        int rejectWaitCount = this.approvelistService.searchRejectWaitCount(userId);
        // 查询待审核件数
        int examineWaitCount = this.approvelistService.searchExamineWaitCount(userId);
        // 查询审核驳回件数
        int examineConfirCount = this.approvelistService.searchExamineConfirCount(userId);
        // 查询一览所有数据
        List<ApproveListExEntity> datagrids = this.approvelistService.searchPage(approveParameterExEntity,userId);
        // 画面显示数据
        List<ApproveListExEntity> datagrid = this.approvelistService.searchViewList(approveParameterExEntity,datagrids);
        // 查询画面状态下拉框
        List<ListExEntity> selectStatus = this.approvelistService.selectStatus();
        ApproveListForm approveListOutputForm = new ApproveListForm();

        // 设定待处理事件返回值
        approveListOutputForm.setRejectConfirCount(Integer.toString(rejectConfirCount));
        approveListOutputForm.setRejectWaitCount(Integer.toString(rejectWaitCount));
        approveListOutputForm.setExamineWaitCount(Integer.toString(examineWaitCount));
        approveListOutputForm.setExamineConfirCount(Integer.toString(examineConfirCount));
        // 设定总件数返回值
        approveListOutputForm.setAllCount(Integer.toString(datagrids.size()));
        // 设定一览显示数据返回值
        approveListOutputForm.setDatagrid(datagrid);
        // 设定状态下拉框返回值
        approveListOutputForm.setSelectStatus(selectStatus);
        logger.info("search#result:{}", datagrid);
        return approveListOutputForm;
    }

    //    /**
////     * 把Form的值保存到ExEntity中
////     *
////     * @param ApproveListForm
////     * @return ApproveParameterExEntity
////     */
    private ApproveParameterExEntity copyFormToapproveParameterExEntity(ApproveListForm approveListInputForm) {
        ApproveParameterExEntity approveParameterExEntity = new ApproveParameterExEntity();
        // 登录者
        approveParameterExEntity.setUserId(approveListInputForm.getUserId());
        // 文件名
        approveParameterExEntity.setSearchFileName(approveListInputForm.getSearchFileName());
        // 确认者
        approveParameterExEntity.setaffirmant(approveListInputForm.getaffirmant());
        // 申請者
        approveParameterExEntity.setappUserNameCard(approveListInputForm.getappUserNameCard());
        // 申請时间
        approveParameterExEntity.setapplicationDate(approveListInputForm.getapplicationDate());

        String[] appDate = approveListInputForm.getAppDate();
        if(appDate !=null&&appDate.length>0) {
            approveParameterExEntity.setAppstartDate(approveListInputForm.getAppDate()[0]);
            approveParameterExEntity.setAppendDate(approveListInputForm.getAppDate()[1]);
        }else {
            approveParameterExEntity.setAppendDate(null);
            approveParameterExEntity.setAppendDate(null);
        }
        // 所属状态名
        approveParameterExEntity.setSearchStatus(approveListInputForm.getSearchStatus());
        // 现在页数
        approveParameterExEntity.setCurrentPage(approveListInputForm.getCurrentPage());
        // １页内表示件数
        approveParameterExEntity.setPageSize(approveListInputForm.getPageSize());
        // 文件路径
        approveParameterExEntity.setFilePath(approveListInputForm.getFilePath());
        // 排序字段
        approveParameterExEntity.setSortColumn(approveListInputForm.getSortColumn());
        // 排序的正倒
        approveParameterExEntity.setSortType(approveListInputForm.getSortType());
        // 文件个数
        approveParameterExEntity.setFileSize(approveListInputForm.getFileSize());
        // 审核终了状态
        approveParameterExEntity.setExamileOver(approveListInputForm.getExamileOver());
        return approveParameterExEntity;
    }

    /**
     * 根据申请编号查询该文件的所在路径
     *
     * @return 文件路径
     */
    @PostMapping("/api/approvelist/filepath")
    @ResponseBody
    public ApproveListForm searchFilePath(@Valid @RequestBody ApproveListForm approveListInputForm)
            throws ParseException {
        logger.info("searchFilePath#param:{}", approveListInputForm);
        ApproveParameterExEntity approveParameterExEntity = this.copyFormToapproveParameterExEntity(approveListInputForm);
        String filePath = approveParameterExEntity.getFilePath();
        // 路径为null时报错
        if(filePath == null || filePath.equals("")){
            logger.info("searchFilePath#error:file is not found.");
            throw new BusinessException("ERR0000010");
        }

        File file = new File(filePath);
        // 用来测试此路径名表示的文件或目录是否存在
        File[] array = file.listFiles();
        // 文件不存在时报错
        if (array == null || array.length == 0) {
            logger.info("searchFilePath#error:file is not found.");
            throw new BusinessException("ERR0000010");
        }
        ApproveListForm outputForm = new ApproveListForm();
        // 返回下载文件个数
        outputForm.setFileSize(array.length);
        logger.info("searchFilePath#result:{}", array.length);
        return outputForm;
    }

    /**
     * 下载文件
     * @return
     *
     */
    @RequestMapping(value = "/api/approvelist/filedownLoad")
    @ResponseBody
    public void downloadFile(HttpServletRequest request, HttpServletResponse response,String filePath,
                             String File, String approvalNum, String reviewerSheet) throws UnsupportedEncodingException {
        logger.info("downloadFile#param:{}", filePath);
        int filei = Integer.parseInt(File);
        File file = new File(filePath);
        // 获得该文件夹内的所有文件
        File[] array = file.listFiles();

        // 如果有文件
        if(array[filei].exists()){
            //设置文本和字体大小
            Workbook workbook = new Workbook();
            String fileName = array[filei].getName();
            //是审核文件的场合添加水印
            if(!fileName.contains("附件")) {
                workbook.loadFromFile(filePath + fileName);
                Font font = new Font("仿宋", Font.ITALIC, 30);
                Worksheet sheet = workbook.getWorksheets().get(Integer.parseInt(reviewerSheet));
                //调用DrawText() 方法插入图片
                BufferedImage imgWtrmrk = ExcelWatermark.drawText(approvalNum, font, Color.gray, Color.white,
                        sheet.getPageSetup().getPageHeight(), sheet.getPageSetup().getPageWidth());
                //将图片设置为页眉
                sheet.getPageSetup().setLeftHeaderImage(imgWtrmrk);
                sheet.getPageSetup().setLeftHeader("&G");
                //将显示模式设置为Layout
                sheet.setViewMode(ViewMode.Preview);
                //保存文档
                workbook.saveToFile(filePath + fileName, ExcelVersion.Version2010);
            }

            fileName = URLEncoder.encode(fileName, "UTF-8");
            fileName = fileName.replaceAll("\\+",  "%20");
            // 配置文件下载
            response.setHeader("content-type", "application/octet-stream");
            response.setContentType("application/octet-stream");
            // 下载文件能正常显示中文
            response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
            // 实现文件下载
            byte[] buffer = new byte[1024];
            FileInputStream fis = null;
            BufferedInputStream bis = null;
            try {
                fis = new FileInputStream(array[filei]);
                bis = new BufferedInputStream(fis);
                // 写入到文件
                OutputStream os = response.getOutputStream();
                int j = bis.read(buffer);
                // 保存文件
                while (j != -1) {
                    os.write(buffer, 0, j);
                    j = bis.read(buffer);
                }
                // 关闭资源
                bis.close();
                fis.close();
                logger.info("Download successfully!");
            } catch (Exception e) {
                logger.info("Download failed!");
            }
        }
    }

    /**
     * 在线预览
     *
     * @param approveNum 申请编号
     * @return
     */
    @RequestMapping(value = "/api/approvelist/previewPdf", method = RequestMethod.GET)
    public void pdfStreamHandler(HttpServletRequest request,HttpServletResponse response,String fileName,String fileId,String approveNum) throws IOException{

        logger.info("pdfStreamHandler#param:{}", fileName,fileId,approveNum);
        //设置标志判断是否是生成pdf文件，是删除，不是保留
         boolean flage=true;
        // 需要在线预览的文件名称
        String fileFullName = approveNum + "//"+ fileName ;
        // 判断要预览的文件是附件还是申请文件
        if (fileName.contains("附件")) {
            // excel转换pdf
            String pdfFileName = "";
            String suffix = fileName.substring(fileName.lastIndexOf("."));
            if(".doc".equalsIgnoreCase(suffix) || ".docx".equalsIgnoreCase(suffix)) {
                // pdf预览
                pdfFileName = ExcelOperatingClass.DocToPdf(fileFullName,approveNum);
            } else if (".xls".equalsIgnoreCase(suffix) || ".xlsx".equalsIgnoreCase(suffix)){
                // excle预览
                pdfFileName = ExcelOperatingClass.ExcelToPdf(fileFullName, approveNum);
            } else if(".png".equalsIgnoreCase(suffix) || ".jpg".equalsIgnoreCase(suffix) || ".pdf".equalsIgnoreCase(suffix) ){
                 flage=false;
                pdfFileName= tempfileurl+approveNum + "\\"+ fileName;

            }else  if(".ppt".equalsIgnoreCase(suffix) || ".pptx".equalsIgnoreCase(suffix)){

                pdfFileName= ExcelOperatingClass.pptToPdf(tempfileurl,approveNum,fileName);
            }
            File file = new File(pdfFileName);

            byte[] data = null;

            try {
                FileInputStream input = new FileInputStream(file);
                data = new byte[input.available()];
                // 读取文件的信息
                input.read(data);
                // 把文件的信息设置给response
                response.getOutputStream().write(data);
                input.close();
                // 在线预览临时生成的pdf文件删除
                if(flage){
                    file.delete();
                }

                File fileDelete = new File(configProperties.getPDFFileUrl());
                // listFiles方法：返回file路径下所有文件和文件夹的绝对路径
                File[] listFiles = fileDelete.listFiles();
                for (File file2 : listFiles) {
                    file2.delete();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {

            // 文件基本信息检索
            RequestNewConfirmEntity ｒequestNewConfirmEntity = this.requestNewConfirmService.selectFileInfo(approveNum);
            RequestNewConfirmEntity ｒequestNewConfirm  = this.requestNewConfirmService.selectSheet(ｒequestNewConfirmEntity);
            // 签名sheet
            String reviewerSheet = ｒequestNewConfirm.getReviewerSheet();
            int sheetNo = Integer.parseInt(reviewerSheet);

            String pdfFileName = "";
            String suffix = fileName.substring(fileName.lastIndexOf("."));
            if(".doc".equalsIgnoreCase(suffix) || ".docx".equalsIgnoreCase(suffix)) {
                // word预览
                pdfFileName = ExcelOperatingClass.DocTo(fileFullName,approveNum);
            } else if (".xls".equalsIgnoreCase(suffix) || ".xlsx".equalsIgnoreCase(suffix)){
                pdfFileName = ExcelOperatingClass.ExcelTo(fileFullName, sheetNo,approveNum);
            }
            File file = new File(pdfFileName);

            if (file.exists()) {
                byte[] data = null;
                try {
                    FileInputStream input = new FileInputStream(file);
                    data = new byte[input.available()];
                    input.read(data);
                    response.getOutputStream().write(data);
                    input.close();
                    // 在线预览临时生成的pdf文件删除
                    if(flage){
                        file.delete();
                    }
                    File fileDelete = new File("C:\\G20005J5\\pdf\\"+approveNum);

                    // listFiles方法：返回file路径下所有文件和文件夹的绝对路径
                    File[] listFiles = fileDelete.listFiles();
                    for (File file2 : listFiles) {
                        file2.delete();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }
    /**
     * 无水印下载文件
     * @return
     *
     */
    @RequestMapping(value = "/api/approvelist/filedownLoad1")
    @ResponseBody
    public void downloadFile1(HttpServletRequest request, HttpServletResponse response,String filePath,
                              String File, String approvalNum, String reviewerSheet) throws UnsupportedEncodingException {
        logger.info("downloadFile#param:{}", filePath);
        int filei = Integer.parseInt(File);
        File file = new File(filePath);
        // 获得该文件夹内的所有文件
        File[] array = file.listFiles();

        // 如果有文件
        if(array[filei].exists()){
            //设置文本和字体大小
            Workbook workbook = new Workbook();
            String fileName = array[filei].getName();
            //是审核文件的场合添加水印
            if(!fileName.contains("附件")) {
                workbook.loadFromFile(filePath +"\\"+ fileName);
                Font font = new Font("仿宋", Font.ITALIC, 30);
                Worksheet sheet = workbook.getWorksheets().get(Integer.parseInt(reviewerSheet));
                //调用DrawText() 方法插入图片
                BufferedImage imgWtrmrk = ExcelWatermark.drawText(approvalNum, font, Color.gray, Color.white,
                        sheet.getPageSetup().getPageHeight(), sheet.getPageSetup().getPageWidth());
                //将图片设置为页眉
                sheet.getPageSetup().setLeftHeaderImage(imgWtrmrk);
                sheet.getPageSetup().setLeftHeader("&G");
                //将显示模式设置为Layout
                sheet.setViewMode(ViewMode.Preview);
                //保存文档
                workbook.saveToFile(filePath + fileName, ExcelVersion.Version2010);
            }

            fileName = URLEncoder.encode(fileName, "UTF-8");
            fileName = fileName.replaceAll("\\+",  "%20");
            // 配置文件下载
            response.setHeader("content-type", "application/octet-stream");
            response.setContentType("application/octet-stream");
            // 下载文件能正常显示中文
            response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
            // 实现文件下载
            byte[] buffer = new byte[1024];
            FileInputStream fis = null;
            BufferedInputStream bis = null;
            try {
                fis = new FileInputStream(array[filei]);
                bis = new BufferedInputStream(fis);
                // 写入到文件
                OutputStream os = response.getOutputStream();
                int j = bis.read(buffer);
                // 保存文件
                while (j != -1) {
                    os.write(buffer, 0, j);
                    j = bis.read(buffer);
                }
                // 关闭资源
                bis.close();
                fis.close();
                logger.info("Download successfully!");
            } catch (Exception e) {
                logger.info("Download failed!");
            }
        }
    }

    //删除驳回
    @GetMapping("/api/approvelist/delete/{approveNum}")
    @ResponseBody
    public String searchFilePath(@PathVariable("approveNum") String approveNum){
        System.out.println(approveNum);
        String delete = approvelistService.delete( approveNum);
        if (delete=="删除成功"){
            return "success";
        }else {
            return "fail";
        }
    }
    /**
     * Form值保存 Entity用
     * @param requestApproveForm 设定值
     * @return requestApproveEntity
     */
    private RequestApproveEntity copyApproveEntity(RequestApproveForm requestApproveForm) {
        RequestApproveEntity requestApproveEntity = new RequestApproveEntity();
        //登录者卡号
        requestApproveEntity.setLoginCode(requestApproveForm.getLoginCode());
        //申请编号
        requestApproveEntity.setApplicationNumber(requestApproveForm.getApplicationNumber());
        return requestApproveEntity;

    }
    /**
     * 删除文件或文件夹
     * @param directory
     */
    public static void delAllFile(File directory){
        if (!directory.isDirectory()){
            directory.delete();
        } else{
            File [] files = directory.listFiles();

            // 空文件夹
            if (files.length == 0){
                directory.delete();
                System.out.println("删除" + directory.getAbsolutePath());
                return;
            }

            // 删除子文件夹和子文件
            for (File file : files){
                if (file.isDirectory()){
                    delAllFile(file);
                } else {
                    file.delete();
                    System.out.println("删除" + file.getAbsolutePath());
                }
            }

            // 删除文件夹本身
            directory.delete();
            System.out.println("删除" + directory.getAbsolutePath());
        }
    }
    }
