/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.approveList.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tci.sds.approval.approveList.Entity.ApproveParameterExEntity;
import tci.sds.approval.approveList.Entity.ApproveListExEntity;
import tci.sds.approval.approveList.Entity.ListExEntity;
import tci.sds.approval.approveList.mapper.ApproveListMapper;
import tci.sds.common.entity.ConfigProperties;
import tci.sds.common.util.Constants;

/**
 * 审批一览用Service
 *
 * @author 房帅
 * @version 0.1
 * @since 2020/11/11
 */
@Service
public class ApproveListService {
    private final Logger logger = LoggerFactory.getLogger(ApproveListService.class);

    @Autowired
    private ApproveListMapper approveListMapper;

    // 状态flg 0
    private static final String STATUS_FLAG_0 = "0";

    // 状态flg 1
    private static final String STATUS_FLAG_1 = "1";

    // 文字确认
    private static final String STATUS_SURE = "确认";

    // 文字审核
    private static final String STATUS_EXAMINE = "审核";

    // 文字修正
    private static final String STATUS_UPDATE = "修正";

    // 文字已审核
    private static final String STATUS_DOWNLOAD = "文件下载";

    // 文字已审核
    private static final String STATUS_CHECKED = "已审核";

    // 符号斜线
    private static final String STATUS_SLANT = "/";


    /**
     * 查询确认驳回件数
     *
     * @param 用户id
     * @return 确认驳回件数
     */
    @Transactional(readOnly = true)
    public int searchRejectConfirCount(String userId) {
        logger.info("searchRejectConfirCount#param:{}", userId);
        // 查询确认驳回件数
        int rejectConfirCount = this.approveListMapper.rejectConfirCount(userId, Constants.CD_REJECT_CONFIR);
        logger.info("searchRejectConfirCount#result:{}", rejectConfirCount);
        return rejectConfirCount;
    }

    /**
     * 查询待确认件数
     *
     * @param 用户id
     * @return 待确认件数
     */
    @Transactional(readOnly = true)
    public int searchRejectWaitCount(String userId) {
        logger.info("searchRejectWaitCount#param:{}", userId);
        // 查询待确认件数
        int rejectWaitCount = this.approveListMapper.rejectWaitCount(userId, Constants.CD_REJECT_WAIT);
        logger.info("searchRejectWaitCount#result:{}", rejectWaitCount);
        return rejectWaitCount;
    }

    /**
     * 查询待审核件数
     *
     * @param 用户id
     * @return 待审核件数
     */
    @Transactional(readOnly = true)
    public int searchExamineWaitCount(String userId) {
        logger.info("searchExamineWaitCount#param:{}", userId);
        int examineWaitCount = 0;
        // 查询登录者是审核者时未审核的数据
        List<ApproveListExEntity> list = this.approveListMapper.allReviewerOrder(userId,
                Constants.CD_STATUS_WEI, Constants.CD_EXAMINE_WAIT);
        for (int i = 0; i < list.size(); i++) {
            ApproveListExEntity approve = list.get(i);
            // 登录者不是第一审核人时查询上一审核人的审核人状态
            if (!approve.getReviewerOrder().equals(STATUS_FLAG_1)) {
                String lastOrder = Integer.toString(Integer.parseInt(approve.getReviewerOrder()) - 1);
                String lastReviewerStatus =
                        this.approveListMapper.lastReviewerStatus(approve.getApproveNum(), lastOrder);
                // 上一审核人的审核人状态是已审核时,待审核件数+1
                if (lastReviewerStatus.equals(Constants.CD_STATUS_YI)) {
                    examineWaitCount++;
                }
            } else {
                // 登录者是第一审核人时,待审核件数+1
                examineWaitCount++;
            }
        }
        logger.info("searchExamineWaitCount#result:{}", examineWaitCount);
        return examineWaitCount;
    }

    /**
     * 查询审核驳回件数
     *
     * @param 用户id
     * @return 审核驳回件数
     */
    @Transactional(readOnly = true)
    public int searchExamineConfirCount(String userId) {
        logger.info("searchExamineConfirCount#param:{}", userId);
        // 审核驳回件数
        int examineConfirCount = this.approveListMapper.examineConfirCount(userId, Constants.CD_EXAMINE_CONFIR);
        logger.info("searchExamineConfirCount#result:{}", examineConfirCount);
        return examineConfirCount;
    }

    /**
     * 查询画面状态下拉框
     *
     * @param
     * @return 画面状态下拉框list
     */
    @Transactional(readOnly = true)
    public List<ListExEntity> selectStatus() {
        logger.info("selectStatus#param:{}", "");
        // 查询画面状态下拉框
        List<ListExEntity> list = this.approveListMapper.selectStatus(Constants.CD_AUDIT_STATUS);
        ListExEntity listex = new ListExEntity();
        // 设定已审核状态
        listex.setCode(STATUS_FLAG_1);
        listex.setName(STATUS_CHECKED);
        // 添加已审核状态
        list.add(4, listex);
        logger.info("selectStatus#result:{}", list);
        return list;
    }

    /**
     * 画面显示一览结果
     *
     * @param approveListInputForm 分页条件
     * @return 画面一览
     */
    @Transactional(readOnly = true)
    public List<ApproveListExEntity> searchViewList(ApproveParameterExEntity approveParameterExEntity,
                                                    List<ApproveListExEntity> list) {
        logger.info("searchViewList#param:{}", approveParameterExEntity, list);
        // 计算画面第一条数据位置
        int start = (approveParameterExEntity.getCurrentPage() - 1) * approveParameterExEntity.getPageSize();
        // 计算画面最后一条数据位置
        int end = approveParameterExEntity.getPageSize() * approveParameterExEntity.getCurrentPage();
        // 计算出的画面最后一条数据位置大于数据总条数时，最后一条数据位置=数据总条数
        if (end > list.size()) {
            end = list.size();
        }
        // 截取出画面显示的数据
        list = list.subList(start, end);
        logger.info("searchViewList#result:{}", list);
        return list;
    }

    /**
     * 查询一览结果
     *
     * @param approveListInputForm 检索条件
     * @return 一览总件数一览结果
     */
    @Transactional(readOnly = true)
    public List<ApproveListExEntity> searchPage(ApproveParameterExEntity approveParameterExEntity, String userId) {
        logger.info("searchPage#param:{}", approveParameterExEntity);
        // 审核终了状态定数
        approveParameterExEntity.setExamileOver(Constants.CD_EXAMINE_OVER);
        // 取得画面状态的值
        String selectStatus = approveParameterExEntity.getSearchStatus();

        // 画面状态是已审核时令审核状态改为待审核
//        if (approveParameterExEntity.getSearchStatus().equals(STATUS_FLAG_1)) {
//            approveParameterExEntity.setSearchStatus(Constants.CD_EXAMINE_WAIT);
//        }
        //判断登陆者是 超级管理999999 还是 普通管理用户
        if (approveParameterExEntity.getUserId().equals("999999")) {
            // 查询出所有符合条件的数据
            List<ApproveListExEntity> list = this.approveListMapper.searchApprovalNine(approveParameterExEntity);

            for (int i = list.size() - 1; i >= 0; i--) {
                ApproveListExEntity approve = list.get(i);
                // 查询审核人个数
                int maxOrder = this.approveListMapper.maxOrder(approve.getApproveNum());
                // 现在的审核进度
                int nowOrder = 0;
                String[] user = approve.getAppUserNameCard().split(STATUS_SLANT);
                // 设定按钮状态不可用
                approve.setActiveFlg(STATUS_FLAG_0);
                // 查询此次申请进行到第几审核人
                nowOrder = this.approveListMapper.nowOrder(approve.getApproveNum(), Constants.CD_STATUS_YI);
                // 查询登录者是第几审核人
                String myOrder = this.approveListMapper.myOrder(approve.getApproveNum(),
                        approveParameterExEntity.getUserId());

                // 登录者是从超级管理员999999操作文件下载按钮可用
                approve.setActiveFlg(STATUS_FLAG_1);
                approve.setActiveName(STATUS_DOWNLOAD);
                // 当前审核进度为审核人个数+2
                nowOrder = maxOrder + 2;
                //申请时间截取到秒
                approve.setApplicationDate(approve.getApplicationDate().substring(0, 19));
                approve.setNowOrder(nowOrder);
                approve.setReviewSchedule(100 * nowOrder / (maxOrder + 2));
                approve.setMaxOrder(maxOrder);
            }
            // 审批进度排序
            if (approveParameterExEntity.getSortColumn().equals("reviewSchedule") && approveParameterExEntity.
                    getSortType() != null && !approveParameterExEntity.getSortType().equals("")) {
                // 画面选择升序时
                if (approveParameterExEntity.getSortType().equals("ascending")) {
                    Collections.sort(list, new Comparator<ApproveListExEntity>() {
                        public int compare(ApproveListExEntity p1, ApproveListExEntity p2) {
                            // 按照进度进行升序排列
                            if (p1.getReviewSchedule() > p2.getReviewSchedule()) {
                                return 1;
                            }
                            if (p1.getReviewSchedule() == p2.getReviewSchedule()) {
                                return 0;
                            }
                            return -1;
                        }
                    });
                } else {
                    // 画面选择降序时
                    Collections.sort(list, new Comparator<ApproveListExEntity>() {
                        public int compare(ApproveListExEntity p2, ApproveListExEntity p1) {
                            // 按照进度进行降序排列
                            if (p1.getReviewSchedule() > p2.getReviewSchedule()) {
                                return 1;
                            }
                            if (p1.getReviewSchedule() == p2.getReviewSchedule()) {
                                return 0;
                            }
                            return -1;
                        }
                    });
                }
            }
            logger.info("searchPage#result:{}", list);
            return list;
        } else {
            // 查询登录者是审核者时未审核的审核顺序
            List<String> quanxianshenhe = new ArrayList<>();
            List<ApproveListExEntity> lista = this.approveListMapper.allReviewerOrder(userId,
                    Constants.CD_STATUS_WEI, Constants.CD_EXAMINE_WAIT);

            for (int i = 0; i < lista.size(); i++) {
                ApproveListExEntity approve = lista.get(i);
                // 登录者不是第一审核人时查询上一审核人的审核人状态
                if (!approve.getReviewerOrder().equals(STATUS_FLAG_1)) {
                    String lastOrder = Integer.toString(Integer.parseInt(approve.getReviewerOrder()) - 1);
                    String lastReviewerStatus =
                            this.approveListMapper.lastReviewerStatus(approve.getApproveNum(), lastOrder);

                    // 上一审核人的审核人状态是已审核时,待审核件数+1
                    if (lastReviewerStatus.equals(Constants.CD_STATUS_YI)) {
                        quanxianshenhe.add(approve.getApproveNum());
                        System.out.println("================++++" + quanxianshenhe);
                        System.out.println(quanxianshenhe.size());
                    }
                } else {
                    // 登录者是第一审核人时,待审核件数+1
                    quanxianshenhe.add(approve.getApproveNum());
                }
            }
            approveParameterExEntity.setWeishenhe(quanxianshenhe);
            // 查询出所有符合条件的数据
            List<ApproveListExEntity> list = this.approveListMapper.searchApproval(approveParameterExEntity);
            for (int i = list.size() - 1; i >= 0; i--) {
                ApproveListExEntity approve = list.get(i);
                // 查询审核人个数
                int maxOrder = this.approveListMapper.maxOrder(approve.getApproveNum());
                // 现在的审核进度
                int nowOrder = 0;
                String[] user = approve.getAppUserNameCard().split(STATUS_SLANT);
                // 设定按钮状态不可用
                approve.setActiveFlg(STATUS_FLAG_0);
                // 查询此次申请进行到第几审核人
                nowOrder = this.approveListMapper.nowOrder(approve.getApproveNum(), Constants.CD_STATUS_YI);
                // 查询登录者是第几审核人
                String myOrder = this.approveListMapper.myOrder(approve.getApproveNum(),
                        approveParameterExEntity.getUserId());

                // 审核状态是驳回或完了的场合
                if (approve.getStatusCode().equals(Constants.CD_REJECT_CONFIR) ||
                        approve.getStatusCode().equals(Constants.CD_EXAMINE_CONFIR)) {
                    // 登录者是审核人的场合画面操作审核按钮不可用
                    if (myOrder != null) {
                        approve.setActiveFlg(STATUS_FLAG_0);
                        approve.setActiveName(STATUS_EXAMINE);
                    }
                    // 登录者是确认者的场合画面操作确认按钮不可用
                    if (approve.getAffirmantId().equals(approveParameterExEntity.getUserId())) {
                        approve.setActiveFlg(STATUS_FLAG_0);
                        approve.setActiveName(STATUS_SURE);
                    }
                    // 登录者是申请者的场合画面操作修正按钮可用
                    if (user[1].equals(approveParameterExEntity.getUserId())) {
                        approve.setActiveFlg(STATUS_FLAG_1);
                        approve.setActiveName(STATUS_UPDATE);
                    }
                    // 当前审核进度为0
                    nowOrder = 0;
                }

                // 审核状态是待确认的场合
                if (approve.getStatusCode().equals(Constants.CD_REJECT_WAIT)) {
                    // 登录者是审核人的场合画面操作审核按钮不可用
                    if (myOrder != null) {
                        approve.setActiveFlg(STATUS_FLAG_0);
                        approve.setActiveName(STATUS_EXAMINE);
                    }
                    // 登录者是申请者的场合画面操作修正按钮可用
                    if (user[1].equals(approveParameterExEntity.getUserId())) {
                        approve.setActiveFlg(STATUS_FLAG_1);
                        approve.setActiveName(STATUS_UPDATE);
                    }
                    // 登录者是确认者的场合画面操作确认按钮可用
                    if (approve.getAffirmantId().equals(approveParameterExEntity.getUserId())) {
                        approve.setActiveFlg(STATUS_FLAG_1);
                        approve.setActiveName(STATUS_SURE);
                    }
                    // 当前审核进度为1
                    nowOrder = 1;
                }

                // 审核状态是审核终了的场合
                if (approve.getStatusCode().equals(Constants.CD_EXAMINE_OVER)) {
                    // 登录者是申请者的场合画面操作修正按钮不可用
                    if (user[1].equals(approveParameterExEntity.getUserId())) {
                        approve.setActiveFlg(STATUS_FLAG_0);
                        approve.setActiveName(STATUS_UPDATE);
                    }
                    // 登录者是审核人的场合画面操作审核按钮不可用
                    if (myOrder != null) {
                        approve.setActiveFlg(STATUS_FLAG_0);
                        approve.setActiveName(STATUS_EXAMINE);
                    }
                    // 登录者是确认者的场合画面操作文件下载按钮可用
                    if (approve.getAffirmantId().equals(approveParameterExEntity.getUserId())) {
                        approve.setActiveFlg(STATUS_FLAG_1);
                        approve.setActiveName(STATUS_DOWNLOAD);
                    }
                    // 当前审核进度为审核人个数+2
                    nowOrder = maxOrder + 2;
                }

                // 审核状态是待审核的场合
                if (approve.getStatusCode().equals(Constants.CD_EXAMINE_WAIT)) {
                    // 登录者是申请者的场合画面操作修正按钮不可用
                    if (user[1].equals(approveParameterExEntity.getUserId())) {
                        approve.setActiveFlg(STATUS_FLAG_0);
                        approve.setActiveName(STATUS_UPDATE);
                    }
                    // 登录者是确认者的场合画面操作确认按钮可用
                    if (approve.getAffirmantId().equals(approveParameterExEntity.getUserId())) {
                        approve.setActiveFlg(STATUS_FLAG_0);
                        approve.setActiveName(STATUS_SURE);
                    }
                    if (myOrder != null) {
                        // 登录者的审核位置小于现在的审核位置 ：已审核 审核按钮不可用
                        if (Integer.valueOf(myOrder) <= nowOrder) {
                            approve.setStatus(STATUS_CHECKED);
                            approve.setActiveFlg(STATUS_FLAG_0);
                            approve.setActiveName(STATUS_EXAMINE);
                        }
                        // 登录者的审核位置=现在已审核位置+1 ：待审核，审核按钮可用
                        if (Integer.valueOf(myOrder) == nowOrder + 1) {
                            approve.setActiveFlg(STATUS_FLAG_1);
                            approve.setActiveName(STATUS_EXAMINE);
                        }
                        // 登录者的审核位置=现在已审核位置+1 ：待审核，审核按钮可用
                        if (Integer.valueOf(myOrder) > nowOrder + 1) {
                            approve.setActiveFlg(STATUS_FLAG_0);
                            approve.setActiveName(STATUS_EXAMINE);
                        }
                    }
                    // 当前审核进度为当前审核位置+2
                    nowOrder = nowOrder + 2;
                }

                // 检索条件是已审核且审核状态不是已审核的场合删除数据
                if (selectStatus.equals(STATUS_FLAG_1) && !approve.getStatus().equals(STATUS_CHECKED)) {
                    list.remove(i);
                }
                // 检索条件是待审核且审核状态是已审核的场合删除数据
                if (selectStatus.equals(Constants.CD_EXAMINE_WAIT) && approve.getStatus().equals(STATUS_CHECKED)) {
                    list.remove(i);
                }
                //申请时间截取到秒
                approve.setApplicationDate(approve.getApplicationDate().substring(0, 19));
                approve.setNowOrder(nowOrder);
                approve.setReviewSchedule(100 * nowOrder / (maxOrder + 2));
                approve.setMaxOrder(maxOrder);
            }

            // 审批进度排序
            if (approveParameterExEntity.getSortColumn().equals("reviewSchedule") && approveParameterExEntity.
                    getSortType() != null && !approveParameterExEntity.getSortType().equals("")) {
                // 画面选择升序时
                if (approveParameterExEntity.getSortType().equals("ascending")) {
                    Collections.sort(list, new Comparator<ApproveListExEntity>() {
                        public int compare(ApproveListExEntity p1, ApproveListExEntity p2) {
                            // 按照进度进行升序排列
                            if (p1.getReviewSchedule() > p2.getReviewSchedule()) {
                                return 1;
                            }
                            if (p1.getReviewSchedule() == p2.getReviewSchedule()) {
                                return 0;
                            }
                            return -1;
                        }
                    });
                } else {
                    // 画面选择降序时
                    Collections.sort(list, new Comparator<ApproveListExEntity>() {
                        public int compare(ApproveListExEntity p2, ApproveListExEntity p1) {
                            // 按照进度进行降序排列
                            if (p1.getReviewSchedule() > p2.getReviewSchedule()) {
                                return 1;
                            }
                            if (p1.getReviewSchedule() == p2.getReviewSchedule()) {
                                return 0;
                            }
                            return -1;
                        }
                    });
                }
            }
            logger.info("searchPage#result:{}", list);
            return list;
        }
    }

    //删除驳回
    public String delete(String approveNum) {
        approveListMapper.delete(approveNum);
        approveListMapper.delete1(approveNum);
        File file = new File("C:\\\\G20005J5\\\\uploads\\\\" + approveNum);
        File file1 = new File("C:\\\\G20005J5\\\\temp\\\\" + approveNum);
        ApproveListService.delAllFile(file);
        ApproveListService.delAllFile(file1);
        return "删除成功";
    }

    /**
     * 删除文件或文件夹
     *
     * @param directory
     */
    public static void delAllFile(File directory) {
        if (!directory.isDirectory()) {
            directory.delete();
        } else {
            File[] files = directory.listFiles();

            // 空文件夹
            if (files.length == 0) {
                directory.delete();
                System.out.println("删除" + directory.getAbsolutePath());
                return;
            }

            // 删除子文件夹和子文件
            for (File file : files) {
                if (file.isDirectory()) {
                    delAllFile(file);
                } else {
                    file.delete();
                    System.out.println("删除" + file.getAbsolutePath());
                }
            }

            // 删除文件夹本身
            directory.delete();
            System.out.println("删除" + directory.getAbsolutePath());
        }
    }
}
