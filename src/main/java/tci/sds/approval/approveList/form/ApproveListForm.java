/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.approveList.form;

import java.util.Arrays;
import java.util.List;

import tci.sds.approval.approveList.Entity.ApproveListExEntity;
import tci.sds.approval.approveList.Entity.ListExEntity;
import tci.sds.common.exception.ErrorType;

/**
 * 审批一览用Form
 * 
 * @author 房帅
 * @since 2020/11/11
 * @version 0.1
 */
public class ApproveListForm {

     /** 画面一覧 */
    private List<ApproveListExEntity> datagrid;

    /** 画面一覧 */
    private List<ListExEntity> selectStatus;

    /**确认驳回件数 */
    private String rejectConfirCount;

    /** 待确认件数 */
    private String rejectWaitCount;

    /** 待审核件数 */
    private String examineWaitCount;

    /** 审核驳回件数 */
    private String examineConfirCount;

    /** 总件数 */
    private String allCount;

    /** 登录者 */
    private String userId;

    /** 文件名 */
    private String searchFileName;

    /** 所属状态名 */
    private String searchStatus;

    /** 确认者 */
    private String affirmant;

    /** 申请者 */
    private String appUserNameCard;

    /** 申請时间 */
    private String applicationDate;
    private String[] appDate;
    private String endDate;
    private String startDate;

    /** 现在页数 */
    private int currentPage;

    /** １页内表示件数 */
    private int pageSize;

    /** 申请编号 */
    private String approveNumber;

    /** 文件路径 */
    private String filePath;

    /** 排序字段 */
    private String sortColumn;

    /** 排序的正倒 */
    private String sortType;

    /** 文件个数 */
    private int fileSize;

    /** 审核终了状态 */
    private String examileOver;

    /** errorCODE */
    private String errorCode = ErrorType.NotError.toString();

    /** 埋入文字 */
    private List<String> errorMessages;

    /**
     * @return the datagrid
     */
    public List<ApproveListExEntity> getDatagrid() {
        return datagrid;
    }

    /**
     * @param datagrid the datagrid to set
     */
    public void setDatagrid(List<ApproveListExEntity> datagrid) {
        this.datagrid = datagrid;
    }

    /**
     * @return the selectStatus
     */
    public List<ListExEntity> getSelectStatus() {
        return selectStatus;
    }

    /**
     * @param selectStatus the selectStatus to set
     */
    public void setSelectStatus(List<ListExEntity> selectStatus) {
        this.selectStatus = selectStatus;
    }

    /**
     * @return the rejectConfirCount
     */
    public String getRejectConfirCount() {
        return rejectConfirCount;
    }

    /**
     * @param rejectConfirCount the rejectConfirCount to set
     */
    public void setRejectConfirCount(String rejectConfirCount) {
        this.rejectConfirCount = rejectConfirCount;
    }

    /**
     * @return the rejectWaitCount
     */
    public String getRejectWaitCount() {
        return rejectWaitCount;
    }

    /**
     * @param rejectWaitCount the rejectWaitCount to set
     */
    public void setRejectWaitCount(String rejectWaitCount) {
        this.rejectWaitCount = rejectWaitCount;
    }

    /**
     * @return the examineWaitCount
     */
    public String getExamineWaitCount() {
        return examineWaitCount;
    }

    /**
     * @param examineWaitCount the examineWaitCount to set
     */
    public void setExamineWaitCount(String examineWaitCount) {
        this.examineWaitCount = examineWaitCount;
    }

    /**
     * @return the examineConfirCount
     */
    public String getExamineConfirCount() {
        return examineConfirCount;
    }

    /**
     * @param examineConfirCount the examineConfirCount to set
     */
    public void setExamineConfirCount(String examineConfirCount) {
        this.examineConfirCount = examineConfirCount;
    }

    /**
     * @return the allCount
     */
    public String getAllCount() {
        return allCount;
    }

    /**
     * @param allCount the allCount to set
     */
    public void setAllCount(String allCount) {
        this.allCount = allCount;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the searchFileName
     */
    public String getSearchFileName() {
        return searchFileName;
    }

    /**
     * @param searchFileName the searchFileName to set
     */
    public void setSearchFileName(String searchFileName) {
        this.searchFileName = searchFileName;
    }



    public String[] getAppDate() {
        return appDate;
    }

    public void setAppDate(String[] appDate) {
        this.appDate = appDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the applicationDate
     */
    public String getapplicationDate() {
        return applicationDate;
    }

    /**
     * @param applicationDate the searchFileName to set
     */
    public void setapplicationDate(String applicationDate) {
        this.applicationDate = applicationDate;
    }

    /**
     * @return the searchStatus
     */
    public String getSearchStatus() {
        return searchStatus;
    }

    /**
     * @param searchStatus the searchStatus to set
     */
    public void setSearchStatus(String searchStatus) {
        this.searchStatus = searchStatus;
    }

    /**
     * @return the affirmant
     */
    public String getaffirmant() {
        return affirmant;
    }

    /**
     * @param affirmant the affirmant to set
     */
    public void setaffirmant(String affirmant) {
        this.affirmant = affirmant;
    }

    /**
     * @return the appUserNameCard
     */
    public String getappUserNameCard() {
        return appUserNameCard;
    }

    /**
     * @param appUserNameCard the appUserNameCard to set
     */
    public void setappUserNameCard(String appUserNameCard) {
        this.appUserNameCard = appUserNameCard;
    }


    /**
     * @return the currentPage
     */
    public int getCurrentPage() {
        return currentPage;
    }

    /**
     * @param currentPage the currentPage to set
     */
    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    /**
     * @return the pageSize
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * @param pageSize the pageSize to set
     */
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * @return the approveNumber
     */
    public String getApproveNumber() {
        return approveNumber;
    }

    /**
     * @param approveNumber the approveNumber to set
     */
    public void setApproveNumber(String approveNumber) {
        this.approveNumber = approveNumber;
    }

    /**
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * @param filePath the filePath to set
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * @return the sortColumn
     */
    public String getSortColumn() {
        return sortColumn;
    }

    /**
     * @param sortColumn the sortColumn to set
     */
    public void setSortColumn(String sortColumn) {
        this.sortColumn = sortColumn;
    }

    /**
     * @return the sortType
     */
    public String getSortType() {
        return sortType;
    }

    /**
     * @param sortType the sortType to set
     */
    public void setSortType(String sortType) {
        this.sortType = sortType;
    }

    /**
     * @return the fileSize
     */
    public int getFileSize() {
        return fileSize;
    }

    /**
     * @param fileSize the fileSize to set
     */
    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    /**
     * @return the examileOver
     */
    public String getExamileOver() {
        return examileOver;
    }

    /**
     * @param examileOver the examileOver to set
     */
    public void setExamileOver(String examileOver) {
        this.examileOver = examileOver;
    }

    
    /**
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * @param errorCode the errorCode to set
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * @return the errorMessages
     */
    public List<String> getErrorMessages() {
        return errorMessages;
    }

    /**
     * @param errorMessages the errorMessages to set
     */
    public void setErrorMessages(List<String> errorMessages) {
        this.errorMessages = errorMessages;
    }

    @Override
    public String toString() {
        return "ApproveListForm{" +
                "datagrid=" + datagrid +
                ", selectStatus=" + selectStatus +
                ", rejectConfirCount='" + rejectConfirCount + '\'' +
                ", rejectWaitCount='" + rejectWaitCount + '\'' +
                ", examineWaitCount='" + examineWaitCount + '\'' +
                ", examineConfirCount='" + examineConfirCount + '\'' +
                ", allCount='" + allCount + '\'' +
                ", userId='" + userId + '\'' +
                ", searchFileName='" + searchFileName + '\'' +
                ", searchStatus='" + searchStatus + '\'' +
                ", affirmant='" + affirmant + '\'' +
                ", appUserNameCard='" + appUserNameCard + '\'' +
                ", applicationDate='" + applicationDate + '\'' +
                ", appDate=" + Arrays.toString(appDate) +
                ", endDate='" + endDate + '\'' +
                ", startDate='" + startDate + '\'' +
                ", currentPage=" + currentPage +
                ", pageSize=" + pageSize +
                ", approveNumber='" + approveNumber + '\'' +
                ", filePath='" + filePath + '\'' +
                ", sortColumn='" + sortColumn + '\'' +
                ", sortType='" + sortType + '\'' +
                ", fileSize=" + fileSize +
                ", examileOver='" + examileOver + '\'' +
                ", errorCode='" + errorCode + '\'' +
                ", errorMessages=" + errorMessages +
                '}';
    }

}
