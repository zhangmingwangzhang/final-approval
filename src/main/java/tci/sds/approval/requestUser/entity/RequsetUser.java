package tci.sds.approval.requestUser.entity;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class RequsetUser {

    /**
     * 文件姓名
     */
    private String fileName;

    /** 当前页数 */
    private int currentPage;

    /** １页的件数 */
    private int pageSize;

    /**
     * 第一审核
     */
    private String oneExamine;
    /**
     * 第二审核
     */
    private String twoExamine;
    /**
     * 第三审核
     */
    private String threeExamine;
    /**
     * 第四审核
     */
    private String fourExamine;

   /**
    *申请人
    */
   private  String koshinName;
    /**
     *总页数
     */
    private int total;
    private  int counts;
    private  String onecode;
    private  String twocode;
    private  String threecode;
    private  String fourcode;
    private  String koshincode;
    private  int id;
   private  String applicationdate;



    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getOneExamine() {
        return oneExamine;
    }

    public void setOneExamine(String oneExamine) {
        this.oneExamine = oneExamine;
    }

    public String getTwoExamine() {
        return twoExamine;
    }

    public void setTwoExamine(String twoExamine) {
        this.twoExamine = twoExamine;
    }

    public String getThreeExamine() {
        return threeExamine;
    }

    public void setThreeExamine(String threeExamine) {
        this.threeExamine = threeExamine;
    }

    public String getFourExamine() {
        return fourExamine;
    }

    public void setFourExamine(String fourExamine) {
        this.fourExamine = fourExamine;
    }

    public String getKoshinName() {
        return koshinName;
    }

    public void setKoshinName(String koshinName) {
        this.koshinName = koshinName;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getCounts() {
        return counts;
    }

    public void setCounts(int counts) {
        this.counts = counts;
    }

    public String getOnecode() {
        return onecode;
    }

    public void setOnecode(String onecode) {
        this.onecode = onecode;
    }

    public String getTwocode() {
        return twocode;
    }

    public void setTwocode(String twocode) {
        this.twocode = twocode;
    }

    public String getThreecode() {
        return threecode;
    }

    public void setThreecode(String threecode) {
        this.threecode = threecode;
    }

    public String getFourcode() {
        return fourcode;
    }

    public void setFourcode(String fourcode) {
        this.fourcode = fourcode;
    }

    public String getKoshincode() {
        return koshincode;
    }

    public void setKoshincode(String koshincode) {
        this.koshincode = koshincode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getApplicationdate() {
        return applicationdate;
    }

    public void setApplicationdate(String applicationdate) {
        this.applicationdate = applicationdate;
    }

    @Override
    public String toString() {
        return "RequsetUser{" +
                "fileName='" + fileName + '\'' +
                ", currentPage=" + currentPage +
                ", pageSize=" + pageSize +
                ", oneExamine='" + oneExamine + '\'' +
                ", twoExamine='" + twoExamine + '\'' +
                ", threeExamine='" + threeExamine + '\'' +
                ", fourExamine='" + fourExamine + '\'' +
                ", koshinName='" + koshinName + '\'' +
                ", total=" + total +
                ", counts=" + counts +
                ", onecode='" + onecode + '\'' +
                ", twocode='" + twocode + '\'' +
                ", threecode='" + threecode + '\'' +
                ", fourcode='" + fourcode + '\'' +
                ", koshincode='" + koshincode + '\'' +
                ", id=" + id +
                ", applicationdate='" + applicationdate + '\'' +
                '}';
    }
}
