package tci.sds.approval.requestUser.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tci.sds.admin.fileManage.entity.FileManageExEntity;
import tci.sds.admin.fileManage.entity.PermissionChangeDataEntity;
import tci.sds.admin.fileManage.form.FileManageForm;
import tci.sds.approval.approveList.form.ApproveListForm;
import tci.sds.approval.requestUser.entity.RequsetUser;

import tci.sds.approval.requestUser.form.Requestform;
import tci.sds.approval.requestUser.service.RequsetUserService;
import tci.sds.common.util.Constants;

import java.util.ArrayList;
import java.util.List;

@Controller
public class RequsetUserController {

    @Autowired
    private RequsetUserService requsetUserService;
    /**
     * 通过
     * @return
     */
    @RequestMapping("/api/requsetUser/pass")
    @ResponseBody
    public void pass( @RequestBody RequsetUser requsetUser){
        requsetUserService.pass(requsetUser);
    }
    /**
     * 驳回
     * @return
     */
    @RequestMapping("/api/requsetUser/bohui")
    @ResponseBody
    public void bohui( @RequestBody RequsetUser requsetUser){

        requsetUserService.bohui(requsetUser);
    }
    /**
     * 页面初始化
     * @return
     */
    @ResponseBody
    @PostMapping("/api/requsetUser/init")
    public List<RequsetUser> init( @RequestBody ApproveListForm approveListForm){
        List<RequsetUser> init = requsetUserService.init(approveListForm);
        int total = this.requsetUserService.searchTotal(approveListForm);
        for (int i = 0; i < init.size(); i++) {
            init.get(i).setTotal(total);
        }
        return init;
    }
    /**
     * 页面搜索
     * @return
     */
    @ResponseBody
    @PostMapping("/api/requsetUser/search")
    public List<RequsetUser> search( @RequestBody Requestform requestform){

        List<RequsetUser> search = requsetUserService.search(requestform);
        return search;
    }


    @ResponseBody
    @GetMapping("/api/requsetUser/reviewsearch/{applicationdate}")
    public List<PermissionChangeDataEntity> reviewsearch(@PathVariable("applicationdate" ) String applicationdate){

        return requsetUserService.reviewsearch(applicationdate);
    }

    @ResponseBody
    @GetMapping("/api/requsetUser/changedatasearch/{applicationdate}")
    public List<PermissionChangeDataEntity> changeDataSearch(@PathVariable("applicationdate" ) String applicationdate){

          return requsetUserService.changedatasearch(applicationdate);
    }
    @ResponseBody
    @GetMapping("/api/requsetUser/viewChangesdata/{applicationdate}")
    public List<PermissionChangeDataEntity> viewChangesdata(@PathVariable("applicationdate" ) String applicationdate){

        return requsetUserService.viewChangesdata(applicationdate);
    }

}
