package tci.sds.approval.requestUser.service;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tci.sds.admin.fileManage.entity.FileManageExEntity;

import tci.sds.admin.fileManage.entity.PermissionChangeDataEntity;
import tci.sds.admin.fileManage.entity.ShenherenExEntity;
import tci.sds.approval.approveList.Entity.ApproveListExEntity;
import tci.sds.approval.approveList.form.ApproveListForm;

import tci.sds.approval.requestNewConfirm.Entity.RequestNewConfirmEntity;
import tci.sds.approval.requestUser.entity.RequsetUser;

import tci.sds.approval.requestUser.form.Requestform;
import tci.sds.approval.requestUser.mapper.RequsetUserMapper;
import tci.sds.common.exception.BusinessException;
import tci.sds.common.service.InfoMessageService;
import tci.sds.common.service.SendEmailPasswordService;
import tci.sds.common.unit.EamilOperatingClass;

import javax.mail.MessagingException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class RequsetUserService {
    @Autowired
    RequsetUserMapper requsetUserMapper;
    @Autowired
    private SendEmailPasswordService sendEmailPasswordService;
    @Autowired
    private InfoMessageService infoMessageService;

    /**
     * 初始化
    * @return
     */
    public List<RequsetUser> init(ApproveListForm approveListForm){
        //
        List<RequsetUser> list = requsetUserMapper.init(approveListForm.getUserId());
        // 画面显示数据
        // 计算画面第一条数据位置
        int start = (approveListForm.getCurrentPage() - 1) * approveListForm.getPageSize();
        // 计算画面最后一条数据位置
        int end = approveListForm.getPageSize() * approveListForm.getCurrentPage();
        // 计算出的画面最后一条数据位置大于数据总条数时，最后一条数据位置=数据总条数
        if(end > list.size()) {
            end = list.size();
        }
        // 截取出画面显示的数据
        list = list.subList(start, end);
        return list;
    }
    /**
     * 搜索
     * @return
     */
    public List<RequsetUser> search(Requestform requestform){
        List<RequsetUser> search = requsetUserMapper.search(requestform);
        // 画面显示数据
        // 计算画面第一条数据位置
        int start = (requestform.getCurrentPage() - 1) * requestform.getPageSize();
        // 计算画面最后一条数据位置
        int end = requestform.getPageSize() * requestform.getCurrentPage();
        // 计算出的画面最后一条数据位置大于数据总条数时，最后一条数据位置=数据总条数
        if(end > search.size()) {
            end = search.size();
        }
        // 截取出画面显示的数据
        search = search.subList(start, end);
        int total = this.requsetUserMapper.allCount(requestform);
        for (int i = 0; i < search.size(); i++) {
            search.get(i).setTotal(total);
        }
        return search;
    }
    /**
     * 总件数取得
     *
     * @param
     * @return 总件数
     */
    @Transactional(readOnly = true)
    public int searchTotal(ApproveListForm approveListForm) {

        // 总件数
        int searchTotalSize = this.requsetUserMapper.getTotalCount(approveListForm.getUserId());

        return searchTotalSize ;
    }
    /**
     * 审核通过
     * @return
     */
    public void pass(RequsetUser requsetUser){
        if (requsetUser.getCounts()==0){
            System.out.println(requsetUser);
            ShenherenExEntity shenherenExEntity1 = requsetUserMapper.select1();
            ShenherenExEntity shenherenExEntity2 = requsetUserMapper.select2();
            requsetUser.setId(requsetUser.getId());
            requsetUser.setTwoExamine(shenherenExEntity2.getUsername()+"（待审核）");
            requsetUser.setOneExamine(shenherenExEntity1.getUsername()+"（已通过）");
            requsetUser.setCounts(requsetUser.getCounts()+1);
            requsetUserMapper.update1(requsetUser);
            //邮箱密码
            String password = this.sendEmailPasswordService.selectPassword();
            String space ="\u3000\u3000";
            // 邮件内容
            String selectmail = requsetUserMapper.selectmail(shenherenExEntity2.getTjCode());
            String mailInfo = shenherenExEntity2.getUsername()+"桑\r\n" + "工作辛苦了，济南审批管理系统。\r\n\r\n"+space+"名为"+"【"+requsetUser.getFileName()+"】"+"的文件权限更改申请请您审批。\r\n\r\n"+space+"内网URL：http://172.17.0.30:8888/index.html\r\n"+space+"外网URL：http://60.216.7.187:9999\r\n\r\n"+"以上，请多关照。\r\n"+"济南审批管理系统\r\n\r\n"+"该邮件为自动发送邮件，不需要回复。";
            EamilOperatingClass eamilOperatingClass = new EamilOperatingClass();
            System.out.println(selectmail);
            System.out.println(mailInfo);
            System.out.println(sendEmailPasswordService);
            boolean sendFlag = false;
            try {
                sendFlag = eamilOperatingClass.sendTest("您有申请文件需要审批", selectmail,mailInfo,password,this.sendEmailPasswordService.getKosinTime());
            } catch (MessagingException e) {
                e.printStackTrace();
            }

        }else if (requsetUser.getCounts()==1){
            requsetUser.setId(requsetUser.getId());
            ShenherenExEntity shenherenExEntity2 = requsetUserMapper.select2();
            ShenherenExEntity shenherenExEntity3 = requsetUserMapper.select3();
            requsetUser.setThreeExamine(shenherenExEntity3.getUsername()+"（待审核）");
            requsetUser.setTwoExamine(shenherenExEntity2.getUsername()+"（已通过）");
            requsetUser.setCounts(requsetUser.getCounts()+1);
            requsetUserMapper.update2(requsetUser);
            //邮箱密码
            String password = this.sendEmailPasswordService.selectPassword();
            String space ="\u3000\u3000";
            // 邮件内容
            String selectmail = requsetUserMapper.selectmail(shenherenExEntity3.getTjCode());
            String mailInfo = shenherenExEntity3.getUsername()+"桑\r\n" + "工作辛苦了，济南审批管理系统。\r\n\r\n"+space+"名为"+"【"+requsetUser.getFileName()+"】"+"的文件权限更改申请请您审批。\r\n\r\n"+space+"内网URL：http://172.17.0.30:8888/index.html\r\n"+space+"外网URL：http://60.216.7.187:9999\r\n\r\n"+"以上，请多关照。\r\n"+"济南审批管理系统\r\n\r\n"+"该邮件为自动发送邮件，不需要回复。";
            EamilOperatingClass eamilOperatingClass = new EamilOperatingClass();
            boolean sendFlag = false;
            try {
                sendFlag = eamilOperatingClass.sendTest("您有申请文件需要审批", selectmail,mailInfo,password,this.sendEmailPasswordService.getKosinTime());
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }else if (requsetUser.getCounts()==2){
            requsetUser.setId(requsetUser.getId());
            ShenherenExEntity shenherenExEntity3 = requsetUserMapper.select3();
            ShenherenExEntity shenherenExEntity4 = requsetUserMapper.select4();
            requsetUser.setFourExamine(shenherenExEntity4.getUsername()+"（待审核）");
            requsetUser.setThreeExamine(shenherenExEntity3.getUsername()+"（已通过）");
            requsetUser.setCounts(requsetUser.getCounts()+1);
            requsetUserMapper.update3(requsetUser);
            //邮箱密码
            String password = this.sendEmailPasswordService.selectPassword();
            String space ="\u3000\u3000";
            // 邮件内容
            String selectmail = requsetUserMapper.selectmail(shenherenExEntity4.getTjCode());
            String mailInfo = shenherenExEntity4.getUsername()+"桑\r\n" + "工作辛苦了，济南审批管理系统。\r\n\r\n"+space+"名为"+"【"+requsetUser.getFileName()+"】"+"的文件权限更改申请请您审批。\r\n\r\n"+space+"内网URL：http://172.17.0.30:8888/index.html\r\n"+space+"外网URL：http://60.216.7.187:9999\r\n\r\n"+"以上，请多关照。\r\n"+"济南审批管理系统\r\n\r\n"+"该邮件为自动发送邮件，不需要回复。";
            EamilOperatingClass eamilOperatingClass = new EamilOperatingClass();
            boolean sendFlag = false;
            try {
                sendFlag = eamilOperatingClass.sendTest("您有申请文件需要审批", selectmail,mailInfo,password,this.sendEmailPasswordService.getKosinTime());
                // 获取InfoMessage信息
                String infoMessage = infoMessageService.getInfo("INF0000007");
                RequestNewConfirmEntity requestNewConfirmEntity=new RequestNewConfirmEntity();
                requestNewConfirmEntity.setInfoMessage(infoMessage);

            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }else if (requsetUser.getCounts()==3){
            requsetUser.setId(requsetUser.getId());
            ShenherenExEntity shenherenExEntity4 = requsetUserMapper.select4();
            requsetUser.setFourExamine(shenherenExEntity4.getUsername()+"（已通过）");
            requsetUser.setCounts(requsetUser.getCounts()+1);
            requsetUserMapper.update4(requsetUser);
        }
    }
    /**
     * 审核驳回
     * @return
     */
    public void bohui(RequsetUser requsetUser){


        if (requsetUser.getCounts()==0){
            System.out.println(requsetUser);
            ShenherenExEntity shenherenExEntity1 = requsetUserMapper.select1();
            ShenherenExEntity shenherenExEntity2 = requsetUserMapper.select2();
            requsetUser.setId(requsetUser.getId());
            requsetUser.setTwoExamine(shenherenExEntity2.getUsername()+"（未审核）");
            requsetUser.setOneExamine(shenherenExEntity1.getUsername()+"（已驳回）");
            requsetUser.setCounts(5);
            requsetUserMapper.update1(requsetUser);
        }else if (requsetUser.getCounts()==1){
            requsetUser.setId(requsetUser.getId());
            ShenherenExEntity shenherenExEntity2 = requsetUserMapper.select2();
            ShenherenExEntity shenherenExEntity3 = requsetUserMapper.select3();
            requsetUser.setThreeExamine(shenherenExEntity3.getUsername()+"（未审核）");
            requsetUser.setTwoExamine(shenherenExEntity2.getUsername()+"（已驳回）");
            requsetUser.setCounts(5);
            requsetUserMapper.update2(requsetUser);
        }else if (requsetUser.getCounts()==2){
            requsetUser.setId(requsetUser.getId());
            ShenherenExEntity shenherenExEntity3 = requsetUserMapper.select3();
            ShenherenExEntity shenherenExEntity4 = requsetUserMapper.select4();
            requsetUser.setFourExamine(shenherenExEntity4.getUsername()+"（未审核）");
            requsetUser.setThreeExamine(shenherenExEntity3.getUsername()+"（已驳回）");
            requsetUser.setCounts(5);
            requsetUserMapper.update3(requsetUser);
        }else if (requsetUser.getCounts()==3){
            requsetUser.setId(requsetUser.getId());
            ShenherenExEntity shenherenExEntity4 = requsetUserMapper.select4();
            requsetUser.setFourExamine(shenherenExEntity4.getUsername()+"（已驳回）");
            requsetUser.setCounts(5);
            requsetUserMapper.update4(requsetUser);
        }

        //邮箱密码
        String password = this.sendEmailPasswordService.selectPassword();
        String space ="\u3000\u3000";
        // 邮件内容
        String selectmail = requsetUserMapper.selectmail(requsetUser.getKoshincode());
        String mailInfo = requsetUser.getKoshinName()+"桑\r\n" + "工作辛苦了，济南审批管理系统。\r\n\r\n"+space+"名为"+"【"+requsetUser.getFileName()+"】"+"的文件权限更改申请已被驳回。\r\n\r\n"+space+"内网URL：http://172.17.0.30:8888/index.html\r\n"+space+"外网URL：http://60.216.7.187:9999\r\n\r\n"+"以上，请多关照。\r\n"+"济南审批管理系统\r\n\r\n"+"该邮件为自动发送邮件，不需要回复。";
        EamilOperatingClass eamilOperatingClass = new EamilOperatingClass();
        boolean sendFlag = false;
        try {
            sendFlag = eamilOperatingClass.sendTest("您有申请文件需要审批", selectmail,mailInfo,password,this.sendEmailPasswordService.getKosinTime());
            if (!sendFlag) {
                // 获取InfoMessage信息
                String infoMessage = infoMessageService.getInfo("INF0000007");
                RequestNewConfirmEntity requestNewConfirmEntity=new RequestNewConfirmEntity();
                requestNewConfirmEntity.setInfoMessage(infoMessage);
            }
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public  List<PermissionChangeDataEntity> changedatasearch(String applicationdate) {
       return requsetUserMapper.changedatasearch(applicationdate);

    }
    public  List<PermissionChangeDataEntity> reviewsearch(String applicationdate) {
        return requsetUserMapper.reviewsearch(applicationdate);

    }

    public List<PermissionChangeDataEntity> viewChangesdata(String applicationdate) {
        return requsetUserMapper.viewChangesdata(applicationdate);
    }
}
