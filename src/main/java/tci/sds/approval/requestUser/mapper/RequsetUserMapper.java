package tci.sds.approval.requestUser.mapper;

import org.apache.ibatis.annotations.Mapper;
import tci.sds.admin.fileManage.entity.FileManageExEntity;

import tci.sds.admin.fileManage.entity.PermissionChangeDataEntity;
import tci.sds.admin.fileManage.entity.ShenherenExEntity;
import tci.sds.approval.approveList.form.ApproveListForm;
import tci.sds.approval.requestUser.entity.RequsetUser;
import tci.sds.approval.requestUser.form.Requestform;

import java.util.List;

@Mapper
public interface RequsetUserMapper {
    /**
     * 初始化
     */
    List<RequsetUser> init(String userId);

    /**
     * 搜索
     */
    List<RequsetUser> search(Requestform requestform);
    int allCount(Requestform requestform);

    /**
     * 总件数取得
     * @return 总件数
     */
    int getTotalCount(String userId);

    /**
     * 审核通过或驳回
     */
    ShenherenExEntity select1();
    ShenherenExEntity select2();
    ShenherenExEntity select3();
    ShenherenExEntity select4();
    void update1(RequsetUser requsetUser);
    void update2(RequsetUser requsetUser);
    void update3(RequsetUser requsetUser);
    void update4(RequsetUser requsetUser);

    //根据卡号查询审核人邮箱地址
    String selectmail(String code);

    List<PermissionChangeDataEntity> changedatasearch(String applicationdate);
    List<PermissionChangeDataEntity> reviewsearch(String applicationdate);


    List<PermissionChangeDataEntity> viewChangesdata(String applicationdate);
}




















