package tci.sds.approval.requestUser.form;


import tci.sds.approval.approveList.Entity.ApproveListExEntity;
import tci.sds.approval.approveList.Entity.ListExEntity;
import tci.sds.common.exception.ErrorType;

import java.util.Arrays;
import java.util.List;

public class Requestform {
    /** 画面一覧 */
    private List<ApproveListExEntity> datagrid;

    /** 画面一覧 */
    private List<ListExEntity> selectStatus;

    /** 总件数 */
    private String allCount;

    /** 登录者 */
    private String userId;

    /** 文件名 */
    private String filename;

    /** 申请者 */
    private String koshinname;

    /** 现在页数 */
    private int currentPage;

    /** １页内表示件数 */
    private int pageSize;

    /** 排序字段 */
    private String sortColumn;

    /** 排序的正倒 */
    private String sortType;

    /** errorCODE */
    private String errorCode = ErrorType.NotError.toString();

    /** 埋入文字 */
    private List<String> errorMessages;

    public List<ApproveListExEntity> getDatagrid() {
        return datagrid;
    }

    public void setDatagrid(List<ApproveListExEntity> datagrid) {
        this.datagrid = datagrid;
    }

    public List<ListExEntity> getSelectStatus() {
        return selectStatus;
    }

    public void setSelectStatus(List<ListExEntity> selectStatus) {
        this.selectStatus = selectStatus;
    }

    public String getAllCount() {
        return allCount;
    }

    public void setAllCount(String allCount) {
        this.allCount = allCount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getKoshinname() {
        return koshinname;
    }

    public void setKoshinname(String koshinname) {
        this.koshinname = koshinname;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSortColumn() {
        return sortColumn;
    }

    public void setSortColumn(String sortColumn) {
        this.sortColumn = sortColumn;
    }

    public String getSortType() {
        return sortType;
    }

    public void setSortType(String sortType) {
        this.sortType = sortType;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public List<String> getErrorMessages() {
        return errorMessages;
    }

    public void setErrorMessages(List<String> errorMessages) {
        this.errorMessages = errorMessages;
    }

    @Override
    public String toString() {
        return "Requestform{" +
                "datagrid=" + datagrid +
                ", selectStatus=" + selectStatus +
                ", allCount='" + allCount + '\'' +
                ", userId='" + userId + '\'' +
                ", filename='" + filename + '\'' +
                ", koshinname='" + koshinname + '\'' +
                ", currentPage=" + currentPage +
                ", pageSize=" + pageSize +
                ", sortColumn='" + sortColumn + '\'' +
                ", sortType='" + sortType + '\'' +
                ", errorCode='" + errorCode + '\'' +
                ", errorMessages=" + errorMessages +
                '}';
    }
}
