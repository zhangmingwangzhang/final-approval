/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.requestNewConfirm.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tci.sds.approval.requestNewConfirm.Entity.RequestNewConfirmEntity;
import tci.sds.approval.requestNewConfirm.mapper.RequestNewConfirmMapper;

/**
 * 确认画面用Service
 * 
 * @author dongyihan
 * @since 2020/11/09
 * @version 0.1
 */
@Service
public class RequestNewConfirmService {
    private final Logger logger = LoggerFactory.getLogger(RequestNewConfirmService.class);

    @Autowired
    private RequestNewConfirmMapper requestNewConfirmMapper;
   
    /**
     * 确认画面的确认人基本信息取得
     * @return RequestNewConfirmEntity
     */
    public RequestNewConfirmEntity searchData(String approveNum,String tjCode) {
        
        logger.info("searchData#param:{}", approveNum,tjCode);
        
        // 查询确认人基本信息
        RequestNewConfirmEntity requestNewConfirmEntity = requestNewConfirmMapper.searchData(approveNum,tjCode);
        
        logger.info("searchData#result:{}", requestNewConfirmEntity);
        
        return requestNewConfirmEntity;

    }

     /**
     * 确认画面的确认文件存在Check
     * @return RequestNewConfirmEntity
     */
    public RequestNewConfirmEntity check(RequestNewConfirmEntity requestNewConfirmExEntity) {

        logger.info("check#param:{}", requestNewConfirmExEntity);
        
        // 确认文件存在Check
        RequestNewConfirmEntity ｒequestNewConfirmEntity = requestNewConfirmMapper.check(requestNewConfirmExEntity);   
        
        logger.info("check#result:{}", ｒequestNewConfirmEntity);
        
        return ｒequestNewConfirmEntity;

    }
    
    /**
     * 确认通过处理
     * @return res
     */
    public void updateConfirm(RequestNewConfirmEntity ｒequestNewConfirmEntity) {

        logger.info("updateConfirm#param:{}", ｒequestNewConfirmEntity);
        
        // 确认通过,更新审批文件存储表
        requestNewConfirmMapper.updateConfirm(ｒequestNewConfirmEntity);
               
    }

    /**
     * 检索审核人的邮箱
     * @return 审核人的Email
     */
    public RequestNewConfirmEntity selectEmail(RequestNewConfirmEntity requestNewConfirmExEntity) {

        logger.info("selectEmail#param:{}", requestNewConfirmExEntity);
        
        // 检索审核人的邮箱
        RequestNewConfirmEntity configInfo = requestNewConfirmMapper.selectEmail(requestNewConfirmExEntity);
        
        logger.info("selectEmail#result:{}", configInfo);
        
        return configInfo;
    }

    /**
     * 确认驳回处理
     * @return res
     */
    public void updateClear(RequestNewConfirmEntity ｒequestNewConfirmEntity) {

        logger.info("updateClear#param:{}", ｒequestNewConfirmEntity);
        
        // 确认驳回处理,更新审批文件存储表
        requestNewConfirmMapper.updateClear(ｒequestNewConfirmEntity);                                 

    }
    
    /**
     * 检索申请人的邮箱
     * @return 申请人的Email
      */  
    public RequestNewConfirmEntity selectApplicationEmail(RequestNewConfirmEntity requestNewConfirmExEntity) {

        logger.info("selectApplicationEmail#param:{}", requestNewConfirmExEntity);
        
        // 检索申请人的邮箱
        RequestNewConfirmEntity applicationInfo = requestNewConfirmMapper.selectApplicationEmail(requestNewConfirmExEntity);
        
        logger.info("selectApplicationEmail#result:{}", applicationInfo);
        
        return applicationInfo;

    }

    /**
     * 确认处理 更新审核伦理存储表状态
     * @return res
     */    
    public int updateStatus(RequestNewConfirmEntity ｒequestNewConfirmEntity) {
        
        logger.info("updateStatus#param:{}", ｒequestNewConfirmEntity);
        
        // 确认处理 更新审核伦理存储表状态
        int res = this.requestNewConfirmMapper.updateStatus(ｒequestNewConfirmEntity);
        
        logger.info("updateStatus#result:{}", res);
        
        return res;
        
    }
    
    /**
     * 检索预览的文件信息
     * @return RequestNewConfirmEntity
      */
    public RequestNewConfirmEntity selectFileInfo(String approveNum) {
       
       logger.info("selectFileInfo#param:{}", approveNum);
        
        // 检索预览的文件信息
        RequestNewConfirmEntity ｒequestNewConfirmEntity = requestNewConfirmMapper.selectFileInfo(approveNum);   
        
        logger.info("selectFileInfo#result:{}", ｒequestNewConfirmEntity);
        
        return ｒequestNewConfirmEntity;

    }
    
    /**
     * 检索预览的文件的签名Sheet
     * @return RequestNewConfirmEntity
      */
    public RequestNewConfirmEntity selectSheet(RequestNewConfirmEntity ｒequestNewConfirmEntity) {
        
        logger.info("selectSheet#param:{}", ｒequestNewConfirmEntity);
        
        // 检索预览的文件信息
        RequestNewConfirmEntity ｒequestNewConfirm = requestNewConfirmMapper.selectSheet(ｒequestNewConfirmEntity);   
        
        logger.info("selectSheet#result:{}", ｒequestNewConfirmEntity);
        
        return ｒequestNewConfirm;


    }

}
