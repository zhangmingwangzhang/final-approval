/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.requestNewConfirm.mapper;

import org.apache.ibatis.annotations.Mapper;
import tci.sds.approval.requestNewConfirm.Entity.RequestNewConfirmEntity;

/**
 * 确认画面用Mapper
 * 
 * @author dongyihan
 * @since 2020/11/09
 * @version 0.1
 */
@Mapper
public interface RequestNewConfirmMapper{

     /**
     * 确认画面的确认人基本信息取得
     * @return RequestNewConfirmEntity
      */
     RequestNewConfirmEntity searchData(String approveNum, String tjCode);
     
     /**
      * 确认画面的确认文件check
      * @return RequestNewConfirmEntity
      */     
     RequestNewConfirmEntity check(RequestNewConfirmEntity requestNewConfirmExEntity); 
     
     /**
      * 确认画面的确认通过处理
      * @return RequestNewConfirmEntity
       */
     void updateConfirm(RequestNewConfirmEntity ｒequestNewConfirmEntity);
     
     /**
      * 检索审核人的邮箱
      * @return 审核人的Email
       */    
     RequestNewConfirmEntity selectEmail(RequestNewConfirmEntity requestNewConfirmExEntity);
     
     /**
      * 确认驳回处理
      * @return res
      */
     void updateClear(RequestNewConfirmEntity ｒequestNewConfirmEntity);     
         
     /**
      * 检索申请人的邮箱
      * @return 申请人的Email
       */    
     RequestNewConfirmEntity selectApplicationEmail(RequestNewConfirmEntity requestNewConfirmExEntity);

     /**
      * 更新审核伦理存储表状态
      * @return res 
      */
     int updateStatus(RequestNewConfirmEntity ｒequestNewConfirmEntity);

     /**
      * 检索预览的文件信息
      * @return RequestNewConfirmEntity 
      */
     RequestNewConfirmEntity selectFileInfo(String approveNum);
     
     /**
      * 检索预览的文件的签名sheet
      * @return RequestNewConfirmEntity 
      */
     RequestNewConfirmEntity selectSheet(RequestNewConfirmEntity ｒequestNewConfirmEntity);
}