/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.requestNewConfirm.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import tci.sds.approval.requestNewConfirm.Entity.RequestNewConfirmEntity;
import tci.sds.approval.requestNewConfirm.form.RequestNewConfirmForm;
import tci.sds.approval.requestNewConfirm.service.RequestNewConfirmService;
import tci.sds.common.entity.ConfigProperties;
import tci.sds.common.entity.FileTypeEnum;
import tci.sds.common.exception.BusinessException;
import tci.sds.common.service.InfoMessageService;
import tci.sds.common.service.SendEmailPasswordService;
import tci.sds.common.unit.EamilOperatingClass;
import tci.sds.common.unit.ExcelOperatingClass;
import tci.sds.common.util.Constants;

/**
 * 确认用Controller
 * 
 * @author dongyihan
 * @since 2020/11/09
 * @version 0.1
 */
@Controller
public class RequestNewConfirmController {
    private final Logger logger = LoggerFactory
         .getLogger(RequestNewConfirmController.class);

    private static ConfigProperties configProperties;
    @Autowired
    private RequestNewConfirmService requestNewConfirmService;
    @Autowired
    private SendEmailPasswordService sendEmailPasswordService;
    @Autowired
    private InfoMessageService infoMessageService;
    @Value("${commpat.config.temp-file-url}")
    private String tempfileurl;
    @Autowired
    public void setConfig(ConfigProperties config) {

        RequestNewConfirmController.configProperties = config;

    }
    
    /**
     * 确认画面初期表示
     * 
     * @param approveNum,tjCode 申请编号,申请者卡号
     * @return requestNewConfirmOutputForm 确认画面初期表示Form
     */
    @RequestMapping("/api/requestNewConfirm/init")
    @ResponseBody
    public RequestNewConfirmForm init(String approveNum ,String tjCode) throws Exception{

        logger.info("init#param:{}", approveNum,tjCode);

        // 确认画面初期化        
        RequestNewConfirmEntity requestNewConfirmEntity = this.requestNewConfirmService.searchData(approveNum,tjCode);
        // 创建requestNewConfirmOutputForm
        RequestNewConfirmForm requestNewConfirmOutputForm = new RequestNewConfirmForm();
         
        // 申请编号
        requestNewConfirmOutputForm.setApproveNum(approveNum);
        // 申请者/卡号
        requestNewConfirmOutputForm.setAppUserNameCard(requestNewConfirmEntity.getUserName() + "/" + requestNewConfirmEntity.getFileApplicantId());
        // 申请文件编号
        requestNewConfirmOutputForm.setFileId(requestNewConfirmEntity.getFileId());
        // 申请文件名称
        requestNewConfirmOutputForm.setFileName(requestNewConfirmEntity.getFileName());
        // 申请部门
        requestNewConfirmOutputForm.setAppUserDepartment(requestNewConfirmEntity.getDeptName());
        // 状态
        requestNewConfirmOutputForm.setStatus(requestNewConfirmEntity.getExamineStatus());
        // 范围
        requestNewConfirmOutputForm.setScopeApplication(requestNewConfirmEntity.getScopeApplication());
        // 业务内容
        requestNewConfirmOutputForm.setCondition(requestNewConfirmEntity.getCondition());          
        // 创建确认文件集合
        List<String> fileIdList = new ArrayList<>();    
        // 获取文件路径
        String filePath = requestNewConfirmEntity.getFilePath();       
        File file = new File(filePath);      
        // 获得该文件夹内的所有文件
        File[] array = file.listFiles();  
        
        // 遍历文件夹下所有的文件，取得文件名赋给文件集合
        for (int i = 0; i < array.length; i++) {
          fileIdList.add(array[i].getName());
        }
        
        // 确认文件一览
        requestNewConfirmOutputForm.setFileIdList(fileIdList);
        
        // 给session初始化
        HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = request.getSession();  
        session.setAttribute("userName", "");
        
        logger.info("init#result:{}", requestNewConfirmOutputForm);

        return requestNewConfirmOutputForm;

    }
      
    /**
     * 编辑下载Check
     * 
     * @param requestNewConfirmForm 确认画面Check的requestNewConfirmForm
     * @return requestNewConfirmEntity
     */   
    @RequestMapping("/api/requestNewConfirm/downLoadCheck")
    @ResponseBody
    public RequestNewConfirmEntity downLoadCheck(@ModelAttribute RequestNewConfirmForm requestNewConfirmForm) throws Exception{
        
        logger.info("downLoadCheck#param:{}", requestNewConfirmForm);
        
        String excleCheckBoolean = null;
        RequestNewConfirmEntity requestNewConfirmExEntity = this.copyFormToEntity(requestNewConfirmForm);
        // 文件基本信息检索
        RequestNewConfirmEntity ｒequestNewConfirmEntity = this.requestNewConfirmService.check(requestNewConfirmExEntity); 
        // 文件路径
        String rootPath = ｒequestNewConfirmEntity.getFilePath();
        
        // 文件路径
        String FullPath = rootPath + requestNewConfirmExEntity.getFileName();
        // 根据文件的路径 创建文件
        File file = new File(FullPath);
        
        // 确认文件存在check
        if (!file.exists()) {                 
          throw new BusinessException("ERR0000010");
        }
        //check预览文件是否损坏
        String suffix = FullPath.substring(FullPath.lastIndexOf("."));
        //判断文件类型
        if(".doc".equalsIgnoreCase(suffix) || ".docx".equalsIgnoreCase(suffix)) {
            // word
            excleCheckBoolean = ExcelOperatingClass.WordToPdfCheck(FullPath);
            if("fileError".equals(excleCheckBoolean)) {
                throw new BusinessException("ERR0000046");
            }
          } else if (".xls".equalsIgnoreCase(suffix) || ".xlsx".equalsIgnoreCase(suffix)){
            // excle
              excleCheckBoolean = ExcelOperatingClass.ExcelToPdfCheck(FullPath);
              if("fileError".equals(excleCheckBoolean)) {
                  throw new BusinessException("ERR0000046");
              }
          }   
        logger.info("downLoadCheck#result:{}", ｒequestNewConfirmEntity);       
        return ｒequestNewConfirmEntity;
        
    }
      
    /**
     * 编辑下载处理
     * 
     * @param filePath ,FileName 文件路径，文件名称
     * @return 
     */   
    @RequestMapping("/api/requestNewConfirm/download")
    @ResponseBody  
    public void downloadFile(HttpServletRequest request,
                HttpServletResponse response,String filePath,String fileName) throws IOException {   
        
        logger.info("downloadFile#param:{}", filePath,fileName);
        // 文件路径
        String rootPath = filePath;
        // 文件名称
        String fileFullName = fileName ;         
        // 将文件路径和文件名拼接得到文件全路径
        String FullPath = rootPath + fileFullName;
        File file = new File(FullPath);
        // 下载的文件名
        String downFileName = URLEncoder.encode(fileName, "UTF-8");
        downFileName = downFileName.replaceAll("\\+",  "%20");
       
        // 配置文件下载
        response.setHeader("content-type", "application/octet-stream");
        response.setContentType("application/octet-stream");
        // 下载文件能正常显示中文
        response.setHeader("Content-Disposition", "attachment;filename=" + downFileName);
        
        // 实现文件下载
        byte[] buffer = new byte[1024];
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        
        try {                    
          // 创建FileInputStream 对象
          fis = new FileInputStream(file);
          // 创建BufferedInputStream 对象
          bis = new BufferedInputStream(fis);
          OutputStream os = response.getOutputStream(); 
          // 从文件中按字节读取内容，到文件尾部时read方法将返回-1
          int num = bis.read(buffer);         
          // 判断 num 是否为-1
          while (num != -1) {
            // 下载读取到的文件内容
            os.write(buffer, 0, num);
            num = bis.read(buffer);
          }              
            logger.info("downloadFile#result:{}","Download successfully!");          
        } catch (Exception e) {           
          logger.info("downloadFile#result:{}","Download  failed!");          
        } finally {          
          // 关闭BufferedInputStream
          if (bis != null) {
            try {
              bis.close();
            } catch (IOException e) {
              e.printStackTrace();
            }
          }
          // 关闭FileInputStream
          if (fis != null) {
            try {
              fis.close();
            } catch (IOException e) {
              e.printStackTrace();
            }
          }
        }
        
    }
    
    /**
     * 在线预览
     * 
     * @param approveNum 申请编号
     * @return
     */
    @RequestMapping(value = "/api/requestNewConfirm/previewPdf", method = RequestMethod.GET)
    public void pdfStreamHandler(HttpServletRequest request,HttpServletResponse response,String fileName,String fileId,String approveNum) throws IOException{
        
        logger.info("pdfStreamHandler#param:{}", fileName,fileId,approveNum);
        //设置标志判断是否是生成pdf文件，是删除，不是保留
        boolean flage=true;
        // 需要在线预览的文件名称
        String fileFullName = approveNum + "//"+ fileName ;  
        // 判断要预览的文件是附件还是申请文件
        if (fileName.contains("附件")) {
          // excel转换pdf
          String pdfFileName = "";
          String suffix = fileName.substring(fileName.lastIndexOf("."));
          if(".doc".equalsIgnoreCase(suffix) || ".docx".equalsIgnoreCase(suffix)) {
            // pdf预览
            pdfFileName = ExcelOperatingClass.DocToPdf(fileFullName,approveNum);
          } else if (".xls".equalsIgnoreCase(suffix) || ".xlsx".equalsIgnoreCase(suffix)){
            // excle预览
            pdfFileName = ExcelOperatingClass.ExcelToPdf(fileFullName, approveNum);
          }  else if(".png".equalsIgnoreCase(suffix) || ".jpg".equalsIgnoreCase(suffix) || ".pdf".equalsIgnoreCase(suffix) ){
              flage=false;
              pdfFileName= configProperties.getExcelFileUrl()+approveNum + "\\"+ fileName;

          }else  if(".ppt".equalsIgnoreCase(suffix) || ".pptx".equalsIgnoreCase(suffix)){

              pdfFileName= ExcelOperatingClass.pptToPdf(configProperties.getExcelFileUrl(),approveNum,fileName);
          }
          File file = new File(pdfFileName);
        
          byte[] data = null; 
            
          try {
            FileInputStream input = new FileInputStream(file);
            data = new byte[input.available()];
            // 读取文件的信息
            input.read(data);
            // 把文件的信息设置给response
            response.getOutputStream().write(data);
            input.close();  
            // 在线预览临时生成的pdf文件删除             
              if(flage){
                  file.delete();
              }
            File fileDelete = new File(configProperties.getPDFFileUrl());
            // listFiles方法：返回file路径下所有文件和文件夹的绝对路径
            File[] listFiles = fileDelete.listFiles();
            for (File file2 : listFiles) {
                file2.delete();
            }
          } catch (Exception e) {       
            e.printStackTrace();          
          }
        }else {
            
            // 文件基本信息检索
            RequestNewConfirmEntity ｒequestNewConfirmEntity = this.requestNewConfirmService.selectFileInfo(approveNum); 
            RequestNewConfirmEntity ｒequestNewConfirm  = this.requestNewConfirmService.selectSheet(ｒequestNewConfirmEntity);
            // 签名sheet
            String reviewerSheet = ｒequestNewConfirm.getReviewerSheet();
            int sheetNo = Integer.parseInt(reviewerSheet);
                     
            String pdfFileName = "";
            String suffix = fileName.substring(fileName.lastIndexOf("."));
            if(".doc".equalsIgnoreCase(suffix) || ".docx".equalsIgnoreCase(suffix)) {
              // pdf预览
              pdfFileName = ExcelOperatingClass.DocToPdf(fileFullName,approveNum);
            } else if (".xls".equalsIgnoreCase(suffix) || ".xlsx".equalsIgnoreCase(suffix)){
              // excle预览
              pdfFileName = ExcelOperatingClass.ExcelToPdf(fileFullName, sheetNo,approveNum);
            }
            File file = new File(pdfFileName);
            
            if (file.exists()) {
              byte[] data = null;
              try {
                FileInputStream input = new FileInputStream(file);
                data = new byte[input.available()];
                input.read(data);
                response.getOutputStream().write(data);
                input.close();
                // 在线预览临时生成的pdf文件删除                            
                  if(flage){
                      file.delete();
                  }
                File fileDelete = new File(configProperties.getPDFFileUrl());
                // listFiles方法：返回file路径下所有文件和文件夹的绝对路径
                File[] listFiles = fileDelete.listFiles();
                for (File file2 : listFiles) {
                    file2.delete();
                }
              } catch (Exception e) {
                e.printStackTrace();
              }    
            }
        }
        
  }
    
    /**
     * 确认通过处理
     * @param requestNewConfirmForm 确认通过requestNewConfirmForm的Form
     * @return
     */   
    @Transactional(rollbackFor = Exception.class)
    @RequestMapping("/api/requestNewConfirm/confirm")
    @ResponseBody
    public RequestNewConfirmEntity confirm(HttpServletRequest request , @Valid  @ModelAttribute RequestNewConfirmForm requestNewConfirmForm) throws Exception{
        
        logger.info("confirm#param:{}", requestNewConfirmForm);  
        RequestNewConfirmEntity requestNewConfirmExEntity = this.copyFormToEntity(requestNewConfirmForm);
        
        // check 备注位数
        if(requestNewConfirmExEntity.getFileRemark().length()>100) {         
          throw new BusinessException("ERR0000006","备注","1～100");             
        }
        
        // chcek驳回理由是否为空
        if (!requestNewConfirmExEntity.getReason().equals("")) {       
          throw new BusinessException("ERR0000025","驳回理由");  
        }             
        
        // 根据申请编号 进行查询
        RequestNewConfirmEntity ｒequestNewConfirmEntity = this.requestNewConfirmService.check(requestNewConfirmExEntity); 
        
        // 申请编号存在check
        if (ｒequestNewConfirmEntity == null) {       
          throw new BusinessException("ERR0000009","审批文件存储表","申请编号");
         }              
                      
        // 文件路径
        String rootPath = ｒequestNewConfirmEntity.getFilePath();      
        File file = new File(rootPath);
        // 获得该文件夹内的所有文件
        File[] array = file.listFiles();          
        // 文件不存在时报错
        if ((array == null || array.length == 0) && requestNewConfirmExEntity.getFileIdList().size() == 0 ) {           
          throw new BusinessException("ERR0000001","确认文件");
        }
        
        // 判断不是附件的文件数
        int count = 0;
        for (int m = 0; m < array.length; m++) {
          if (!array[m].getName().contains("附件")) {
            count++;
          }
        }
        if (count!=1) {
            throw new BusinessException("ERR0000040"); 
        }
         
        // 判断是否有文件上传
        boolean flag = requestNewConfirmExEntity.isUploadFlag();         
        if(flag) {
          // 上传的文件存在check
          if (requestNewConfirmExEntity.getFile() == null){        
            throw new BusinessException("ERR0000035");
          }
          
          // 上传文件的总大小
          int countSzie = 0;   
          // 循环所有上传的文件
          for (int i = 0; i < requestNewConfirmForm.getFile().length; i++) {
            // 获取文件大小
            int size = (int) requestNewConfirmExEntity.getFile()[i].getSize();
            // 获取文件总大小
            countSzie = countSzie + size ;           
            // 设置文件大小
            ｒequestNewConfirmEntity.setFileSize(countSzie);           
          }
        }
               
          // 设置备注
          ｒequestNewConfirmEntity.setFileRemark(requestNewConfirmExEntity.getFileRemark());
          // 设置更新者
          ｒequestNewConfirmEntity.setKoshinName(requestNewConfirmExEntity.getKoshinName());
          // 设置审核状态
          ｒequestNewConfirmEntity.setExamineStatus(Constants.CD_EXAMINE_WAIT);
          // 设置更新日时
          ｒequestNewConfirmEntity.setKoshinTime(new Date());
          // 进行DB更新操作更新审批文件存储表
          this.requestNewConfirmService.updateConfirm(ｒequestNewConfirmEntity);
          // 进行DB更新操作更新审核伦理存储表 
          this.requestNewConfirmService.updateStatus(ｒequestNewConfirmEntity);
                   
          // 获取路径下除附件的文件的名字
          String name = null;
          File fileNew = new File(rootPath);
          // 获得该文件夹内的所有文件
          File[] arrayNew = fileNew.listFiles(); 
          
          // 上传文件名集合
          List<String> listName = new ArrayList<>();
          if (requestNewConfirmExEntity.getFile()==null) {           
            for (int i = 0; i < array.length; i++) {
              if (!arrayNew[i].getName().contains("附件")) {
                name = arrayNew[i].getName();
              }
            }
          }else{             
            // 循环所有上传的文件
            for (int i = 0; i < requestNewConfirmExEntity.getFile().length; i++) {       
              // 获取上传文件的文件名称
              String packageName = requestNewConfirmExEntity.getFile()[i].getOriginalFilename();
              // 创建确认文件集合         
              listName.add(packageName);       
            }                                        
          }
          
          // 判断是否点击删除按钮
          if (requestNewConfirmExEntity.isDeleteFlag()) {
            int fileNum = 0;
            for (int i = 0; i < listName.size(); i++) {
              if (!listName.get(i).contains("附件")) {
                fileNum++;
              }
            }
            for (int i = 0; i < requestNewConfirmExEntity.getFileIdList().size(); i++) {
              if (!requestNewConfirmExEntity.getFileIdList().get(i).contains("附件")) {             
                fileNum++;
              }
            }
            if (fileNum == 0) {
              throw new BusinessException("ERR0000041");
            }
            
            int Num=0;
            // 获取发送邮件的文件名称
            for (int i = 0; i < listName.size(); i++) {           
              if (!listName.get(i).contains("附件")) {
                  Num++;
              }
            }
            if (Num==0) {
              for (int j = 0; j < arrayNew.length; j++) {  
                if (!arrayNew[j].getName().contains("附件")) {
                  name = arrayNew[j].getName();             
                }
              }            
            }else {
              for (int i = 0; i < listName.size(); i++) {    
                if (!listName.get(i).contains("附件")) { 
                  name = listName.get(i);
                }
              }
            }
            
            int number = 0;
            for (int i = 0; i < listName.size(); i++) {
              if (!listName.get(i).contains("附件")) {
                number++; 
              }
            } 
            if (number!=0 &&number!=1) {
              throw new BusinessException("ERR0000040");
            }
            
            // 删除文件 获取不是附件的文件名称
            String nameString=""; 
            for (int i = 0; i < requestNewConfirmExEntity.getFileIdList().size(); i++) {           
              if (!requestNewConfirmExEntity.getFileIdList().get(i).contains("附件")) {
                  nameString = requestNewConfirmExEntity.getFileIdList().get(i);
              }
            }
            // 删除文件 不是删除申请文件的情况下
            if (nameString!="") {
              String nameString2 ="";
              for (int i = 0; i < listName.size(); i++) {           
                if (!listName.get(i).contains("附件")) {
                  nameString2=listName.get(i);
                  if(!nameString2.equals(nameString)) {
                    throw new BusinessException("ERR0000042");
                  }
                }
              }
            }
           }else {           
             if (requestNewConfirmExEntity.getFile()!=null) {
               // 取不是附件的文件名
               for (int i = 0; i < listName.size(); i++) {           
                 if (listName.get(i).contains("附件")) {
                   for (int j = 0; j < arrayNew.length; j++) {  
                     if (!arrayNew[j].getName().contains("附件")) {
                       name = arrayNew[j].getName();             
                     } 
                   }
                 }else {
                   name=listName.get(i);
                 }
               }
               
               int number = 0;
               for (int i = 0; i < listName.size(); i++) {
                 if (!listName.get(i).contains("附件")) {
                   number++; 
                  }
                }
                if (number!=0 &&number!=1) {
                  throw new BusinessException("ERR0000040");
                }
                
                String fileCheck = "";
                String checkName="";
                int fileCount = 0;
                for (int i = 0; i < requestNewConfirmExEntity.getFileIdList().size(); i++) {
                  if (!requestNewConfirmExEntity.getFileIdList().get(i).contains("附件")) {             
                      fileCheck=requestNewConfirmExEntity.getFileIdList().get(i);
                  }
                }
                for (int i = 0; i < listName.size(); i++) {
                  if (!listName.get(i).contains("附件")) {
                    checkName =listName.get(i);
                    if (!checkName.equals(fileCheck)) {
                      fileCount++;
                    }
                  }                     
                }         
                if (fileCount!=0) {
                    throw new BusinessException("ERR0000042");
                }              
             }
           }
          
          // 获取截取的位数
          int nameLastIndex = name.lastIndexOf(".");
          // 文件名
          String fileName =  name.substring(0, nameLastIndex);
          //邮箱密码
          String password = this.sendEmailPasswordService.selectPassword();
          String space ="\u3000\u3000";
          // 检索审核人的信息 并发送邮件
          RequestNewConfirmEntity configInfo = requestNewConfirmService.selectEmail(requestNewConfirmExEntity);                   
          if (configInfo==null) {
              throw new BusinessException("ERR0000009","用户信息表","审核人");
          }
          // 邮件内容
          String mailInfo = configInfo.getUserName()+"桑\r\n" + "工作辛苦了，济南审批管理系统。\r\n\r\n"+space+"名为"+"【"+fileName+"】"+"的申请文件请您审批。\r\n\r\n"+space+"内网URL：http://172.17.0.30:8888/index.html\r\n"+space+"外网URL：http://60.216.7.187:9999\r\n\r\n"+"以上，请多关照。\r\n"+"济南审批管理系统\r\n\r\n"+"该邮件为自动发送邮件，不需要回复。";        
          EamilOperatingClass eamilOperatingClass = new EamilOperatingClass();
          boolean sendFlag = eamilOperatingClass.sendTest("您有申请文件需要审批", configInfo.getMail(),mailInfo,password,this.sendEmailPasswordService.getKosinTime());
          
          if (!sendFlag) {
            // 获取InfoMessage信息
            String infoMessage = infoMessageService.getInfo("INF0000007");
            requestNewConfirmExEntity.setInfoMessage(infoMessage);
          }
          
          // 确认文件为0件时
          if (requestNewConfirmExEntity.getFileIdList().size()==0){
            for (int i = 0; i < array.length; i++) {
              array[i].delete();
            }
          }
          
          // 判断文件夹的个数和确认文件的个数是否一致
          if (array.length != requestNewConfirmExEntity.getFileIdList().size()) {
            // 遍历文件夹下所有的文件
            for (int j = 0; j < array.length; j++) {
              int x = 0;
              for (int i = 0; i < requestNewConfirmExEntity.getFileIdList().size(); i++) {                 
                // 判断是否有删除的文件
                if(array[j].getName().equals(requestNewConfirmExEntity.getFileIdList().get(i))) {                 
                  // 如果进行了删除文件的操作则删除本地的文件
                  x++;                       
                }
              }  
              if(x==0) {
                array[j].delete();
              }
            }
          }
          
          // 上传文件
          if(flag) {                        
            for (int i = 0; i < requestNewConfirmForm.getFile().length; i++) {
              // 获取文件流
              InputStream in = requestNewConfirmExEntity.getFile()[i].getInputStream();
              // 获取上传文件的文件名称
              String packageName = requestNewConfirmExEntity.getFile()[i].getOriginalFilename();
              // 将上传的文件保存到本地           
              ExcelOperatingClass.saveTempFile(in, FileTypeEnum.FILE, packageName,request,requestNewConfirmExEntity.getApplicationNumber());
            }
          }
          // 清空上传文件
          requestNewConfirmExEntity.setFile(null);
          logger.info("confirm#result:{}", requestNewConfirmExEntity); 
          
          return requestNewConfirmExEntity;
    }
    /**
     * 确认驳回处理
     * 
     * @param requestNewConfirmForm 确认驳回的requestNewConfirmForm
     * @return
     */   
    @Transactional(rollbackFor = Exception.class)
    @RequestMapping("/api/requestNewConfirm/Clear")
    @ResponseBody
    public RequestNewConfirmEntity clear(HttpServletRequest request , @Valid  @ModelAttribute RequestNewConfirmForm requestNewConfirmForm) throws Exception{
        
        logger.info("clear#param:{}", requestNewConfirmForm);  
        RequestNewConfirmEntity requestNewConfirmExEntity = this.copyFormToEntity(requestNewConfirmForm);
        
        
        // chcek驳回理由是否为空
        if (requestNewConfirmExEntity.getReason().equals("")) {        
         throw new BusinessException("ERR0000001","驳回理由");  
        }
        
        // check 备注位数
        if(requestNewConfirmExEntity.getFileRemark().length()>100) {         
          throw new BusinessException("ERR0000006","备注","1～100");             
        }
        
        // check 驳回理由
        if(requestNewConfirmExEntity.getReason().length()>250) {       
          throw new BusinessException("ERR0000006","驳回理由","1～250");             
        }      
        
        // 根据申请编号 进行查询
        RequestNewConfirmEntity ｒequestNewConfirmEntity = this.requestNewConfirmService.check(requestNewConfirmExEntity); 
        
        // 申请编号存在check
        if (ｒequestNewConfirmEntity == null) {      
          throw new BusinessException("ERR0000009","审批文件存储表","申请编号"); 
        }
        // 确认文件存在check
        if (requestNewConfirmExEntity.getFileIdList().size() == 0){        
            throw new BusinessException("ERR0000001","确认文件");
        }
        // 文件路径
        String rootPath = ｒequestNewConfirmEntity.getFilePath();      
        File file = new File(rootPath);
        // 获得该文件夹内的所有文件
        File[] array = file.listFiles();         
        // 文件不存在时报错
        if (array == null || array.length == 0) {           
            throw new BusinessException("ERR0000010");
        }
        
        // 设置审核状态
        ｒequestNewConfirmEntity.setExamineStatus(Constants.CD_REJECT_CONFIR);
        // 设置 驳回理由
        ｒequestNewConfirmEntity.setReason(requestNewConfirmExEntity.getReason());
        // 设置备注
        ｒequestNewConfirmEntity.setFileRemark(requestNewConfirmExEntity.getFileRemark());
        // 设置 驳回者
        ｒequestNewConfirmEntity.setConfirId(requestNewConfirmExEntity.getTjCode());
        // 设置更新者
        ｒequestNewConfirmEntity.setKoshinName(requestNewConfirmExEntity.getKoshinName()); 
        // 设置更新日时
        ｒequestNewConfirmEntity.setKoshinTime(new Date());
        // 进行DB更新操作更新审批文件存储表
        this.requestNewConfirmService.updateClear(ｒequestNewConfirmEntity); 
        // 进行DB更新操作更新审核伦理存储表 
        this.requestNewConfirmService.updateStatus(ｒequestNewConfirmEntity);
        
        // 检索申请人的邮箱 并发送邮件          
        RequestNewConfirmEntity ApplicationInfo = requestNewConfirmService.selectApplicationEmail(requestNewConfirmExEntity);
        if (ApplicationInfo==null) {
            throw new BusinessException("ERR0000009","用户信息表","申请人");
        }
        // 获取路径下除附件的文件的名字
        String name = null;
        File fileNew = new File(rootPath);
        // 获得该文件夹内的所有文件
        File[] arrayNew = fileNew.listFiles(); 
        
        for (int i = 0; i < arrayNew.length; i++) {
          if (!arrayNew[i].getName().contains("附件")) {
            name = arrayNew[i].getName();
          }
        }
        // 获取截取的位数
        int nameLastIndex = name.lastIndexOf(".");
        // 文件名
        String fileName =  name.substring(0, nameLastIndex);
        //邮箱密码
        String password = this.sendEmailPasswordService.selectPassword();
        String space ="\u3000\u3000";
        // 邮件内容
        String mailInfo = ApplicationInfo.getUserName()+"桑\r\n" + "工作辛苦了，济南审批管理系统。\r\n\r\n"+space+"您申请的名为"+"【"+fileName+"】"+"的申请文件已被驳回,"+"\r\n"+space+"驳回理由为"+"【"+requestNewConfirmExEntity.getReason()+"】"+"，请登录审批管理系统进行修改。\r\n\r\n"+space+"内网URL：http://172.17.0.30:8888/index.html\r\n"+space+"外网URL：http://60.216.7.187:9999\r\n\r\n"+"以上，请多关照。\r\n"+"济南审批管理系统\r\n\r\n"+"该邮件为自动发送邮件，不需要回复。";                
        EamilOperatingClass eamilOperatingClass = new EamilOperatingClass();
        boolean sendFlag =  eamilOperatingClass.sendTest("您提交的申请文件被驳回", ApplicationInfo.getMail(),mailInfo,password,this.sendEmailPasswordService.getKosinTime());
        
        if (!sendFlag) {
          // 获取InfoMessage信息
          String infoMessage = infoMessageService.getInfo("INF0000007");
          requestNewConfirmExEntity.setInfoMessage(infoMessage);
        }
        
        logger.info("clear#result:{}", requestNewConfirmExEntity);  
        return requestNewConfirmExEntity;
    }
    
    /**
     * Form的值放到ExEntity里保存
     * 
     * @param requestNewModifyForm Formの設定値
     * @return Entity
     */
    private RequestNewConfirmEntity copyFormToEntity(RequestNewConfirmForm requestNewConfirmForm) {
        
        RequestNewConfirmEntity requestNewConfirmExEntity = new RequestNewConfirmEntity();
        
        // 申请编号     
        requestNewConfirmExEntity.setApplicationNumber(requestNewConfirmForm.getApproveNum());
        // 申请文件编号
        requestNewConfirmExEntity.setFileId(requestNewConfirmForm.getFileId());
        // 文件名
        requestNewConfirmExEntity.setFileName(requestNewConfirmForm.getFileName());      
        // 申请文件一览
        requestNewConfirmExEntity.setFileIdList(requestNewConfirmForm.getFileIdList());
        // 上传文件
        requestNewConfirmExEntity.setFile(requestNewConfirmForm.getFile());       
        // 设置更新者
        requestNewConfirmExEntity.setKoshinName(requestNewConfirmForm.getKoshinName());
        // 驳回理由
        requestNewConfirmExEntity.setReason(requestNewConfirmForm.getReason());
        // 备注     
        requestNewConfirmExEntity.setFileRemark(requestNewConfirmForm.getFileRemark());
        // 上传文件Flag
        requestNewConfirmExEntity.setUploadFlag(requestNewConfirmForm.isUploadFlag());
        // 天津卡号
        requestNewConfirmExEntity.setTjCode(requestNewConfirmForm.getTjCode());
        // 设置删除Flag
        requestNewConfirmExEntity.setDeleteFlag(requestNewConfirmForm.isDeleteFlag());
        
        return requestNewConfirmExEntity;
    }
    
}