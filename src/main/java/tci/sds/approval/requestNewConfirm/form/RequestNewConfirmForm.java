/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.requestNewConfirm.form;

import java.util.List;
import org.springframework.web.multipart.MultipartFile;

import tci.sds.common.exception.ErrorType;

/**
 * 确认画面用Form
 * 
 * @author dongyihan
 * @since 2020/11/09
 * @version 0.1
 */
public class RequestNewConfirmForm {
    
    /** 申请编号 */
    private String approveNum;
    /** 天津卡号 */
    private String tjCode;
    /** 文件编号 */
    private String fileId;
    /** 文件名称 */
    private String fileName;
    /** 申请者 */
    private String appUserNameCard;
    /** 部门番号 */
    private String appUserDepartment;
    /** 确认者 */
    private String affirmant;
    /** 驳回者 */
    private String rejectUser;  
    /** 申请时间 */
    private String applicationDate;
    /** 审核状态 */
    private String status;
    /** 适用范围 */
    private String scopeApplication;
    /** 业务内容 */
    private String condition;
    /** 驳回理由 */
    private String reason;
    /** 文件编号集合 */
    private List<String> fileIdList;    
    /** 上传文件 */
    private MultipartFile[] file;   
    /** 备注 */
    private String  fileRemark;
    /** 更新者 */
    private String  koshinName;
    /** 上传文件Flag */
    private boolean  uploadFlag;
    /** errorCode */
    private String errorCode = ErrorType.NotError.toString();
    /** Info文字 */
    private String infoMessage;
    /** 删除按钮Flag */
    private boolean deleteFlag;   
    
    public boolean isDeleteFlag() {
    
        return deleteFlag;
    
    }

    public void setDeleteFlag(boolean deleteFlag) {
    
        this.deleteFlag = deleteFlag;
    
    }

    public String getErrorCode() {
    
        return errorCode;
    
    }

    public void setErrorCode(String errorCode) {
    
        this.errorCode = errorCode;
    
    }

    public String getInfoMessage() {
    
        return infoMessage;
    
    }

    public void setInfoMessage(String infoMessage) {
    
        this.infoMessage = infoMessage;
    
    }

    public boolean isUploadFlag() {
    
        return uploadFlag;
    
    }

    public void setUploadFlag(boolean uploadFlag) {
    
        this.uploadFlag = uploadFlag;
    
    }

    public String getTjCode() {
    
        return tjCode;
    
    }
    
    public void setTjCode(String tjCode) {
    
        this.tjCode = tjCode;
    
    }
    
    public String getKoshinName() {
    
        return koshinName;
    
    }
    
    public void setKoshinName(String koshinName) {
    
        this.koshinName = koshinName;
    
    }
    
    public String getFileRemark() {
    
        return fileRemark;
    
    }
    
    public void setFileRemark(String fileRemark) {
    
        this.fileRemark = fileRemark;
    
    }
    
    public MultipartFile[] getFile() {

        return file;

    }
    public void setFile(MultipartFile[] file) {

        this.file = file;

    }

    public String getFileId() {
    
        return fileId;
    
    }

    public void setFileId(String fileId) {
    
        this.fileId = fileId;
    
    }    
    
    public List<String> getFileIdList() {
    
        return fileIdList;
    
    }

    public void setFileIdList(List<String> fileIdList) {
    
        this.fileIdList = fileIdList;
    
    }

    public String getScopeApplication() {
    
        return scopeApplication;
    
    }

    public void setScopeApplication(String scopeApplication) {
    
        this.scopeApplication = scopeApplication;
    
    }

    public String getCondition() {
    
        return condition;
    
    }

    public void setCondition(String condition) {
    
        this.condition = condition;
    
    }

    public String getReason() {
    
        return reason;
    
    }

    public void setReason(String reason) {
    
        this.reason = reason;
    
    }

    public String getApproveNum() {
    
        return approveNum;
    
    }

    public void setApproveNum(String approveNum) {
    
        this.approveNum = approveNum;
    
    }

    public String getFileName() {
    
        return fileName;
    
    }

    public void setFileName(String fileName) {
    
        this.fileName = fileName;
    
    }

    public String getAppUserNameCard() {
    
        return appUserNameCard;
    
    }

    public void setAppUserNameCard(String appUserNameCard) {
    
        this.appUserNameCard = appUserNameCard;
    
    }

    public String getAppUserDepartment() {
    
        return appUserDepartment;
    
    }

    public void setAppUserDepartment(String appUserDepartment) {
    
        this.appUserDepartment = appUserDepartment;
    
    }

    public String getAffirmant() {
    
        return affirmant;
    
    }

    public void setAffirmant(String affirmant) {
    
        this.affirmant = affirmant;
    
    }

    public String getRejectUser() {
    
        return rejectUser;
    
    }

    public void setRejectUser(String rejectUser) {
    
        this.rejectUser = rejectUser;
    
    }

    public String getApplicationDate() {
    
        return applicationDate;
    
    }

    public void setApplicationDate(String applicationDate) {
    
        this.applicationDate = applicationDate;
    
    }

    public String getStatus() {
    
        return status;
    
    }

    public void setStatus(String status) {
    
        this.status = status;
    
    }

    @Override
    public String toString() {

        return super.toString() + String.format(
                ",approveNum=%s, fileName=%s, appUserNameCard=%s, appUserDepartment=%s, affirmant=%s,rejectUser=%s,applicationDate=%s,status=%s,reason=%s,scopeApplication=%s,condition=%s,fileIdList=%s,fileId=%s,file=%s,fileRemark=%s,koshinName=%s,tjCode=%s,uploadFlag=%s,errorCode=%s,infoMessage=%s,deleteFlag=%s",
                approveNum, fileName, appUserNameCard, appUserDepartment, affirmant,
                rejectUser, applicationDate, status,reason,scopeApplication,condition,fileIdList,fileId,file,fileRemark,koshinName,tjCode,uploadFlag,errorCode,infoMessage,deleteFlag);

    }

}
