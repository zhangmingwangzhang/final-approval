/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.requestNewModify.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tci.sds.approval.requestNewModify.Entity.RequestNewModifyEntity;
import tci.sds.approval.requestNewModify.mapper.RequestNewModifyMapper;

/**
 * 申請修正画面用Service
 * 
 * @author dongyihan
 * @since 2020/11/09
 * @version 0.1
 */
@Service
public class RequestNewModifyService {
    private final Logger logger = LoggerFactory.getLogger(RequestNewModifyService.class);

    @Autowired
    private RequestNewModifyMapper requestNewModifyMapper;
    
    /**
     * 申请修正的修正人基本信息取得
     * @return RequestNewModifyEntity
     */
    public RequestNewModifyEntity search(String approveNum, String tjCode) {

        logger.info("search#param:{}", approveNum,tjCode);
        
        // 检索修正人基本信息
        RequestNewModifyEntity requestNewModifyEntity = requestNewModifyMapper.search(approveNum,tjCode);
        
        logger.info("search#result:{}", requestNewModifyEntity);
        
        return requestNewModifyEntity;        

    }

    /**
     * 申请修正的修正文件存在Check
     * @return RequestNewModifyEntity
     */
    public RequestNewModifyEntity check(RequestNewModifyEntity requestNewModifyExEntity) {

        logger.info("check#param:{}", requestNewModifyExEntity);
        
        // 修正文件存在Check
        RequestNewModifyEntity requestNewModifyEntity = requestNewModifyMapper.check(requestNewModifyExEntity);   
        
        logger.info("check#result:{}", requestNewModifyEntity);
        
        return requestNewModifyEntity;

    }

    /**
     * 申请文件Check
     * @return fileEntitiesList
     */
    public List<RequestNewModifyEntity> searchFileExist(RequestNewModifyEntity requestNewModifyExEntity) {
        
        logger.info("searchFileExist#param:{}", requestNewModifyExEntity);
        
        // 申请文件存在Check
        List<RequestNewModifyEntity> fileEntitiesList = this.requestNewModifyMapper.searchFileExist(requestNewModifyExEntity);
        
        logger.info("searchFileExist#result:{}", fileEntitiesList);
        
        return fileEntitiesList;

    }

    /**
     * 删除文件
     * @return res
     */
    public int delete(RequestNewModifyEntity requestNewModifyExEntity) {
        
        logger.info("delete#param:{}", requestNewModifyExEntity);
        
        // 删除审核伦理存储表中的数据
        int res = this.requestNewModifyMapper.delete(requestNewModifyExEntity);
        
        logger.info("delete#result:{}", res);
        
        return res;

    }
    
    /**
     *  确认处理 更新审批文件存储表
     * @return res
     */
    public int update(RequestNewModifyEntity requestNewModifyEntity) {
        
        logger.info("update#param:{}", requestNewModifyEntity);
        
        // 确认处理 更新审批文件存储表
        int res = this.requestNewModifyMapper.update(requestNewModifyEntity);
        
        logger.info("update#result:{}", res);
        
        return res;

    }
    /**
     * 确认处理 更新审核伦理存储表状态
     * @return res
     */    
    public int updateStatus(RequestNewModifyEntity requestNewModifyEntity) {
        
        logger.info("updateStatus#param:{}", requestNewModifyEntity);
        
        // 确认处理 更新审核伦理存储表状态
        int res = this.requestNewModifyMapper.updateStatus(requestNewModifyEntity);
        
        logger.info("updateStatus#result:{}", res);
        
        return res;
        
    }
    
    /**
     * 检索确认人的邮箱
     * @return 确认人的Email
     */
    public RequestNewModifyEntity selectEmail(RequestNewModifyEntity requestNewModifyExEntity) {
      
       logger.info("selectEmail#param:{}", requestNewModifyExEntity);
       
       // 检索确认人的信息
       RequestNewModifyEntity configInfo = requestNewModifyMapper.selectEmail(requestNewModifyExEntity);
        
       logger.info("selectEmail#result:{}", configInfo);
       
       return configInfo;

    }
 
}
