/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.requestNewModify.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import tci.sds.approval.requestNewModify.Entity.RequestNewModifyEntity;
import tci.sds.approval.requestNewModify.form.RequestNewModifyForm;
import tci.sds.approval.requestNewModify.service.RequestNewModifyService;
import tci.sds.common.entity.FileTypeEnum;
import tci.sds.common.exception.BusinessException;
import tci.sds.common.service.InfoMessageService;
import tci.sds.common.service.SendEmailPasswordService;
import tci.sds.common.unit.EamilOperatingClass;
import tci.sds.common.unit.ExcelOperatingClass;
import tci.sds.common.util.Constants;

/**
 * 申请修正用Controller
 * 
 * @author dongyihan
 * @since 2020/11/09
 * @version 0.1
 */
@Controller
public class RequestNewModifyController {
    private final Logger logger = LoggerFactory
            .getLogger(RequestNewModifyController.class);

    @Autowired
    private RequestNewModifyService requestNewModifyService;
    @Autowired
    private SendEmailPasswordService sendEmailPasswordService;
    @Autowired
    private InfoMessageService infoMessageService;
    
    /**
     * 申请修正画面初期表示
     * 
     * @param approveNum,tjCode 申请编号,申请者卡号
     * @return 申请修正画面初期表示Form RequestNewModifyForm
     */
    @RequestMapping("/api/requestNewModify/init")
    @ResponseBody                                                                 
    public RequestNewModifyForm init(String approveNum ,String tjCode) throws Exception{
       
        logger.info("init#param:{}", approveNum,tjCode);
        
        // 申请修正画面初期化
        RequestNewModifyEntity requestNewModifyEntity = this.requestNewModifyService.search(approveNum,tjCode);  
        // 创建requestNewModifyOutputForm
        RequestNewModifyForm requestNewModifyOutputForm = new RequestNewModifyForm();   
        
        // 创建确认文件集合
        List<String> fileIdList = new ArrayList<>();  
        // 获取文件路径
        String filePath = requestNewModifyEntity.getFilePath();       
        File file = new File(filePath);
        // 获得该文件夹内的所有文件
        File[] array = file.listFiles();
        
        // 遍历文件夹下所有的文件，取得文件名赋给文件集合        
        for (int i = 0; i < array.length; i++) {
          fileIdList.add(array[i].getName());
        }
        
        // 确认文件一览
        requestNewModifyOutputForm.setFileIdList(fileIdList);     
        // 驳回理由       
        requestNewModifyOutputForm.setReason(requestNewModifyEntity.getConfirReasons());        
        // 申请编号     
        requestNewModifyOutputForm.setApproveNum(approveNum);
        // 申请者/卡号
        requestNewModifyOutputForm.setAppUserNameCard(requestNewModifyEntity.getUserName() + "/" + requestNewModifyEntity.getFileApplicantId());
        // 申请文件编号
        requestNewModifyOutputForm.setFileId(requestNewModifyEntity.getFileId());
        // 文件名
        requestNewModifyOutputForm.setFileName(requestNewModifyEntity.getFileName());       
        // 申请部门
        requestNewModifyOutputForm.setAppUserDepartment(requestNewModifyEntity.getDeptName());
        // 状态
        requestNewModifyOutputForm.setStatus(requestNewModifyEntity.getExamineStatus());
        // 范围
        requestNewModifyOutputForm.setScopeApplication(requestNewModifyEntity.getScopeApplication());
        // 业务内容
        requestNewModifyOutputForm.setCondition(requestNewModifyEntity.getCondition());
        
        // 给session初始化       
        HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = request.getSession();  
        session.setAttribute("userName", "");
        
        logger.info("init#result:{}", requestNewModifyOutputForm);
                           
        return requestNewModifyOutputForm;
    }
        
    /**
     * 编辑下载的Check处理
     * 
     * @param requestNewModifyForm 申请修正画面Check的requestNewModifyForm
     * @return RequestNewModifyEntity
     */   
    @RequestMapping("/api/requestNewModify/downLoadCheck")
    @ResponseBody
    public RequestNewModifyEntity downLoadCheck(@ModelAttribute RequestNewModifyForm requestNewModifyForm) throws Exception{
        
        logger.info("downLoadCheck#param:{}", requestNewModifyForm);
        RequestNewModifyEntity requestNewModifyExEntity = this.copyFormToEntity(requestNewModifyForm);
        // 文件基本信息检索
        RequestNewModifyEntity requestNewModifyEntity = this.requestNewModifyService.check(requestNewModifyExEntity); 
        // 文件路径
        String rootPath = requestNewModifyEntity.getFilePath(); 
        // 将文件路径和文件名拼接得到文件全路径
        String FullPath = rootPath + requestNewModifyExEntity.getFileName();
        // 根据文件的路径 创建文件
        File file = new File(FullPath);
        
        // 修正文件存在check
        if (!file.exists()) {       
          throw new BusinessException("ERR0000010");
        }
        
        logger.info("downLoadCheck#result:{}", requestNewModifyEntity);
        
        return requestNewModifyEntity;
        
    }
    
    /**
     * 编辑下载处理
     * 
     * @param filePath ,FileName 文件路径，文件名称
     * @return 
     */   
    @RequestMapping("/api/requestNewModify/download")
    @ResponseBody  
    public void downloadFile(HttpServletRequest request,
                          HttpServletResponse response,String filePath,String fileName) throws IOException {   
        
        logger.info("downloadFile#param:{}", filePath,fileName);
        // 文件路径
        String rootPath = filePath;
        // 文件名称
        String fileFullName = fileName ;  
        // 将文件路径和文件名拼接得到文件全路径
        String FullPath = rootPath + fileFullName;
        File file = new File(FullPath); 
        // 下载的文件名         
        String downFileName = URLEncoder.encode(fileName, "UTF-8");
        downFileName = downFileName.replaceAll("\\+",  "%20");
        
        // 配置文件下载
        response.setHeader("content-type", "application/octet-stream");
        response.setContentType("application/octet-stream");
        // 下载文件能正常显示中文
        response.setHeader("Content-Disposition", "attachment;filename=" + downFileName);
        // 实现文件下载
        byte[] buffer = new byte[1024];
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        
        try {
            // 创建FileInputStream 对象
            fis = new FileInputStream(file);
            // 创建BufferedInputStream 对象
            bis = new BufferedInputStream(fis);
            OutputStream os = response.getOutputStream();
            // 从文件中按字节读取内容，到文件尾部时read方法将返回-1
            int num = bis.read(buffer);   
            // 判断 num 是否为-1
            while (num != -1) {
                // 下载读取到的文件内容
                os.write(buffer, 0, num);
                num = bis.read(buffer);
            }              
            logger.info("downloadFile#result:{}","Download successfully!");              
        } catch (Exception e) {
            logger.info("downloadFile#result:{}","Download  failed!");
        } finally {
            // 关闭BufferedInputStream
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }          
            // 关闭FileInputStream
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
    }
    
    /**
     * 确认处理
     * 
     * @param requestNewModifyForm requestNewModifyForm的设定值
     * @return
     */  
    @Transactional(rollbackFor = Exception.class)
    @RequestMapping("/api/requestNewModify/confirm")
    @ResponseBody
    public RequestNewModifyEntity confirm(HttpServletRequest request , @ModelAttribute RequestNewModifyForm requestNewModifyForm) throws Exception{
        
        logger.info("confirm#param:{}", requestNewModifyForm); 
        
        RequestNewModifyEntity requestNewModifyExEntity = this.copyFormToEntity(requestNewModifyForm);
        // 根据申请编号 进行查询
        RequestNewModifyEntity requestNewModifyEntity = this.requestNewModifyService.check(requestNewModifyExEntity); 
        
        // 申请编号存在Check
        if (requestNewModifyEntity == null) {
          //删除审核伦理存储表
          this.requestNewModifyService.delete(requestNewModifyExEntity);        
          throw new BusinessException("ERR0000009","审批文件存储表","申请编号");   
        }
        
        // 适用范围 
        requestNewModifyExEntity.setScopeApplication(requestNewModifyEntity.getScopeApplication());
        // 业务内容
        requestNewModifyExEntity.setCondition(requestNewModifyEntity.getCondition());
        // 文件申请check
        List<RequestNewModifyEntity> fileEntitiesList = this.requestNewModifyService.searchFileExist(requestNewModifyExEntity);
        
        // check fileEntitiesList的个数  
        if (fileEntitiesList.size() == 0) {          
          throw new BusinessException("ERR0000021", requestNewModifyExEntity.getFileId() +" "+ requestNewModifyEntity.getFileName()); 
        }
        
        // 文件路径
        String rootPath = requestNewModifyEntity.getFilePath();      
        File file = new File(rootPath);
        // 获得该文件夹内的所有文件
        File[] array = file.listFiles();
        // check是否有文件
        if (array == null || array.length == 0) {           
            throw new BusinessException("ERR0000010");
        }
        
        // 判断不是附件的文件数
        int count = 0;
        for (int i = 0; i < array.length; i++) {
          if (!array[i].getName().contains("附件")) {
            count++;
          }
        }
        if (count!=1) {
            throw new BusinessException("ERR0000040");
        }
        
        // 上传的文件存在check
        if (requestNewModifyExEntity.getFile() == null){        
            throw new BusinessException("ERR0000035");
        }                    
             
        // 循环所有上传的文件
        for (int i = 0; i < requestNewModifyExEntity.getFile().length; i++) {
          // 上传文件的总大小
          int countSzie = 0; 
          // 获取文件大小
          int size = (int) requestNewModifyExEntity.getFile()[i].getSize();  
          // 获取文件总大小
          countSzie = countSzie + size ;         
          // 设置文件大小
          requestNewModifyEntity.setFileSize(countSzie);   
        }
        
        // 申请编号
        requestNewModifyEntity.setApplicationNumber(requestNewModifyExEntity.getApplicationNumber());
        // 文件编号
        requestNewModifyEntity.setFileId(requestNewModifyExEntity.getFileId());
        // 设置审核状态
        requestNewModifyEntity.setExamineStatus(Constants.CD_REJECT_WAIT);
        // 设置更新者
        requestNewModifyEntity.setKoshinName(requestNewModifyExEntity.getKoshinName());
        // 设置更新日时
        requestNewModifyEntity.setKoshinTime(new Date());
        // 进行DB更新操作更新审批文件存储表
        this.requestNewModifyService.update(requestNewModifyEntity);     
        // 进行DB更新操作更新审核伦理存储表 
        this.requestNewModifyService.updateStatus(requestNewModifyEntity);
             
        // 检索确认人的信息 并发送邮件
        RequestNewModifyEntity configInfo = requestNewModifyService.selectEmail(requestNewModifyExEntity); 
        if (configInfo==null) {
            throw new BusinessException("ERR0000009","用户信息表","确认人");
        }
        //邮箱密码
        String password = this.sendEmailPasswordService.selectPassword();
        // 获取路径下除附件的文件的名字
        String name = null;
        // 上传文件名集合
        List<String> listName = new ArrayList<>();  
        // 循环所有上传的文件
        for (int i = 0; i < requestNewModifyExEntity.getFile().length; i++) {       
          // 获取上传文件的文件名称
          String packageName = requestNewModifyExEntity.getFile()[i].getOriginalFilename();
          // 创建确认文件集合         
          listName.add(packageName);
        }               
        
        File fileNew = new File(rootPath);
        // 获得该文件夹内的所有文件
        File[] arrayNew = fileNew.listFiles();         
       
        if (requestNewModifyExEntity.isDeleteFlag()) {
          int fileNum = 0;
          for (int i = 0; i < listName.size(); i++) {
            if (!listName.get(i).contains("附件")) {
              fileNum++;
            }
          }
          for (int i = 0; i < requestNewModifyExEntity.getFileIdList().size(); i++) {
            if (!requestNewModifyExEntity.getFileIdList().get(i).contains("附件")) {             
              fileNum++;
            }
          }
          if (fileNum == 0) {
            throw new BusinessException("ERR0000041");
          }
          
          int Num=0;
          // 获取发送邮件的文件名称
          for (int i = 0; i < listName.size(); i++) {           
            if (!listName.get(i).contains("附件")) {
                Num++;
            }
          }
          if (Num==0) {
            for (int j = 0; j < arrayNew.length; j++) {  
              if (!arrayNew[j].getName().contains("附件")) {
                name = arrayNew[j].getName();             
              }
            }            
          }else {
            for (int i = 0; i < listName.size(); i++) {    
              if (!listName.get(i).contains("附件")) { 
                name = listName.get(i);
              }
            }
          }
          
          // 判断上传文件都不叫附件的文件，如果有两个文件报错        
          int number = 0;        
          for (int i = 0; i < listName.size(); i++) {
            if (!listName.get(i).contains("附件")) {
              number++;
             }           
          }
          if (number!=0 &&number!=1) {
            throw new BusinessException("ERR0000040");
          }
          
          // 删除文件 获取不是附件的文件名称
          String nameString=""; 
          for (int i = 0; i < requestNewModifyExEntity.getFileIdList().size(); i++) {           
            if (!requestNewModifyExEntity.getFileIdList().get(i).contains("附件")) {
                nameString = requestNewModifyExEntity.getFileIdList().get(i);
            }
          }
          // 删除文件 不是删除申请文件的情况下
          if (nameString!="") {
            String nameString2 ="";
            for (int i = 0; i < listName.size(); i++) {           
              if (!listName.get(i).contains("附件")) {
                nameString2=listName.get(i);
                if(!nameString2.equals(nameString)) {
                  throw new BusinessException("ERR0000042");
                }
              }
            }
          }
         
        }else {
          for (int i = 0; i < listName.size(); i++) {           
            if (listName.get(i).contains("附件")) {
              for (int j = 0; j < arrayNew.length; j++) {  
                if (!arrayNew[j].getName().contains("附件")) {
                  name = arrayNew[j].getName();             
                } 
              }
            }else {
              name=listName.get(i);
            }
          } 
          
          int number = 0;
          for (int i = 0; i < listName.size(); i++) {
            if (!listName.get(i).contains("附件")) {
              number++; 
             }
           }
           if (number!=0 &&number!=1) {
             throw new BusinessException("ERR0000040");
           }
           
           String fileCheck = "";
           String checkName="";
           int fileCount = 0;
           for (int i = 0; i < requestNewModifyExEntity.getFileIdList().size(); i++) {
             if (!requestNewModifyExEntity.getFileIdList().get(i).contains("附件")) {             
                 fileCheck=requestNewModifyExEntity.getFileIdList().get(i);
             }
           }
           for (int i = 0; i < listName.size(); i++) {
             if (!listName.get(i).contains("附件")) {
               checkName =listName.get(i);
               if (!checkName.equals(fileCheck)) {
                 fileCount++;
               }
             }                     
           }         
           if (fileCount!=0) {
               throw new BusinessException("ERR0000042");
           }   
         }
        
        // 获取截取的位数
        int nameLastIndex = name.lastIndexOf(".");
        // 文件名
        String fileName =  name.substring(0, nameLastIndex);
        String space ="\u3000\u3000";
        // 邮件内容
        String mailInfo = configInfo.getUserName()+"桑\r\n" + "工作辛苦了，济南审批管理系统。\r\n\r\n"+space+"名为"+"【"+fileName+"】"+"的申请文件请您确认。\r\n\r\n"+space+"内网URL：http://172.17.0.30:8888/index.html\n"+space+"外网URL：http://60.216.7.187:9999"+"\r\n\r\n以上，请多关照。\r\n"+"济南审批管理系统\r\n\r\n"+"该邮件为自动发送邮件，不需要回复。";        
        EamilOperatingClass eamilOperatingClass = new EamilOperatingClass();
        boolean flag = eamilOperatingClass.sendTest("您有申请文件需要确认", configInfo.getMail(),mailInfo,password,this.sendEmailPasswordService.getKosinTime());
        
        if (!flag) {
          // 获取InfoMessage信息
          String infoMessage = infoMessageService.getInfo("INF0000007");
          requestNewModifyEntity.setInfoMessage(infoMessage);
        } 
        
        // 判断是否点击删除按钮
        if (requestNewModifyExEntity.isDeleteFlag()) {
          
          // 删除文件后，确认文件为0件的时候
          if (requestNewModifyExEntity.getFileIdList().size()==0) {
            for (int i = 0; i < array.length; i++) {
               array[i].delete();
            }
          }
          
          // 删除文件后还剩一个或者多个确认文件的时候，判断文件夹的个数和确认文件的个数是否一致
          if (array.length != requestNewModifyExEntity.getFileIdList().size()) {               
            // 遍历文件夹下所有的文件
            for (int j = 0; j < array.length; j++) {
              int x = 0;
              for (int i = 0; i < requestNewModifyExEntity.getFileIdList().size(); i++) {                 
                // 判断是否有删除的文件
                if(array[j].getName().equals(requestNewModifyExEntity.getFileIdList().get(i))) {                 
                  // 如果进行了删除文件的操作则删除本地的文件
                  x++;                       
                }
              }  
              if(x==0) {
                array[j].delete();
              }
            }
          }
        }
        
        // 循环所有上传的文件
        for (int i = 0; i < requestNewModifyExEntity.getFile().length; i++) {
          // 获取文件流
          InputStream in = requestNewModifyExEntity.getFile()[i].getInputStream();
          // 获取上传文件的文件名称
          String packageName = requestNewModifyExEntity.getFile()[i].getOriginalFilename();
          //将上传的文件保存到本地   
          ExcelOperatingClass.saveTempFile(in, FileTypeEnum.FILE, packageName,request,requestNewModifyExEntity.getApplicationNumber());         
        }
        // 清空上传文件
        requestNewModifyEntity.setFile(null);
        
        return requestNewModifyEntity;
    }
    
    /**
     * Form的值放到ExEntity里保存
     * 
     * @param requestNewModifyForm Formの設定値
     * @return Entity
     */
    private RequestNewModifyEntity copyFormToEntity(RequestNewModifyForm requestNewModifyForm) {
        
        RequestNewModifyEntity requestNewModifyEntity = new RequestNewModifyEntity();
        
        // 申请编号     
        requestNewModifyEntity.setApplicationNumber(requestNewModifyForm.getApproveNum());
        // 申请文件编号
        requestNewModifyEntity.setFileId(requestNewModifyForm.getFileId());
        // 文件名
        requestNewModifyEntity.setFileName(requestNewModifyForm.getFileName());
        // 业务内容
        requestNewModifyEntity.setCondition(requestNewModifyForm.getCondition());
        // 申请文件一览
        requestNewModifyEntity.setFileIdList(requestNewModifyForm.getFileIdList());
        // 上传文件
        requestNewModifyEntity.setFile(requestNewModifyForm.getFile());
        // 适用范围 
        requestNewModifyEntity.setScopeApplication(requestNewModifyForm.getScopeApplication());
        // 设置更新者
        requestNewModifyEntity.setKoshinName(requestNewModifyForm.getKoshinName());
        // 设置删除Flag
        requestNewModifyEntity.setDeleteFlag(requestNewModifyForm.isDeleteFlag());
        
        return requestNewModifyEntity;
    }

}