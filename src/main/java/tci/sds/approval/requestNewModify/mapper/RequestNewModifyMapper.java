/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.requestNewModify.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import tci.sds.approval.requestNewModify.Entity.RequestNewModifyEntity;

/**
 * 申请修正画面用Mapper
 * 
 * @author dongyihan
 * @since 2020/11/09
 * @version 0.1
 */
@Mapper
public interface RequestNewModifyMapper{

    /**
     * 申请修正的修正人基本信息取得
     * @return RequestNewModifyEntity
     */
    RequestNewModifyEntity search(String approveNum, String tjCode);
    
    /**
     * 申请修正的修正文件存在Check
     * @return RequestNewModifyEntity
     */
    RequestNewModifyEntity check(RequestNewModifyEntity requestNewModifyExEntity);

    /**
     * 文件申请check
     * @return RequestNewModifyEntity
     */
    List<RequestNewModifyEntity> searchFileExist(RequestNewModifyEntity requestNewModifyExEntity);

    /**
     * 文件删除
     * @return res 
     */
    int delete(RequestNewModifyEntity requestNewModifyExEntity);

    /**
     * 文件更新
     * @return res 
     */
    int update(RequestNewModifyEntity requestNewModifyEntity);

    /**
     * 更新审核伦理存储表状态
     * @return res 
     */
    int updateStatus(RequestNewModifyEntity requestNewModifyEntity);

    /**
     * 检索确认人的信息
     * @return 确认人的信息
     */
    RequestNewModifyEntity selectEmail(RequestNewModifyEntity requestNewModifyExEntity);
    
}