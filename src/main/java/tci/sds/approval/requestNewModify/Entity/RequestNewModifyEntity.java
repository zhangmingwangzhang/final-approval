/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.approval.requestNewModify.Entity;

import java.util.Date;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import tci.sds.common.exception.ErrorType;

/**
 * 申请修正画面用Entity
 * 
 * @author dongyihan
 * @since 2020/11/09
 * @version 0.1
 */
public class RequestNewModifyEntity  {

    /** 申请编号 */
    private String applicationNumber;
    /** 天津卡号 */
    private String tjCode;
    /** 姓名 */
    private String userName;
    /** 申请者 */
    private String fileApplicantId;
    /** 部门番号 */
    private String deptId;
    /** 部门名称 */
    private String deptName;
    /** 文件编号 */
    private String fileId;    
    /** 文件名称 */
    private String fileName;
    /** 文件路径 */
    private String filePath;
    /** 文件大小 */
    private Integer fileSize;
    /** 适用范围 */
    private String scopeApplication;
    /** 业务内容 */
    private String condition;
    /** 审核状态 */
    private String examineStatus;
    /** 审核人 */
    private String reviewer;
    /** 驳回理由 */
    private String confirReasons;
    /** 更新者 */
    private String  koshinName;
    /** 更新日时 */
    private Date  koshinTime; 
    /** 文件编号集合 */
    private List<String> fileIdList;
    /** 上传文件 */
    private MultipartFile[] file;
    /** errorCode */
    private String errorCode = ErrorType.NotError.toString();
    /** Info文字 */
    private String infoMessage;
    /** 邮箱 */
    private String mail;
    /** 删除按钮Flag */
    private boolean deleteFlag;   

    public boolean isDeleteFlag() {
    
        return deleteFlag;
    
    }

    public void setDeleteFlag(boolean deleteFlag) {
    
        this.deleteFlag = deleteFlag;
    
    }

    public String getMail() {
    
        return mail;
    
    }

    public void setMail(String mail) {
    
        this.mail = mail;
    
    }

    public String getErrorCode() {
    
        return errorCode;
    
    }

    public void setErrorCode(String errorCode) {
    
        this.errorCode = errorCode;
    
    }

    public String getInfoMessage() {
    
        return infoMessage;
    
    }

    public void setInfoMessage(String infoMessage) {
    
        this.infoMessage = infoMessage;
    
    }

    public List<String> getFileIdList() {
    
        return fileIdList;
    
    }

    public void setFileIdList(List<String> fileIdList) {
    
        this.fileIdList = fileIdList;
    
    }

    public MultipartFile[] getFile() {
    
        return file;
    
    }

    public void setFile(MultipartFile[] file) {
    
        this.file = file;
    
    }

    public Date getKoshinTime() {
    
        return koshinTime;
    
    }

    public void setKoshinTime(Date koshinTime) {
    
        this.koshinTime = koshinTime;
    
    }

    public String getKoshinName() {
    
        return koshinName;
    
    }

    public void setKoshinName(String koshinName) {
    
        this.koshinName = koshinName;
    
    }

    public String getDeptId() {
    
        return deptId;
    
    }

    public void setDeptId(String deptId) {
    
        this.deptId = deptId;
    
    }

    public String getUserName() {
    
        return userName;
    
    }

    public void setUserName(String userName) {
    
        this.userName = userName;
    
    }

   
   

    public Integer getFileSize() {
    
        return fileSize;
    
    }

    public void setFileSize(Integer fileSize) {
    
        this.fileSize = fileSize;
    
    }

    public String getReviewer() {
    
        return reviewer;
    
    }

    public void setReviewer(String reviewer) {
    
        this.reviewer = reviewer;
    
    }

    public String getApplicationNumber() {

        return applicationNumber;

    }

    public void setApplicationNumber(String applicationNumber) {

        this.applicationNumber = applicationNumber;

    }

    public String getTjCode() {

        return tjCode;

    }

    public void setTjCode(String tjCode) {

        this.tjCode = tjCode;

    }

    public String getFileApplicantId() {

        return fileApplicantId;

    }

    public void setFileApplicantId(String fileApplicantId) {

        this.fileApplicantId = fileApplicantId;

    }
   
    
    public String getConfirReasons() {
    
        return confirReasons;
    
    }

    public void setConfirReasons(String confirReasons) {
    
        this.confirReasons = confirReasons;
    
    }

    public String getDeptName() {
    
        return deptName;
    
    }

    public void setDeptName(String deptName) {
    
        this.deptName = deptName;
    
    }

    public String getFileId() {

        return fileId;

    }

    public void setFileId(String fileId) {

        this.fileId = fileId;

    }

    public String getFileName() {

        return fileName;

    }

    public void setFileName(String fileName) {

        this.fileName = fileName;

    }

    public String getFilePath() {
    
        return filePath;
    
    }

    public void setFilePath(String filePath) {
    
        this.filePath = filePath;
    
    }

    public String getScopeApplication() {

        return scopeApplication;

    }

    public void setScopeApplication(String scopeApplication) {

        this.scopeApplication = scopeApplication;

    }

    public String getCondition() {

        return condition;

    }

    public void setCondition(String condition) {

        this.condition = condition;

    }

    public String getExamineStatus() {

        return examineStatus;

    }

    public void setExamineStatus(String examineStatus) {

        this.examineStatus = examineStatus;

    }

    @Override
    public String toString() {

        return super.toString() + String.format(
                ",applicationNumber=%s, tjCode=%s, fileApplicantId=%s, deptName=%s, fileId=%s,fileName=%s,filePath=%s，scopeApplication=%s,condition=%s,confirReasons=%s,examineStatus=%s,reviewer=%s,fileSize=%s,userName=%s,deptId=%s,koshinName=%s,koshinTime=%s,fileIdList=%s,file=%s,errorCode=%s,infoMessage=%s,mail=%s,deleteFlag=%s",
                applicationNumber, tjCode, fileApplicantId, deptName, fileId,
                fileName,filePath, scopeApplication, condition,examineStatus,reviewer,fileSize,userName,confirReasons,deptId,koshinName,koshinTime,fileIdList,file,errorCode,infoMessage,mail,deleteFlag);

    }

}
