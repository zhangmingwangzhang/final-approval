/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.admin.userList.Entity;

/**
 * 下拉框ExEntity
 * 
 * @author sunliqiang
 * @since 2020/11/13
 * @version 0.1
 */
public class ListExEntity  {
    
    /** CODE值 */
    private String value;
    
    /** CODE名称 */
    private String label;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
