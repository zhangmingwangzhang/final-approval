/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.admin.userList.Entity;

import java.sql.Timestamp;

/**
 * 用户一览ExEntity
 * 
 * @author sunliqiang
 * @since 2020/11/13
 * @version 0.1
 */
public class UserListExEntity{
    
    /** 姓名 */
    private String userName;
    
    /** 卡号 */
    private String tjCode;
    
    /** 职位Id */
    private String positionId;
    
    /** 职位名称 */
    private String positionName;
    
    /** 部门Id */
    private String deptId;
    
    /** 部门 名称*/
    private String deptName;
    
    /** 职务权限Code */
    private String postCode;
    
    /** 职务权限名称 */
    private String postName;
    
    /** 审核权限Code */
    private String roleCode;
    
    /** 审核权限名称 */
    private String roleName;
    
    /** 邮箱 */
    private String email;
    
    /** 管理权限有无 */
    private String manageRoleFlag;
    
    /** 管理权限有无名称 */
    private String manageRoleName;
    
    /** 当前页数 */
    private int currentPage;

    /** 一页的件数 */
    private int pageSize;

    /** 排序字段 */
    private String sortColumn;
    
    /** 排序顺序 */
    private String sortType;

    /** 申请人卡号 */
    private String applicantCode;

    /** 申请人姓名 */
    private String applicantName;
    
    /** 登录者姓名 */
    private String loginUserName;
    
    /** 更新日时 */
    private Timestamp updateTime;
    
    /** 确认人人卡号 */
    private String configCode;
    
    /** 确认人姓名 */
    private String configName;
    
    /** 审核人卡号 */
    private String reviewerCode;
    
    /** 审核人姓名 */
    private String reviewerName;
    
    /** 被修改人姓名 */
    private String updateUserName;
    
    /** 初次登陆与否 */
    private String firstLogin;
    
    /** CODE区分值 */
    private String cdKubun;
    
    /** CODE区分名称 */
    private String cdKubunName;
    
    /** CODE值 */
    private String cdCode;
    
    /** CODE名称 */
    private String kjName;
    
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTjCode() {
        return tjCode;
    }

    public void setTjCode(String tjCode) {
        this.tjCode = tjCode;
    }

    public String getPositionId() {
        return positionId;
    }

    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getManageRoleFlag() {
        return manageRoleFlag;
    }

    public void setManageRoleFlag(String manageRoleFlag) {
        this.manageRoleFlag = manageRoleFlag;
    }

    public String getManageRoleName() {
        return manageRoleName;
    }

    public void setManageRoleName(String manageRoleName) {
        this.manageRoleName = manageRoleName;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSortColumn() {
        return sortColumn;
    }

    public void setSortColumn(String sortColumn) {
        this.sortColumn = sortColumn;
    }

    public String getSortType() {
        return sortType;
    }

    public void setSortType(String sortType) {
        this.sortType = sortType;
    }

    public String getApplicantCode() {
        return applicantCode;
    }

    public void setApplicantCode(String applicantCode) {
        this.applicantCode = applicantCode;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    public String getLoginUserName() {
        return loginUserName;
    }

    public void setLoginUserName(String loginUserName) {
        this.loginUserName = loginUserName;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public String getConfigCode() {
        return configCode;
    }

    public void setConfigCode(String configCode) {
        this.configCode = configCode;
    }

    public String getConfigName() {
        return configName;
    }

    public void setConfigName(String configName) {
        this.configName = configName;
    }

    public String getReviewerCode() {
        return reviewerCode;
    }

    public void setReviewerCode(String reviewerCode) {
        this.reviewerCode = reviewerCode;
    }

    public String getReviewerName() {
        return reviewerName;
    }

    public void setReviewerName(String reviewerName) {
        this.reviewerName = reviewerName;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public String getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(String firstLogin) {
        this.firstLogin = firstLogin;
    }

    public String getCdKubun() {
        return cdKubun;
    }

    public void setCdKubun(String cdKubun) {
        this.cdKubun = cdKubun;
    }

    public String getCdKubunName() {
        return cdKubunName;
    }

    public void setCdKubunName(String cdKubunName) {
        this.cdKubunName = cdKubunName;
    }

    public String getCdCode() {
        return cdCode;
    }

    public void setCdCode(String cdCode) {
        this.cdCode = cdCode;
    }

    public String getKjName() {
        return kjName;
    }

    public void setKjName(String kjName) {
        this.kjName = kjName;
    }
    
    @Override
    public String toString() {

        return super.toString() + String.format(
                ",userName=%s, tjCode=%s, positionId=%s, deptId=%s, postCode=%s,"
                + "roleCode=%s,manageRoleFlag=%s,currentPage=%s,pageSize=%s,sortColumn=%s,sortType=%s,positionName=%s,deptName=%s,"
                + "postName=%s,roleName=%s,email=%s,configName=%s,applicantCode=%s,applicantName=%s,updateTime=%s,loginUserName=%s,configCode=%s"
                + ",configCode=%s,reviewerCode=%s,reviewerName=%s,updateUserName=%s,firstLogin=%s,cdKubun=%s,cdKubunName=%s,cdCode=%s,kjName=%s",
                userName, tjCode, positionId, deptId, postCode,roleCode, manageRoleFlag, currentPage,pageSize,
                sortColumn,sortType,positionName,deptName,postName,roleName,email,
                manageRoleName,applicantCode,applicantName,updateTime,loginUserName,configCode
                ,configName,reviewerCode,reviewerName,updateUserName,firstLogin,cdKubun,cdKubunName,cdCode,kjName);

    }
}