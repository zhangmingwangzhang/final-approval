/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.admin.userList.form;

/**
 * 用户追加PopUpForm
 * 
 * @author sunliqiang
 * @since 2020/11/13
 * @version 0.1
 */
public class UserListPopUpForm {

    /** popUp姓名 */
    private String popUserName;
    
    /** popUp天津卡号 */
    private String popTjCode;
    
    /** popUp职位 */
    private String popPositionId;
    
    /** popUp部门 */
    private String popDeptId;
    
    /** popUp职务权限 */
    private String popPostCode;
    
    /** popUp审核权限 */
    private String popReviewRoleCode;
    
    /** popUp邮箱 */
    private String popEmail;
    
    /** popUp管理权限 */
    private String popRoleFlag;
    
    /** 登录者姓名 */
    private String loginUserName;
    
    /** 被修改人姓名 */
    private String updateUserName;
    

    public String getPopPositionId() {
        return popPositionId;
    }

    public void setPopPositionId(String popPositionId) {
        this.popPositionId = popPositionId;
    }

    public String getPopDeptId() {
        return popDeptId;
    }

    public void setPopDeptId(String popDeptId) {
        this.popDeptId = popDeptId;
    }

    public String getPopPostCode() {
        return popPostCode;
    }

    public void setPopPostCode(String popPostCode) {
        this.popPostCode = popPostCode;
    }

    public String getPopReviewRoleCode() {
        return popReviewRoleCode;
    }

    public void setPopReviewRoleCode(String popReviewRoleCode) {
        this.popReviewRoleCode = popReviewRoleCode;
    }

    public String getPopEmail() {
        return popEmail;
    }

    public void setPopEmail(String popEmail) {
        this.popEmail = popEmail;
    }

    public String getPopRoleFlag() {
        return popRoleFlag;
    }

    public void setPopRoleFlag(String popRoleFlag) {
        this.popRoleFlag = popRoleFlag;
    }

    public String getPopUserName() {
        return popUserName;
    }

    public void setPopUserName(String popUserName) {
        this.popUserName = popUserName;
    }

    public String getPopTjCode() {
        return popTjCode;
    }

    public void setPopTjCode(String popTjCode) {
        this.popTjCode = popTjCode;
    }

    public String getLoginUserName() {
        return loginUserName;
    }

    public void setLoginUserName(String loginUserName) {
        this.loginUserName = loginUserName;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }
    
    @Override
    public String toString() {

        return super.toString() + String.format(
                ",popUserName=%s, popTjCode=%s, popPositionId=%s, popDeptId=%s, "
                + "popPostCode=%s,popReviewRoleCode=%s,popEmail=%s,popRoleFlag=%s,loginUserName=%s,updateUserName=%s",
                popUserName, popTjCode, popPositionId, popDeptId, popPostCode,
                popReviewRoleCode, popEmail, popRoleFlag,loginUserName,updateUserName);

    }
}