/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.admin.userList.form;

import java.util.List;

import tci.sds.admin.userList.Entity.ListExEntity;
import tci.sds.admin.userList.Entity.UserListExEntity;
import tci.sds.common.exception.ErrorType;

/**
 * 用户一览Form
 * 
 * @author sunliqiang
 * @since 2020/11/13
 * @version 0.1
 */
public class UserListForm {

    /** 姓名 */
    private String userName;
    
    /** 卡号 */
    private String tjCode;
    
    /** 职位 */
    private String positionId;
    
    /** 部门 */
    private String deptId;
    
    /** 职务权限 */
    private String postCode;
    
    /** 审核权限 */
    private String roleCode;

    /** 管理权限有无 */
    private String manageRoleFlag;
    
    /** 当前页数 */
    private int currentPage;

    /** 一页的件数 */
    private int pageSize;

    /** 排序字段 */
    private String sortColumn;

    /** 排序顺序 */
    private String sortType;

    /** 部门PullDown */
    private List<ListExEntity> deptPullDown;
    
    /** 职位 PullDown*/
    private List<ListExEntity> positionPullDown;
    
    /** 职务权限 PullDown*/
    private List<ListExEntity> postPullDown;
    
    /** 审核权限PullDown */
    private List<ListExEntity> reviewRolePullDown;
    
    /** 管理权限PullDown */
    private List<ListExEntity> manageRolePullDown;
    
    /** 被删除者天津卡号 */
    private String deleteTjCode;
    
    /** 被删除者姓名 */
    private String deleteUserName;
    
    /** 画面一览*/
    private List<UserListExEntity> userData;
    
    /** errorCode */
    private String errorCode = ErrorType.NotError.toString();

    /** 埋入文字List */
    private List<String> errorMessages;
    
    /** Info文字 */
    private String infoMessage;
    
    /** 总件数 */
    private int total;
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTjCode() {
        return tjCode;
    }

    public void setTjCode(String tjCode) {
        this.tjCode = tjCode;
    }

    public String getPositionId() {
        return positionId;
    }

    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getManageRoleFlag() {
        return manageRoleFlag;
    }

    public void setManageRoleFlag(String manageRoleFlag) {
        this.manageRoleFlag = manageRoleFlag;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSortColumn() {
        return sortColumn;
    }

    public void setSortColumn(String sortColumn) {
        this.sortColumn = sortColumn;
    }

    public String getSortType() {
        return sortType;
    }

    public void setSortType(String sortType) {
        this.sortType = sortType;
    }

    public List<ListExEntity> getDeptPullDown() {
        return deptPullDown;
    }

    public void setDeptPullDown(List<ListExEntity> deptPullDown) {
        this.deptPullDown = deptPullDown;
    }

    public List<ListExEntity> getPositionPullDown() {
        return positionPullDown;
    }

    public void setPositionPullDown(List<ListExEntity> positionPullDown) {
        this.positionPullDown = positionPullDown;
    }

    public List<ListExEntity> getPostPullDown() {
        return postPullDown;
    }

    public void setPostPullDown(List<ListExEntity> postPullDown) {
        this.postPullDown = postPullDown;
    }

    public List<ListExEntity> getReviewRolePullDown() {
        return reviewRolePullDown;
    }

    public void setReviewRolePullDown(List<ListExEntity> reviewRolePullDown) {
        this.reviewRolePullDown = reviewRolePullDown;
    }

    public List<ListExEntity> getManageRolePullDown() {
        return manageRolePullDown;
    }

    public void setManageRolePullDown(List<ListExEntity> manageRolePullDown) {
        this.manageRolePullDown = manageRolePullDown;
    }

    public String getDeleteTjCode() {
        return deleteTjCode;
    }

    public void setDeleteTjCode(String deleteTjCode) {
        this.deleteTjCode = deleteTjCode;
    }

    public List<UserListExEntity> getUserData() {
        return userData;
    }

    public void setUserData(List<UserListExEntity> userData) {
        this.userData = userData;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public List<String> getErrorMessages() {
        return errorMessages;
    }

    public void setErrorMessages(List<String> errorMessages) {
        this.errorMessages = errorMessages;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getDeleteUserName() {
        return deleteUserName;
    }

    public void setDeleteUserName(String deleteUserName) {
        this.deleteUserName = deleteUserName;
    }

    public String getInfoMessage() {
        return infoMessage;
    }

    public void setInfoMessage(String infoMessage) {
        this.infoMessage = infoMessage;
    }

    @Override
    public String toString() {

        return super.toString() + String.format(
                ",userName=%s, tjCode=%s, positionId=%s, deptId=%s, postCode=%s,"
                + "roleCode=%s,manageRoleFlag=%s,currentPage=%s,pageSize=%s,"
                + "sortColumn=%s,sortType=%s,deptPullDown=%s,positionPullDown=%s,"
                + "postPullDown=%s,reviewRolePullDown=%s,manageRolePullDown=%s,"
                + "deleteTjCode=%s,deleteUserName=%s,userData=%s,errorMessages=%s,errorCode=%s,infoMessage=%s",
                userName, tjCode, positionId, deptId, postCode,
                roleCode, manageRoleFlag, currentPage,pageSize,
                sortColumn,sortType,deptPullDown,positionPullDown,
                postPullDown,reviewRolePullDown,manageRolePullDown,
                deleteTjCode,deleteUserName,userData,errorMessages,errorCode,infoMessage);

    }
}