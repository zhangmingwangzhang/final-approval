/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.admin.userList.controller;

import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import tci.sds.admin.userList.Entity.ListExEntity;
import tci.sds.admin.userList.Entity.UserListExEntity;
import tci.sds.admin.userList.form.UserListForm;
import tci.sds.admin.userList.form.UserListPopUpForm;
import tci.sds.admin.userList.service.UserListService;
import tci.sds.common.exception.BusinessException;
import tci.sds.common.service.InfoMessageService;
import tci.sds.common.util.Constants;

/**
 * 用户一览Controller
 * 
 * @author sunliqiang
 * @since 2020/11/13
 * @version 0.1
 */
@Controller
public class UserListController {
    private final Logger logger = LoggerFactory
            .getLogger(UserListController.class);

    @Autowired
    private UserListService userListService;

    @Autowired
    private InfoMessageService infoMessageService;
    /**
     * 用户一览初期表示
     * 
     * @return UserListForm
     */
    @RequestMapping("/api/userlist/init")
    @ResponseBody
    public UserListForm init() throws Exception {
        //拦截器
        HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = request.getSession();  
        session.setAttribute("userName", "");
        
        // 部门PullDown取得
        String dept = Constants.CD_DEPART;
        List<ListExEntity> deptName = this.userListService.searchDept(dept);
        // 职位PullDown取得
        String position = Constants.CD_POSITION;
        List<ListExEntity> positionName = this.userListService.searchPosition(position);
        // 职务权限PullDown取得
        String post = Constants.CD_POST_AUTHORITY;
        List<ListExEntity> postName = this.userListService.searchPostRole(post);
        // 审核权限PullDown取得
        String role = Constants.CD_AUDIT_AUTHORITY;
        List<ListExEntity> reviewRoleName = this.userListService.searchReviewRole(role);
        // 管理权限PullDown取得
        String manage = Constants.CD_MANAGE_AUTHORITY;
        List<ListExEntity> manageRoleName = this.userListService.getManageRole(manage);
        
        // 一览初期
        UserListExEntity userListExEntity = new UserListExEntity();
        // 每页显示条数
        userListExEntity.setPageSize(20);
        // 当前页数
        userListExEntity.setCurrentPage(1);
        // 初期排序字段
        userListExEntity.setSortColumn("tjCode");
        // 初期排序顺序
        userListExEntity.setSortType("asc");
        // 一览Data
        List<UserListExEntity> userData = this.userListService.search(userListExEntity);
        // 一览DataTotal
        int total = this.userListService.searchTotal(userListExEntity);
        
        UserListForm userList = new UserListForm();
        // 一览数据
        userList.setUserData(userData);
        // 部门PullDown
        userList.setDeptPullDown(deptName); 
        // 职位PullDown
        userList.setPositionPullDown(positionName);
        // 职务权限PullDown
        userList.setPostPullDown(postName);
        // 审核权限PullDown
        userList.setReviewRolePullDown(reviewRoleName);
        // 管理权限PullDown
        userList.setManageRolePullDown(manageRoleName);
        // 总条数
        userList.setTotal(total);
        // 排序字段
        userList.setSortColumn(userListExEntity.getSortColumn());
        // 排序顺序
        userList.setSortType(userListExEntity.getSortType());
        
        return userList;
    }

    /**
     * 用户一览检索
     * 
     * @param UserListForm 
     * @return UserListForm
     */
    @PostMapping("/api/userList/search")
    @ResponseBody public UserListForm search(
         @Valid @RequestBody UserListForm userListForm) throws Exception {
         
         logger.info("search#param:{}", userListForm);
         
         UserListExEntity userListExEntity = this.copyFormToExEntity(userListForm);
         // 一览检索总条数
         int total = this.userListService.searchTotal(userListExEntity); 
         // 一览检索数据
         List<UserListExEntity> userList = this.userListService.search(userListExEntity);
         
         // 检索0件时
         UserListForm UserList = new UserListForm();
         if(userList.size() == 0) {
             String infoMessage = infoMessageService.getInfo("INF0000003");
             UserList.setInfoMessage(infoMessage);
         }
         // 一览数据
         UserList.setUserData(userList);
         // 总条数
         UserList.setTotal(total);
         // errorCode
         UserList.setErrorCode("0");
         
         logger.info("search#result:{}", userList.size()); 
         
         return UserList;
    }
    
    /**
     * 用户追加
     * 
     * @param UserListPopUpForm 
     * @return UserListForm
     */
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/api/userList/insert")
    @ResponseBody public UserListForm insert(
         @Valid @RequestBody UserListPopUpForm userListPopUpForm) throws Exception {
         
         logger.info("insert#param:{}", userListPopUpForm);
         // 单项目Check
         // 姓名必须Check
         String userName = userListPopUpForm.getPopUserName();
         if(userName == null || userName == "") {
             throw new BusinessException("ERR0000001","姓名");
         }
         
         // 姓名范围Check
         if(userName.length() < 1 || userName.length() > 5) {
             throw new BusinessException("ERR0000006","姓名","1～5");
         }
         
         // 天津卡号必须Check
         String tjCode = userListPopUpForm.getPopTjCode();
         if(tjCode == null || tjCode == "") {
             throw new BusinessException("ERR0000001","天津卡号");
         }
         
         // 天津卡号半角数字Check
         Pattern pattern = Pattern.compile("[0-9]*"); 
         boolean tjCodeMatch = pattern.matcher(tjCode).matches();
         if(!tjCodeMatch) {
             throw new BusinessException("ERR0000005","天津卡号","半角数字");
         }
         
         // 天津卡号范围Check
         if(tjCode.length() < 4 || tjCode.length() > 6) {
             throw new BusinessException("ERR0000006","天津卡号","4～6");
         }
         
         // 部门必须Check
         String depart = userListPopUpForm.getPopDeptId();
         if(depart == null || depart == "") {
             throw new BusinessException("ERR0000001","部门");
         }
         
         // 职位必须Check
         String position = userListPopUpForm.getPopPositionId();
         if(position == null || position == "") {
             throw new BusinessException("ERR0000001","职位");
         }
         
         // 职务权限必须Check
         String post = userListPopUpForm.getPopPostCode();
         if(post == null || post == "") {
             throw new BusinessException("ERR0000001","职务权限");
         }
         
         // 邮箱必须Check
         String email = userListPopUpForm.getPopEmail();
         if(email == null || email == "") {
             throw new BusinessException("ERR0000001","邮箱");
         }
         
         // 邮箱半角英数字Check
         Pattern patternEmail = Pattern.compile("[A-Za-z0-9]+");
         String emailCheck = email.replace(".","");
         boolean emailMatch = patternEmail.matcher(emailCheck).matches();
         if(!emailMatch) {
             throw new BusinessException("ERR0000005","邮箱","半角英数字");
         }
         
         // 邮箱范围Check
         if(email.length() < 1 || email.length() > 30) {
             throw new BusinessException("ERR0000006","邮箱","1～30");
         }
         
         // 邮箱小数点必须Check
         int emailIndex = userListPopUpForm.getPopEmail().indexOf(".");
         if(emailIndex == -1) {
             throw new BusinessException("ERR0000027");
         }
         
         // 审核权限必须Check
         String reviewRole = userListPopUpForm.getPopReviewRoleCode();
         if(reviewRole == null || reviewRole == "") {
             throw new BusinessException("ERR0000001","审核权限");
         }
         
         UserListExEntity userListExEntity = this.copyFormToPopUpExEntity(userListPopUpForm);
         // 追加
         userListService.insertUser(userListExEntity);
         
         // 获取InfoMessage信息
         String infoMessage = infoMessageService.getInfo("INF0000006");
         
         // 部门PullDown取得
         String deptStr = Constants.CD_DEPART;
         List<ListExEntity> deptPull = this.userListService.searchDept(deptStr);
         // 职务权限PullDown取得
         String postStr = Constants.CD_POST_AUTHORITY;
         List<ListExEntity> postPull = this.userListService.searchPostRole(postStr);
         
         UserListForm userList = new UserListForm();
         // 部门PullDown
         userList.setDeptPullDown(deptPull); 
         // 职务权限PullDown
         userList.setPostPullDown(postPull);
         // errorCode
         userList.setErrorCode("0");
         // InfoMessage
         userList.setInfoMessage(infoMessage);
         
         logger.info("insert#result:{}", userList); 
         
         return userList;
    }
    
    /**
     * 用户变更
     * 
     * @param UserListPopUpForm 
     * @return UserListForm
     */
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/api/userList/update")
    @ResponseBody public UserListForm update(
         @Valid @RequestBody UserListPopUpForm userListPopUpForm) throws Exception {
         
         logger.info("update#param:{}", userListPopUpForm);
         
         // 单项目Check
         // 姓名必须Check
         String userName = userListPopUpForm.getPopUserName();
         if(userName == null || userName == "") {
             throw new BusinessException("ERR0000001","姓名");
         }
         
         // 姓名范围Check
         if(userName.length() < 1 || userName.length() > 5) {
             throw new BusinessException("ERR0000006","姓名","1～5");
         }
         
         // 部门必须Check
         String depart = userListPopUpForm.getPopDeptId();
         if(depart == null || depart == "") {
             throw new BusinessException("ERR0000001","部门");
         }
         
         // 职位必须Check
         String position = userListPopUpForm.getPopPositionId();
         if(position == null || position == "") {
             throw new BusinessException("ERR0000001","职位");
         }
         
         // 职务权限必须Check
         String post = userListPopUpForm.getPopPostCode();
         if(post == null || post == "") {
             throw new BusinessException("ERR0000001","职务权限");
         }
         
         // 邮箱必须Check
         String email = userListPopUpForm.getPopEmail();
         if(email == null || email == "") {
             throw new BusinessException("ERR0000001","邮箱");
         }
         
         // 邮箱半角英数字Check
         Pattern patternEmail = Pattern.compile("[A-Za-z0-9]+");
         String emailCheck = email.replace(".",""); 
         boolean emailMatch = patternEmail.matcher(emailCheck).matches();
         if(!emailMatch) {
             throw new BusinessException("ERR0000005","邮箱","半角英数字");
         }
         
         // 邮箱范围Check
         if(email.length() < 1 || email.length() > 30) {
             throw new BusinessException("ERR0000006","邮箱","1～30");
         }
         
         // 邮箱小数点必须Check
         int emailIndex = userListPopUpForm.getPopEmail().indexOf(".");
         if(emailIndex == -1) {
             throw new BusinessException("ERR0000027");
         }
         
         // 审核权限必须Check
         String reviewRole = userListPopUpForm.getPopReviewRoleCode();
         if(reviewRole == null || reviewRole == "") {
             throw new BusinessException("ERR0000001","审核权限");
         }

         UserListExEntity userListExEntity = this.copyFormToPopUpExEntity(userListPopUpForm);
         // 变更
         userListService.updateUser(userListExEntity); 
         
         // 获取InfoMessage信息
         String infoMessage = infoMessageService.getInfo("INF0000004");
         
         // 部门PullDown取得
         String deptStr = Constants.CD_DEPART;
         List<ListExEntity> deptPull = this.userListService.searchDept(deptStr);
         // 职务权限PullDown取得
         String postStr = Constants.CD_POST_AUTHORITY;
         List<ListExEntity> postPull = this.userListService.searchPostRole(postStr);
         
         UserListForm userList = new UserListForm();
         // 部门PullDown
         userList.setDeptPullDown(deptPull); 
         // 职务权限PullDown
         userList.setPostPullDown(postPull);
         // errorCode
         userList.setErrorCode("0");
         // InfoMessage
         userList.setInfoMessage(infoMessage);
         logger.info("update#result:{}", userList); 
         
         return userList;
    }
    
    /**
     * 用户删除
     * 
     * @param UserListForm 
     * @return UserListForm
     */
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/api/userList/delete")
    @ResponseBody public UserListForm delete(
         @Valid @RequestBody UserListForm userListForm) throws Exception {
         
         logger.info("delete#param:{}", userListForm);
         
         UserListExEntity userListExEntity = new UserListExEntity();
         userListExEntity.setTjCode(userListForm.getDeleteTjCode());
         userListExEntity.setUserName(userListForm.getDeleteUserName());
         // 删除
         userListService.deleteUser(userListExEntity);
         
         // 获取InfoMessage信息
         String infoMessage = infoMessageService.getInfo("INF0000005");
         
         UserListForm userList = new UserListForm();
         // errorCode
         userList.setErrorCode("0");
         // InfoMessage
         userList.setInfoMessage(infoMessage);
         
         logger.info("delete#result:{}", userList); 
         
         return userList;
    }
    
    /**
     * 把Form的值保存到ExEntity中
     * 
     * @param UserListForm
     * @return ExEntity
     */
    private UserListExEntity copyFormToExEntity(UserListForm userListForm) {

        UserListExEntity userListExEntity = new UserListExEntity();
        // 姓名
        userListExEntity.setUserName(userListForm.getUserName());
        // 天津卡号
        userListExEntity.setTjCode(userListForm.getTjCode());
        // 职位
        userListExEntity.setPositionId(userListForm.getPositionId());
        // 部门
        userListExEntity.setDeptId(userListForm.getDeptId());
        // 职务权限
        userListExEntity.setPostCode(userListForm.getPostCode());
        // 审核权限
        userListExEntity.setRoleCode(userListForm.getRoleCode());
        // 管理权限
        userListExEntity.setManageRoleFlag(userListForm.getManageRoleFlag());
        // 当前页数
        userListExEntity.setCurrentPage(userListForm.getCurrentPage());
        // 页面显示条数
        userListExEntity.setPageSize(userListForm.getPageSize());
        // 排序字段 
        userListExEntity.setSortColumn(userListForm.getSortColumn());
        // 排序顺序
        userListExEntity.setSortType(userListForm.getSortType());
        
        return userListExEntity;
    }
    
    /**
     * 把Form的值保存到ExEntity中
     * 
     * @param userListPopUpForm
     * @return ExEntity
     */
    private UserListExEntity copyFormToPopUpExEntity(UserListPopUpForm userListPopUpForm) {

        UserListExEntity userListExEntity = new UserListExEntity();
        // 姓名
        userListExEntity.setUserName(userListPopUpForm.getPopUserName());
        // 天津卡号
        userListExEntity.setTjCode(userListPopUpForm.getPopTjCode());
        // 职位
        userListExEntity.setPositionId(userListPopUpForm.getPopPositionId());
        // 部门
        userListExEntity.setDeptId(userListPopUpForm.getPopDeptId());
        // 职务权限
        userListExEntity.setPostCode(userListPopUpForm.getPopPostCode());
        // 邮箱
        userListExEntity.setEmail(userListPopUpForm.getPopEmail()+"@trans-cosmos.com.cn");
        // 审核权限
        userListExEntity.setRoleCode(userListPopUpForm.getPopReviewRoleCode());
        // 管理权限
        userListExEntity.setManageRoleFlag(userListPopUpForm.getPopRoleFlag());
        // 登录者姓名
        userListExEntity.setLoginUserName(userListPopUpForm.getLoginUserName());
        // 被修改者姓名
        userListExEntity.setUpdateUserName(userListPopUpForm.getUpdateUserName());
        
        return userListExEntity;
    }
}