/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.admin.userList.mapper;

import java.util.List;

import tci.sds.admin.userList.Entity.ListExEntity;
import tci.sds.admin.userList.Entity.UserListExEntity;

/**
 * 用户一览Mapper
 * 
 * @author sunliqiang
 * @since 2020/11/13
 * @version 0.1
 */
public interface UserListExMapper  {
	
    /**
     * 部门下拉框获取
     * @return 
     */
    List<ListExEntity> searchDept(String dept);
    
    /**
     * 职位下拉框获取
     * @return 
     */
    List<ListExEntity> searchPosition(String position);
    
    /**
     * 职务权限下拉框获取
     * @return 
     */
    List<ListExEntity> searchPostRole(String post);
    
    /**
     * 审核权限下拉框获取
     * @return 
     */
    List<ListExEntity> searchReviewRole(String role);
    
    /**
     * 管理权限下拉框获取
     * @return 
     */
    List<ListExEntity> getManageRole(String manage);
    
    /**
     * 一览检索结果
     * @param currentPage
     * @param pagesize
     * @return 
     */
    List<UserListExEntity> search(UserListExEntity userListExEntity);
    
    /**
     * 检索总件数
     * @return 总件数
     */
    int getTotalCount(UserListExEntity userListExEntity);

    
    /**
     * Code名称取得
     * @return 部门名称
     */
    String searchName(String id);
    
    
    /**
     * Code取得
     * @return 部门名称
     */
    String searchId(UserListExEntity userListExEntity);
    
    /**
     * 用户存在或不存在Check
     * @return 
     */
    int userCheck(String tjCode);
    
    /**
     * 用户追加
     * @return
     */
    int insertUser(UserListExEntity userListExEntity);

    /**
     * 用户变更
     * @return
     */
    int updateUser(UserListExEntity userListExEntity);
    
    /**
     * 申请者或确认者在业务中check
     * @return
     */
    int searchConfimAppUser(String tjCode);
    
    /**
     * 审核人在业务中check
     * @return
     */
    int searchApproveUser(String tjCode);
    
    /**
     * 用户删除
     * @return
     */
    int deleteUser(String tjcode);
    
    /**
     * 下拉框输入内容有无check
     * @return
     */
    int searchByCode(String cdCode);
    
    /**
     * 下拉框输入内容有无check
     * @return
     */
    int searchByKjName(UserListExEntity userListExEntity);
    
    /**
     * 检索CODE表中部门的所有信息
     * @return
     */
    List<UserListExEntity> getDeptCode(UserListExEntity userListExEntity);
    
    /**
     * 添加新CodeList
     * @return
     */
    int addCode(UserListExEntity userListExEntity);
    
    /**
     * 检索CODE表中职务权限的所有信息
     * @return
     */
    List<UserListExEntity> getPostCode(UserListExEntity userListExEntity);
    
    /**
     * 济南事业部经理或天津事业部部长存在check
     * @return
     */
    List<UserListExEntity> searchPostUser(UserListExEntity userListExEntity);
    
    /**
     * 职务权限除普通员工，济南事业部经理，天津事业部部长唯一check
     * @return
     */
    int searchSpecialUser(String postCode);
    
    /**
     * 用户变更，职位查询
     * @return
     */
    String searUserByCode(String tjCode);
    
}