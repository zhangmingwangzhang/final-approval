/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.admin.userList.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tci.sds.admin.userList.Entity.ListExEntity;
import tci.sds.admin.userList.Entity.UserListExEntity;
import tci.sds.admin.userList.mapper.UserListExMapper;
import tci.sds.common.exception.BusinessException;
import tci.sds.common.util.Constants;

/**
 * 用户一览Service
 * 
 * @author sunliqiang
 * @since 2020/11/13
 * @version 0.1
 */
@Service
public class UserListService {
    private final Logger logger = LoggerFactory.getLogger(UserListService.class);

    @Autowired
    private UserListExMapper userListExMapper;

    /**
     * 部门下拉框取得
     * 
     * @param 
     * @return 部门下拉框
     */
    @Transactional(readOnly = true)
    public List<ListExEntity> searchDept(String dept) {

        logger.info("searchDept#param:{}", dept);
        // 部门下拉框
        List<ListExEntity> deptName = this.userListExMapper.searchDept(dept);
        
        logger.info("searchDept#result:{}", deptName.size());
        return deptName;
    }
    
    /**
     * 职位下拉框取得
     * 
     * @param 
     * @return 职位下拉框
     */
    @Transactional(readOnly = true)
    public List<ListExEntity> searchPosition(String position) {

        logger.info("searchPosition#param:{}", position);
        // 职位下拉框
        List<ListExEntity> positionName = this.userListExMapper.searchPosition(position);
        
        logger.info("searchPosition#result:{}", positionName.size());
        return positionName;
    }
    
    /**
     * 职务权限下拉框取得
     * 
     * @param 
     * @return 职位下拉框
     */
    @Transactional(readOnly = true)
    public List<ListExEntity> searchPostRole(String post) {

        logger.info("searchPostRole#param:{}", post);
        //职务权限下拉框
        List<ListExEntity> postName = this.userListExMapper.searchPostRole(post);
        
        logger.info("searchPostRole#result:{}", postName.size());
        return postName;
    }
    
    /**
     * 审核权限下拉框取得
     * 
     * @param 
     * @return 职位下拉框
     */
    @Transactional(readOnly = true)
    public List<ListExEntity> searchReviewRole(String role) {

        logger.info("searchReviewRole#param:{}", role);
        // 审核权限下拉框
        List<ListExEntity> reviewRoleName = this.userListExMapper.searchReviewRole(role);
        
        logger.info("searchReviewRole#result:{}", reviewRoleName.size());
        return reviewRoleName;
    }

    /**
     * 管理权限下拉框取得
     * 
     * @param 
     * @return 职位下拉框
     */
    @Transactional(readOnly = true)
    public List<ListExEntity> getManageRole(String manage) {

        logger.info("getManageRole#param:{}", manage);
        // 管理权限下拉框
        List<ListExEntity> manageRoleName = this.userListExMapper.getManageRole(manage);
        
        logger.info("getManageRole#result:{}", manageRoleName.size());
        return manageRoleName;
    }
    
    /**
     * 一览检索
     * 
     * @param 
     * @return UserList
     */
    @Transactional(readOnly = true)
    public List<UserListExEntity> search(UserListExEntity userListExEntity) {

        logger.info("search#param:{}", userListExEntity);
        // 一览数据
        List<UserListExEntity> UserList = this.userListExMapper.search(userListExEntity);
        
        logger.info("search#result:{}", UserList.size());
        return UserList;
    }
    
    /**
     * 检索总件数
     * 
     * @param userListExEntity
     * @return 总件数
     */
    @Transactional(readOnly = true)
    public int searchTotal(UserListExEntity userListExEntity) {
        
        logger.info("searchTotal#param:{}", userListExEntity);
        // 一览数据总件数
        int total = this.userListExMapper.getTotalCount(userListExEntity);
        
        logger.info("searchTotal#result:{}", total);
        return total;
    }

    /**
     * 用户更新
     * 
     * @param userListExEntity
     */
    @Transactional(readOnly = false)
    public void updateUser(UserListExEntity userListExEntity) throws Exception {
        
        logger.info("updateUser#param:{}", userListExEntity);
        // 部门下拉框输入内容DB有无check
        int deptCode = this.userListExMapper.searchByCode(userListExEntity.getDeptId());
        userListExEntity.setCdKubun(Constants.CD_DEPART);
        userListExEntity.setKjName(userListExEntity.getDeptId());
        int deptKjName = this.userListExMapper.searchByKjName(userListExEntity);
        
        if(deptCode == 0 && deptKjName == 0) {
            UserListExEntity deptCodeEntity = new UserListExEntity();
            // CODE区分值
            deptCodeEntity.setCdKubun(Constants.CD_DEPART);
            // CODE区分名称
            deptCodeEntity.setCdKubunName(Constants.CD_DEPARTNAME);
            // CODE名称
            deptCodeEntity.setKjName(userListExEntity.getDeptId());
            // CODE表中部门的所有信息
            List<UserListExEntity> deptCodeList = this.userListExMapper.getDeptCode(deptCodeEntity);
            if(deptCodeList.size() != 0){
                String maxDeptCode = deptCodeList.get(0).getCdCode().replaceAll(".*[^\\d](?=(\\d+))","");
                // CODE值
                deptCodeEntity.setCdCode("DEPART_" + String.valueOf(Integer.valueOf(maxDeptCode) + 1));
            } else {
                deptCodeEntity.setCdCode("DEPART_0");
            }
            // CODE表中添加输入的部门
            userListExMapper.addCode(deptCodeEntity);
            
            // 部门名称
            userListExEntity.setDeptName(userListExEntity.getDeptId());
            // 部门Code
            userListExEntity.setDeptId(deptCodeEntity.getCdCode());
        } else {
            
            if(deptKjName != 0) {
                userListExEntity.setDeptName(userListExEntity.getDeptId());
                userListExEntity.setKjName(userListExEntity.getDeptId());
                String deptId = this.userListExMapper.searchId(userListExEntity);
                userListExEntity.setDeptId(deptId);
            } else {
                // 获取部门名称
                String deptName = this.userListExMapper.searchName(userListExEntity.getDeptId());
                // 部门名称
                userListExEntity.setDeptName(deptName);
            }
        }
        //变更者职位查询
        String postUserCode = this.userListExMapper.searUserByCode(userListExEntity.getTjCode());
        // 职务权限下拉框输入内容DB有无check
        int postCode = this.userListExMapper.searchByCode(userListExEntity.getPostCode());
        userListExEntity.setCdKubun(Constants.CD_POST_AUTHORITY);
        userListExEntity.setKjName(userListExEntity.getPostCode());
        int postjName = this.userListExMapper.searchByKjName(userListExEntity);
        
        if(postCode == 0 && postjName == 0) {
            UserListExEntity postCodeEntity = new UserListExEntity();
            // CODE区分值
            postCodeEntity.setCdKubun(Constants.CD_POST_AUTHORITY);
            // CODE区分名称
            postCodeEntity.setCdKubunName(Constants.CD_POSTNAME);
            // CODE名称
            postCodeEntity.setKjName(userListExEntity.getPostCode());
            // CODE表中职务权限的所有信息
            List<UserListExEntity> postCodeList = this.userListExMapper.getPostCode(postCodeEntity);
            if(postCodeList.size() != 0){
                String maxPostCode = postCodeList.get(0).getCdCode().replaceAll(".*[^\\d](?=(\\d+))","");
                // CODE值
                postCodeEntity.setCdCode("POST_" + String.valueOf(Integer.valueOf(maxPostCode) + 1));
            } else {
                postCodeEntity.setCdCode("POST_1");
            }
            // CODE表中添加输入的职务权限
            userListExMapper.addCode(postCodeEntity);
            
            // 职务权限Code
            userListExEntity.setPostCode(postCodeEntity.getCdCode());
        } else {
            if(postjName != 0) {
                userListExEntity.setPostName(userListExEntity.getPostCode());
                String postId = this.userListExMapper.searchId(userListExEntity);
                userListExEntity.setPostCode(postId);
            } else {
                // 获取职务权限名称
                String postName = this.userListExMapper.searchName(userListExEntity.getPostCode());
                // 职务权限名称
                userListExEntity.setPostName(postName);
            }
            
            // 济南事业部经理或天津事业部部长存在check
            if(userListExEntity.getPostCode().equals(Constants.CD_POST_JN1) || userListExEntity.getPostCode().equals(Constants.CD_POST_TJ1) ) {
                List<UserListExEntity> count = this.userListExMapper.searchPostUser(userListExEntity);
                if (count.size() !=0 && !count.get(0).getTjCode().equals(userListExEntity.getTjCode())) {
                    throw new BusinessException("ERR0000037",userListExEntity.getDeptName(),userListExEntity.getPostName());
                }
            }
        }
        
        // 更新日时
        Timestamp updateTime = new Timestamp(new Date().getTime());
        userListExEntity.setUpdateTime(updateTime);
        // DBCheck
        // 用户不存在check
        int userCheck = this.userListExMapper.userCheck(userListExEntity.getTjCode());
        
        if(userCheck == 0) {
            throw new BusinessException("ERR0000026");
        }
        
        // 是否在业务中check
        int confimAppUser = this.userListExMapper.searchConfimAppUser(userListExEntity.getTjCode());
        int approveUser=this.userListExMapper.searchApproveUser(userListExEntity.getTjCode());
        
        if(confimAppUser != 0 || approveUser != 0) {
            String errorUser = userListExEntity.getTjCode() + "/"+ userListExEntity.getUpdateUserName();
            throw new BusinessException("ERR0000023",errorUser);
        }
        
        //判断职位是否该改变
        if(!userListExEntity.getPostCode().equals(postUserCode)) {
         // 职务权限除普通员工，济南事业部经理，天津事业部部长唯一check
            if(!userListExEntity.getPostCode().equals(Constants.CD_POST_1) && !userListExEntity.getPostCode().equals(Constants.CD_POST_TJ1)
                    && !userListExEntity.getPostCode().equals(Constants.CD_POST_JN1)) {
                int post =this.userListExMapper.searchSpecialUser(userListExEntity.getPostCode());
                if (post != 0) {
                    throw new BusinessException("ERR0000044",userListExEntity.getPostName());
                }
            }
        }
        // 执行变更
        int result = this.userListExMapper.updateUser(userListExEntity);
        logger.info("updateUser#result:{}", result);
    }

    /**
     * 用户追加
     * 
     * @param userListExEntity
     */
    @Transactional(readOnly = false)
    public void insertUser(UserListExEntity userListExEntity) throws Exception {

        logger.info("insertUser#param:{}", userListExEntity);
        // 部门下拉框输入内容DB有无check
        int deptCode = this.userListExMapper.searchByCode(userListExEntity.getDeptId());
        userListExEntity.setCdKubun(Constants.CD_DEPART);
        userListExEntity.setKjName(userListExEntity.getDeptId());
        int deptKjName = this.userListExMapper.searchByKjName(userListExEntity);
        
        if(deptCode == 0 && deptKjName == 0) {
            UserListExEntity deptCodeEntity = new UserListExEntity();
            // CODE区分值
            deptCodeEntity.setCdKubun(Constants.CD_DEPART);
            // CODE区分名称
            deptCodeEntity.setCdKubunName(Constants.CD_DEPARTNAME);
            // CODE名称
            deptCodeEntity.setKjName(userListExEntity.getDeptId());
            // CODE表中部门的所有信息
            List<UserListExEntity> deptCodeList = this.userListExMapper.getDeptCode(deptCodeEntity);
            if(deptCodeList.size() != 0){
                String maxDeptCode = deptCodeList.get(0).getCdCode().replaceAll(".*[^\\d](?=(\\d+))","");
                // CODE值
                deptCodeEntity.setCdCode("DEPART_" + String.valueOf(Integer.valueOf(maxDeptCode) + 1));
            } else {
                deptCodeEntity.setCdCode("DEPART_0");
            }
            // CODE表中添加输入的部门
            userListExMapper.addCode(deptCodeEntity);
            
            // 部门名称
            userListExEntity.setDeptName(userListExEntity.getDeptId());
            // 部门Code
            userListExEntity.setDeptId(deptCodeEntity.getCdCode());
        } else {
            if(deptKjName != 0) {
                userListExEntity.setDeptName(userListExEntity.getDeptId());
                String deptId = this.userListExMapper.searchId(userListExEntity);
                userListExEntity.setDeptId(deptId);
            } else {
                // 获取部门名称
                String deptName = this.userListExMapper.searchName(userListExEntity.getDeptId());
                // 部门名称
                userListExEntity.setDeptName(deptName);
            }
        }
        
        // 职务权限下拉框输入内容DB有无check
        int postCode = this.userListExMapper.searchByCode(userListExEntity.getPostCode());
        userListExEntity.setCdKubun(Constants.CD_POST_AUTHORITY);
        userListExEntity.setKjName(userListExEntity.getPostCode());
        int postjName = this.userListExMapper.searchByKjName(userListExEntity);
        
        if(postCode == 0 && postjName == 0) {
            UserListExEntity postCodeEntity = new UserListExEntity();
            // CODE区分值
            postCodeEntity.setCdKubun(Constants.CD_POST_AUTHORITY);
            // CODE区分名称
            postCodeEntity.setCdKubunName(Constants.CD_POSTNAME);
            // CODE名称
            postCodeEntity.setKjName(userListExEntity.getPostCode());
            // CODE表中职务权限的所有信息
            List<UserListExEntity> postCodeList = this.userListExMapper.getPostCode(postCodeEntity);
            if(postCodeList.size() != 0){
                String maxPostCode = postCodeList.get(0).getCdCode().replaceAll(".*[^\\d](?=(\\d+))","");
                // CODE值
                postCodeEntity.setCdCode("POST_" + String.valueOf(Integer.valueOf(maxPostCode) + 1));
            } else {
                postCodeEntity.setCdCode("POST_1");
            }
            // CODE表中添加输入的职务权限
            userListExMapper.addCode(postCodeEntity);
            
            // 职务权限Code
            userListExEntity.setPostCode(postCodeEntity.getCdCode());
        } else {
            if(postjName != 0) {
                userListExEntity.setPostName(userListExEntity.getPostCode());
                String postId = this.userListExMapper.searchId(userListExEntity);
                userListExEntity.setPostCode(postId);
            } else {
                // 获取职务权限名称
                String postName = this.userListExMapper.searchName(userListExEntity.getPostCode());
                // 职务权限名称
                userListExEntity.setPostName(postName);
            }
            
            // 济南事业部经理或天津事业部部长存在check
            if(userListExEntity.getPostCode().equals(Constants.CD_POST_JN1) || userListExEntity.getPostCode().equals(Constants.CD_POST_TJ1) ) {
                List<UserListExEntity> count = this.userListExMapper.searchPostUser(userListExEntity);
                if (count.size() !=0 && !count.get(0).getTjCode().equals(userListExEntity.getTjCode())) {
                    throw new BusinessException("ERR0000037",userListExEntity.getDeptName(),userListExEntity.getPostName());
                }
            }
        }
        
        // 更新日时
        Timestamp updateTime = new Timestamp(new Date().getTime());
        userListExEntity.setUpdateTime(updateTime);
        // 初次登录与否
        userListExEntity.setFirstLogin(Constants.CD_FIRST_LOGIN_YES);
        // DBCheck
        // 用户存在check
        int userCheck = this.userListExMapper.userCheck(userListExEntity.getTjCode());
        
        if(userCheck != 0) {
            throw new BusinessException("ERR0000012");
        }
        
        // 职务权限除普通员工，济南事业部经理，天津事业部部长唯一check
        if(!userListExEntity.getPostCode().equals(Constants.CD_POST_1) && !userListExEntity.getPostCode().equals(Constants.CD_POST_TJ1)
                && !userListExEntity.getPostCode().equals(Constants.CD_POST_JN1)) {
            int post =this.userListExMapper.searchSpecialUser(userListExEntity.getPostCode());
            if (post != 0) {
                throw new BusinessException("ERR0000044",userListExEntity.getPostName());
            }
        }
        // 执行追加
        int result = this.userListExMapper.insertUser(userListExEntity);
        logger.info("insertUser#result:{}", result);
    }
    
    /**
     * 用户删除
     * 
     * @param userListExEntity
     */
    @Transactional(readOnly = false)
    public void deleteUser(UserListExEntity userListExEntity) throws Exception {

        logger.info("deleteUser#param:{}", userListExEntity);
        // DBCheck
        // 用户不存在check
        int userCheck = this.userListExMapper.userCheck(userListExEntity.getTjCode());
        
        if(userCheck == 0) {
            throw new BusinessException("ERR0000026");
        }
        // 是否在业务中check
        int confimAppUser = this.userListExMapper.searchConfimAppUser(userListExEntity.getTjCode());
        int approveUser=this.userListExMapper.searchApproveUser(userListExEntity.getTjCode());
        
        if(confimAppUser != 0 || approveUser != 0) {
            String errorUser = userListExEntity.getTjCode() + "/"+ userListExEntity.getUserName();
            throw new BusinessException("ERR0000024",errorUser);
        }
        //执行删除
        int result = this.userListExMapper.deleteUser(userListExEntity.getTjCode());
        logger.info("deleteUser#result:{}", result);
    }
}