/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.admin.fileManage.controller;

import java.io.InputStream;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import tci.sds.admin.fileManage.entity.FileAddExEntity;
import tci.sds.admin.fileManage.form.FileAddForm;
import tci.sds.admin.fileManage.service.FileAddService;
import tci.sds.common.entity.ConfigProperties;
import tci.sds.common.entity.FileTypeEnum;
import tci.sds.common.exception.BusinessException;
import tci.sds.common.unit.ExcelOperatingClass;
import tci.sds.common.util.Constants;
/**
 * 文件追加用Controller
 * 
 * @author 谭艳
 * @since 2020/11/09
 * 
 */
@Controller
@Component
public class FileAddController {
    private final Logger logger = LoggerFactory.getLogger(FileAddController.class);
    
    private static ConfigProperties configProperties;
    
    @Autowired
    private FileAddService fileAddService;
    
    @Autowired
    public void setConfig(ConfigProperties config) {

        FileAddController.configProperties = config;

    }
    /**
     * 文件追加画面
     * 
     * @param fileAddForm 文件追加用Form
     * @return fileAddForm 文件追加用Form
     */
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/api/fileAdd/FileAddComfirm")
    @ResponseBody
    public FileAddForm search(HttpServletRequest request,
            @ModelAttribute FileAddForm fileAddForm) throws Exception {

        logger.info("search#param:{}", fileAddForm);

        FileAddExEntity fileAddExEntity = this.copyFormToExEntity(fileAddForm);
        
        // 根据cd_code查询code表中的适用范围
        if (fileAddService.selectCountCode(fileAddExEntity) > 0) {
            fileAddExEntity.setScopeApplication(fileAddService.selectCodekjName(fileAddExEntity));
        }
        
        // 根据cd_code查询code表中的业务内容
        if (fileAddService.selectCountContent(fileAddExEntity) > 0) {
            fileAddExEntity.setCondition(fileAddService.selectCodeContent(fileAddExEntity));
        }
        
        // 判断上传文件是否为空
        if (fileAddForm.getFile() == null) {
            throw new BusinessException("ERR0000001","上传的文件");
        }
        
        // 判断文件后缀名
        // 非图片文件
        if (!fileAddForm.getFileAbstract().equals("03")) {
            if (fileAddForm.getFile().getOriginalFilename().lastIndexOf(".") < 0) {
                throw new BusinessException("ERR0000033");
            }
            String lastName = fileAddForm.getFile().getOriginalFilename().substring(
                    fileAddForm.getFile().getOriginalFilename().lastIndexOf(".") + 1);
            if (lastName.equals("xlsx") || lastName.equals("xls") || lastName.equals("docx") || lastName.equals("doc")
                || lastName.equals("XLSX") || lastName.equals("XLS") || lastName.equals("DOCX") || lastName.equals("DOC")) {
            } else {
                throw new BusinessException("ERR0000033");
            }
        }
        
        // 图片文件
        if (fileAddForm.getFileAbstract().equals("03")) {
            if (fileAddForm.getFile().getOriginalFilename().lastIndexOf(".") < 0) {
                throw new BusinessException("ERR0000034");
            }
            String lastName = fileAddForm.getFile().getOriginalFilename().substring(
                    fileAddForm.getFile().getOriginalFilename().lastIndexOf(".") + 1);
            if (lastName.equals("png") || lastName.equals("PNG")) {
            } else {
                throw new BusinessException("ERR0000034");
            }
        }
        
        // 去掉上传文件后缀名
        String uplodeFileName = fileAddForm.getFile().getOriginalFilename().substring(
                0, fileAddForm.getFile().getOriginalFilename().lastIndexOf("."));
        
        // 判断文件名是否是：文件编号 文件名称,文件名称和文件编号之间是半角空格
        if (uplodeFileName.indexOf(" ") < 0 || uplodeFileName.substring(0, uplodeFileName.indexOf(" ")).equals("")
                || uplodeFileName.substring(uplodeFileName.indexOf(" ") + 1).equals("")) {
            throw new BusinessException("ERR0000032");
        }
        
        // 截取空格之前的赋给文件编号
        fileAddExEntity.setFileId(uplodeFileName.substring(0, uplodeFileName.indexOf(" ")));
        
        // 判断上传文件的文件编号小于等于50位
        if (uplodeFileName.substring(0, uplodeFileName.indexOf(" ")).length() >50) {
            throw new BusinessException("ERR0000036");
        }
        
        // 截取空格之后的赋给文件名称
        fileAddExEntity.setFileName(uplodeFileName.substring(uplodeFileName.indexOf(" ") + 1));
        
        // 获取文件大小
        int size = (int) fileAddForm.getFile().getSize();
        // 文件大小
        fileAddExEntity.setFileSize(size);
        
       
        
        // 获取上传文件的文件名称
        String packageName = fileAddForm.getFile().getOriginalFilename();
        // 获取文件流
        InputStream in = fileAddForm.getFile().getInputStream();
        
        // 参照文件文件路径
        if (fileAddForm.getFileAbstract().equals("02")) {
            // 文件路径
            fileAddExEntity.setFilePath(configProperties.getSampleUrl()+"\\"+fileAddForm.getFile().getOriginalFilename());
        }

        // 图片文件文件路径
        if (fileAddForm.getFileAbstract().equals("03")) {
            fileAddExEntity.setFilePath(configProperties.getSignatureUrl()+"\\"+fileAddForm.getFile().getOriginalFilename());
            
        }
        
        // 当文件类别为模板文件时check
        if (fileAddForm.getFileAbstract().equals("01")) {
            
            // 单项目check
            // 判断确认人是否为空
            if (fileAddForm.getFileAddConfirmer() == null || fileAddForm.getFileAddConfirmer().equals("")) {
                throw new BusinessException("ERR0000001","确认人");
            }
            
            // 判断签名sheet是否为空
            if (fileAddForm.getReviewerSheet() == null || fileAddForm.getReviewerSheet().equals("")) {
                throw new BusinessException("ERR0000001","签名sheet");
            }
            
            // 判断签名sheet输入的是否为半角数字
            char[] sheet = fileAddForm.getReviewerSheet().toCharArray();
            for (int i = 0; i < sheet.length; i++) {
                String temp = String.valueOf(sheet[i]);
                // 判断是全角字符
                if (temp.matches("[^\\x00-\\xff]")) {
                    throw new BusinessException("ERR0000029","签名sheet");
                }
            }
            
            // 判断签名sheet输入的是否大于0
            if (Integer.valueOf(fileAddForm.getReviewerSheet()) <= 0) {
                throw new BusinessException("ERR0000029","签名sheet");
            }
            
            // 判断签名sheet是否小于两位
            if (fileAddForm.getReviewerSheet().length() < 0 || fileAddForm.getReviewerSheet().length() > 2) {
                throw new BusinessException("ERR0000030","签名sheet");
            }
            
            // 签名sheet
            fileAddExEntity.setReviewerSheet(String.valueOf(Integer.valueOf(fileAddForm.getReviewerSheet()) - 1));
            
            // 判断适用范围是否为空
            if (fileAddForm.getApplication() == null || fileAddForm.getApplication().equals("")) {
                throw new BusinessException("ERR0000001","适用范围");
            }
            
            // 判断业务内容是否为空
            if (fileAddForm.getContent() == null || fileAddForm.getContent().equals("")) {
                throw new BusinessException("ERR0000001","业务内容");
            }
            
            // 判断是否有审核人
            if (fileAddForm.getAddDatas() == null) {
                throw new BusinessException("ERR0000039");
            }
            for (int i = 0;i < fileAddForm.getAddDatas().size();i++) {
                // 判断审核人是否为空
                if (fileAddForm.getAddDatas().get(i).getReviewerValue() == null 
                        || fileAddForm.getAddDatas().get(i).getReviewerValue().equals("")
                        || fileAddForm.getAddDatas().get(i).getReviewerValue().equals("undefined")) {
                    throw new BusinessException("ERR0000001","第"+ String.valueOf(i+1) +"审核人");
                }
                
                // 判断签名位置是否为空
                if (fileAddForm.getAddDatas().get(i).getPosition() == null 
                        || fileAddForm.getAddDatas().get(i).getPosition().equals("")
                        || fileAddForm.getAddDatas().get(i).getPosition().equals("undefined")) {
                    throw new BusinessException("ERR0000002",String.valueOf(i+1),"签名位置");
                }
                
                //判断签名位置是否有逗号隔开
                if (fileAddForm.getAddDatas().get(i).getPosition().indexOf(",") < 0) {
                    throw new BusinessException("ERR0000028",String.valueOf(i+1),"签名位置");
                }
                
                // 判断签名位置是否为整数
                // 截取，之前的
                String x = fileAddForm.getAddDatas().get(i).getPosition().substring(
                        0, fileAddForm.getAddDatas().get(i).getPosition().indexOf(","));
                Pattern patternx = Pattern.compile("[0-9]+");   
                Matcher matcherx = patternx.matcher((CharSequence) x);
                boolean xresult = matcherx.matches();
                if (!xresult || Integer.valueOf(x) == 0) {
                    throw new BusinessException("ERR0000003",String.valueOf(i+1),"签名位置");
                }
                
                // 纵坐标
                // 截取，之后
                String y = fileAddForm.getAddDatas().get(i).getPosition().substring(
                        fileAddForm.getAddDatas().get(i).getPosition().indexOf(",") + 1);
                Pattern patterny = Pattern.compile("[0-9]+");   
                Matcher matchery = patterny.matcher((CharSequence) y);
                boolean yresult = matchery.matches();
                if (!yresult || Integer.valueOf(y) == 0) {
                    throw new BusinessException("ERR0000003",String.valueOf(i+1),"签名位置");
                }
                
                // 判断逗号是不是半角
                char[] chars = fileAddForm.getAddDatas().get(i).getPosition().toCharArray();
                for (int z = 0; z < chars.length; z++) {
                    String temp = String.valueOf(chars[z]);
                    // 判断是全角字符
                    if (temp.matches("[^\\x00-\\xff]")) {
                        throw new BusinessException("ERR0000028",String.valueOf(i+1),"签名位置");
                    }
                }
                // 判断横纵位数不超过四位
                if (x.length() < 0 || x.length() > 4) {
                    throw new BusinessException("ERR0000004",String.valueOf(i+1),"签名位置");
                }
                if (y.length() <0 || y.length() > 4) {
                    throw new BusinessException("ERR0000004",String.valueOf(i+1),"签名位置");
                }
                
            }
            // 文件路径
            fileAddExEntity.setFilePath(configProperties.getExcelTemplateUrl()+"\\"+fileAddForm.getFile().getOriginalFilename());
            // list
            fileAddExEntity.setAddDatas(fileAddForm.getAddDatas());
        }
        
        //
        // 确认提交
        boolean insert = fileAddService.insertData(fileAddExEntity);
        // 提交失败的场合
        if (insert == false) {
            return null;
        }
        
        // 将上传的模板文件保存到本地 
        if (fileAddForm.getFileAbstract().equals("01")) { 
            ExcelOperatingClass.saveTempFile(in, FileTypeEnum.TEMPLATE, packageName,request);
        }
        // 将上传的 参照文件保存到本地 
        if (fileAddForm.getFileAbstract().equals("02")) { 
            ExcelOperatingClass.saveTempFile(in, FileTypeEnum.SAMPLE, packageName,request);
        }
        // 将上传的 图片文件保存到本地 
        if (fileAddForm.getFileAbstract().equals("03")) { 
             ExcelOperatingClass.saveTempFile(in, FileTypeEnum.SIGNATURE, packageName,request);
        }
        
        // 获取InfoMessage信息
        String infoMessage = fileAddService.getInfo("INF0000006");
        
        FileAddForm fileAddOutputForm = new FileAddForm();
        // errorCode
        fileAddOutputForm.setErrorCode("0");
        fileAddOutputForm.setInfoMessage(infoMessage);
        
        logger.info("search#result:{}","");
        return fileAddOutputForm;
    }
    
    /**
     * 文件追加画面确认人下拉列表
     * 
     * @param fileManageForm 
     * @return fileManageOutPutForm
     */
    @PostMapping("/api/fileAdd/searchConfirmerValue")
    @ResponseBody
    public FileAddForm searchConfirmerValue(@RequestBody @ModelAttribute FileAddForm fileAddForm) {

        logger.info("search#param:{}", fileAddForm);

        FileAddExEntity fileAddExEntity = this.copyFormToExEntity(fileAddForm);
        
        // 审核权限
        fileAddExEntity.setRoleCode(Constants.CD_AUDIT_AUTHORITY_QUE);
        // 确认人下拉列表
        List<FileAddExEntity> searchConfirmerValue = this.fileAddService.searchConfirmerValue(fileAddExEntity);
        
        // 职务权限
        fileAddExEntity.setCdKubun(Constants.CD_POST_AUTHORITY);
        // 审核人下拉列表
        List<FileAddExEntity> searchReviewerList = this.fileAddService.searchReviewerList(fileAddExEntity);
       
        FileAddForm fileAddOutPutForm = new FileAddForm();
        fileAddOutPutForm.setConfirmer(searchConfirmerValue);
        fileAddOutPutForm.setReviewer(searchReviewerList);

        logger.info("search#result:{}", searchConfirmerValue.size());
        return fileAddOutPutForm;
        

    }
    /**
     * Formの設定値からExEntityへ保存する
     * 
     * @param fileAddForm Formの設定値
     * @return fileAddExEntity
     */
    private FileAddExEntity copyFormToExEntity(FileAddForm fileAddForm) {

        FileAddExEntity fileAddExEntity = new FileAddExEntity();
        
        // 文件类别
        fileAddExEntity.setFileAbstract(fileAddForm.getFileAbstract());
        // 适用范围code名称
        fileAddExEntity.setScopeApplication(fileAddForm.getApplication());
        // 业务内容code名称
        fileAddExEntity.setCondition(fileAddForm.getContent());
        // 确认人
        fileAddExEntity.setFileConfigId(fileAddForm.getFileAddConfirmer());
        
        // 作成者
        fileAddExEntity.setCreateName(fileAddForm.getCreateName());
        // 作成时间
        Timestamp ts = new Timestamp(new Date().getTime());
        fileAddExEntity.setCreateTime(ts);
        // 更新者
        fileAddExEntity.setKoshinName(null);
        // 更新时间
        fileAddExEntity.setKoshinTimestamp(null);
        
        // 文件名称定数
        fileAddExEntity.setSearchFileNameKubun(Constants.CD_FILE_NAME);
        // 适用范围定数
        fileAddExEntity.setSearchScopeKubun(Constants.CD_BUSINESS_SCOPE);
        // 业务内容定数
        fileAddExEntity.setSearchContentKubun(Constants.CD_BUSINESS_CONTENT);
        
        return fileAddExEntity;
    }
   
}
