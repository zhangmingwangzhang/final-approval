/*
 * 审批管理システム（NTTD向け） COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.admin.fileManage.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import tci.sds.admin.fileManage.entity.FileManageExEntity;
import tci.sds.admin.fileManage.form.FileManageForm;
import tci.sds.admin.fileManage.service.FileManageService;
import tci.sds.common.util.Constants;

/**
 * 文件管理用Controller
 * 
 * @author 谭艳
 * @since 2020/11/09
 * 
 */
@Controller
public class FileManageController {
    private final Logger logger = LoggerFactory.getLogger(FileManageController.class);

    @Autowired
    private FileManageService fileManageService;
    
    /**
     * 文件管理画面初期和检索
     * 
     * @param fileManageForm 文件管理用Form
     * @return fileManageOutPutForm
     */
    @PostMapping("/api/fileManage/search")
    @ResponseBody
    public FileManageForm search(@RequestBody FileManageForm fileManageForm) {

        logger.info("search#param:{}", fileManageForm);

        // 拦截器
        HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = request.getSession();  
        session.setAttribute("userName", "");
        FileManageExEntity fileManageExEntity = this.copyFormToExEntity(fileManageForm);
        
        // 件数
        int total = this.fileManageService.searchTotal(fileManageExEntity);
        // 一览结果
        List<FileManageExEntity> records = this.fileManageService.searchList(fileManageExEntity);
        // 文件类别下拉列表
        List<FileManageExEntity> fileTypeList = this.fileManageService.searchFileTypeList(fileManageExEntity);
        

        FileManageForm fileManageOutPutForm = new FileManageForm();
        // 件数
        fileManageOutPutForm.setTotal(total);
        // 一览
        fileManageOutPutForm.setTableData(records);
        // 文件类别下拉列表
        fileManageOutPutForm.setFileType(fileTypeList);

        logger.info("search#result:{}", records.size());
        return fileManageOutPutForm;

    }
    /**
     * 文件管理画面删除
     * 
     * @param fileManageForm 
     * @return fileManageOutPutForm
     */
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/api/fileManage/delete")
    @ResponseBody
    public FileManageForm delete(@Valid @RequestBody FileManageForm fileManageForm) throws Exception {

        logger.info("delete#param:{}", fileManageForm);
        FileManageExEntity fileManageExEntity = this.copyFormToExEntity(fileManageForm);
        
        // 文件名称
        fileManageExEntity.setfName(fileManageForm.getfName());
        // 文件类别
        fileManageExEntity.setFileAbstractData(fileManageForm.getFileAbstractData());
        
        // 删除
        boolean deleteSuccess = this.fileManageService.delete(fileManageExEntity);
        // 删除失败的场合
        if (deleteSuccess == false) {
            return null;
        }
        // 获取InfoMessage信息
        String infoMessage = fileManageService.getInfo("INF0000005");
        
        FileManageForm fileManageOutPutForm = new FileManageForm();
        // errorCode
        fileManageOutPutForm.setErrorCode("0");
        fileManageOutPutForm.setInfoMessage(infoMessage);
        
        logger.info("delete#result:{}", "");
        
        return fileManageOutPutForm;

    }
    /**
     * Formの設定値からExEntityへ保存する
     * 
     * @param fileManageForm Formの設定値
     * @return Entity
     */
    private FileManageExEntity copyFormToExEntity(FileManageForm fileManageForm) {

        FileManageExEntity fileManageExEntity = new FileManageExEntity();
        // 文件名
        fileManageExEntity.setFileName(fileManageForm.getFileName());
        // 文件类别
        fileManageExEntity.setFileValue(fileManageForm.getFileValue());
        // codeList文件类别
        fileManageExEntity.setCdKubun(Constants.CD_FILE_TYPE);
        // 当前页数
        fileManageExEntity.setCurrentPage(fileManageForm.getCurrentPage());
        // 每页显示条数
        fileManageExEntity.setPageSize(fileManageForm.getPageSize());
        // 排序字段
        fileManageExEntity.setSortColumn(fileManageForm.getSortColumn());
        // 排序顺序
        fileManageExEntity.setSortType(fileManageForm.getSortType());
        // 文件编号
        fileManageExEntity.setFileId(fileManageForm.getFileId());
        
        return fileManageExEntity;

    }
}
