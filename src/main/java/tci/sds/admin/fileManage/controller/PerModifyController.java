/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.admin.fileManage.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.SimpleFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import tci.sds.admin.fileManage.entity.PerModifyExEntity;
import tci.sds.admin.fileManage.entity.PermissionChangeDataEntity;
import tci.sds.admin.fileManage.entity.ShenherenExEntity;
import tci.sds.admin.fileManage.form.PerModifyForm;
import tci.sds.admin.fileManage.mapper.PerModifyExMapper;
import tci.sds.admin.fileManage.service.PerModifyService;
import tci.sds.approval.requestNewConfirm.Entity.RequestNewConfirmEntity;
import tci.sds.common.exception.BusinessException;
import tci.sds.common.service.InfoMessageService;
import tci.sds.common.service.SendEmailPasswordService;
import tci.sds.common.unit.EamilOperatingClass;
import tci.sds.common.util.Constants;


/**
 * 权限变更用Controller
 * 
 * @author 谭艳
 * @since 2020/11/09
 * 
 */
@Controller
public class PerModifyController {
    private final Logger logger = LoggerFactory.getLogger(PerModifyController.class);

    @Autowired
    private PerModifyService perModifyService;
    @Autowired
    private SendEmailPasswordService sendEmailPasswordService;
    @Autowired
    private PerModifyExMapper perModifyExMapper;
    @Autowired
    private InfoMessageService infoMessageService;
    /**
     * 权限变更适用范围下拉列表
     * 
     * @param perModifyForm 权限变更用Form
     * @return perModifyOutputForm
     */
    @PostMapping("/api/perModify/searchApplicationList")
    @ResponseBody
    public PerModifyForm searchApplicationList(@RequestBody PerModifyForm perModifyForm) {
        
        logger.info("searchList#param:{}", perModifyForm);

        PerModifyExEntity perModifyExEntity = this.copyFormToExEntity(perModifyForm);
        
        // 适用范围下拉列表
        List<PerModifyExEntity> searchApplication = this.perModifyService.searchApplication(perModifyExEntity);
        // 确认人下拉框回显值
        String searchConfigId = this.perModifyService.searchConfigId(perModifyExEntity);
        // 确认人文本框回显值
        String searchConfigName = this.perModifyService.searchConfigName(perModifyExEntity);
        // 确认人下拉列表
        List<PerModifyExEntity> searchConfigIdList = this.perModifyService.searchConfigIdList(perModifyExEntity);
        
        PerModifyForm perModifyOutputForm = new PerModifyForm();
        
        perModifyOutputForm.setPerModifyApplication(searchApplication);
        perModifyOutputForm.setConfirmer(searchConfigIdList);
        perModifyOutputForm.setConfirmationPerson(searchConfigName);
        perModifyOutputForm.setCdCode(searchConfigId);
        
        return perModifyOutputForm;
    }
    
    /**
     * 权限变更业务内容下拉列表
     * 
     * @param perModifyForm 权限变更用Form
     * @return perModifyOutputForm
     */
    @PostMapping("/api/perModify/searchConditionList")
    @ResponseBody
    public PerModifyForm searchConditionList(@RequestBody PerModifyForm perModifyForm) {
        
        logger.info("searchList#param:{}", perModifyForm);

        PerModifyExEntity perModifyExEntity = this.copyFormToExEntity(perModifyForm);
        // 业务内容下拉列表
        List<PerModifyExEntity> searchCondition = this.perModifyService.searchCondition(perModifyExEntity);
        
        PerModifyForm perModifyOutputForm = new PerModifyForm();
        perModifyOutputForm.setPerModifyContent(searchCondition);
        
        return perModifyOutputForm;
    }
    
    /**
     * 权限变更审核人回显及下拉列表和签名位置
     * 
     * @param perModifyForm 权限变更用Form
     * @return perModifyOutputForm
     */
    @PostMapping("/api/perModify/searchPerModifyData")
    @ResponseBody
    public PerModifyForm searchConfigId(@RequestBody PerModifyForm perModifyForm) {
        
        logger.info("searchList#param:{}", perModifyForm);

        PerModifyExEntity perModifyExEntity = this.copyFormToExEntity(perModifyForm);
        
        // 审核人，签名位置
        List<PerModifyExEntity> searchList = this.perModifyService.searchList(perModifyExEntity);
        // 审核人下拉列表
        List<PerModifyExEntity> searchReviewerList = this.perModifyService.searchReviewerList(perModifyExEntity);
        
        PerModifyForm perModifyOutputForm = new PerModifyForm();
        
        perModifyOutputForm.setPerModifyData(searchList);
        perModifyOutputForm.setReviewer(searchReviewerList);
        
        return perModifyOutputForm;
    }
    
    /**
     * 适用范围，业务内容删除
     * 
     * @param perModifyForm 权限变更用Form
     * @return perModifyOutputForm
     */
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/api/perModify/scopeConditionDel")
    @ResponseBody
    public PerModifyForm scopeConditionDel(@RequestBody PerModifyForm perModifyForm) {
        
        logger.info("deleteList#param:{}", perModifyForm);

        PerModifyExEntity perModifyExEntity = this.copyFormToExEntity(perModifyForm);
        
        // 执行删除
        this.perModifyService.scopeConditionDel(perModifyExEntity);
        
        // 适用范围下拉列表
        List<PerModifyExEntity> searchApplication = this.perModifyService.searchApplication(perModifyExEntity);
        // 确认人下拉框回显值
        String searchConfigId = this.perModifyService.searchConfigId(perModifyExEntity);
        // 确认人文本框回显值
        String searchConfigName = this.perModifyService.searchConfigName(perModifyExEntity);
        // 确认人下拉列表
        List<PerModifyExEntity> searchConfigIdList = this.perModifyService.searchConfigIdList(perModifyExEntity);
        
        // 获取InfoMessage信息
        String infoMessage = perModifyService.getInfo("INF0000005");
        
        PerModifyForm perModifyOutputForm = new PerModifyForm();
        // 适用范围
        perModifyOutputForm.setPerModifyApplication(searchApplication);
        // 确认人下拉框
        perModifyOutputForm.setCdCode(searchConfigId);
        // 确认人文本框回显值
        perModifyOutputForm.setConfirmationPerson(searchConfigName);
        // 确认人下拉列表
        perModifyOutputForm.setConfirmer(searchConfigIdList);
        // errorCode
        perModifyOutputForm.setErrorCode("0");
        // info
        perModifyOutputForm.setInfoMessage(infoMessage);
        
        return perModifyOutputForm;
    }
    /**
     * biao添加进值
     *
     * @param perModifyForm 权限变更用Form
     * @return perModifyOutputForm
     */
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/api/perModify/perModifyadd")
    @ResponseBody
    public PerModifyForm addbiao(@RequestBody PerModifyForm perModifyForm) throws Exception {
        System.out.println("perModifyForm=============="+perModifyForm);
        logger.info("searchList#param:{}", perModifyForm);
        PerModifyExEntity perModifyExEntity = this.copyFormToExEntity(perModifyForm);

        // 单项目check
        // 不变更确认人的场合
        if (perModifyForm.getUpdateConfirmer().equals("01")) {
            // 判断适用范围是否为空
            if (perModifyForm.getPerModifyApplicationValue() == null
                    || perModifyForm.getPerModifyApplicationValue().equals("")) {
                throw new BusinessException("ERR0000001","适用范围");
            }

            // 判断业务内容是否为空
            if (perModifyForm.getPerModifyContentValue() == null
                    || perModifyForm.getPerModifyContentValue().equals("")) {
                throw new BusinessException("ERR0000001","业务内容");
            }

            // 判断是否有审核人
            if (perModifyForm.getPerModifyDatas().size() == 0) {
                throw new BusinessException("ERR0000039");
            }
            Date date =new Date();
            SimpleDateFormat formatter=new  SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String format = formatter.format(date);
            // 更新
            perModifyExEntity.setApplicationdate(format);
            boolean updateSucccess =this.perModifyService.addbiaoMainFlie(perModifyExEntity);
            PermissionChangeDataEntity  pcdata=new PermissionChangeDataEntity();
            for (int i = 0; i < perModifyForm.getPerModifyDatas().size(); i++) {
                pcdata.setfName(perModifyForm.getfName());
                pcdata.setFileId(perModifyForm.getFileId());
                pcdata.setPerModifyContentValue(perModifyForm.getPerModifyContentValue());
                pcdata.setConfirmationPerson(perModifyForm.getConfirmationPerson());
                pcdata.setReviewerValue(perModifyForm.getPerModifyDatas().get(i).getReviewerValue());
                pcdata.setPosition(perModifyForm.getPerModifyDatas().get(i).getPosition());
                pcdata.setOrderreviewer(i+1);
                pcdata.setApplicationdate(format);
                pcdata.setPerModifyApplicationValue(perModifyForm.getPerModifyApplicationValue());
                pcdata.setFileAbstractData(perModifyForm.getFileAbstractData());
                pcdata.setKoshinName(perModifyForm.getKoshinName());
                pcdata.setCdCode(perModifyForm.getCdCode());
                pcdata.setLoginTjCode(perModifyForm.getLoginTjCode());
                pcdata.setUpdateConfirmer(perModifyForm.getUpdateConfirmer());

              this.perModifyService.permissionChangeData( pcdata);
            }

            // 更新失败的场合
            if(updateSucccess == false) {
                return null;
            }
        } else {
            // 判断确认人是否为空
            if (perModifyForm.getCdCode() == null || perModifyForm.getCdCode().equals("")) {
                throw new BusinessException("ERR0000001","确认人");
            }

            boolean updateSucccess =this.perModifyService.addbiaoMain(perModifyExEntity);
            // 更新失败的场合
            if(updateSucccess == false) {
                return null;
            }
        }
        // 文件名
        String fileName = perModifyForm.getfName();

        //邮箱密码
        String password = this.sendEmailPasswordService.selectPassword();
        String space ="\u3000\u3000";
        // 检索审核人的信息 并发送邮件
        ShenherenExEntity configInfo = perModifyExMapper.select1();
        if (configInfo==null) {
            throw new BusinessException("ERR0000009","用户信息表","审核人");
        }
        // 邮件内容
        String mailInfo = configInfo.getUsername()+"桑\r\n" + "工作辛苦了，济南审批管理系统。\r\n\r\n"+space+"名为"+"【"+fileName+"】"+"的权限变更数据请您审批。\r\n\r\n"+space+"内网URL：http://172.17.0.30:8888/index.html\r\n"+space+"外网URL：http://60.216.7.187:9999\r\n\r\n"+"以上，请多关照。\r\n"+"济南审批管理系统\r\n\r\n"+"该邮件为自动发送邮件，不需要回复。";
        EamilOperatingClass eamilOperatingClass = new EamilOperatingClass();
        boolean sendFlag = eamilOperatingClass.sendTest("您有申请文件需要审批", configInfo.getPersonMail(),mailInfo,password,this.sendEmailPasswordService.getKosinTime());
        if (!sendFlag) {
            // 获取InfoMessage信息
            String infoMessage = infoMessageService.getInfo("INF0000007");
            RequestNewConfirmEntity requestNewConfirmEntity=new RequestNewConfirmEntity();
            requestNewConfirmEntity.setInfoMessage(infoMessage);
        }
        // 更新成功
        // 获取InfoMessage信息
        String infoMessage = perModifyService.getInfo("INF0000004");

        PerModifyForm perModifyOutputForm = new PerModifyForm();
        // errorCode
        perModifyOutputForm.setErrorCode("0");
        perModifyOutputForm.setInfoMessage(infoMessage);

        return perModifyOutputForm;
    }

    /**
     * 权限变更 确认
     * 
     * @param perModifyForm 权限变更用Form
     * @return perModifyOutputForm
     */
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/api/perModify/perModifySubmit")
    @ResponseBody
    public PerModifyForm submit(@RequestBody PerModifyForm perModifyForm) throws Exception {
        logger.info("searchList#param:{}", perModifyForm);
        System.out.println("========================="+perModifyForm);
        PerModifyExEntity perModifyExEntity = this.copyFormToExEntity(perModifyForm);
        
        // 单项目check
        // 不变更确认人的场合
        if (perModifyForm.getUpdateConfirmer().equals("01")) {
            // 判断适用范围是否为空
            if (perModifyForm.getPerModifyApplicationValue() == null 
                    || perModifyForm.getPerModifyApplicationValue().equals("")) {
                throw new BusinessException("ERR0000001","适用范围");
            }
            
            // 判断业务内容是否为空
            if (perModifyForm.getPerModifyContentValue() == null 
                    || perModifyForm.getPerModifyContentValue().equals("")) {
                throw new BusinessException("ERR0000001","业务内容");
            }
            
            // 判断是否有审核人
            if (perModifyForm.getPerModifyDatas().size() == 0) {
                throw new BusinessException("ERR0000039");
            }
            
            for (int i = 0;i < perModifyForm.getPerModifyDatas().size();i++) {
                // 判断审核人是否为空
                if (perModifyForm.getPerModifyDatas().get(i).getReviewerValue() == null ||
                        perModifyForm.getPerModifyDatas().get(i).getReviewerValue().equals("")) {
                    throw new BusinessException("ERR0000001","第"+ String.valueOf(i+1) +"审核人");
                }
                
                // 判断签名位置是否为空
                if (perModifyForm.getPerModifyDatas().get(i).getPosition() == null || 
                        perModifyForm.getPerModifyDatas().get(i).getPosition().equals("")) {
                    throw new BusinessException("ERR0000002",String.valueOf(i+1),"签名位置");
                }
                
                // 判断签名位置是否有逗号分隔符
                if(perModifyForm.getPerModifyDatas().get(i).getPosition().indexOf(",") < 0) {
                    throw new BusinessException("ERR0000028",String.valueOf(i+1),"签名位置");
                }
                
                // 签名位置是否为整数,并且位数不超过四位
                String x = perModifyForm.getPerModifyDatas().get(i).getPosition().substring(
                        0, perModifyForm.getPerModifyDatas().get(i).getPosition().indexOf(","));
                Pattern patternx = Pattern.compile("[0-9]+");   
                Matcher matcherx = patternx.matcher((CharSequence) x);
                boolean xresult = matcherx.matches();
                if (!xresult || Integer.valueOf(x) == 0) {
                    throw new BusinessException("ERR0000003",String.valueOf(i+1),"签名位置");
                }
                
                String y = perModifyForm.getPerModifyDatas().get(i).getPosition().substring(
                        perModifyForm.getPerModifyDatas().get(i).getPosition().indexOf(",") + 1);
                Pattern patterny = Pattern.compile("[0-9]+");   
                Matcher matchery = patterny.matcher((CharSequence) y);
                boolean yresult = matchery.matches();
                if (!yresult || Integer.valueOf(y) == 0) {
                    throw new BusinessException("ERR0000003",String.valueOf(i+1),"签名位置");
                }
                
                // 判断逗号是不是半角
                char[] chars= perModifyForm.getPerModifyDatas().get(i).getPosition().toCharArray();
                for (int z = 0; z < chars.length; z++) {
                    String temp = String.valueOf(chars[z]);
                    // 判断是全角字符
                    if (temp.matches("[^\\x00-\\xff]")) {
                        throw new BusinessException("ERR0000028",String.valueOf(i+1),"签名位置");
                    }
                }
                
                // 判断签名位置是否小于等于4位
                if (x.length() < 0 || x.length() > 4) {
                    throw new BusinessException("ERR0000004",String.valueOf(i+1),"签名位置");
                }
                if (y.length() <0 || y.length() > 4) {
                    throw new BusinessException("ERR0000004",String.valueOf(i+1),"签名位置");
                }
                
                // 判断画面输入的审核人是否重复
                for (int j = 0;j < perModifyForm.getPerModifyDatas().size();j++) {
                    if (j > i) {
                        if (perModifyForm.getPerModifyDatas().get(i).getReviewerValue().equals(
                                perModifyForm.getPerModifyDatas().get(j).getReviewerValue())) {
                            throw new BusinessException("ERR0000020",String.valueOf(j+1));
                        }
                    }
                }
            }
            for (int i = 0;i < perModifyForm.getPerModifyDatas().size();i++) {
                // 审核人
                perModifyExEntity.setReviewerValue(perModifyForm.getPerModifyDatas().get(i).getReviewerValue());
                // 签名位置
                perModifyExEntity.setPosition(perModifyForm.getPerModifyDatas().get(i).getPosition());
                // 审核顺序
                perModifyExEntity.setReviewerOrder(String.valueOf(i+1));
                // 审核人总数
                perModifyExEntity.setReviewerTotal(String.valueOf(perModifyForm.getPerModifyDatas().size()));
                
                // 更新
                boolean updateSucccess = this.perModifyService.updateMainFlie(perModifyExEntity);
                
                // 删除多余审核人数据
                this.perModifyService.delete(perModifyExEntity);
                
                // 更新失败的场合
                if(updateSucccess == false) {
                    return null;
                }
            }
        } else {
            // 判断确认人是否为空
            if (perModifyForm.getCdCode() == null || perModifyForm.getCdCode().equals("")) {
                throw new BusinessException("ERR0000001","确认人");
            }
            boolean updateSucccess = this.perModifyService.updateMain(perModifyExEntity);
            // 更新失败的场合
            if(updateSucccess == false) {
                return null;
            }
        }
        
        // 更新成功
        // 获取InfoMessage信息
        String infoMessage = perModifyService.getInfo("INF0000004");
        
        PerModifyForm perModifyOutputForm = new PerModifyForm();
        // errorCode
        perModifyOutputForm.setErrorCode("0");
        perModifyOutputForm.setInfoMessage(infoMessage);
        
        return perModifyOutputForm;
    }
    /**
     * Formの設定値からExEntityへ保存する
     * 
//     * @param PerModifyForm Formの設定値
     * @return Entity
     */
   private PerModifyExEntity copyFormToExEntity(PerModifyForm perModifyForm) {

       PerModifyExEntity perModifyExEntity = new PerModifyExEntity();
       // 文件编号
       perModifyExEntity.setFileId(perModifyForm.getFileId());
       // 文件名称
       perModifyExEntity.setfName(perModifyForm.getfName());
       // 适用范围
       perModifyExEntity.setPerModifyApplicationValue(perModifyForm.getPerModifyApplicationValue());
       // 业务内容
       perModifyExEntity.setPerModifyContentValue(perModifyForm.getPerModifyContentValue());
       // 确认人
       perModifyExEntity.setConfirmerValue(perModifyForm.getCdCode());
       // 职务权限
       perModifyExEntity.setCdKubun(Constants.CD_POST_AUTHORITY);
       // 审核权限
       perModifyExEntity.setRoleCode(Constants.CD_AUDIT_AUTHORITY_QUE);
       // 更新者
       perModifyExEntity.setKoshinName(perModifyForm.getKoshinName());
       // 更新者code
       perModifyExEntity.setLoginTjCode(perModifyForm.getLoginTjCode());
       // 更新时间
       Timestamp koshinTime = new Timestamp(new Date().getTime());
       perModifyExEntity.setKoshinTime(koshinTime);
       //文件类别
       perModifyExEntity.setFileAbstractData(perModifyForm.getFileAbstractData());
       return perModifyExEntity;
       
   }
   
   
   /**
    * 文件追加适用范围下拉列表
    * 
    * @param perModifyForm 
    * @return perModifyOutputForm
    */
   @PostMapping("/api/fileAdd/searchApplicationValue")
   @ResponseBody
   public PerModifyForm searchApplicationValue(@RequestBody PerModifyForm perModifyForm) {

       logger.info("search#param:{}", perModifyForm);

       PerModifyExEntity perModifyExEntity = this.copyFormToExEntity(perModifyForm);
       
       // 适用范围code
       perModifyExEntity.setCdKubun(Constants.CD_BUSINESS_SCOPE);
       // 适用范围下拉列表
       List<PerModifyExEntity> searchConfirmerValue = this.perModifyService.searchApplicationValue(perModifyExEntity);
       
       PerModifyForm perModifyOutputForm = new PerModifyForm();
       
       perModifyOutputForm.setPerModifyApplication(searchConfirmerValue);

       logger.info("search#result:{}", searchConfirmerValue.size());
       return perModifyOutputForm;
       
   }

   /**
    * 文件追加业务内容下拉列表
    * 
    * @param perModifyForm 
    * @return perModifyOutputForm
    */
   @PostMapping("/api/fileAdd/searchContentValue")
   @ResponseBody
   public PerModifyForm searchContentValue(@RequestBody PerModifyForm perModifyForm) {

       logger.info("search#param:{}", perModifyForm);

       PerModifyExEntity perModifyExEntity = this.copyFormToExEntity(perModifyForm);
       
       // 业务内容code
       perModifyExEntity.setCdKubun(Constants.CD_BUSINESS_CONTENT);
       // 业务内容下拉列表
       List<PerModifyExEntity> searchConfirmerValue = this.perModifyService.searchContentValue(perModifyExEntity);
       
       PerModifyForm perModifyOutputForm = new PerModifyForm();
       
       perModifyOutputForm.setPerModifyContent(searchConfirmerValue);

       logger.info("search#result:{}", searchConfirmerValue.size());
       return perModifyOutputForm;
       
   }
}
