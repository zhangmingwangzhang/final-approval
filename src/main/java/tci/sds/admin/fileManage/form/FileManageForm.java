/*
 * 审批管理システム（NTTD向け） COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.admin.fileManage.form;

import java.util.List;

import tci.sds.admin.fileManage.entity.FileManageExEntity;
import tci.sds.common.exception.ErrorType;

/**
 * 文件管理画面用Form
 * 
 * @author 谭艳
 * @since 2020/11/09
 * 
 */
public class FileManageForm {

    /** 文件名（文件编号+文件名称） */
    private String fileName;
    
    /** 文件类别 */
    private String fileValue;
    
    /** 检索状态 */
    private String  selectedStatus;

    /** 当前页数 */
    private int currentPage;

    /** 1页表示的件数 */
    private int pageSize;
    
    /** 总件数 */
    private int total;
    
    /** 排序字段 */
    private String sortColumn;

    /** 排序顺序 */
    private String sortType;
    
    /** 文件管理画面一览 */
    private List<FileManageExEntity> tableData;
    
    /** 文件类别下拉列表 */
    private List<FileManageExEntity> fileType;
    
    /** 删除用文件编号 */
    private String fileId;
    
    /** 删除用文件名称 */
    private String fName;
    
    /** 删除用文件类别 */
    private String fileAbstractData;
    
    /** エラーコード */
    private String errorCode = ErrorType.NotError.toString();

    /** Info文字 */
    private String infoMessage;

    public String getfName() {
    
        return fName;
    
    }

    public void setfName(String fName) {
    
        this.fName = fName;
    
    }

    public String getInfoMessage() {
    
        return infoMessage;
    
    }

    public void setInfoMessage(String infoMessage) {
    
        this.infoMessage = infoMessage;
    
    }

    public String getSortType() {
    
        return sortType;
    
    }

    public void setSortType(String sortType) {
    
        this.sortType = sortType;
    
    }

    public String getFileName() {
    
        return fileName;
    
    }

    public void setFileName(String fileName) {
    
        this.fileName = fileName;
    
    }

    public String getFileValue() {
    
        return fileValue;
    
    }

    public void setFileValue(String fileValue) {
    
        this.fileValue = fileValue;
    
    }

    public String getSelectedStatus() {
    
        return selectedStatus;
    
    }

    public void setSelectedStatus(String selectedStatus) {
    
        this.selectedStatus = selectedStatus;
    
    }

    public int getCurrentPage() {
    
        return currentPage;
    
    }

    public void setCurrentPage(int currentPage) {
    
        this.currentPage = currentPage;
    
    }

    public int getPageSize() {
    
        return pageSize;
    
    }

    public void setPageSize(int pageSize) {
    
        this.pageSize = pageSize;
    
    }

    public String getSortColumn() {
    
        return sortColumn;
    
    }

    public void setSortColumn(String sortColumn) {
    
        this.sortColumn = sortColumn;
    
    }

    public int getTotal() {
    
        return total;
    
    }

    public void setTotal(int total) {
    
        this.total = total;
    
    }

    public List<FileManageExEntity> getTableData() {
    
        return tableData;
    
    }

    public void setTableData(List<FileManageExEntity> tableData) {
    
        this.tableData = tableData;
    
    }

    public List<FileManageExEntity> getFileType() {
    
        return fileType;
    
    }

    public void setFileType(List<FileManageExEntity> fileType) {
    
        this.fileType = fileType;
    
    }

    public String getFileId() {
    
        return fileId;
    
    }

    public void setFileId(String fileId) {
    
        this.fileId = fileId;
    
    }

    public String getErrorCode() {
    
        return errorCode;
    
    }

    public void setErrorCode(String errorCode) {
    
        this.errorCode = errorCode;
    
    }

    public String getFileAbstractData() {

        return fileAbstractData;

    }

    public void setFileAbstractData(String fileAbstractData) {

        this.fileAbstractData = fileAbstractData;

    }

    @Override
    public String toString() {

        return "FileManageForm [fileName=" + fileName + ", fileValue="
                + fileValue + ", selectedStatus=" + selectedStatus
                + ", currentPage=" + currentPage + ", pageSize=" + pageSize
                + ", total=" + total + ", sortColumn=" + sortColumn
                + ", sortType=" + sortType + ", tableData=" + tableData
                + ", fileType=" + fileType + ", fileId=" + fileId + ", fName="
                + fName + ", fileAbstractData=" + fileAbstractData
                + ", errorCode=" + errorCode + ", infoMessage=" + infoMessage
                + "]";

    }

}
