package tci.sds.admin.fileManage.form;

public class AddForm {

    /** 审核人 */
    private String reviewerValue;
    
    /** 签名位置 */
    private String position;

    public String getReviewerValue() {
    
        return reviewerValue;
    
    }

    public void setReviewerValue(String reviewerValue) {
    
        this.reviewerValue = reviewerValue;
    
    }

    public String getPosition() {
    
        return position;
    
    }

    public void setPosition(String position) {
    
        this.position = position;
    
    }
    
}
