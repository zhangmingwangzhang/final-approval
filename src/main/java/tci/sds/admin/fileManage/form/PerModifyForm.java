/*
 * 审批管理システム（NTTD向け） COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.admin.fileManage.form;

import java.util.List;

import tci.sds.admin.fileManage.entity.PerModifyExEntity;
import tci.sds.common.exception.ErrorType;

/**
 * 权限变更画面用Form
 * 
 * @author 谭艳
 * @since 2020/11/09
 * 
 */
public class PerModifyForm {

    /** 文件编号 */
    private String fileId;

    /** 文件名称 */
    private String fName;
    
    /** 选择下拉列表的值 */
    private String cdCode;
    
    /** 是否更新确认人 */
    private String updateConfirmer;
    
    /** 选择下拉列表的值 */
    private String kjName;
    
    /** 适用范围下拉列表value值 */
    private String perModifyApplicationValue;
    
    /** 业务内容下拉列表value值 */
    private String perModifyContentValue;
    
    /** 确认人下拉列表value值 */
    private String confirmerValue;
    
    /** 确认人文本框值 */
    private String confirmationPerson;
    
    /** 确认人文本框值 */
    private String confirmationPersonCode;
    
    /** 审核人下拉列表value值 */
    private String reviewerValue;
    
    /** 变更用文件类别 */
    private String fileAbstractData;
    
    /** 签名位置 */
    private String position;
    
    /** 适用范围下拉列表 */
    private List<PerModifyExEntity> perModifyApplication;
    
    /** 业务内容下拉列表 */
    private List<PerModifyExEntity> perModifyContent;
    
    /** 确认人下拉列表 */
    private List<PerModifyExEntity> confirmer;

    /** 检索传到js的list */
    private List<PerModifyExEntity> perModifyData;
    
    /** js传到form的list */
    private List<PerModifyForm> perModifyDatas;
    
    /** 审核人下拉列表 */
    private List<PerModifyExEntity> reviewer;
    
    /** 更新者 */
    private String koshinName;

    /** 更新者 code*/
    private String loginTjCode;
    
    /** errorCode */
    private String errorCode = ErrorType.NotError.toString();
    
    /** Info文字 */
    private String infoMessage;

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getCdCode() {
        return cdCode;
    }

    public void setCdCode(String cdCode) {
        this.cdCode = cdCode;
    }

    public String getUpdateConfirmer() {
        return updateConfirmer;
    }

    public void setUpdateConfirmer(String updateConfirmer) {
        this.updateConfirmer = updateConfirmer;
    }

    public String getKjName() {
        return kjName;
    }

    public void setKjName(String kjName) {
        this.kjName = kjName;
    }

    public String getPerModifyApplicationValue() {
        return perModifyApplicationValue;
    }

    public void setPerModifyApplicationValue(String perModifyApplicationValue) {
        this.perModifyApplicationValue = perModifyApplicationValue;
    }

    public String getPerModifyContentValue() {
        return perModifyContentValue;
    }

    public void setPerModifyContentValue(String perModifyContentValue) {
        this.perModifyContentValue = perModifyContentValue;
    }

    public String getConfirmerValue() {
        return confirmerValue;
    }

    public void setConfirmerValue(String confirmerValue) {
        this.confirmerValue = confirmerValue;
    }

    public String getConfirmationPerson() {
        return confirmationPerson;
    }

    public void setConfirmationPerson(String confirmationPerson) {
        this.confirmationPerson = confirmationPerson;
    }

    public String getConfirmationPersonCode() {
        return confirmationPersonCode;
    }

    public void setConfirmationPersonCode(String confirmationPersonCode) {
        this.confirmationPersonCode = confirmationPersonCode;
    }

    public String getReviewerValue() {
        return reviewerValue;
    }

    public void setReviewerValue(String reviewerValue) {
        this.reviewerValue = reviewerValue;
    }

    public String getFileAbstractData() {
        return fileAbstractData;
    }

    public void setFileAbstractData(String fileAbstractData) {
        this.fileAbstractData = fileAbstractData;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public List<PerModifyExEntity> getPerModifyApplication() {
        return perModifyApplication;
    }

    public void setPerModifyApplication(List<PerModifyExEntity> perModifyApplication) {
        this.perModifyApplication = perModifyApplication;
    }

    public List<PerModifyExEntity> getPerModifyContent() {
        return perModifyContent;
    }

    public void setPerModifyContent(List<PerModifyExEntity> perModifyContent) {
        this.perModifyContent = perModifyContent;
    }

    public List<PerModifyExEntity> getConfirmer() {
        return confirmer;
    }

    public void setConfirmer(List<PerModifyExEntity> confirmer) {
        this.confirmer = confirmer;
    }

    public List<PerModifyExEntity> getPerModifyData() {
        return perModifyData;
    }

    public void setPerModifyData(List<PerModifyExEntity> perModifyData) {
        this.perModifyData = perModifyData;
    }

    public List<PerModifyForm> getPerModifyDatas() {
        return perModifyDatas;
    }

    public void setPerModifyDatas(List<PerModifyForm> perModifyDatas) {
        this.perModifyDatas = perModifyDatas;
    }

    public List<PerModifyExEntity> getReviewer() {
        return reviewer;
    }

    public void setReviewer(List<PerModifyExEntity> reviewer) {
        this.reviewer = reviewer;
    }

    public String getKoshinName() {
        return koshinName;
    }

    public void setKoshinName(String koshinName) {
        this.koshinName = koshinName;
    }

    public String getLoginTjCode() {
        return loginTjCode;
    }

    public void setLoginTjCode(String loginTjCode) {
        this.loginTjCode = loginTjCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getInfoMessage() {
        return infoMessage;
    }

    public void setInfoMessage(String infoMessage) {
        this.infoMessage = infoMessage;
    }

    @Override
    public String toString() {
        return "PerModifyForm{" +
                "fileId='" + fileId + '\'' +
                ", fName='" + fName + '\'' +
                ", cdCode='" + cdCode + '\'' +
                ", updateConfirmer='" + updateConfirmer + '\'' +
                ", kjName='" + kjName + '\'' +
                ", perModifyApplicationValue='" + perModifyApplicationValue + '\'' +
                ", perModifyContentValue='" + perModifyContentValue + '\'' +
                ", confirmerValue='" + confirmerValue + '\'' +
                ", confirmationPerson='" + confirmationPerson + '\'' +
                ", confirmationPersonCode='" + confirmationPersonCode + '\'' +
                ", reviewerValue='" + reviewerValue + '\'' +
                ", fileAbstractData='" + fileAbstractData + '\'' +
                ", position='" + position + '\'' +
                ", perModifyApplication=" + perModifyApplication +
                ", perModifyContent=" + perModifyContent +
                ", confirmer=" + confirmer +
                ", perModifyData=" + perModifyData +
                ", perModifyDatas=" + perModifyDatas +
                ", reviewer=" + reviewer +
                ", koshinName='" + koshinName + '\'' +
                ", loginTjCode='" + loginTjCode + '\'' +
                ", errorCode='" + errorCode + '\'' +
                ", infoMessage='" + infoMessage + '\'' +
                '}';
    }
}
