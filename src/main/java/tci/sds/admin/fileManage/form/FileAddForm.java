/*
 * 审批管理システム（NTTD向け） COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.admin.fileManage.form;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import tci.sds.admin.fileManage.entity.FileAddExEntity;
import tci.sds.common.exception.ErrorType;
import tci.sds.common.form.BaseForm;

/**
 * 文件追加用Form
 * 
 * @author 谭艳
 * @since 2020/11/09
 * 
 */
public class FileAddForm extends BaseForm {

    /** 文件编号 */
    private String fileId;

    /** 文件名称 */
    private String fileName;

    /** 文件路径 */
    private String filepath;

    /** 文件大小 */
    private Integer filesize;

    /** 文件类别 */
    private String fileAbstract;

    /** 适用范围 */
    private String application;

    /** 业务内容 */
    private String content;

    /** 确认人 */
    private String fileAddConfirmer;

    /** 签名sheet */
    private String reviewerSheet;

    /** 作成者 */
    private String createName;

    /** 确认人下拉列表 */
    private List<FileAddExEntity> confirmer;

    /** 审核人下拉列表 */
    private List<FileAddExEntity> reviewer;

    /** 上传文件 */
    private MultipartFile file;

    /** js传到Form的list */
    private List<AddForm> addDatas;

    /** errorCode */
    private String errorCode = ErrorType.NotError.toString();

    /** Info文字 */
    private String infoMessage;

    public MultipartFile getFile() {

        return file;

    }

    public void setFile(MultipartFile file) {

        this.file = file;

    }

    public String getInfoMessage() {

        return infoMessage;

    }

    public void setInfoMessage(String infoMessage) {

        this.infoMessage = infoMessage;

    }

    public String getReviewerSheet() {

        return reviewerSheet;

    }

    public void setReviewerSheet(String reviewerSheet) {

        this.reviewerSheet = reviewerSheet;

    }

    public String getCreateName() {

        return createName;

    }

    public void setCreateName(String createName) {

        this.createName = createName;

    }

    public String getFileId() {

        return fileId;

    }

    public void setFileId(String fileId) {

        this.fileId = fileId;

    }

    public String getFileName() {

        return fileName;

    }

    public void setFileName(String fileName) {

        this.fileName = fileName;

    }

    public String getFilepath() {

        return filepath;

    }

    public void setFilepath(String filepath) {

        this.filepath = filepath;

    }

    public Integer getFilesize() {

        return filesize;

    }

    public void setFilesize(Integer filesize) {

        this.filesize = filesize;

    }

    public String getFileAbstract() {

        return fileAbstract;

    }

    public void setFileAbstract(String fileAbstract) {

        this.fileAbstract = fileAbstract;

    }

    public String getApplication() {

        return application;

    }

    public void setApplication(String application) {

        this.application = application;

    }

    public String getContent() {

        return content;

    }

    public void setContent(String content) {

        this.content = content;

    }

    public String getFileAddConfirmer() {

        return fileAddConfirmer;

    }

    public void setFileAddConfirmer(String fileAddConfirmer) {

        this.fileAddConfirmer = fileAddConfirmer;

    }

    public List<FileAddExEntity> getConfirmer() {

        return confirmer;

    }

    public void setConfirmer(List<FileAddExEntity> confirmer) {

        this.confirmer = confirmer;

    }

    public List<FileAddExEntity> getReviewer() {

        return reviewer;

    }

    public void setReviewer(List<FileAddExEntity> reviewer) {

        this.reviewer = reviewer;

    }

    public List<AddForm> getAddDatas() {

        return addDatas;

    }

    public void setAddDatas(List<AddForm> addDatas) {

        this.addDatas = addDatas;

    }

    public String getErrorCode() {

        return errorCode;

    }

    public void setErrorCode(String errorCode) {

        this.errorCode = errorCode;

    }

    @Override
    public String toString() {

        return "FileAddForm [fileId=" + fileId + ", fileName=" + fileName
                + ", filepath=" + filepath + ", filesize=" + filesize
                + ", fileAbstract=" + fileAbstract + ", application="
                + application + ", content=" + content + ", fileAddConfirmer="
                + fileAddConfirmer + ", reviewerSheet=" + reviewerSheet
                + ", createName=" + createName + ", confirmer=" + confirmer
                + ", reviewer=" + reviewer + ", file=" + file + ", addDatas="
                + addDatas + ", errorCode=" + errorCode + ", infoMessage="
                + infoMessage + "]";

    }

}
