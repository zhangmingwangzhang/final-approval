/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.admin.fileManage.service;

import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tci.sds.admin.fileManage.entity.FileAddExEntity;
import tci.sds.admin.fileManage.mapper.FileAddExMapper;
import tci.sds.common.exception.BusinessException;
import tci.sds.common.util.Constants;

/**
 * 文件追加用Service
 * 
 * @author 谭艳
 * @since 2020/11/13
 * 
 */
@Service
public class FileAddService {
    private final Logger logger = LoggerFactory.getLogger(FileAddService.class);

    @Autowired
    private FileAddExMapper fileAddExMapper;
    
    @Autowired
    private MessageSource messageSource;
    
    /**
     * 文件追加画面
     * 
     * @param fileAddExEntity 
     * @return insertSuccess
     */
    @Transactional(readOnly = false)
    public boolean insertData(FileAddExEntity fileAddExEntity) {

        boolean insertSuccess = true;
        logger.info("searchByFileIdCount#param:{}", fileAddExEntity);
        
        // 根据文件名称在code表查询有无文件名称code值 
        String searchByCodeCount = this.fileAddExMapper.searchByCodeCount(fileAddExEntity);
        // 文件名称code
        fileAddExEntity.setFnName(searchByCodeCount);
        // 根据FILE_NAME在code表检索有无数据
        int fileNameCount = this.fileAddExMapper.fileNameCount(fileAddExEntity);
        // 查询最大文件名称
        List<FileAddExEntity> searchFileName = this.fileAddExMapper.searchFileName(fileAddExEntity);
        
        // 模板文件的场合
        if (fileAddExEntity.getFileAbstract().equals("01")) {
            // 根据画面的适用范围在code表里查出适用范围对应的code值
            String searchScopeApplication = this.fileAddExMapper.searchScopeApplication(fileAddExEntity);
            // 根据画面的业务内容在code表里查出业务内容对应的code值
            String searchCondition = this.fileAddExMapper.searchCondition(fileAddExEntity);
           
            // 适用范围
            fileAddExEntity.setScopeCode(searchScopeApplication);
            // 业务内容
            fileAddExEntity.setCdtCode(searchCondition);
            // 根据文件名称code，适用范围和业务内容检索主表有没有数据
            int searchByMainCount = this.fileAddExMapper.searchByMainCount(fileAddExEntity);
            // 根据文件名称code检索主表和空表有没有数据（模板文件的场合）
            int selectMainAndEmpty = this.fileAddExMapper.selectMainAndEmpty(fileAddExEntity);
            
            // 根据BUSINESS_SCOPE在code表检索有无数据
            int businessScopeCount = this.fileAddExMapper.businessScopeCount(fileAddExEntity);
            // 查询最大适用范围
            List<FileAddExEntity> searchBusinessScope = this.fileAddExMapper.searchBusinessScope(fileAddExEntity);
            // 根据BUSINESS_CONTENT在code表检索有无数据
            int businessContentCount = this.fileAddExMapper.businessContentCount(fileAddExEntity);
            // 查询最大业务内容
            List<FileAddExEntity> searchBusinessContent= this.fileAddExMapper.searchBusinessContent(fileAddExEntity);
            
            // 判断code是否有BUSINESS_SCOPE数据，没有的场合，从JN_SCOPE_1开始插入code
            if (businessScopeCount == 0) {
                String scopeApplicationCode = "JN_SCOPE_0";
                // 适用范围code值
                fileAddExEntity.setScopeApplicationCode(scopeApplicationCode);
                // 适用范围code区分名称
                fileAddExEntity.setScopeKubunName(Constants.CD_BUSINESSSCOPE);
                // 适用范围code区分值
                fileAddExEntity.setScopeKubun(Constants.CD_BUSINESS_SCOPE);
                // 适用范围插入code
                int insertCode = fileAddExMapper.insertScopeApplicationByCode(fileAddExEntity);
                if (insertCode == 0) {
                    insertSuccess = false;
                    return insertSuccess;
                }
            } else {
                // 有数据的场合，查出最大适用范围code，末尾数字+1
                // 适用范围
                if (searchScopeApplication == null) {
                    // 截取最大适用范围code末尾数字
                    String a = searchBusinessScope.get(0).getScopeApplicationCode().replaceAll(".*[^\\d](?=(\\d+))","");
                    // 最大适用范围code+1赋值
                    String scopeApplicationCode = "";
                    scopeApplicationCode = "JN_SCOPE_" + String.valueOf(Integer.valueOf(a) + 1);
                    // 适用范围code值
                    fileAddExEntity.setScopeApplicationCode(scopeApplicationCode);
                    // 适用范围code区分名称
                    fileAddExEntity.setScopeKubunName(searchBusinessScope.get(0).getScopeKubunName());
                    // 适用范围code区分值
                    fileAddExEntity.setScopeKubun(searchBusinessScope.get(0).getScopeKubun());
                    // 适用范围插入code
                    int insertCode = fileAddExMapper.insertScopeApplicationByCode(fileAddExEntity);
                    if (insertCode == 0) {
                        insertSuccess = false;
                        return insertSuccess;
                    }
                } else {
                    fileAddExEntity.setScopeApplicationCode(searchScopeApplication);
                }
            }
            
            // 判断code是否有BUSINESS_CONTENT数据，没有的场合，从JN_BUSIN_C0N_1开始插入code
            if (businessContentCount == 0) {
                String conditionCode = "JN_BUSIN_C0N_0";
                // 业务内容code值
                fileAddExEntity.setConditionCode(conditionCode);
                // 业务内容code区分名称
                fileAddExEntity.setConditionKubunName(Constants.CD_BUSINESSCONTENT);
                // 业务内容code区分值
                fileAddExEntity.setConditionKubun(Constants.CD_BUSINESS_CONTENT);
                // 业务内容插入code表
                int insertCode = fileAddExMapper.insertConditionByCode(fileAddExEntity);
                if (insertCode == 0) {
                    insertSuccess = false;
                    return insertSuccess;
                }
            } else {
                // 有数据的场合，查出最大业务内容code，末尾数字+1
                // 业务内容
                if (searchCondition == null) {
                    // 截取业务内容最大code末尾数字
                    String b = searchBusinessContent.get(0).getConditionCode().replaceAll(".*[^\\d](?=(\\d+))","");
                    // 最大业务内容code+1赋值
                    String conditionCode = "";
                    conditionCode = "JN_BUSIN_C0N_" + String.valueOf(Integer.valueOf(b) + 1);
                    // 业务内容code值
                    fileAddExEntity.setConditionCode(conditionCode);
                    // 业务内容code区分名称
                    fileAddExEntity.setConditionKubunName(searchBusinessContent.get(0).getConditionKubunName());
                    // 业务内容code区分值
                    fileAddExEntity.setConditionKubun(searchBusinessContent.get(0).getConditionKubun());
                    // 业务内容插入code表
                    int insertCode = fileAddExMapper.insertConditionByCode(fileAddExEntity);
                    if (insertCode == 0) {
                        insertSuccess = false;
                        return insertSuccess;
                    }
                } else {
                    fileAddExEntity.setConditionCode(searchCondition);
                }
            }
            
            // 判断code表是否存在文件
            // code表不存在，直接插入采番的文件名称，适用范围，业务内容
            if (searchByCodeCount == null) {
                // 判断code是否有FILE_NAME数据，没有的场合，从JN_FILE_1开始插入code
                if (fileNameCount == 0) {
                    String fileName = "JN_FILE_0";
                    // 文件名称code值
                    fileAddExEntity.setFileNameCode(fileName);
                    // 文件名称code区分名称
                    fileAddExEntity.setFileNameKubunName(Constants.CD_FILENAME);
                    // 文件名称code区分值
                    fileAddExEntity.setFileNameKubun(Constants.CD_FILE_NAME);
                } else {
                    // 有数据的场合，查出最大文件code，末尾数字+1
                    // 截取最大文件名称code末尾数字
                    String file = searchFileName.get(0).getFileNameCode().replaceAll(".*[^\\d](?=(\\d+))","");
                    // 最大文件名称code+1赋值
                    String fileName = "";
                    fileName = "JN_FILE_" + String.valueOf(Integer.valueOf(file) + 1);
                    // 文件名称code值
                    fileAddExEntity.setFileNameCode(fileName);
                    // 文件名称code区分名称
                    fileAddExEntity.setFileNameKubunName(searchFileName.get(0).getFileNameKubunName());
                    // 文件名称code区分值
                    fileAddExEntity.setFileNameKubun(searchFileName.get(0).getFileNameKubun());
                }
                // 文件名称插入code
                int insertFileName = fileAddExMapper.insertFileNameByCode(fileAddExEntity);
                // 插入主表
                boolean insertMainSuccess = this.insertMailFile(fileAddExEntity);
                //插入空表
                int insertEmptyFile = fileAddExMapper.insertEmptyFile(fileAddExEntity);
                if (insertFileName == 0 || insertMainSuccess == false || insertEmptyFile == 0) {
                    insertSuccess = false;
                    return insertSuccess;
                }
                
            // code表存在,根据文件编号，文件名称和文件类别检索主表和空表有没有数据
            } else {
                // 无数据的场合，插入空表和主表
                if (selectMainAndEmpty == 0) {
                    // 文件名称code
                    fileAddExEntity.setFileNameCode(searchByCodeCount);
                    boolean insertMainSuccess = this.insertMailFile(fileAddExEntity);
                    //插入空表
                    int insertEmptyFile = fileAddExMapper.insertEmptyFile(fileAddExEntity);
                    if (insertMainSuccess == false || insertEmptyFile == 0) {
                        insertSuccess = false;
                        return insertSuccess;
                    }
                // 有数据的场合，只插入主表
                } else {
                    // 文件名称code
                    fileAddExEntity.setFileNameCode(searchByCodeCount);
                    int searchCount = this.fileAddExMapper.searchCount(fileAddExEntity);
                    
                    // 同一文件不可有多个确认人（根据文件类别，文件名称和确认人查询主表）
                    if (searchCount == 0) {
                        throw new BusinessException("ERR0000043");
                    }
                    
                    // 同一适用范围对应的业务内容不等，可以插入数据
                    if (searchByMainCount != 0) {
                        throw new BusinessException("ERR0000019",fileAddExEntity.getFileId()+" "+fileAddExEntity.getFileName());
                    } 
                    boolean insertMainSuccess = this.insertMailFile(fileAddExEntity);
                    if (insertMainSuccess == false) {
                        insertSuccess = false;
                        return insertSuccess;
                    }
                }
                
            }
        }
        // 非模板文件的场合
        // 参照文件
        if (fileAddExEntity.getFileAbstract().equals("02")) {
            // 根据文件编号，文件名称code和文件类别检索空表有没有数据（非模板文件的场合）
            int selectEmpty = this.fileAddExMapper.selectEmpty(fileAddExEntity);
            // 判断code表是否存在文件
            // code表不存在，直接插入空表
            if (searchByCodeCount == null) {
                // 插入code表和空表
                insertSuccess = insertCodeAndEmpty (fileAddExEntity, fileNameCount, searchFileName);
            } else {
                if (selectEmpty != 0) {
                    throw new BusinessException("ERR0000015",fileAddExEntity.getFileId()+" "+fileAddExEntity.getFileName());
                } 
                // 文件名称code
                fileAddExEntity.setFileNameCode(searchByCodeCount);
                // 插入空表
                int insertEmpty = fileAddExMapper.insertEmptyFile(fileAddExEntity);
                if (insertEmpty == 0) {
                    insertSuccess = false;
                    return insertSuccess;
                }
            }
        }
        
        // 图片文件
        if (fileAddExEntity.getFileAbstract().equals("03")) {
            // 根据文件编号和文件类别检索空表有没有数据（图片文件的场合）
            int selectEmptyPng = this.fileAddExMapper.selectEmptyPng(fileAddExEntity);
            // 判断code表是否存在文件
            // code表不存在，直接插入空表
            if (searchByCodeCount == null) {
                // 插入code表和空表
                insertSuccess = insertCodeAndEmpty (fileAddExEntity, fileNameCount, searchFileName);
            } else {
                if (selectEmptyPng != 0) {
                    throw new BusinessException("ERR0000015",fileAddExEntity.getFileId()+" "+fileAddExEntity.getFileName());
                } 
                // 文件名称code
                fileAddExEntity.setFileNameCode(searchByCodeCount);
                // 插入空表
                int insertEmpty = fileAddExMapper.insertEmptyFile(fileAddExEntity);
                if (insertEmpty == 0) {
                    insertSuccess = false;
                    return insertSuccess;
                }
            }
        }
        logger.info("searchCondition#result:{}", searchByCodeCount);
        return insertSuccess;
    }
    
    /**
     * 文件追加画面非模板的场合插入code表和空表
     * 
     * @param fileAddExEntity 
     * @return insertMainSuccess
     */
    private boolean insertCodeAndEmpty (
            FileAddExEntity fileAddExEntity,int fileNameCount,List<FileAddExEntity> searchFileName) {
        boolean insertMainSuccess = true;

        // 判断code是否有FILE_NAME数据，没有的场合，从JN_FILE_0开始插入code
        if (fileNameCount == 0) {
            String fileName = "JN_FILE_0";
            // 文件名称code值
            fileAddExEntity.setFileNameCode(fileName);
            // 文件名称code区分名称
            fileAddExEntity.setFileNameKubunName(Constants.CD_FILENAME);
            // 文件名称code区分值
            fileAddExEntity.setFileNameKubun(Constants.CD_FILE_NAME);
        } else {
            // 有数据的场合，查出最大文件code，末尾数字+1
            // 截取最大文件名称code末尾数字
            String file = searchFileName.get(0).getFileNameCode().replaceAll(".*[^\\d](?=(\\d+))","");
            // 最大文件名称code+1赋值
            String fileName = "";
            fileName = "JN_FILE_" + String.valueOf(Integer.valueOf(file) + 1);
            // 文件名称code值
            fileAddExEntity.setFileNameCode(fileName);
            // 文件名称code区分名称
            fileAddExEntity.setFileNameKubunName(searchFileName.get(0).getFileNameKubunName());
            // 文件名称code区分值
            fileAddExEntity.setFileNameKubun(searchFileName.get(0).getFileNameKubun());
        }
        // 文件名称插入code表
        int insertCode = fileAddExMapper.insertFileNameByCode(fileAddExEntity);
        // 插入空表
        int insertEmpty = fileAddExMapper.insertEmptyFile(fileAddExEntity);
        if (insertCode == 0 || insertEmpty == 0) {
            insertMainSuccess = false;
            return insertMainSuccess;
        }
    
        return insertMainSuccess;
    }
    /**
     * 文件追加画面插入主表
     * 
     * @param fileAddExEntity 
     * @return insertMainSuccess
     */
    private boolean insertMailFile (FileAddExEntity fileAddExEntity) {
        boolean insertMainSuccess = true;
        for (int i = 0; i < fileAddExEntity.getAddDatas().size(); i++) {
            for (int j = 0; j < fileAddExEntity.getAddDatas().size(); j++) {
                // 不和自己进行比较
                if (j > i) {
                    // 进行比较
                    if (fileAddExEntity.getAddDatas().get(i).getReviewerValue().equals(
                            fileAddExEntity.getAddDatas().get(j).getReviewerValue())) {
                        throw new BusinessException("ERR0000020",String.valueOf(j+1)); 
                    }
                }
            }
            fileAddExEntity.setReviewer(fileAddExEntity.getAddDatas().get(i).getReviewerValue());
            fileAddExEntity.setReviewerPosition(fileAddExEntity.getAddDatas().get(i).getPosition());
            fileAddExEntity.setReviewerOrder(String.valueOf(i+1));
            // 插入主表
            int res = fileAddExMapper.insertMailFile(fileAddExEntity);
            if (res == 0) {
                insertMainSuccess = false;
                break;
            }
        }
        return insertMainSuccess;
    }
    /**
     * 确认人下拉列表
     * 
     * @param fileAddExEntity 
     * @return searchConfirmerValue
     */
    @Transactional(readOnly = true)
    public List<FileAddExEntity> searchConfirmerValue(FileAddExEntity fileAddExEntity) {

        logger.info("searchConfirmerValue#param:{}", fileAddExEntity);
        
        // 确认人下拉列表
        List<FileAddExEntity> searchConfirmerValue = this.fileAddExMapper.searchConfirmerValue(fileAddExEntity);
        
        logger.info("searchConfirmerValue#result:{}", searchConfirmerValue.size());
        
        return searchConfirmerValue;

    }
    
    /**
     * 审核人下拉列表
     * 
     * @param fileAddExEntity 
     * @return searchReviewerList
     */
    @Transactional(readOnly = true)
    public List<FileAddExEntity> searchReviewerList(FileAddExEntity fileAddExEntity) {

        logger.info("searchConfirmerValue#param:{}", fileAddExEntity);
        
        // 审核人下拉列表
        List<FileAddExEntity> searchReviewerList = this.fileAddExMapper.searchReviewerList(fileAddExEntity);
        
        logger.info("searchConfirmerValue#result:{}", searchReviewerList.size());
        
        return searchReviewerList;

    }
    
    /**
     * 根据cd_code查询code表中的适用范围个数
     * 
     * @param fileAddExEntity 
     * @return int
     */
    @Transactional(readOnly = true)
    public int selectCountCode(FileAddExEntity fileAddExEntity) {

        logger.info("selectCountCode#param:{}", fileAddExEntity);
        
        // 根据cd_code查询code表中kj_name
        int count = this.fileAddExMapper.selectCountCode(fileAddExEntity);
        
        logger.info("selectCountCode#result:{}", count);
        
        return count;

    }
    
    /**
     * 根据cd_code查询code表中适用范围
     * 
     * @param fileAddExEntity 
     * @return String
     */
    @Transactional(readOnly = true)
    public String selectCodekjName(FileAddExEntity fileAddExEntity) {

        logger.info("selectCountCode#param:{}", fileAddExEntity);
        
        // 根据cd_code查询code表中kj_name
        String kjName = this.fileAddExMapper.selectCodekjName(fileAddExEntity);
        
        logger.info("selectCountCode#result:{}", kjName);
        
        return kjName;

    }
    
    /**
     * 根据cd_code查询code表中的业务内容个数
     * 
     * @param fileAddExEntity 
     * @return int
     */
    @Transactional(readOnly = true)
    public int selectCountContent(FileAddExEntity fileAddExEntity) {

        logger.info("selectCountCode#param:{}", fileAddExEntity);
        
        // 根据cd_code查询code表中的业务内容个数
        int count = this.fileAddExMapper.selectCountContent(fileAddExEntity);
        
        logger.info("selectCountCode#result:{}", count);
        
        return count;

    }
    
    /**
     * 根据cd_code查询code表中的业务内容
     * 
     * @param fileAddExEntity 
     * @return String
     */
    @Transactional(readOnly = true)
    public String selectCodeContent(FileAddExEntity fileAddExEntity) {

        logger.info("selectCountCode#param:{}", fileAddExEntity);
        
        // 根据cd_code查询code表中的业务内容
        String kjName = this.fileAddExMapper.selectCodeContent(fileAddExEntity);
        
        logger.info("selectCountCode#result:{}", kjName);
        
        return kjName;

    }
    
    /**
     * Info信息获取
     * 
     * @param infoCode
     * @return message
     */
    public String getInfo(String infoCode) {

        logger.info("getInfo#param:{}", infoCode);
        
        String[] parStrings = new String[1];
        Locale locale = LocaleContextHolder.getLocale();
        String message = messageSource.getMessage(infoCode, parStrings, locale);
        
        logger.info("getInfo#result:{}", message);
        return message;
    }
}
