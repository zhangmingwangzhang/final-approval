/*
 * 审批管理システム（NTTD向け） COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.admin.fileManage.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tci.sds.admin.fileManage.entity.BiaoAddEntity;
import tci.sds.admin.fileManage.entity.PerModifyExEntity;
import tci.sds.admin.fileManage.entity.PermissionChangeDataEntity;
import tci.sds.admin.fileManage.entity.ShenherenExEntity;
import tci.sds.admin.fileManage.mapper.PerModifyExMapper;
import tci.sds.common.exception.BusinessException;


/**
 * 权限变更用Service
 * 
 * @author 谭艳
 * @since 2020/11/09
 * 
 */
@Service
public class PerModifyService {
    private final Logger logger = LoggerFactory.getLogger(PerModifyService.class);

    @Autowired
    private PerModifyExMapper perModifyExMapper;
    
    @Autowired
    private MessageSource messageSource;
    
    /**
     * 权限变更适用范围下拉列表
     * 
     * @param perModifyExEntity 
     * @return searchApplication
     */
    @Transactional(readOnly = true)
    public List<PerModifyExEntity> searchApplication(PerModifyExEntity perModifyExEntity) {

        logger.info("searchList#param:{}", perModifyExEntity);
        
        // 根据FileName在code表查出文件名称code
        String selectNameCode = this.perModifyExMapper.selectNameCode(perModifyExEntity);
        perModifyExEntity.setfNameCode(selectNameCode);
        // 适用范围下拉列表
        List<PerModifyExEntity> searchApplication = this.perModifyExMapper.searchApplication(perModifyExEntity);
        
        logger.info("searchList#result:{}", searchApplication.size());
        
        return searchApplication;
    }
    
    /**
     * 权限变更业务内容下拉列表
     * 
     * @param perModifyExEntity 
     * @return searchCondition
     */
    @Transactional(readOnly = true)
    public List<PerModifyExEntity> searchCondition(PerModifyExEntity perModifyExEntity) {

        logger.info("searchCondition#param:{}", perModifyExEntity);
        
        // 根据FileName在code表查出文件名称code
        String selectNameCode = this.perModifyExMapper.selectNameCode(perModifyExEntity);
        perModifyExEntity.setfNameCode(selectNameCode);
        // 业务内容下拉列表
        List<PerModifyExEntity> searchCondition = this.perModifyExMapper.searchCondition(perModifyExEntity);
        
        logger.info("searchCondition#result:{}", searchCondition.size());
        
        return searchCondition;
    }

    /**
     * 确认人下拉列表数据回显
     * 
     * @param perModifyExEntity 
     * @return searchConfigId
     */
    @Transactional(readOnly = true)
    public String searchConfigId(PerModifyExEntity perModifyExEntity) {

        logger.info("searchCondition#param:{}", perModifyExEntity);
        
        // 根据FileName在code表查出文件名称code
        String selectNameCode = this.perModifyExMapper.selectNameCode(perModifyExEntity);
        perModifyExEntity.setfNameCode(selectNameCode);
        // 确认人下拉列表数据回显
        String searchConfigId = this.perModifyExMapper.searchConfigId(perModifyExEntity);
               
        logger.info("searchCondition#result:{}", searchConfigId);
        
        return searchConfigId;
    }
    
    /**
     * 确认人文本框数据回显
     * 
     * @param perModifyExEntity
     * @return searchConfigId
     */
    @Transactional(readOnly = true)
    public String searchConfigName(PerModifyExEntity perModifyExEntity) {

        logger.info("searchCondition#param:{}", perModifyExEntity);
        
        // 根据FileName在code表查出文件名称code
        String selectNameCode = this.perModifyExMapper.selectNameCode(perModifyExEntity);
        perModifyExEntity.setfNameCode(selectNameCode);
        // 确认人文本框数据回显
        String searchConfigName = this.perModifyExMapper.searchConfigName(perModifyExEntity);
               
        logger.info("searchCondition#result:{}", searchConfigName);
        
        return searchConfigName;
    }
    
    /**
     * 确认人下拉列表
     * 
     * @param perModifyExEntity 
     * @return searchConfigIdList
     */
    @Transactional(readOnly = true)
    public List<PerModifyExEntity> searchConfigIdList(PerModifyExEntity perModifyExEntity) {

        logger.info("searchCondition#param:{}", perModifyExEntity);
        
        // 确认人下拉列表
        List<PerModifyExEntity> searchConfigIdList = this.perModifyExMapper.searchConfigIdList(perModifyExEntity);
        
        logger.info("searchCondition#result:{}", searchConfigIdList.size());
        
        return searchConfigIdList;
    }
    
    /**
     * 审核人下拉列表数据和签名位置回显
     * 
     * @param perModifyExEntity 
     * @return bodyInfoList
     */
    @Transactional(readOnly = true)
    public List<PerModifyExEntity> searchList(PerModifyExEntity perModifyExEntity) {

        logger.info("searchCondition#param:{}", perModifyExEntity);
        
        // 根据FileName在code表查出文件名称code
        String selectNameCode = this.perModifyExMapper.selectNameCode(perModifyExEntity);
        perModifyExEntity.setfNameCode(selectNameCode);
        // 检索审核人和签名位置
        List<PerModifyExEntity> searchList = this.perModifyExMapper.searchList(perModifyExEntity);
        
        List<PerModifyExEntity> bodyInfoList = new ArrayList<>();
        // 把审核人和签名位置放到对应的字段
        for (PerModifyExEntity perModify :searchList) {
            PerModifyExEntity perModifyOutputForm1 = new PerModifyExEntity();
            perModifyOutputForm1.setReviewerValue(perModify.getReviewerValue());
            perModifyOutputForm1.setReviewerjName(perModify.getReviewerjName());
            perModifyOutputForm1.setPosition(perModify.getPosition());
            bodyInfoList.add(perModifyOutputForm1);
        }
        
        logger.info("searchCondition#result:{}", searchList.size());
        
        return bodyInfoList;
    }
    
    /**
     * 审核人下拉列表
     * 
     * @param perModifyExEntity 
     * @return searchReviewerList
     */
    @Transactional(readOnly = true)
    public List<PerModifyExEntity> searchReviewerList(PerModifyExEntity perModifyExEntity) {

        logger.info("searchCondition#param:{}", perModifyExEntity);
        
        // 审核人下拉列表
        List<PerModifyExEntity> searchReviewerList = this.perModifyExMapper.searchReviewerList(perModifyExEntity);
        
        logger.info("searchCondition#result:{}", searchReviewerList.size());
        
        return searchReviewerList;
    }
    
    /**
     * 适用范围，业务内容删除
     * 
     * @param perModifyExEntity 
     * @return 
     */
    @Transactional(readOnly = true)
    public void scopeConditionDel(PerModifyExEntity perModifyExEntity) {

        logger.info("scopeConditionDel#param:{}", perModifyExEntity);
        
        // 根据文件类别在code检索出code值 
        String searchFileAbstractCode = this.perModifyExMapper.searchFileAbstractCode(perModifyExEntity);
        perModifyExEntity.setFileAbstractCode(searchFileAbstractCode);
        
        // 根据FileName在code表查出文件名称code
        String selectNameCode = this.perModifyExMapper.selectNameCode(perModifyExEntity);
        perModifyExEntity.setfNameCode(selectNameCode);
        
        // 检索审核状态
        List<PerModifyExEntity> selectStatus = this.perModifyExMapper.selectStatus(perModifyExEntity);
        
        // 审核状态不是终了的场合
        for (int j = 0;j < selectStatus.size();j++) {
            if (!selectStatus.get(j).getExamineStatus().equals("EXAMINE_OVER")) {
                throw new BusinessException("ERR0000017"); 
            }
        }
        
        // 执行删除
        int scopeDelete = this.perModifyExMapper.scopeConditionDel(perModifyExEntity);
        
        //当前删除适用范围，业务内容是否为文件唯一Check
        int unique = this.perModifyExMapper.scopeSelect(perModifyExEntity);
        
        //当前删除适用范围，业务内容为文件唯一时删除空表该文件
        if(unique == 0) {
            this.perModifyExMapper.scopeEmptyDel(perModifyExEntity);
        }
        
        logger.info("scopeConditionDel#result:{}", scopeDelete);
        
    }
    
    /**
     * 确认  更新主表（确认人不变更的场合）
     * 
     * @param perModifyExEntity
     */
    @Transactional(readOnly = false)
    public boolean updateMainFlie(PerModifyExEntity perModifyExEntity) {
        
        boolean updateSuccess = true;
        logger.info("searchByFileIdCount#param:{}", perModifyExEntity);
        
        // 根据文件类别在code检索出code值 
        String searchFileAbstractCode = this.perModifyExMapper.searchFileAbstractCode(perModifyExEntity);
        perModifyExEntity.setFileAbstractCode(searchFileAbstractCode);
        
        // 根据FileName在code表查出文件名称code
        String selectNameCode = this.perModifyExMapper.selectNameCode(perModifyExEntity);
        perModifyExEntity.setfNameCode(selectNameCode);
        // 检索审核状态
       // List<PerModifyExEntity> selectStatus = this.perModifyExMapper.selectStatus(perModifyExEntity);
        
        // 审核状态不是终了的场合
//        for (int j = 0;j < selectStatus.size();j++) {
//            if (!selectStatus.get(j).getExamineStatus().equals("EXAMINE_OVER")) {
//                throw new BusinessException("ERR0000016");
//            }
//        }
        
        
        // 更新空表
      //  int empty = this.perModifyExMapper.updateEmptyFlie(perModifyExEntity);
        
        // 当前审核顺序是否存在check
        int orderCount = this.perModifyExMapper.searchOrder(perModifyExEntity);
        if(orderCount == 0) {
            // 签名sheet
            String reviewerSheet = this.perModifyExMapper.searchSheet(perModifyExEntity);
            
            perModifyExEntity.setReviewerSheet(reviewerSheet);
            // 更新主表
            int res = this.perModifyExMapper.insertMailFile(perModifyExEntity);
            
            // 判断如果插入成功，则返回true || empty == 0
            if (res == 0 ) {
                updateSuccess = false;
            }
        } else {
            // 更新主表
            int res = this.perModifyExMapper.updateMainFlie(perModifyExEntity);
            
            // 判断如果插入成功，则返回true || empty == 0
            if (res == 0 ) {
                updateSuccess = false;
            }
        }
        
        return updateSuccess;
    }
    /**
     * 确认  更新biao（确认人不变更的场合）
     *
     * @param perModifyExEntity
     */
    @Transactional(readOnly = false)
    public boolean addbiaoMainFlie(PerModifyExEntity perModifyExEntity) {

        logger.info("searchByFileIdCount#param:{}", perModifyExEntity);
        boolean updateSuccess = true;
//        // 根据文件类别在code检索出code值
//        String searchFileAbstractCode = this.perModifyExMapper.searchFileAbstractCode(perModifyExEntity);
//        perModifyExEntity.setFileAbstractCode(searchFileAbstractCode);
//
//        // 根据FileName在code表查出文件名称code
//        String selectNameCode = this.perModifyExMapper.selectNameCode(perModifyExEntity);
//        perModifyExEntity.setfNameCode(selectNameCode);
//        // 检索审核状态
//        List<PerModifyExEntity> selectStatus = this.perModifyExMapper.selectStatus(perModifyExEntity);
//        // 审核状态不是终了的场合
//        for (int j = 0;j < selectStatus.size();j++) {
//            if (!selectStatus.get(j).getExamineStatus().equals("EXAMINE_OVER")) {
//                throw new BusinessException("ERR0000016");
//            }
//        }
        // 更新biao
        BiaoAddEntity biaoAddEntity=new BiaoAddEntity();
        biaoAddEntity.setCounts(0);
        biaoAddEntity.setApplicationdate(perModifyExEntity.getApplicationdate());
        biaoAddEntity.setFilename(perModifyExEntity.getFileId()+perModifyExEntity.getfName());
        biaoAddEntity.setKoshin_name(perModifyExEntity.getKoshinName());
        biaoAddEntity.setKoshincode(perModifyExEntity.getLoginTjCode());
        System.out.println(perModifyExEntity.getLoginTjCode());
        ShenherenExEntity select1 = perModifyExMapper.select1();
        biaoAddEntity.setOnecode(select1.getTjCode());
        biaoAddEntity.setOne(select1.getUsername()+"（待审核）");
        ShenherenExEntity select2 = perModifyExMapper.select2();
        biaoAddEntity.setTwocode(select2.getTjCode());
        biaoAddEntity.setTwo(select2.getUsername()+"（未审核）");
        ShenherenExEntity select3 = perModifyExMapper.select3();
        biaoAddEntity.setThreecode(select3.getTjCode());
        biaoAddEntity.setThree(select3.getUsername()+"（未审核）");
        ShenherenExEntity select4 = perModifyExMapper.select4();
        biaoAddEntity.setFourcode(select4.getTjCode());
        biaoAddEntity.setFour(select4.getUsername()+"（未审核）");
        System.out.println("biaoAddEntity1111"+biaoAddEntity);
        // 更新biao
        int res=perModifyExMapper.addbiao(biaoAddEntity);
        // 判断如果插入成功，则返回true
        if (res == 0 ) {
            updateSuccess = false;
        }

        return updateSuccess;

    }
    
    /**
     * 删除多余审核人
     * 
     * @param perModifyExEntity
     */
    @Transactional(readOnly = false)
    public void delete(PerModifyExEntity perModifyExEntity) {
        
        logger.info("delete#param:{}", perModifyExEntity);
        
        this.perModifyExMapper.deleteReviewer(perModifyExEntity);
        
    }
    
    /**
     * 确认  更新主表（只更新确认人的场合）
     * 
     * @param perModifyExEntity
     */
    @Transactional(readOnly = false)
    public boolean updateMain(PerModifyExEntity perModifyExEntity) {
        
        boolean updateSuccess = true;
        logger.info("searchByFileIdCount#param:{}", perModifyExEntity);
        
        // 根据文件类别在code检索出code值 
        String searchFileAbstractCode = this.perModifyExMapper.searchFileAbstractCode(perModifyExEntity);
        perModifyExEntity.setFileAbstractCode(searchFileAbstractCode);
        
        // 根据FileName在code表查出文件名称code
        String selectNameCode = this.perModifyExMapper.selectNameCode(perModifyExEntity);
        perModifyExEntity.setfNameCode(selectNameCode);
        
        // 关于确认人变更检索审核状态
        List<PerModifyExEntity> searchStatus = this.perModifyExMapper.searchStatus(perModifyExEntity);
        
        // 审核状态不是终了的场合
        for (int j = 0;j < searchStatus.size();j++) {
            if (!searchStatus.get(j).getExamineStatus().equals("EXAMINE_OVER")) {
                throw new BusinessException("ERR0000016"); 
            }
        }
        
        // 更新空表
        int empty = this.perModifyExMapper.updateEmptyFlie(perModifyExEntity);
        // 更新主表
        int res = this.perModifyExMapper.updateMain(perModifyExEntity);
        
        // 判断如果插入成功，则返回true
        if (res == 0 || empty == 0) {
            updateSuccess = false;
        } 
        
        return updateSuccess;
    }
    /**
     * 确认  更新biao（只更新确认人的场合）
     *
     * @param perModifyExEntity
     */
    @Transactional(readOnly = false)
    public boolean addbiaoMain(PerModifyExEntity perModifyExEntity) {
        logger.info("searchByFileIdCount#param:{}", perModifyExEntity);
        boolean updateSuccess = true;
        // 根据文件类别在code检索出code值
        String searchFileAbstractCode = this.perModifyExMapper.searchFileAbstractCode(perModifyExEntity);
        perModifyExEntity.setFileAbstractCode(searchFileAbstractCode);

        // 根据FileName在code表查出文件名称code
        String selectNameCode = this.perModifyExMapper.selectNameCode(perModifyExEntity);
        perModifyExEntity.setfNameCode(selectNameCode);

        // 关于确认人变更检索审核状态
        List<PerModifyExEntity> searchStatus = this.perModifyExMapper.searchStatus(perModifyExEntity);

        // 审核状态不是终了的场合
        for (int j = 0;j < searchStatus.size();j++) {
            if (!searchStatus.get(j).getExamineStatus().equals("EXAMINE_OVER")) {
                throw new BusinessException("ERR0000016");
            }
        }
        BiaoAddEntity biaoAddEntity=new BiaoAddEntity();
        biaoAddEntity.setCounts(0);
        biaoAddEntity.setFilename(perModifyExEntity.getFileId()+perModifyExEntity.getfName());
        biaoAddEntity.setKoshin_name(perModifyExEntity.getKoshinName());
        biaoAddEntity.setKoshincode(perModifyExEntity.getLoginTjCode());
        ShenherenExEntity select1 = perModifyExMapper.select1();
        biaoAddEntity.setOnecode(select1.getTjCode());
        biaoAddEntity.setOne(select1.getUsername()+"（待审核）");
        ShenherenExEntity select2 = perModifyExMapper.select2();
        biaoAddEntity.setTwocode(select2.getTjCode());
        biaoAddEntity.setTwo(select2.getUsername()+"（未审核）");
        ShenherenExEntity select3 = perModifyExMapper.select3();
        biaoAddEntity.setThreecode(select3.getTjCode());
        biaoAddEntity.setThree(select3.getUsername()+"（未审核）");
        ShenherenExEntity select4 = perModifyExMapper.select4();
        biaoAddEntity.setFourcode(select4.getTjCode());
        biaoAddEntity.setFour(select4.getUsername()+"（未审核）");
        System.out.println("biaoAddEntity2222"+biaoAddEntity);
        // 更新biao
        int res=perModifyExMapper.addbiao(biaoAddEntity);
        // 判断如果插入成功，则返回true
        if (res == 0 ) {
            updateSuccess = false;
        }

        return updateSuccess;
    }
    
    /**
     * 文件追加适用范围下拉列表
     * 
     * @param perModifyExEntity 
     * @return searchApplication
     */
    @Transactional(readOnly = true)
    public List<PerModifyExEntity> searchApplicationValue(PerModifyExEntity perModifyExEntity) {

        logger.info("searchApplicationValue#param:{}", perModifyExEntity);
        
        // 适用范围下拉列表
        List<PerModifyExEntity> searchApplication = this.perModifyExMapper.searchApplicationValue(perModifyExEntity);
        
        logger.info("searchApplicationValue#result:{}", searchApplication.size());
        
        return searchApplication;
    }
    
    /**
     * 文件追加业务内容下拉列表
     * 
     * @param perModifyExEntity 
     * @return searchApplication
     */
    @Transactional(readOnly = true)
    public List<PerModifyExEntity> searchContentValue(PerModifyExEntity perModifyExEntity) {

        logger.info("searchContentValue#param:{}", perModifyExEntity);
        
        // 业务内容下拉列表
        List<PerModifyExEntity> searchContent = this.perModifyExMapper.searchApplicationValue(perModifyExEntity);
        
        logger.info("searchContentValue#result:{}", searchContent.size());
        
        return searchContent;
    }

    /**
     * Info信息获取
     * 
     * @param infoCode
     * @return message
     */
    public String getInfo(String infoCode) {

        logger.info("getInfo#param:{}", infoCode);
        
        String[] parStrings = new String[1];
        Locale locale = LocaleContextHolder.getLocale();
        String message = messageSource.getMessage(infoCode, parStrings, locale);
        
        logger.info("getInfo#result:{}", message);
        
        return message;
    }

    /**
     * 插入权限变更数据
     * @param pcdata
     */
    public void permissionChangeData(PermissionChangeDataEntity pcdata) {
        perModifyExMapper.permissionChangeData(pcdata);
    }
}
