/*
 * 审批管理システム（NTTD向け） COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.admin.fileManage.service;

import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tci.sds.admin.fileManage.entity.FileManageExEntity;
import tci.sds.admin.fileManage.mapper.FileManageExMapper;
import tci.sds.common.exception.BusinessException;

/**
 * 文件管理用Service
 * 
 * @author 谭艳
 * @since 2020/11/09
 * 
 */
@Service
public class FileManageService {
    private final Logger logger = LoggerFactory.getLogger(FileManageService.class);

    @Autowired
    private FileManageExMapper fileManageExMapper;
    
    @Autowired
    private MessageSource messageSource;
    
    /**
     * 文件管理画面检索
     * 
     * @param fileManageExEntity 
     * @return 文件管理画面
     */
    @Transactional(readOnly = true)
    public List<FileManageExEntity> searchList(FileManageExEntity fileManageExEntity) {

        logger.info("searchList#param:{}", fileManageExEntity);
        // 一览
        List<FileManageExEntity> searchList = this.fileManageExMapper.searchList(fileManageExEntity);
        // 时间年月日 时分秒
        for (int i = 0;i < searchList.size();i++) {
            searchList.get(i).setUploadTime(searchList.get(i).getUploadTime().substring(0, 19));
        }
       
        logger.info("searchList#result:{}", searchList.size());
        return searchList;

    }

    /**
     * 总件数取得
     * 
     * @param fileManageExEntity 
     * @return 总件数
     */
    @Transactional(readOnly = true)
    public int searchTotal(FileManageExEntity fileManageExEntity) {

        logger.info("searchTotal#param:{}", fileManageExEntity);
        // 总件数
        int searchTotalSize = this.fileManageExMapper.getTotalCount(fileManageExEntity);
        
        logger.info("searchTotal#result:{}", searchTotalSize );
        
        return searchTotalSize ;
    }
    
    /**
     * 文件类别下拉列表检索结果
     * 
     * @param fileManageExEntity 
     * @return searchFileTypeLists
     */
    @Transactional(readOnly = true)
    public List<FileManageExEntity> searchFileTypeList(FileManageExEntity fileManageExEntity) {

        logger.info("searchFileTypeLists#param:{}", fileManageExEntity);
        // 文件类别下拉列表
        List<FileManageExEntity> searchFileTypeLists = this.fileManageExMapper.searchFileTypeList(fileManageExEntity);
        
        logger.info("searchFileTypeLists#result:{}", searchFileTypeLists.size());
        
        return searchFileTypeLists;
    }
   
    /**
     * 删除
     * 
     * @param fileManageExEntity サンプルEntity
     */
    @Transactional(readOnly = false)
    public boolean delete(FileManageExEntity fileManageExEntity) {
        
        boolean deleteSuccess = true;
        logger.info("searchByFileIdCount#param:{}", fileManageExEntity);
        
        // 根据文件名称在code检索出code值
        String searchFnameCode = this.fileManageExMapper.searchFnameCode(fileManageExEntity);
        fileManageExEntity.setfNameCode(searchFnameCode);
        // 根据文件类别在code检索出code值 
        String searchFileAbstractCode = this.fileManageExMapper.searchFileAbstractCode(fileManageExEntity);
        fileManageExEntity.setFileAbstractCode(searchFileAbstractCode);
        
        // 模板文件的场合
        if (fileManageExEntity.getFileAbstractData().equals("模板文件")) {
            // 检索审核状态
            List<FileManageExEntity> searchExamineOver = this.fileManageExMapper.searchExamineOver(fileManageExEntity);
            // 判断审核状态是否为审核终了
            for (int i = 0;i < searchExamineOver.size();i++) {
                if (!searchExamineOver.get(i).getExamineStatus().equals("EXAMINE_OVER")) {
                    throw new BusinessException("ERR0000017"); 
                } 
            }
           // 删除主表
            int main = this.fileManageExMapper.deleteMainData(fileManageExEntity);
            if (main == 0) {
                deleteSuccess = false;
                return deleteSuccess;
            }
        }
        
        // 图片文件的场合
        if (fileManageExEntity.getFileAbstractData().equals("图片文件")) {
            // 检索审核人状态
            int searchReviewerStatus = this.fileManageExMapper.searchReviewerStatus(fileManageExEntity);
            // 判断审核人状态是否为已审核
            if (searchReviewerStatus != 0) {
                throw new BusinessException("ERR0000017"); 
            }
        }
        
        
        // 删除空表
        int empty = this.fileManageExMapper.deleteEmptyData(fileManageExEntity);
        
        if (empty == 0) {
            deleteSuccess = false;
            return deleteSuccess;
        }
        
        return deleteSuccess;
    }
    
    /**
     * Info信息获取
     * 
     * @param infoCode
     * @return message
     */
    public String getInfo(String infoCode) {

        logger.info("getInfo#param:{}", infoCode);
        
        String[] parStrings = new String[1];
        Locale locale = LocaleContextHolder.getLocale();
        String message = messageSource.getMessage(infoCode, parStrings, locale);
        
        logger.info("getInfo#result:{}", message);
        
        return message;
    }
}
