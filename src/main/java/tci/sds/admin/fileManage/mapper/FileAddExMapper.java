/*
 * 审批管理系统 COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.admin.fileManage.mapper;

import java.util.List;

import tci.sds.admin.fileManage.entity.FileAddExEntity;

/**
 * 文件追加画面用Form
 * 
 * @author 谭艳
 * @since 2020/11/09
 * 
 */
public interface FileAddExMapper {
    /**
     * 根据文件名称在code表查询有无文件名称code值
     * @return 检索结果
     */
    String searchByCodeCount(FileAddExEntity fileAddExEntity);
    
    /**
     * 根据文件名称，业务内容，适用范围检索空表件数取得
     * @return 件数
     */
    int searchByMainCount(FileAddExEntity fileAddExEntity);
    
    /**
     * 主表数据插入
     * @return 件数
     */
    int insertMailFile(FileAddExEntity fileAddExEntity);
    
    /**
     * 空表数据插入
     * @return 件数
     */
    int insertEmptyFile(FileAddExEntity fileAddExEntity);
    
    /**
     * 确认人下拉列表
     * @return 检索结果
     */
    List<FileAddExEntity> searchConfirmerValue(FileAddExEntity fileAddExEntity);
    
    /**
     * 审核人下拉列表
     * @return 检索结果
     */
    List<FileAddExEntity> searchReviewerList(FileAddExEntity fileAddExEntity);
    
    /**
     * 根据FILE_NAME在code表检索有无数据
     * @return 检索结果
     */
    int fileNameCount(FileAddExEntity fileAddExEntity);
    
    /**
     * 检索出最大的文件名称
     * @return 检索结果
     */
    List<FileAddExEntity> searchFileName(FileAddExEntity fileAddExEntity);
    
    /**
     * 根据BUSINESS_SCOPE在code表检索有无数据
     * @return 检索结果
     */
    int businessScopeCount(FileAddExEntity fileAddExEntity);
    
    /**
     * 检索出最大的适用范围
     * @return 检索结果
     */
    List<FileAddExEntity> searchBusinessScope(FileAddExEntity fileAddExEntity);
    
    /**
     * 根据BUSINESS_CONTENT在code表检索有无数据
     * @return 检索结果
     */
    int businessContentCount(FileAddExEntity fileAddExEntity);
    
    /**
     * 检索出最大的业务内容
     * @return 检索结果
     */
    List<FileAddExEntity> searchBusinessContent(FileAddExEntity fileAddExEntity);
    
    /**
     * 文件名称插入code表
     * @return 件数
     */
    int insertFileNameByCode(FileAddExEntity fileAddExEntity);
    
    /**
     * 适用范围插入code表
     * @return 件数
     */
    int insertScopeApplicationByCode(FileAddExEntity fileAddExEntity);
    
    /**
     * 业务内容插入code表
     * @return 件数
     */
    int insertConditionByCode(FileAddExEntity fileAddExEntity);
    
    /**
     * 根据画面的适用范围在code表里查出适用范围对应的code值
     * @return 检索结果
     */
    String searchScopeApplication(FileAddExEntity fileAddExEntity);
    
    /**
     * 根据画面的业务内容在code表里查出业务内容对应的code值 
     * @return 检索结果
     */
    String searchCondition(FileAddExEntity fileAddExEntity);
  
    /**
     * 根据文件名称检索主表和空表有没有数据
     * @return 件数
     */
    int selectMainAndEmpty(FileAddExEntity fileAddExEntity);
    
    /**
     * 根据文件名称检索空表有没有数据 
     * @return 件数
     */
    int selectEmpty(FileAddExEntity fileAddExEntity);
    
    /**
     * 根据cd_code查询code表中适用范围
     * @return 件数
     */
    String selectCodekjName(FileAddExEntity fileAddExEntity);
    
    /**
     * 根据cd_code查询code表中的适用范围个数
     * @return 件数
     */
    int selectCountCode(FileAddExEntity fileAddExEntity);
    
    /**
     * 根据cd_code查询code表中的业务内容
     * @return 件数
     */
    String selectCodeContent(FileAddExEntity fileAddExEntity);
    
    /**
     * 根据cd_code查询code表中的业务内容个数
     * @return 件数
     */
    int selectCountContent(FileAddExEntity fileAddExEntity);
    
    /**
     * 根据文件名称检索空表有没有数据（图片文件的场合）
     * @return 件数
     */
    int selectEmptyPng(FileAddExEntity fileAddExEntity);
    
    /**
     * 根据文件编号，文件名称和确认人查询主表
     * @param fileManageExEntity 
     * @return 检索结果
     */
    int searchCount(FileAddExEntity fileAddExEntity);
}
