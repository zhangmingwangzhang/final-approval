/*
 * 审批管理システム（NTTD向け） COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.admin.fileManage.mapper;

import java.util.List;

import tci.sds.admin.fileManage.entity.FileManageExEntity;

/**
 * 文件管理画面用Form
 * 
 * @author 谭艳
 * @since 2020/11/09
 * 
 */
public interface FileManageExMapper {
	
    /**
     * 文件管理画面总件数取得
     * @return 总件数
     */
	int getTotalCount(FileManageExEntity fileManageExEntity);
	
	/**
	 * 文件管理画面检索结果
	 * @return 检索结果
	 */
	List<FileManageExEntity> searchList(FileManageExEntity fileManageExEntity);
	
	/**
     * 根据文件名称在code检索出code值
     * @param fileManageExEntity 
     * @return 削除件数
     */
    String searchFnameCode(FileManageExEntity fileManageExEntity);
    
    /**
     * 根据文件类别在code检索出code值 
     * @param fileManageExEntity 
     * @return 削除件数
     */
    String searchFileAbstractCode(FileManageExEntity fileManageExEntity);
    
	/**
     * 文件管理画面删除空表
     * @param fileManageExEntity 
     * @return 削除件数
     */
    int deleteEmptyData(FileManageExEntity fileManageExEntity);
	
    /**
     * 文件管理画面删除主表
     * @param fileManageExEntity 
     * @return 削除件数
     */
    int deleteMainData(FileManageExEntity fileManageExEntity);
    
    /**
     * 检索审核状态
     * @param fileManageExEntity 
     * @return 检索结果
     */
    List<FileManageExEntity> searchExamineOver(FileManageExEntity fileManageExEntity);
    
    /**
     * 检索审核人状态(图片)
     * @param fileManageExEntity 
     * @return 检索结果
     */
    int searchReviewerStatus(FileManageExEntity fileManageExEntity);
    
    /**
     * 文件类别下拉列表
     * @return 检索结果
     */
    List<FileManageExEntity> searchFileTypeList(FileManageExEntity fileManageExEntity);
    
}
