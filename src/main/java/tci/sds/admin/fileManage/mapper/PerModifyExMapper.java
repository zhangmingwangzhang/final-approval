/*
 * 审批管理システム（NTTD向け） COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.admin.fileManage.mapper;

import java.util.List;

import tci.sds.admin.fileManage.entity.BiaoAddEntity;
import tci.sds.admin.fileManage.entity.PerModifyExEntity;
import tci.sds.admin.fileManage.entity.PermissionChangeDataEntity;
import tci.sds.admin.fileManage.entity.ShenherenExEntity;

/**
 * 权限变更画面用Form
 * 
 * @author 谭艳
 * @since 2020/11/09
 * 
 */
public interface PerModifyExMapper {

    /**
     * 权限变更画面适用范围下拉列表
     * @return 检索结果
     */
    List<PerModifyExEntity> searchApplication(PerModifyExEntity perModifyExEntity);
    
    /**
     * 权限变更画面业务内容下拉列表
     * @return 检索结果
     */
    List<PerModifyExEntity> searchCondition(PerModifyExEntity perModifyExEntity);
    
    /**
     * 权限变更画面确认人回显
     * @return 检索结果
     */
    String searchConfigId(PerModifyExEntity perModifyExEntity);
    
    /**
     * 权限变更画面根据文件编号检索确认人名称
     * @return 检索结果
     */
    String searchConfigName(PerModifyExEntity perModifyExEntity);
    
    /**
     * 权限变更画面确认人
     * @return 检索结果
     */
    List<PerModifyExEntity> searchConfigIdList(PerModifyExEntity perModifyExEntity);
    
    /**
     * 检索结果
     * @return 检索结果
     */
    List<PerModifyExEntity> searchList(PerModifyExEntity perModifyExEntity);
    
    /**
     * 审核人下拉列表
     * @return 检索结果
     */
    List<PerModifyExEntity> searchReviewerList(PerModifyExEntity perModifyExEntity);
    
    /**
     * 根据FileName在code表查出文件名称code
     * @return selectNameCode
     */
    String selectNameCode(PerModifyExEntity perModifyExEntity);
    
    /**
     * 适用范围，业务内容删除
     * @return 件数
     */
    int scopeConditionDel(PerModifyExEntity perModifyExEntity);
    
    /**
     * 当前删除适用范围，业务内容是否为文件唯一
     * @return 件数
     */
    int scopeSelect(PerModifyExEntity perModifyExEntity);
    
    /**
     * 当前删除适用范围，业务内容为文件唯一时删除空表该文件
     * @return 件数
     */
    int scopeEmptyDel(PerModifyExEntity perModifyExEntity);
    
    /**
     * 确认 更新主表（确认人不变更的场合）
     * @return 件数
     */
    int updateMainFlie(PerModifyExEntity perModifyExEntity);
    
    /**
     * 当前审核顺序是否存在check
     * @return 件数
     */
    int searchOrder(PerModifyExEntity perModifyExEntity);
    
    /**
     * 审核Sheet检索
     * @return 
     */
    String searchSheet(PerModifyExEntity perModifyExEntity);
    
    /**
     * 确认 插入主表（确认人不变更的场合）
     * @return 件数
     */
    int insertMailFile(PerModifyExEntity perModifyExEntity);
    
    /**
     * 确认 更新主表（只更新确认人的场合）
     * @return 件数
     */
    int updateMain(PerModifyExEntity perModifyExEntity);
    
    /**删除多余审核人
     * @return 件数
     */
    int deleteReviewer(PerModifyExEntity perModifyExEntity);
    
    /**
     * 确认 更新主表
     * @return 件数
     */
    int updateEmptyFlie(PerModifyExEntity perModifyExEntity);
    
    /**
     * 检索状态
     * @return 检索结果
     */
    List<PerModifyExEntity> selectStatus(PerModifyExEntity perModifyExEntity);
    
    /**
     * 关于确认人变更检索审核状态
     * @return 检索结果
     */
    List<PerModifyExEntity> searchStatus(PerModifyExEntity perModifyExEntity);
    
    /**
     * 根据文件类别在code检索出code值 
     * @param PerModifyExEntity 
     * @return 件数
     */
    String searchFileAbstractCode(PerModifyExEntity perModifyExEntity);
    
    /**
     * 文件追加适用范围下拉列表
     * @return 检索结果
     */
    List<PerModifyExEntity> searchApplicationValue(PerModifyExEntity perModifyExEntity);

    /**
     * 查四个审核人姓名卡号
     * @return
     */
    ShenherenExEntity select1();
    ShenherenExEntity select2();
    ShenherenExEntity select3();
    ShenherenExEntity select4();
    /**
     * addbiao
     * @return
     */
    int addbiao(BiaoAddEntity biaoAddEntity);

    /**
     * 插入权限变更数据
     * @param pcdata
     */
    void permissionChangeData(PermissionChangeDataEntity pcdata);
}
