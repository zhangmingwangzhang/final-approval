/*
 * 审批管理システム（NTTD向け） COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.admin.fileManage.entity;

import java.sql.Timestamp;
import java.util.List;

import tci.sds.admin.fileManage.form.AddForm;
import tci.sds.common.form.BaseForm;

public class FileAddExEntity extends BaseForm {

    /** 文件编号 */
    private String fileId;
    
    /** 文件名称 */
    private String fileName;

    /** 文件类别 */
    private String fileAbstract;
    
    /** 适用范围code名称 */
    private String scopeApplication;
    
    /** 业务内容code名称 */
    private String condition;
    
    /** 确认人 */
    private String fileConfigId;
    
    /** 审核人 */
    private String reviewer;

    /** 审核人签名位置 */
    private String reviewerPosition;
    
    /** 签名sheet */
    private String reviewerSheet;
    
    /** 审核顺序 */
    private String reviewerOrder;
    
    /** 文件路径 */
    private String filePath;

    /** 文件大小 */
    private Integer fileSize;
    
    /** 作成者 */
    private String createName;
    
    /** 作成日时 */
    private Timestamp createTime;
    
    /** 更新者 */
    private String koshinName;
    
    /** 更新日时 */
    private Timestamp koshinTimestamp;
    
    /** 审核权限 */
    private String roleCode;
    
    /** Code区分 */
    private String cdKubun;
    
    /** 下拉列表label标签 */
    private String label;
    
    /** 确认人value值 */
    private String value;
    
    /** 审核人value值 */
    private String reviewerValue;
    
    /** 文件名称code值 */
    private String fileNameCode;
    
    /** 文件名称code区分名称 */
    private String fileNameKubunName;
    
    /** 文件名称code区分值 */
    private String fileNameKubun;
    
    /** 适用范围code值 */
    private String scopeApplicationCode;
    
    /** 适用范围code区分名称 */
    private String scopeKubunName;
    
    /** 适用范围code区分值 */
    private String scopeKubun;
    
    /** 查询适用范围code值 */
    private String scopekjName;
    
    /** 业务内容code值 */
    private String conditionCode;
    
    /** 业务内容code区分名称 */
    private String conditionKubunName;
    
    /** 业务内容code区分值 */
    private String conditionKubun;
    
    /** 根据文件名称在code表查询有无文件名称code值 */
    private String fnName;
    
    /** 根据画面适用范围在code表里查出的适用范围code值 */
    private String scopeCode;
    
    /** 根据画面业务内容在code表里查出的业务内容code值 */
    private String cdtCode;
    
    /** 文件名称定数 */
    private String searchFileNameKubun;
    
    /** 业务内容定数 */
    private String searchContentKubun;
    
    /** 适用范围定数 */
    private String searchScopeKubun;
    
    /** js传到form的list */
    private List<AddForm> addDatas;

    public String getSearchFileNameKubun() {
    
        return searchFileNameKubun;
    
    }

    public void setSearchFileNameKubun(String searchFileNameKubun) {
    
        this.searchFileNameKubun = searchFileNameKubun;
    
    }

    public String getSearchContentKubun() {
    
        return searchContentKubun;
    
    }

    public void setSearchContentKubun(String searchContentKubun) {
    
        this.searchContentKubun = searchContentKubun;
    
    }

    public String getSearchScopeKubun() {
    
        return searchScopeKubun;
    
    }

    public void setSearchScopeKubun(String searchScopeKubun) {
    
        this.searchScopeKubun = searchScopeKubun;
    
    }

    public String getFnName() {
    
        return fnName;
    
    }

    public void setFnName(String fnName) {
    
        this.fnName = fnName;
    
    }

    public String getFileNameCode() {
    
        return fileNameCode;
    
    }

    public void setFileNameCode(String fileNameCode) {
    
        this.fileNameCode = fileNameCode;
    
    }

    public String getFileNameKubunName() {
    
        return fileNameKubunName;
    
    }

    public void setFileNameKubunName(String fileNameKubunName) {
    
        this.fileNameKubunName = fileNameKubunName;
    
    }

    public String getFileNameKubun() {
    
        return fileNameKubun;
    
    }

    public void setFileNameKubun(String fileNameKubun) {
    
        this.fileNameKubun = fileNameKubun;
    
    }

    public String getScopeCode() {
    
        return scopeCode;
    
    }

    public void setScopeCode(String scopeCode) {
    
        this.scopeCode = scopeCode;
    
    }

    public String getCdtCode() {
    
        return cdtCode;
    
    }

    public void setCdtCode(String cdtCode) {
    
        this.cdtCode = cdtCode;
    
    }

    public String getScopekjName() {
    
        return scopekjName;
    
    }

    public void setScopekjName(String scopekjName) {
    
        this.scopekjName = scopekjName;
    
    }

    public String getConditionKubunName() {
    
        return conditionKubunName;
    
    }

    public void setConditionKubunName(String conditionKubunName) {
    
        this.conditionKubunName = conditionKubunName;
    
    }

    public String getConditionKubun() {
    
        return conditionKubun;
    
    }

    public void setConditionKubun(String conditionKubun) {
    
        this.conditionKubun = conditionKubun;
    
    }

    public String getScopeKubunName() {
    
        return scopeKubunName;
    
    }

    public void setScopeKubunName(String scopeKubunName) {
    
        this.scopeKubunName = scopeKubunName;
    
    }

    public String getScopeKubun() {
    
        return scopeKubun;
    
    }

    public void setScopeKubun(String scopeKubun) {
    
        this.scopeKubun = scopeKubun;
    
    }

    public String getReviewerSheet() {
    
        return reviewerSheet;
    
    }

    public void setReviewerSheet(String reviewerSheet) {
    
        this.reviewerSheet = reviewerSheet;
    
    }

    public String getFileId() {
    
        return fileId;
    
    }

    public void setFileId(String fileId) {
    
        this.fileId = fileId;
    
    }

    public String getFileName() {
    
        return fileName;
    
    }

    public void setFileName(String fileName) {
    
        this.fileName = fileName;
    
    }

    public String getFileAbstract() {
    
        return fileAbstract;
    
    }

    public void setFileAbstract(String fileAbstract) {
    
        this.fileAbstract = fileAbstract;
    
    }

    public String getScopeApplication() {
    
        return scopeApplication;
    
    }

    public void setScopeApplication(String scopeApplication) {
    
        this.scopeApplication = scopeApplication;
    
    }

    public String getCondition() {
    
        return condition;
    
    }

    public void setCondition(String condition) {
    
        this.condition = condition;
    
    }

    public String getFileConfigId() {
    
        return fileConfigId;
    
    }

    public void setFileConfigId(String fileConfigId) {
    
        this.fileConfigId = fileConfigId;
    
    }

    public String getReviewer() {
    
        return reviewer;
    
    }

    public void setReviewer(String reviewer) {
    
        this.reviewer = reviewer;
    
    }

    public String getReviewerPosition() {
    
        return reviewerPosition;
    
    }

    public void setReviewerPosition(String reviewerPosition) {
    
        this.reviewerPosition = reviewerPosition;
    
    }

    public String getReviewerOrder() {
    
        return reviewerOrder;
    
    }

    public void setReviewerOrder(String reviewerOrder) {
    
        this.reviewerOrder = reviewerOrder;
    
    }

    public String getFilePath() {
    
        return filePath;
    
    }

    public void setFilePath(String filePath) {
    
        this.filePath = filePath;
    
    }

    public Integer getFileSize() {
    
        return fileSize;
    
    }

    public void setFileSize(Integer fileSize) {
    
        this.fileSize = fileSize;
    
    }

    public String getCreateName() {
    
        return createName;
    
    }

    public void setCreateName(String createName) {
    
        this.createName = createName;
    
    }

    public Timestamp getCreateTime() {
    
        return createTime;
    
    }

    public void setCreateTime(Timestamp createTime) {
    
        this.createTime = createTime;
    
    }

    public String getKoshinName() {
    
        return koshinName;
    
    }

    public void setKoshinName(String koshinName) {
    
        this.koshinName = koshinName;
    
    }

    public Timestamp getKoshinTimestamp() {
    
        return koshinTimestamp;
    
    }

    public void setKoshinTimestamp(Timestamp koshinTimestamp) {
    
        this.koshinTimestamp = koshinTimestamp;
    
    }

    public String getRoleCode() {
    
        return roleCode;
    
    }

    public void setRoleCode(String roleCode) {
    
        this.roleCode = roleCode;
    
    }

    public String getCdKubun() {
    
        return cdKubun;
    
    }

    public void setCdKubun(String cdKubun) {
    
        this.cdKubun = cdKubun;
    
    }

    public String getLabel() {
    
        return label;
    
    }

    public void setLabel(String label) {
    
        this.label = label;
    
    }

    public String getValue() {
    
        return value;
    
    }

    public void setValue(String value) {
    
        this.value = value;
    
    }

    public String getReviewerValue() {
    
        return reviewerValue;
    
    }

    public void setReviewerValue(String reviewerValue) {
    
        this.reviewerValue = reviewerValue;
    
    }

    public List<AddForm> getAddDatas() {
    
        return addDatas;
    
    }

    public void setAddDatas(List<AddForm> addDatas) {
    
        this.addDatas = addDatas;
    
    }

    public String getConditionCode() {

        return conditionCode;

    }

    public void setConditionCode(String conditionCode) {

        this.conditionCode = conditionCode;

    }

    public String getScopeApplicationCode() {

        return scopeApplicationCode;

    }

    public void setScopeApplicationCode(String scopeApplicationCode) {

        this.scopeApplicationCode = scopeApplicationCode;

    }

    @Override
    public String toString() {

        return "FileAddExEntity [fileId=" + fileId + ", fileName=" + fileName
                + ", fileAbstract=" + fileAbstract + ", scopeApplication="
                + scopeApplication + ", condition=" + condition
                + ", fileConfigId=" + fileConfigId + ", reviewer=" + reviewer
                + ", reviewerPosition=" + reviewerPosition + ", reviewerSheet="
                + reviewerSheet + ", reviewerOrder=" + reviewerOrder
                + ", filePath=" + filePath + ", fileSize=" + fileSize
                + ", createName=" + createName + ", createTime=" + createTime
                + ", koshinName=" + koshinName + ", koshinTimestamp="
                + koshinTimestamp + ", roleCode=" + roleCode + ", cdKubun="
                + cdKubun + ", label=" + label + ", value=" + value
                + ", reviewerValue=" + reviewerValue + ", fileNameCode="
                + fileNameCode + ", fileNameKubunName=" + fileNameKubunName
                + ", fileNameKubun=" + fileNameKubun + ", scopeApplicationCode="
                + scopeApplicationCode + ", scopeKubunName=" + scopeKubunName
                + ", scopeKubun=" + scopeKubun + ", scopekjName=" + scopekjName
                + ", conditionCode=" + conditionCode + ", conditionKubunName="
                + conditionKubunName + ", conditionKubun=" + conditionKubun
                + ", fnName=" + fnName + ", scopeCode=" + scopeCode
                + ", cdtCode=" + cdtCode + ", searchFileNameKubun="
                + searchFileNameKubun + ", searchContentKubun="
                + searchContentKubun + ", searchScopeKubun=" + searchScopeKubun
                + ", addDatas=" + addDatas + "]";

    }

}
