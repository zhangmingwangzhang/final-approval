/*
 * 审批管理システム（NTTD向け） COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.admin.fileManage.entity;

import java.sql.Timestamp;
import java.util.List;

import tci.sds.admin.fileManage.form.PerModifyForm;

/**
 * Entity
 * 
 * @author 谭艳
 * @since 2020/11/09
 * 
 */
public class PerModifyExEntity {

    /** 文件编号 */
    private String fileId;
    
    /** 文件名称 */
    private String fName;
    
    /** 文件名称code值 */
    private String fNameCode;
    
    /** 标签 */
    private String kjName;
    
    /** 内容 */
    private String cdCode;
    
    /** 签名位置 */
    private String position;
    
    /** 签名Sheet */
    private String reviewerSheet;
    
    /** 适用范围下拉列表value值 */
    private String perModifyApplicationValue;
    
    /** 业务内容下拉列表value值 */
    private String perModifyContentValue;
    
    /** 确认人下拉列表value值 */
    private String confirmerValue;
    
    /** 确认人文本框值 */
    private String confirmationPerson;
    
    /** 确认人文本框值 */
    private String confirmationPersonCode;
    
    /** 审核人下拉列表value值 */
    private String reviewerValue;
    
    /** 权限变更用文件类别code */
    private String fileAbstractCode;
    
    /** 权限变更用文件类别code */
    private String fileAbstractData;
    
    /** 审核人下拉列表label值 */
    private String reviewerjName;
    
    /** 更新者 */
    private String koshinName;

    /** 更新者 code*/
    private String loginTjCode;
    
    /** 更新日时 */
    private Timestamp koshinTime;
    
    /** 审核顺序 */
    private String reviewerOrder;
    
    /** js传到form的list */
    private List<PerModifyForm> perModifyDatas;
    
    /** 审核状态 */
    private String examineStatus;
    
    /** code区分 */
    private String cdKubun;
    
    /** 审核权限 */
    private String roleCode;
    
    /** 审核人总数 */
    private String reviewerTotal;

    /**
     * 申请时间
     * @return
     */
    private  String applicationdate;

    public String getApplicationdate() {
        return applicationdate;
    }

    public void setApplicationdate(String applicationdate) {
        this.applicationdate = applicationdate;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getfNameCode() {
        return fNameCode;
    }

    public void setfNameCode(String fNameCode) {
        this.fNameCode = fNameCode;
    }

    public String getKjName() {
        return kjName;
    }

    public void setKjName(String kjName) {
        this.kjName = kjName;
    }

    public String getCdCode() {
        return cdCode;
    }

    public void setCdCode(String cdCode) {
        this.cdCode = cdCode;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getReviewerSheet() {
        return reviewerSheet;
    }

    public void setReviewerSheet(String reviewerSheet) {
        this.reviewerSheet = reviewerSheet;
    }

    public String getPerModifyApplicationValue() {
        return perModifyApplicationValue;
    }

    public void setPerModifyApplicationValue(String perModifyApplicationValue) {
        this.perModifyApplicationValue = perModifyApplicationValue;
    }

    public String getPerModifyContentValue() {
        return perModifyContentValue;
    }

    public void setPerModifyContentValue(String perModifyContentValue) {
        this.perModifyContentValue = perModifyContentValue;
    }

    public String getConfirmerValue() {
        return confirmerValue;
    }

    public void setConfirmerValue(String confirmerValue) {
        this.confirmerValue = confirmerValue;
    }

    public String getConfirmationPerson() {
        return confirmationPerson;
    }

    public void setConfirmationPerson(String confirmationPerson) {
        this.confirmationPerson = confirmationPerson;
    }

    public String getConfirmationPersonCode() {
        return confirmationPersonCode;
    }

    public void setConfirmationPersonCode(String confirmationPersonCode) {
        this.confirmationPersonCode = confirmationPersonCode;
    }

    public String getReviewerValue() {
        return reviewerValue;
    }

    public void setReviewerValue(String reviewerValue) {
        this.reviewerValue = reviewerValue;
    }

    public String getFileAbstractCode() {
        return fileAbstractCode;
    }

    public void setFileAbstractCode(String fileAbstractCode) {
        this.fileAbstractCode = fileAbstractCode;
    }

    public String getFileAbstractData() {
        return fileAbstractData;
    }

    public void setFileAbstractData(String fileAbstractData) {
        this.fileAbstractData = fileAbstractData;
    }

    public String getReviewerjName() {
        return reviewerjName;
    }

    public void setReviewerjName(String reviewerjName) {
        this.reviewerjName = reviewerjName;
    }

    public String getKoshinName() {
        return koshinName;
    }

    public void setKoshinName(String koshinName) {
        this.koshinName = koshinName;
    }

    public String getLoginTjCode() {
        return loginTjCode;
    }

    public void setLoginTjCode(String loginTjCode) {
        this.loginTjCode = loginTjCode;
    }

    public Timestamp getKoshinTime() {
        return koshinTime;
    }

    public void setKoshinTime(Timestamp koshinTime) {
        this.koshinTime = koshinTime;
    }

    public String getReviewerOrder() {
        return reviewerOrder;
    }

    public void setReviewerOrder(String reviewerOrder) {
        this.reviewerOrder = reviewerOrder;
    }

    public List<PerModifyForm> getPerModifyDatas() {
        return perModifyDatas;
    }

    public void setPerModifyDatas(List<PerModifyForm> perModifyDatas) {
        this.perModifyDatas = perModifyDatas;
    }

    public String getExamineStatus() {
        return examineStatus;
    }

    public void setExamineStatus(String examineStatus) {
        this.examineStatus = examineStatus;
    }

    public String getCdKubun() {
        return cdKubun;
    }

    public void setCdKubun(String cdKubun) {
        this.cdKubun = cdKubun;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getReviewerTotal() {
        return reviewerTotal;
    }

    public void setReviewerTotal(String reviewerTotal) {
        this.reviewerTotal = reviewerTotal;
    }

    @Override
    public String toString() {
        return "PerModifyExEntity{" +
                "fileId='" + fileId + '\'' +
                ", fName='" + fName + '\'' +
                ", fNameCode='" + fNameCode + '\'' +
                ", kjName='" + kjName + '\'' +
                ", cdCode='" + cdCode + '\'' +
                ", position='" + position + '\'' +
                ", reviewerSheet='" + reviewerSheet + '\'' +
                ", perModifyApplicationValue='" + perModifyApplicationValue + '\'' +
                ", perModifyContentValue='" + perModifyContentValue + '\'' +
                ", confirmerValue='" + confirmerValue + '\'' +
                ", confirmationPerson='" + confirmationPerson + '\'' +
                ", confirmationPersonCode='" + confirmationPersonCode + '\'' +
                ", reviewerValue='" + reviewerValue + '\'' +
                ", fileAbstractCode='" + fileAbstractCode + '\'' +
                ", fileAbstractData='" + fileAbstractData + '\'' +
                ", reviewerjName='" + reviewerjName + '\'' +
                ", koshinName='" + koshinName + '\'' +
                ", loginTjCode='" + loginTjCode + '\'' +
                ", koshinTime=" + koshinTime +
                ", reviewerOrder='" + reviewerOrder + '\'' +
                ", perModifyDatas=" + perModifyDatas +
                ", examineStatus='" + examineStatus + '\'' +
                ", cdKubun='" + cdKubun + '\'' +
                ", roleCode='" + roleCode + '\'' +
                ", reviewerTotal='" + reviewerTotal + '\'' +
                ", applicationdate='" + applicationdate + '\'' +
                '}';
    }
}
