package tci.sds.admin.fileManage.entity;

public class PermissionChangeDataEntity {
    private String fileId;
    private String fName;
    private String perModifyContentValue;
    private String confirmationPerson;
    private String reviewerValue;
    private String position;
    private int  orderreviewer;
     private  String  applicationdate;
   private  String perModifyApplicationValue;
   private  String fileAbstractData;
   private  String koshinName;

   private  String  cdCode;

   private  String  loginTjCode;
   private  String   updateConfirmer;
   private  String  kjName;

    public String getKjName() {
        return kjName;
    }

    public void setKjName(String kjName) {
        this.kjName = kjName;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getPerModifyContentValue() {
        return perModifyContentValue;
    }

    public void setPerModifyContentValue(String perModifyContentValue) {
        this.perModifyContentValue = perModifyContentValue;
    }

    public String getConfirmationPerson() {
        return confirmationPerson;
    }

    public void setConfirmationPerson(String confirmationPerson) {
        this.confirmationPerson = confirmationPerson;
    }

    public String getReviewerValue() {
        return reviewerValue;
    }

    public void setReviewerValue(String reviewerValue) {
        this.reviewerValue = reviewerValue;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getOrderreviewer() {
        return orderreviewer;
    }

    public void setOrderreviewer(int orderreviewer) {
        this.orderreviewer = orderreviewer;
    }

    public String getApplicationdate() {
        return applicationdate;
    }

    public void setApplicationdate(String applicationdate) {
        this.applicationdate = applicationdate;
    }

    public String getPerModifyApplicationValue() {
        return perModifyApplicationValue;
    }

    public void setPerModifyApplicationValue(String perModifyApplicationValue) {
        this.perModifyApplicationValue = perModifyApplicationValue;
    }

    public String getFileAbstractData() {
        return fileAbstractData;
    }

    public void setFileAbstractData(String fileAbstractData) {
        this.fileAbstractData = fileAbstractData;
    }

    public String getKoshinName() {
        return koshinName;
    }

    public void setKoshinName(String koshinName) {
        this.koshinName = koshinName;
    }



    public String getCdCode() {
        return cdCode;
    }

    public void setCdCode(String cdCode) {
        this.cdCode = cdCode;
    }


    public String getLoginTjCode() {
        return loginTjCode;
    }

    public void setLoginTjCode(String loginTjCode) {
        this.loginTjCode = loginTjCode;
    }

    public String getUpdateConfirmer() {
        return updateConfirmer;
    }

    public void setUpdateConfirmer(String updateConfirmer) {
        this.updateConfirmer = updateConfirmer;
    }

    @Override
    public String toString() {
        return "PermissionChangeDataEntity{" +
                "fileId='" + fileId + '\'' +
                ", fName='" + fName + '\'' +
                ", perModifyContentValue='" + perModifyContentValue + '\'' +
                ", confirmationPerson='" + confirmationPerson + '\'' +
                ", reviewerValue='" + reviewerValue + '\'' +
                ", position='" + position + '\'' +
                ", orderreviewer=" + orderreviewer +
                ", applicationdate='" + applicationdate + '\'' +
                ", perModifyApplicationValue='" + perModifyApplicationValue + '\'' +
                ", fileAbstractData='" + fileAbstractData + '\'' +
                ", koshinName='" + koshinName + '\'' +
                ", cdCode='" + cdCode + '\'' +
                ", loginTjCode='" + loginTjCode + '\'' +
                ", updateConfirmer='" + updateConfirmer + '\'' +
                ", kjName='" + kjName + '\'' +
                '}';
    }
}
