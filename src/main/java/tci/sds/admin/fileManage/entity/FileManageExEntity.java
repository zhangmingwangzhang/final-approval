/*
 * 审批管理システム（NTTD向け） COPYRIGHT(c) 2020 trans-cosmos.com.cn
 */
package tci.sds.admin.fileManage.entity;

/**
 * Entity
 * 
 * @author 谭艳
 * @since 2020/11/09
 * 
 */
public class FileManageExEntity {

    /** 文件名（文件编号+文件名称） */
    private String fileName;
    
    /** 文件类别 */
    private String fileValue;
    
    /** 一览文件名 */
    private String file;
    
    /** 一览文件类别 */
    private String fileAbstract;

    /** 一览确认人 */
    private String principal;
    
    /** 一览上传时间 */
    private String uploadTime;
    
    /** 做成时间 */
    private String createTime;
    
    /** 当前页数 */
    private int currentPage;
    
    /** １页的件数 */
    private int pageSize;
    
    /** 检索状态*/
    private String selectedStatus;
    
    /** CODE区分*/
    private String cdKubun;
    
    /** 文件类别下拉列表label标签 */
    private String label;
    
    /** 文件下拉列表value值 */
    private String value;
    
    /** 确认人内容 */
    private String confirmerValue;
    
    /** 确认人内容 */
    private String tjCode;
    
    /** 删除用文件编号 */
    private String fileId;
    
    /** 删除用文件名称 */
    private String fName;
    
    /** 删除用文件名称code */
    private String fNameCode;
    
    /** 删除用文件类别code */
    private String fileAbstractCode;
    
    /** 删除用文件类别code */
    private String fileAbstractData;
    
    /** 审核状态 */
    private String examineStatus;

    /** 排序字段 */
    private String sortColumn;
    
    /** 排序顺序 */
    private String sortType;

    public String getFileAbstractData() {
    
        return fileAbstractData;
    
    }

    public void setFileAbstractData(String fileAbstractData) {
    
        this.fileAbstractData = fileAbstractData;
    
    }

    public String getfNameCode() {
    
        return fNameCode;
    
    }

    public void setfNameCode(String fNameCode) {
    
        this.fNameCode = fNameCode;
    
    }

    public String getFileAbstractCode() {
    
        return fileAbstractCode;
    
    }

    public void setFileAbstractCode(String fileAbstractCode) {
    
        this.fileAbstractCode = fileAbstractCode;
    
    }

    public String getfName() {
    
        return fName;
    
    }

    public void setfName(String fName) {
    
        this.fName = fName;
    
    }

    public String getFileName() {
    
        return fileName;
    
    }

    public void setFileName(String fileName) {
    
        this.fileName = fileName;
    
    }

    public String getFileValue() {
    
        return fileValue;
    
    }

    public void setFileValue(String fileValue) {
    
        this.fileValue = fileValue;
    
    }

    public String getFile() {
    
        return file;
    
    }

    public void setFile(String file) {
    
        this.file = file;
    
    }

    public String getFileAbstract() {
    
        return fileAbstract;
    
    }

    public void setFileAbstract(String fileAbstract) {
    
        this.fileAbstract = fileAbstract;
    
    }

    public String getPrincipal() {
    
        return principal;
    
    }

    public void setPrincipal(String principal) {
    
        this.principal = principal;
    
    }

    public String getUploadTime() {
    
        return uploadTime;
    
    }

    public void setUploadTime(String uploadTime) {
    
        this.uploadTime = uploadTime;
    
    }

    public String getCreateTime() {
    
        return createTime;
    
    }

    public void setCreateTime(String createTime) {
    
        this.createTime = createTime;
    
    }

    public int getCurrentPage() {
    
        return currentPage;
    
    }

    public void setCurrentPage(int currentPage) {
    
        this.currentPage = currentPage;
    
    }

    public int getPageSize() {
    
        return pageSize;
    
    }

    public void setPageSize(int pageSize) {
    
        this.pageSize = pageSize;
    
    }

    public String getSelectedStatus() {
    
        return selectedStatus;
    
    }

    public void setSelectedStatus(String selectedStatus) {
    
        this.selectedStatus = selectedStatus;
    
    }

    public String getCdKubun() {
    
        return cdKubun;
    
    }

    public void setCdKubun(String cdKubun) {
    
        this.cdKubun = cdKubun;
    
    }

    public String getLabel() {
    
        return label;
    
    }

    public void setLabel(String label) {
    
        this.label = label;
    
    }

    public String getValue() {
    
        return value;
    
    }

    public void setValue(String value) {
    
        this.value = value;
    
    }

    public String getConfirmerValue() {
    
        return confirmerValue;
    
    }

    public void setConfirmerValue(String confirmerValue) {
    
        this.confirmerValue = confirmerValue;
    
    }

    public String getTjCode() {
    
        return tjCode;
    
    }

    public void setTjCode(String tjCode) {
    
        this.tjCode = tjCode;
    
    }

    public String getFileId() {
    
        return fileId;
    
    }

    public void setFileId(String fileId) {
    
        this.fileId = fileId;
    
    }

    public String getExamineStatus() {
    
        return examineStatus;
    
    }

    public void setExamineStatus(String examineStatus) {
    
        this.examineStatus = examineStatus;
    
    }

    public String getSortColumn() {
    
        return sortColumn;
    
    }

    public void setSortColumn(String sortColumn) {
    
        this.sortColumn = sortColumn;
    
    }

    public String getSortType() {
    
        return sortType;
    
    }

    public void setSortType(String sortType) {
    
        this.sortType = sortType;
    
    }

    @Override
    public String toString() {

        return "FileManageExEntity [fileName=" + fileName + ", fileValue="
                + fileValue + ", file=" + file + ", fileAbstract="
                + fileAbstract + ", principal=" + principal + ", uploadTime="
                + uploadTime + ", createTime=" + createTime + ", currentPage="
                + currentPage + ", pageSize=" + pageSize + ", selectedStatus="
                + selectedStatus + ", cdKubun=" + cdKubun + ", label=" + label
                + ", value=" + value + ", confirmerValue=" + confirmerValue
                + ", tjCode=" + tjCode + ", fileId=" + fileId + ", fName="
                + fName + ", fNameCode=" + fNameCode + ", fileAbstractCode="
                + fileAbstractCode + ", fileAbstractData=" + fileAbstractData
                + ", examineStatus=" + examineStatus + ", sortColumn="
                + sortColumn + ", sortType=" + sortType
                + ", getFileAbstractData()=" + getFileAbstractData()
                + ", getfNameCode()=" + getfNameCode()
                + ", getFileAbstractCode()=" + getFileAbstractCode()
                + ", getfName()=" + getfName() + ", getFileName()="
                + getFileName() + ", getFileValue()=" + getFileValue()
                + ", getFile()=" + getFile() + ", getFileAbstract()="
                + getFileAbstract() + ", getPrincipal()=" + getPrincipal()
                + ", getUploadTime()=" + getUploadTime() + ", getCreateTime()="
                + getCreateTime() + ", getCurrentPage()=" + getCurrentPage()
                + ", getPageSize()=" + getPageSize() + ", getSelectedStatus()="
                + getSelectedStatus() + ", getCdKubun()=" + getCdKubun()
                + ", getLabel()=" + getLabel() + ", getValue()=" + getValue()
                + ", getConfirmerValue()=" + getConfirmerValue()
                + ", getTjCode()=" + getTjCode() + ", getFileId()="
                + getFileId() + ", getExamineStatus()=" + getExamineStatus()
                + ", getSortColumn()=" + getSortColumn() + ", getSortType()="
                + getSortType() + ", getClass()=" + getClass() + ", hashCode()="
                + hashCode() + ", toString()=" + super.toString() + "]";

    }

}
