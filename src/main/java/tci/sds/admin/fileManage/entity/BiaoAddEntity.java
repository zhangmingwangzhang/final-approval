package tci.sds.admin.fileManage.entity;

public class BiaoAddEntity {
    private String filename;
    private String one;
    private String two;
    private String three;
    private String four;
    private String koshin_name;
    private int counts;
    private String onecode;
    private String twocode;
    private String threecode;
    private String fourcode;
    private String koshincode;
    private int id;
    /**
     * 申请时间
     * @return
     */
    private  String applicationdate;
    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getOne() {
        return one;
    }

    public void setOne(String one) {
        this.one = one;
    }

    public String getTwo() {
        return two;
    }

    public void setTwo(String two) {
        this.two = two;
    }

    public String getThree() {
        return three;
    }

    public void setThree(String three) {
        this.three = three;
    }

    public String getFour() {
        return four;
    }

    public void setFour(String four) {
        this.four = four;
    }

    public String getKoshin_name() {
        return koshin_name;
    }

    public void setKoshin_name(String koshin_name) {
        this.koshin_name = koshin_name;
    }

    public int getCounts() {
        return counts;
    }

    public void setCounts(int counts) {
        this.counts = counts;
    }

    public String getOnecode() {
        return onecode;
    }

    public void setOnecode(String onecode) {
        this.onecode = onecode;
    }

    public String getTwocode() {
        return twocode;
    }

    public void setTwocode(String twocode) {
        this.twocode = twocode;
    }

    public String getThreecode() {
        return threecode;
    }

    public void setThreecode(String threecode) {
        this.threecode = threecode;
    }

    public String getFourcode() {
        return fourcode;
    }

    public void setFourcode(String fourcode) {
        this.fourcode = fourcode;
    }

    public String getKoshincode() {
        return koshincode;
    }

    public void setKoshincode(String koshincode) {
        this.koshincode = koshincode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getApplicationdate() {
        return applicationdate;
    }

    public void setApplicationdate(String applicationdate) {
        this.applicationdate = applicationdate;
    }

    @Override
    public String toString() {
        return "BiaoAddEntity{" +
                "filename='" + filename + '\'' +
                ", one='" + one + '\'' +
                ", two='" + two + '\'' +
                ", three='" + three + '\'' +
                ", four='" + four + '\'' +
                ", koshin_name='" + koshin_name + '\'' +
                ", counts=" + counts +
                ", onecode='" + onecode + '\'' +
                ", twocode='" + twocode + '\'' +
                ", threecode='" + threecode + '\'' +
                ", fourcode='" + fourcode + '\'' +
                ", koshincode='" + koshincode + '\'' +
                ", id=" + id +
                ", applicationdate='" + applicationdate + '\'' +
                '}';
    }
}
