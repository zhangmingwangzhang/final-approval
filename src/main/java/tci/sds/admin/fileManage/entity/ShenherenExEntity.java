package tci.sds.admin.fileManage.entity;

public class ShenherenExEntity {
    private String username;
    private String tjCode;
     private  String personMail;



    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTjCode() {
        return tjCode;
    }

    public void setTjCode(String tjCode) {
        this.tjCode = tjCode;
    }

    public String getPersonMail() {
        return personMail;
    }

    public void setPersonMail(String personMail) {
        this.personMail = personMail;
    }

    @Override
    public String toString() {
        return "ShenherenExEntity{" +
                "username='" + username + '\'' +
                ", tjCode='" + tjCode + '\'' +
                ", personMail='" + personMail + '\'' +
                '}';
    }
}
